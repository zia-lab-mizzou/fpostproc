/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* Modified pair_yukawa for Polydisperse Two-Yukawa function
 * Edited by: Brian K. Ryu
 * Last edited: Apr 16 2019 fixing scaling issues
 * Note: This is a Two-Yukawa modification!
 * Refer to Dec.03 2018 version for a single Yukawa
 */

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "pair_yukawa.h"
#include "atom.h"
#include "force.h"
#include "comm.h"
#include "neigh_list.h"
#include "memory.h"
#include "error.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

PairYukawa::PairYukawa(LAMMPS *lmp) : Pair(lmp)
{
  writedata = 1;
}

/* ---------------------------------------------------------------------- */

PairYukawa::~PairYukawa()
{
  if (allocated) {
    memory->destroy(setflag);
    memory->destroy(cutsq);

    memory->destroy(rad);
    memory->destroy(cut);
    // Add d0 and Two-Yukawa parameterse to destroy
    memory->destroy(K1);
    memory->destroy(K2);
    memory->destroy(Z1);
    memory->destroy(Z2);
    memory->destroy(d0);
    memory->destroy(offset);
  }
}

/* ---------------------------------------------------------------------- */

void PairYukawa::compute(int eflag, int vflag)
{
  int i,j,ii,jj,inum,jnum,itype,jtype;
  double xtmp,ytmp,ztmp,delx,dely,delz,evdwl,fpair;
  double rsq,r2inv,r,rinv,screening,forceyukawa,factor;
  double rdinv;
  int *ilist,*jlist,*numneigh,**firstneigh;

  double screening1, screening2, forceyukawa1, forceyukawa2;
  double evdwl1, evdwl2;

  evdwl = 0.0;
  if (eflag || vflag) ev_setup(eflag,vflag);
  else evflag = vflag_fdotr = 0;

  double **x = atom->x;
  double **f = atom->f;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  double *special_lj = force->special_lj;
  int newton_pair = force->newton_pair;

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    xtmp = x[i][0];
    ytmp = x[i][1];
    ztmp = x[i][2];
    itype = type[i];
    jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      factor = special_lj[sbmask(j)];
      j &= NEIGHMASK;

      delx = xtmp - x[j][0];
      dely = ytmp - x[j][1];
      delz = ztmp - x[j][2];
      rsq = delx*delx + dely*dely + delz*delz;
      jtype = type[j];

      if (rsq < cutsq[itype][jtype]) {
        r2inv = 1.0/rsq;
        r = sqrt(rsq);
        rinv = 1.0/r;

        // Screening was modified
        // screening = exp(-k0[itype][jtype]*r);
        screening1 = 0.5*d0[itype][jtype]*exp(-Z1[itype][jtype]*(r-0.5*d0[itype][jtype]));
        screening2 = 0.5*d0[itype][jtype]*exp(-Z2[itype][jtype]*(r-0.5*d0[itype][jtype]));

        // forceyukawa was modified
        //forceyukawa = a[itype][jtype] * screening * (k0[itype][jtype] + rinv);
        forceyukawa1 = K1[itype][jtype]*screening1 * rinv * (rinv + Z1[itype][jtype]);
        forceyukawa2 = K2[itype][jtype]*screening2 * rinv * (rinv + Z2[itype][jtype]);

        forceyukawa = forceyukawa1 + forceyukawa2;

        // fpair was slightly modified: r2inv -> rinv
        // Note: need an additional 1/r compared to functional form
        fpair = factor*forceyukawa * rinv;

        f[i][0] += delx*fpair;
        f[i][1] += dely*fpair;
        f[i][2] += delz*fpair;
        if (newton_pair || j < nlocal) {
          f[j][0] -= delx*fpair;
          f[j][1] -= dely*fpair;
          f[j][2] -= delz*fpair;
        }

        if (eflag) {
          // evdwl = a[itype][jtype] * d0[itype][jtype] * screening * rdinv - offset[itype][jtype];
          evdwl1 = K1[itype][jtype] * screening1 * rinv;
          evdwl2 = K2[itype][jtype] * screening2 * rinv;
          evdwl = evdwl1 + evdwl2;
          evdwl *= factor;
        }

        if (evflag) ev_tally(i,j,nlocal,newton_pair,
                             evdwl,0.0,fpair,delx,dely,delz);
      }
    }
  }

  if (vflag_fdotr) virial_fdotr_compute();
}


/* ----------------------------------------------------------------------
   allocate all arrays
------------------------------------------------------------------------- */

void PairYukawa::allocate()
{
  allocated = 1;
  int n = atom->ntypes;

  memory->create(setflag,n+1,n+1,"pair:setflag");
  for (int i = 1; i <= n; i++)
    for (int j = i; j <= n; j++)
      setflag[i][j] = 0;

  memory->create(cutsq,n+1,n+1,"pair:cutsq");
  memory->create(rad,n+1,"pair:rad");
  memory->create(cut,n+1,n+1,"pair:cut");
  //memory->create(a,n+1,n+1,"pair:a");
  memory->create(offset,n+1,n+1,"pair:offset");
  // Added this line for d0 and k0
  memory->create(d0,n+1,n+1,"pair:d0");
  memory->create(K1,n+1,n+1,"pair:K1");
  memory->create(K2,n+1,n+1,"pair:K2");
  memory->create(Z1,n+1,n+1,"pair:Z1");
  memory->create(Z2,n+1,n+1,"pair:Z2");

}

/* ----------------------------------------------------------------------
   global settings
------------------------------------------------------------------------- */
// Note from Brian:
// This is assigning variables when using pair_style yukawa (Kappa) (Global Cutoff) inside the script.
// Order is (Global Cutoff)
// This was not changed since we want to have d0 in each pair_coeff
void PairYukawa::settings(int narg, char **arg)
{
  //reduce narg since kappa is no longer global (moved to pair-specific)
  if (narg != 1) error->all(FLERR,"Illegal pair_style command");
  // remove kappa and make it k0 in the pair
  //kappa = force->numeric(FLERR,arg[0]);
  cut_global = force->numeric(FLERR,arg[0]);

  // reset cutoffs that have been explicitly set

  if (allocated) {
    int i,j;
    for (i = 1; i <= atom->ntypes; i++)
      for (j = i; j <= atom->ntypes; j++)
        if (setflag[i][j]) cut[i][j] = cut_global;
  }
}

/* ----------------------------------------------------------------------
   set coeffs for one or more type pairs
------------------------------------------------------------------------- */

void PairYukawa::coeff(int narg, char **arg)
// Here I changed the narg < 3 || narg > 4 to the ones below
// To take in one more arguments
// Order is (type i) (type j) (K1) (Z1) (K2) (Z2) (d0) (cutoff) 
// Many changes were made throughout this function
{
  if (narg < 7 || narg > 8)
    error->all(FLERR,"Incorrect args for pair coefficients");
  if (!allocated) allocate();

  int ilo,ihi,jlo,jhi;
  force->bounds(FLERR,arg[0],atom->ntypes,ilo,ihi);
  force->bounds(FLERR,arg[1],atom->ntypes,jlo,jhi);

  double K1_one = force->numeric(FLERR,arg[2]);
  double Z1_one = force->numeric(FLERR,arg[3]);

  double K2_one = force->numeric(FLERR,arg[4]);
  double Z2_one = force->numeric(FLERR,arg[5]);

  double d0_one = force->numeric(FLERR,arg[6]);

  double cut_one = cut_global;
  if (narg == 8) cut_one = force->numeric(FLERR,arg[7]);

  int count = 0;
  for (int i = ilo; i <= ihi; i++) {
    for (int j = MAX(jlo,i); j <= jhi; j++) {
      
      d0[i][j] = d0_one;
      K1[i][j] = K1_one;
      Z1[i][j] = Z1_one;

      K2[i][j] = K2_one;
      Z2[i][j] = Z2_one;
      
      cut[i][j] = cut_one;
      setflag[i][j] = 1;
      count++;
    }
  }

  if (count == 0) error->all(FLERR,"Incorrect args for pair coefficients");
}

/* ----------------------------------------------------------------------
   init for one type pair i,j and corresponding j,i
------------------------------------------------------------------------- */
/* Note from Brian:
 * Now that we are dealing with polydispersity, the user must define parameters for
 * each pair type by pair_coeff. If not, LAMMPS will spit out an error
 */
// Edits were made here

double PairYukawa::init_one(int i, int j)
{
  if (setflag[i][j] == 0) error->all(FLERR,"All pair coeffs are not set");   
  // if (setflag[i][j] == 0) {
  //   a[i][j] = mix_energy(a[i][i],a[j][j],1.0,1.0);
  //   cut[i][j] = mix_distance(cut[i][i],cut[j][j]);
  // }

  if (offset_flag && (cut[i][j] > 0.0)) {
    double screening = exp(-Z1[i][j] * (cut[i][j]-d0[i][j]));
    offset[i][j] = K1[i][j] * screening / cut[i][j];
    offset[i][j] = 0;
  } else offset[i][j] = 0.0;

  //a[j][i] = a[i][j];
  d0[j][i] = d0[i][j];

  K1[j][i] = K1[i][j];
  Z1[j][i] = Z1[i][j];

  K2[j][i] = K2[i][j];
  Z2[j][i] = Z2[i][j];
  
  offset[j][i] = offset[i][j];

  return cut[i][j];
}

/* ----------------------------------------------------------------------
   proc 0 writes to restart file
------------------------------------------------------------------------- */
// Edits were made here to keep track of d0 and (K1,Z1,K2,Z2)
void PairYukawa::write_restart(FILE *fp)
{
  write_restart_settings(fp);

  int i,j;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      fwrite(&setflag[i][j],sizeof(int),1,fp);
      if (setflag[i][j]) {
        //fwrite(&a[i][j],sizeof(double),1,fp);
        fwrite(&d0[i][j],sizeof(double),1,fp);
        fwrite(&K1[i][j],sizeof(double),1,fp);
        fwrite(&Z1[i][j],sizeof(double),1,fp);

        fwrite(&K2[i][j],sizeof(double),1,fp);
        fwrite(&Z2[i][j],sizeof(double),1,fp);

        fwrite(&cut[i][j],sizeof(double),1,fp);
      }
    }
}

/* ----------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */
// Edits were made here to add d0 and (K1,Z1,K2,Z2)
void PairYukawa::read_restart(FILE *fp)
{
  read_restart_settings(fp);

  allocate();

  int i,j;
  int me = comm->me;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      if (me == 0) fread(&setflag[i][j],sizeof(int),1,fp);
      MPI_Bcast(&setflag[i][j],1,MPI_INT,0,world);
      if (setflag[i][j]) {
        if (me == 0) {
          //fread(&a[i][j],sizeof(double),1,fp);
          fread(&d0[i][j],sizeof(double),1,fp);
          fread(&K1[i][j],sizeof(double),1,fp);
          fread(&Z1[i][j],sizeof(double),1,fp);

          fread(&K2[i][j],sizeof(double),1,fp);
          fread(&Z2[i][j],sizeof(double),1,fp);

          fread(&cut[i][j],sizeof(double),1,fp);
        }
        //MPI_Bcast(&a[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&d0[i][j],1,MPI_DOUBLE,0,world);

        MPI_Bcast(&K1[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&Z1[i][j],1,MPI_DOUBLE,0,world);

        MPI_Bcast(&K2[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&Z2[i][j],1,MPI_DOUBLE,0,world);

        MPI_Bcast(&cut[i][j],1,MPI_DOUBLE,0,world);
      }
    }
}

/* ----------------------------------------------------------------------
   proc 0 writes to restart file
------------------------------------------------------------------------- */

void PairYukawa::write_restart_settings(FILE *fp)
{
  //fwrite(&kappa,sizeof(double),1,fp); Remove since no longer kappa
  fwrite(&cut_global,sizeof(double),1,fp);
  fwrite(&offset_flag,sizeof(int),1,fp);
  fwrite(&mix_flag,sizeof(int),1,fp);
}

/* ----------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */

void PairYukawa::read_restart_settings(FILE *fp)
{
  if (comm->me == 0) {
    //fread(&kappa,sizeof(double),1,fp); Remove since no longer kappa
    fread(&cut_global,sizeof(double),1,fp);
    fread(&offset_flag,sizeof(int),1,fp);
    fread(&mix_flag,sizeof(int),1,fp);
  }
  //MPI_Bcast(&kappa,1,MPI_DOUBLE,0,world); Remove since no longer kappa
  MPI_Bcast(&cut_global,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&offset_flag,1,MPI_INT,0,world);
  MPI_Bcast(&mix_flag,1,MPI_INT,0,world);
}


/* ----------------------------------------------------------------------
   proc 0 writes to data file
------------------------------------------------------------------------- */

void PairYukawa::write_data(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    fprintf(fp,"%d %g %g %g %g %g\n",i,d0[i][i], K1[i][i], Z1[i][i], K2[i][i], Z2[i][i]);
}

/* ----------------------------------------------------------------------
   proc 0 writes all pairs to data file
------------------------------------------------------------------------- */

void PairYukawa::write_data_all(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    for (int j = i; j <= atom->ntypes; j++)
      fprintf(fp,"%d %d %g %g %g %g %g %g\n",i,j,d0[i][j], K1[i][j],Z1[i][j],K2[i][j],Z2[i][j],cut[i][j]);
}

/* ---------------------------------------------------------------------- */

double PairYukawa::single(int i, int j, int itype, int jtype, double rsq,
                          double factor_coul, double factor_lj,
                          double &fforce)
{
  double r2inv,r,rinv,screening1, screening2 ,forceyukawa,phi;
  double rdinv;
  double forceyukawa1, forceyukawa2, phi1, phi2;

  r2inv = 1.0/rsq;
  r = sqrt(rsq);
  rinv = 1.0/r;
  // Screening was modified

  /* 
   // forceyukawa was modified
  //forceyukawa = a[itype][jtype] * screening * (k0[itype][jtype] + rinv);
  forceyukawa1 = K1[itype][jtype]*screening1 * rinv * (rinv + Z1[itype][jtype]);
  forceyukawa2 = K2[itype][jtype]*screening2 * rinv * (rinv + Z2[itype][jtype]);

  forceyukawa = forceyukawa1 + forceyukawa2;

  // fpair was slightly modified: r2inv -> rinv
  fpair = factor*forceyukawa;
  */
  screening1 = 0.5*d0[itype][jtype]*exp(-Z1[itype][jtype]*(r-0.5*d0[itype][jtype]));

  screening2 = 0.5*d0[itype][jtype]*exp(-Z2[itype][jtype]*(r-0.5*d0[itype][jtype]));

  // New forceyukawa is used here. Instead of this:
  //forceyukawa = a[itype][jtype] * screening * (k0 + rinv);
  // We use this:
  forceyukawa1 = K1[itype][jtype]*screening1 * rinv * (rinv + Z1[itype][jtype]);
  forceyukawa2 = K2[itype][jtype]*screening2 * rinv * (rinv + Z2[itype][jtype]);

  forceyukawa = forceyukawa1 + forceyukawa2;
  // Note sure what this does. this may be incorrect! I think this is right though..
  // If something seems wrong, come back here
  fforce = factor_lj*forceyukawa * rinv;

  // New potential energy is used here. Instead of this;
  // phi = a[itype][jtype] * screening * rinv - offset[itype][jtype];
  // We use this:

  phi1 = K1[itype][jtype] * screening1 * rinv;
  phi2 = K2[itype][jtype] * screening2 * rinv;
  phi = phi1 + phi2;

  return factor_lj*phi;
}

/* ---------------------------------------------------------------------- */
// Added this function
// added d0 and 2Y parameters
void *PairYukawa::extract(const char *str, int &dim)
{
  dim = 2;
  //if (strcmp(str,"a") == 0) return (void *) a;
  if (strcmp(str,"d0") == 0) return (void *) d0;
  if (strcmp(str,"K1") == 0) return (void *) K1;
  if (strcmp(str,"Z1") == 0) return (void *) Z1;
  if (strcmp(str,"K2") == 0) return (void *) K2;
  if (strcmp(str,"Z2") == 0) return (void *) Z2;

  return NULL;
}
