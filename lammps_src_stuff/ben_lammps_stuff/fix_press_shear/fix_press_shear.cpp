#include "string.h"
#include "math.h"
#include "fix_press_shear.h"
#include "atom.h"
#include "compute.h"
#include "domain.h"
#include "error.h"
#include "force.h"
#include "irregular.h"
#include "math_const.h"
#include "modify.h"
#include "update.h"
#include "mpi.h"

// assume periodic everywhere?

using namespace LAMMPS_NS;
using namespace FixConst;
using namespace MathConst;

/* ---------------------------------------------------------------------- */

FixPressShear::FixPressShear(LAMMPS *lmp, int narg, char **arg) :
  Fix(lmp, narg, arg)
{
  if (narg < 5) error->all(FLERR,"Illegal fix press/shear command");

  scalar_flag = 1;
  extscalar = 0;
  box_change_shape = 1;
  global_freq = nevery = 1;

  // default values

  flip_total = 0;
  p_targ = visc = 0.0;
  component = 0;

  diam = new double[atom->ntypes+1];
  for (int i = 1; i <= atom->ntypes; i++) diam[i] = 1.0;

  // process keywords
  // fix ID group-ID press/shear component p_targ visc 
  //   OPTIONAL: diam [scale val] [scale val] ...

  if      (strcmp(arg[3],"xy") == 0) component = 5;
  else if (strcmp(arg[3],"xz") == 0) component = 4;
  else if (strcmp(arg[3],"yz") == 0) component = 3;
  else error->all(FLERR,"Fix press/shear component must be xy, xz, or yz");
  p_targ = force->numeric(FLERR,arg[4]);
  visc = force->numeric(FLERR,arg[5]);

  int iarg = 6;
  while (iarg < narg) {
    if (strcmp(arg[iarg],"diam") == 0) {
      if (iarg+3 > narg) error->all(FLERR,"Illegal fix press/shear command");
      int itype = force->inumeric(FLERR,arg[iarg+1]);
      double scale = force->numeric(FLERR,arg[iarg+2]);
      if (itype <= 0 || itype > atom->ntypes) {
	error->all(FLERR,"Illegal fix press/shear command");
      }
      diam[itype] = scale;
      iarg += 3;
    } else if (strcmp(arg[iarg],"looptotal") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal fix press/shear command");
      flip_total = force->inumeric(FLERR,arg[iarg+1]);
      iarg += 2;
    } else error->all(FLERR,"Illegal fix press/shear command");
  }

  // create a new compute temp style
  // id = fix-ID + temp
  // compute group = all since pressure is always global (group all)
  //   and thus its KE/temperature contribution should use group all
  // currently not used in stress controller (only using virial)

  int n = strlen(id) + 6;
  id_temp = new char[n];
  strcpy(id_temp,id);
  strcat(id_temp,"_temp");

  char **newarg = new char*[3];
  newarg[0] = id_temp;
  newarg[1] = (char *) "all";
  newarg[2] = (char *) "temp";
  modify->add_compute(3,newarg);
  delete [] newarg;
  tflag = 1;

  // create a new compute pressure style
  // id = fix-ID + press, compute group = all
  // pass id_temp as 4th arg to pressure constructor

  n = strlen(id) + 7;
  id_press = new char[n];
  strcpy(id_press,id);
  strcat(id_press,"_press");

  newarg = new char*[5];
  newarg[0] = id_press;
  newarg[1] = (char *) "all";
  newarg[2] = (char *) "pressure";
  newarg[3] = id_temp;
  newarg[4] = (char *) "virial";
  modify->add_compute(5,newarg);
  delete [] newarg;
  pflag = 1;

  // reneighboring, since flips can occur due to shape changing

  next_reneighbor = -1;
  flip = 0;

  irregular = new Irregular(lmp);
}

/* ---------------------------------------------------------------------- */

FixPressShear::~FixPressShear()
{
  delete [] diam;
  delete irregular;

  // delete temperature and pressure if fix created them

  if (tflag) modify->delete_compute(id_temp);
  if (pflag) modify->delete_compute(id_press);
  delete [] id_temp;
  delete [] id_press;

  // reset domain's h_rate = 0.0 like in fix deform

  double *h_rate = domain->h_rate;
  double *h_ratelo = domain->h_ratelo;

  h_rate[0] = h_rate[1] = h_rate[2] =
    h_rate[3] = h_rate[4] = h_rate[5] = 0.0;
  h_ratelo[0] = h_ratelo[1] = h_ratelo[2] = 0.0;
}

/* ---------------------------------------------------------------------- */

int FixPressShear::setmask()
{
  int mask = 0;
  mask |= PRE_EXCHANGE;
  mask |= END_OF_STEP;
  return mask;
}

/* ---------------------------------------------------------------------- */

void FixPressShear::init()
{
  if (domain->triclinic == 0)
    error->all(FLERR,"Fix press/shear requires triclinic box");

  // error if there is another fix press/shear or a fix deform

  int count = 0;
  for (int i = 0; i < modify->nfix; i++) {
    if (strcmp(modify->fix[i]->style,"deform") == 0) {
      error->all(FLERR,"Cannot have fix deform with fix press/shear");
    }
    if (strcmp(modify->fix[i]->style,"press/shear") == 0) count++;
  }
  if (count > 1) error->all(FLERR,"More than one fix press/shear");

  // set temperature and pressure ptrs

  int icompute = modify->find_compute(id_temp);
  if (icompute < 0)
    error->all(FLERR,"Temperature ID for fix press/shear does not exist");
  temperature = modify->compute[icompute];

  icompute = modify->find_compute(id_press);
  if (icompute < 0)
    error->all(FLERR,"Pressure ID for fix press/shear does not exist");
  pressure = modify->compute[icompute];

  // calculate gain, based on viscosity and "volume fraction"

  double xprd = domain->xprd;
  double yprd = domain->yprd;
  double zprd = domain->zprd;
  int nlocal = atom->nlocal;
  int *type = atom->type;
  int *mask = atom->mask;
  double v, vol_part, inv_volume;

  inv_volume = 1.0 / (xprd * yprd * zprd);
  v = vol_part = 0.0;
  for (int i = 0; i < nlocal; i++) {
    if (mask[i] & groupbit) {
      v += diam[type[i]] * diam[type[i]] * diam[type[i]];
    }
  }
  MPI_Allreduce(&v,&vol_part,1,MPI_DOUBLE,MPI_SUM,world);
  vol_part *= THIRD * MY_PI2;
  gain = 1.0 / visc / (1.0 + inv_volume*2.5*vol_part);

  if (component == 5) gain *= domain->yprd;
  else gain *= domain->zprd;
}

/* ----------------------------------------------------------------------
   compute T,P before integrator starts
------------------------------------------------------------------------- */

void FixPressShear::setup(int vflag)
{
  // trigger virial computation on next timestep

  pressure->addstep(update->ntimestep+1);
}

/* ----------------------------------------------------------------------
  box flipped on previous step
  reset box tilts for flipped config and create new box in domain
  image_flip() adjusts image flags due to box shape change induced by flip
  remap() puts atoms outside the new box back into the new box
  perform irregular on atoms in lamda coords to migrate atoms to new procs
  important that image_flip comes before remap, since remap may change
    image flags to new values, making eqs in doc of Domain:image_flip incorrect
------------------------------------------------------------------------- */

void FixPressShear::pre_exchange()
{
  if (flip == 0) return;

  // updating total # of flips here (for use in strain output)

  if (component == 5) {
    domain->xy = tilt_target = tilt_flip;
    flip_total -= flipxy;
  } else if (component == 4) {
    domain->xz = tilt_target = tilt_flip;
    flip_total -= flipxz;
  } else if (component == 3) {
    domain->yz = tilt_target = tilt_flip;
    flip_total -= flipyz;
  }
  domain->set_global_box();
  domain->set_local_box();

  domain->image_flip(flipxy,flipxz,flipyz);

  double **x = atom->x;
  tagint *image = atom->image;
  int nlocal = atom->nlocal;
  for (int i = 0; i < nlocal; i++) domain->remap(x[i],image[i]);

  domain->x2lamda(atom->nlocal);
  irregular->migrate_atoms();
  domain->lamda2x(atom->nlocal);

  flip = 0;
}

/* ---------------------------------------------------------------------- */

void FixPressShear::end_of_step()
{
  double p_current,h_rate,tilt_current,delta_tilt;
  double *tensor = pressure->vector;
  double *h = domain->h;
  double delt = (update->ntimestep - update->beginstep) * update->dt;

  // compute pressure (temperature left out for now ... using virial only)

  //temperature->compute_vector();
  pressure->compute_vector();

  // get current pressure variable
  // in this fix, component 3 is yz, component 4 is xz, and component 5 is xy
  // in pressure tensor, index 3 is xy, index 4 is xz, and index 5 is yz

  p_current = tensor[8-component];

  // determine box sliding rate

  h_rate = (p_targ + p_current) * gain;

  // determine tilt_target (assuming checking every timestep here)

  if      (component == 5) tilt_current = domain->xy;
  else if (component == 4) tilt_current = domain->xz;
  else if (component == 3) tilt_current = domain->yz;
  delta_tilt = h_rate * update->dt;
  tilt_target = tilt_current + delta_tilt;

  // BELOW COPIED FROM fix_deform.cpp
  // tilt_target can be large positive or large negative value
  // add/subtract box lengths until tilt_target is closest to current value

  int idenom;
  if      (component == 5) idenom = 0;
  else if (component == 4) idenom = 0;
  else if (component == 3) idenom = 1;
  double denom = domain->boxhi[idenom] - domain->boxlo[idenom];

  double current = h[component]/h[idenom];

  while (tilt_target/denom - current > 0.0)
    tilt_target -= denom;
  while (tilt_target/denom - current < 0.0)
    tilt_target += denom;
  if (fabs(tilt_target/denom - 1.0 - current) <
      fabs(tilt_target/denom - current))
    tilt_target -= denom;

  // BELOW COPIED FROM fix_deform.cpp
  // if any tilt ratios exceed 0.5, set flip = 1 and compute new tilt values
  // do not flip in x or y if non-periodic (can tilt but not flip)
  //   this is b/c the box length would be changed (dramatically) by flip
  // if yz tilt exceeded, adjust C vector by one B vector
  // if xz tilt exceeded, adjust C vector by one A vector
  // if xy tilt exceeded, adjust B vector by one A vector
  // check yz first since it may change xz, then xz check comes after
  // flip is performed on next timestep, before reneighboring in pre-exchange()

  double prd,prdinv;
  int periodic;
  int *flip_ptr;

  flip = flipxy = flipxz = flipyz = 0;

  if (component == 5) {
    prd = domain->boxhi[0] - domain->boxlo[0];
    periodic = domain->xperiodic;
    flip_ptr = &flipxy;
  } else if (component == 4) {
    prd = domain->boxhi[0] - domain->boxlo[0];
    periodic = domain->xperiodic;
    flip_ptr = &flipxz;
  } else if (component == 3) {
    prd = domain->boxhi[1] - domain->boxlo[1];
    periodic = domain->yperiodic;
    flip_ptr = &flipyz;
  }
  prdinv = 1.0 / prd;

  if (tilt_target*prdinv < -0.5 || tilt_target*prdinv > 0.5) {

    tilt_flip = tilt_target;

    if (periodic) {
      if (tilt_flip*prdinv < -0.5) {
	tilt_flip += prd;
	*flip_ptr = 1;
      } else if (tilt_flip*prdinv > 0.5) {
	tilt_flip -= prd;
	*flip_ptr = -1;
      }
    }

    if (*flip_ptr) flip = 1;
    if (flip) next_reneighbor = update->ntimestep + 1;
  }

  // convert atoms to lamda coords

  double **x = atom->x;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  int i;

  for (i = 0; i < nlocal; i++)
    if (mask[i] & groupbit)
      domain->x2lamda(x[i],x[i]);

  // reset global and local box to new size/shape
  // only if deform fix is controlling the dimension

  if      (component == 5) domain->xy = tilt_target;
  else if (component == 4) domain->xz = tilt_target;
  else if (component == 3) domain->yz = tilt_target;

  domain->set_global_box();
  domain->set_local_box();

  // convert atoms back to box coords

  for (i = 0; i < nlocal; i++)
    if (mask[i] & groupbit)
      domain->lamda2x(x[i],x[i]);

  // trigger virial computation on next timestep
  // TODO: do I need virial on every timestep?

  pressure->addstep(update->ntimestep+1);
}

/* ---------------------------------------------------------------------- */

double FixPressShear::compute_scalar()
{
  double strain;
  if      (component == 5) strain = domain->xy / domain->yprd;
  else if (component == 4) strain = domain->xz / domain->zprd;
  else if (component == 3) strain = domain->yz / domain->zprd;

  // add the total number of (signed) flips

  strain += double(flip_total);

  return strain;
}
