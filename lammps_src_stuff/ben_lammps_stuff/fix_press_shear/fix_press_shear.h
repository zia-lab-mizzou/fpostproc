#ifdef FIX_CLASS

FixStyle(press/shear,FixPressShear)

#else

#ifndef LMP_FIX_PRESS_SHEAR_H
#define LMP_FIX_PRESS_SHEAR_H

#include "fix.h"

namespace LAMMPS_NS {

class FixPressShear : public Fix {
 public:
  FixPressShear(class LAMMPS *, int, char **);
  ~FixPressShear();
  int setmask();
  void init();
  void setup(int);
  void pre_exchange();
  void end_of_step();
  double compute_scalar();

 private:
  class FixDeform *fix;
  int component;    // 3 for yz, 4 for xz, 5 for xy
  double p_targ;    // target pressure, including 0,1,2-body terms
  double visc;      // solvent viscosity, in units of (m/tau/sigma)
  double *diam;     // equivalent sphere diameters for 1-body terms
                    // in units of sigma
  double gain;      // gain, in inverse (time*pressure) units

  class Irregular *irregular;  // for migrating atoms after box flips
  int flipxy,flipxz,flipyz;    // tell domain->image_flip what to flip
  double tilt_target;          // target tilt
  double tilt_flip;            // target tilt, adjusted for overhangs
  int flip;                    // specifies whether or not we're flipping
  int flip_total;              // keep track of total flips for total strain

  // temperature and pressure computes used to determine strain rate

  char *id_temp,*id_press;
  class Compute *temperature,*pressure;
  int tflag,pflag;
};

}

#endif
#endif
