/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   https://www.lammps.org/, Sandia National Laboratories
   LAMMPS development team: developers@lammps.org

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef PAIR_CLASS
// clang-format off
PairStyle(table/rxn,PairTableRxn);
// clang-format on
#else

#ifndef LMP_PAIR_TABLE_RXN_H
#define LMP_PAIR_TABLE_RXN_H

#include "pair_table.h"
#include <random>

namespace LAMMPS_NS {

class PairTableRxn : public PairTable {
 public:
  PairTableRxn(class LAMMPS *);
  ~PairTableRxn() override;

  void compute(int, int) override;
  void settings(int, char **) override;
  void coeff(int, char **) override;

 protected:
  int seed; // This is the seed for reaction times generation. 
  double damp; // This is the damp value that is set in the input script and is used as a global parameter for this pair style. Cannot find a way to directly use it from source code
  double acceleration; // This is to speed up the simulation to reduce computational cost. Needs to be slower than the average unbinding time
  std::random_device rd; // Random number engine
  std::mt19937 generator; // Generator for the engine
};

}    // namespace LAMMPS_NS

#endif
#endif
