/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* Modified pair_yukawa for Polydisperse Two-Yukawa function
 * Edited by: Brian K. Ryu
 * Last edited: Dec 04. 2018
 * Galen: added a stand-alone Two-Yukawa potential
 */

#ifdef PAIR_CLASS

PairStyle(twoyukawa,PairTwoYukawa)

#else

#ifndef LMP_PAIR_TWOYUKAWA_H
#define LMP_PAIR_TWOYUKAWA_H

#include "pair.h"

namespace LAMMPS_NS {

class PairTwoYukawa : public Pair {
 public:
  PairTwoYukawa(class LAMMPS *);
  virtual ~PairTwoYukawa();
  virtual void compute(int, int);
  void settings(int, char **);
  void coeff(int, char **);
  virtual double init_one(int, int);
  void write_restart(FILE *);
  void read_restart(FILE *);
  void write_restart_settings(FILE *);
  void read_restart_settings(FILE *);
  void write_data(FILE *);
  void write_data_all(FILE *);
  virtual double single(int, int, int, int, double, double, double, double &);
  /* Note on edit by Brian:
   * The pair_yukawa by default does not contain an extract method that allows changing of parameters by fix
   * This has been added to this pair_yukawa.h and pair_yukawa.cpp to allow fixing parameters in the LAMMPS script
   */
  void *extract(const char *, int &);


 protected:
  double cut_global;
  //double kappa; Remove since no longer kappa
  double *rad;
  double **cut,**offset;
  /* Note on edit by Brian:
   * The default pair_yukawa has only A, kappa, and cutoff.
   * Here I add d0, a third coefficient for the offset in radius.
   * Added K1 Z2 K2 and Z2 for Two-Yukawa
   */
  double **d0;

  double **K1;
  double **Z1;

  double **K2;
  double **Z2;

  virtual void allocate();
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

E: Incorrect args for pair coefficients

Self-explanatory.  Check the input script or data file.

*/
