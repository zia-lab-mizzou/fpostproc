/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef PAIR_CLASS

PairStyle(gayberne/morse,PairGayBerneMorse)

#else

#ifndef LMP_PAIR_GAYBERNE_MORSE_H
#define LMP_PAIR_GAYBERNE_MORSE_H

#include "pair.h"

namespace LAMMPS_NS {

class PairGayBerneMorse : public Pair {
 public:
  PairGayBerneMorse(LAMMPS *lmp);
  virtual ~PairGayBerneMorse();
  virtual void compute(int, int);
  virtual void settings(int, char **);
  void coeff(int, char **);
  virtual void init_style();
  double init_one(int, int);
  void write_restart(FILE *);
  void read_restart(FILE *);
  void write_restart_settings(FILE *);
  void read_restart_settings(FILE *);
  void write_data(FILE *);
  void write_data_all(FILE *);

 protected:
  enum{PATCH_PATCH,ISO_ISO,MORSE_MORSE};

  double cut_global;
  double **cut;

  double d_o_morse,alpha_morse;   // parameters for morse attraction
  double **shape1;           // per-type radii in x, y and z
  double **shape2;           // per-type radii in x, y and z SQUARED
  double *lshape;            // precalculation based on the shape
  double *theta_m;           // patchsize/2 for each particle type
  double *costheta_m;        // cosine(patchsize/2) for each particle type
  int **interact_case_all;   // sets pair: patch-patch(1), iso-iso(1), hs-hs(0)
  double **eps_ij_all,**z_ij_all;  // strength, range for atom-type pair attr
  double **r_shift_all;      // shift factor to align well at surface (for attr)
  double **r_shiftup_all,**r_scale_all;

  int **form;
  double **offset;
  class AtomVecEllipsoid *avec;

  void allocate();
  double gayberne_analytic(const int i, const int j, double a1[3][3],
                           double a2[3][3], double *r12,
                           const double rsq, double *fforce, double *ttor,
                           double *rtor);
};
  
}
#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

E: Incorrect args for pair coefficients

Self-explanatory.  Check the input script or data file.

E: Pair gayberne requires atom style ellipsoid

Self-explanatory.

E: Pair gayberne requires atoms with same type have same shape

Self-explanatory.

E: Pair gayberne epsilon a,b,c coeffs are not all set

Each atom type involved in pair_style gayberne must
have these 3 coefficients set at least once.

E: Bad matrix inversion in mldivide3

This error should not occur unless the matrix is badly formed.

*/
