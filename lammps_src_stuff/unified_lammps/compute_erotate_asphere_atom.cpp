/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "compute_erotate_asphere_atom.h"
#include <mpi.h>
#include "math_extra.h"
#include "atom.h"
#include "atom_vec_ellipsoid.h"
#include "atom_vec_line.h"
#include "atom_vec_tri.h"
#include "update.h"
#include "force.h"
#include "error.h"
#include <cstring>
#include "comm.h"
#include "pair.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "kspace.h"
#include "modify.h"
#include "fix.h"
#include "memory.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

ComputeERotateAsphereAtom::ComputeERotateAsphereAtom(LAMMPS *lmp, int narg, char **arg) :
  Compute(lmp, narg, arg),
  wbody_out(NULL)
{
  if (narg != 3) error->all(FLERR,"Illegal compute erotate/asphere/atom command");

  peratom_flag = 1;
  pressatomflag = 1;
  timeflag = 1;
  comm_reverse = 3;
  size_peratom_cols = 3;
  nmax = 0;

}

/* ---------------------------------------------------------------------- */

ComputeERotateAsphereAtom::~ComputeERotateAsphereAtom()
{
  memory->destroy(wbody_out);
}

/* ---------------------------------------------------------------------- */

void ComputeERotateAsphereAtom::init()
{

  // error check
  
  avec_ellipsoid = (AtomVecEllipsoid *) atom->style_match("ellipsoid");
  if (!avec_ellipsoid)
  error->all(FLERR,"Compute erotate/asphere/atom requires "
             "atom style ellipsoid");
  
  // check that all particles are finite-size
  // no point particles allowed, spherical is OK
  
  int *ellipsoid = atom->ellipsoid;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  
  int flag;
  for (int i = 0; i < nlocal; i++)
  if (mask[i] & groupbit) {
    flag = 0;
    if (ellipsoid && ellipsoid[i] >= 0) flag = 1;
    if (!flag)
    error->one(FLERR,"Compute erotate/asphere/atom requires extended particles");
  }

}

/* ---------------------------------------------------------------------- */

void ComputeERotateAsphereAtom::compute_peratom()
{
  int i,j;
  double onemass;

  invoked_peratom = update->ntimestep;
  
  if (atom->nmax > nmax) {
    memory->destroy(wbody_out);
    nmax = atom->nmax;
    memory->create(wbody_out,nmax,3,"erotate/asphere/atom:wbody_out");
    array_atom = wbody_out;
  }
  
  AtomVecEllipsoid::Bonus *ebonus;
  if (avec_ellipsoid) ebonus = avec_ellipsoid->bonus;
  int *ellipsoid = atom->ellipsoid;
  double **omega = atom->omega;
  double **angmom = atom->angmom;
  double *rmass = atom->rmass;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  
  // sum rotational energy for each particle
  // no point particles since divide by inertia
  
  double length;
  double *shape,*quat;
  double wbody[3],inertia[3];
  double rot[3][3];
  
  for (int i = 0; i < nlocal; i++)
    if (mask[i] & groupbit) {
      shape = ebonus[ellipsoid[i]].shape;
      quat = ebonus[ellipsoid[i]].quat;
      
      // principal moments of inertia
      
      inertia[0] = rmass[i] * (shape[1]*shape[1]+shape[2]*shape[2]) / 5.0;
      inertia[1] = rmass[i] * (shape[0]*shape[0]+shape[2]*shape[2]) / 5.0;
      inertia[2] = rmass[i] * (shape[0]*shape[0]+shape[1]*shape[1]) / 5.0;
      
      // wbody = angular velocity in body frame
      
      MathExtra::quat_to_mat(quat,rot);
      MathExtra::transpose_matvec(rot,angmom[i],wbody);
      wbody_out[i][0] = wbody[0]/inertia[0];
      wbody_out[i][1] = wbody[1]/inertia[1];
      wbody_out[i][2] = wbody[2]/inertia[2];
    }
}

/* ---------------------------------------------------------------------- */

int ComputeERotateAsphereAtom::pack_reverse_comm(int n, int first, double *buf)
{
  int i,m,last;

  m = 0;
  last = first + n;
  for (i = first; i < last; i++) {
    buf[m++] = wbody_out[i][0];
    buf[m++] = wbody_out[i][1];
    buf[m++] = wbody_out[i][2];
  }
  return m;
}

/* ---------------------------------------------------------------------- */

void ComputeERotateAsphereAtom::unpack_reverse_comm(int n, int *list, double *buf)
{
  int i,j,m;

  m = 0;
  for (i = 0; i < n; i++) {
    j = list[i];
    wbody_out[j][0] += buf[m++];
    wbody_out[j][1] += buf[m++];
    wbody_out[j][2] += buf[m++];
  }
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based array
------------------------------------------------------------------------- */

double ComputeERotateAsphereAtom::memory_usage()
{
  double bytes = nmax*3 * sizeof(double);
  return bytes;
}
