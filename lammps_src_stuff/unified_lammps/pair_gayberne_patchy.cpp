/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Mike Brown (SNL)
------------------------------------------------------------------------- */

#include "pair_gayberne_patchy.h"
#include <mpi.h>
#include <cmath>
#include "math_extra.h"
#include "atom.h"
#include "atom_vec_ellipsoid.h"
#include "comm.h"
#include "force.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "citeme.h"
#include "memory.h"
#include "error.h"
#include "utils.h"

using namespace LAMMPS_NS;

static const char cite_pair_gayberne[] =
  "pair gayberne command:\n\n"
  "@Article{Brown09,\n"
  " author =  {W. M. Brown, M. K. Petersen, S. J. Plimpton, and G. S. Grest},\n"
  " title =   {Liquid crystal nanodroplets in solution},\n"
  " journal = {J.~Chem.~Phys.},\n"
  " year =    2009,\n"
  " volume =  130,\n"
  " pages =   {044901}\n"
  "}\n\n";

/* ---------------------------------------------------------------------- */

PairGayBernePatchy::PairGayBernePatchy(LAMMPS *lmp) : Pair(lmp)
{
  if (lmp->citeme) lmp->citeme->add(cite_pair_gayberne);

  single_enable = 0;
  writedata = 1;
}

/* ----------------------------------------------------------------------
   free all arrays
------------------------------------------------------------------------- */

PairGayBernePatchy::~PairGayBernePatchy()
{
  if (allocated) {
    memory->destroy(setflag);
    memory->destroy(cutsq);

    memory->destroy(eps_ij_all);
    memory->destroy(z_ij_all);
    memory->destroy(shape1);
    memory->destroy(shape2);
    memory->destroy(cut);
    memory->destroy(offset);
    delete [] lshape;
    delete [] costheta_m;
    delete [] theta_m;
  }
}

/* ---------------------------------------------------------------------- */

void PairGayBernePatchy::compute(int eflag, int vflag)
{
  int i,j,ii,jj,inum,jnum,itype,jtype;
  double evdwl,one_eng,rsq,forcelj,factor_lj;
  double r,r_o,dr,dexp;
  double fforce[3],ttor[3],rtor[3],r12[3];
  double a1[3][3],a2[3][3];
  int *ilist,*jlist,*numneigh,**firstneigh;
  double *iquat,*jquat;

  evdwl = 0.0;
  ev_init(eflag,vflag);

  AtomVecEllipsoid::Bonus *bonus = avec->bonus;
  int *ellipsoid = atom->ellipsoid;
  double **x = atom->x;
  double **f = atom->f;
  double **tor = atom->torque;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  double *special_lj = force->special_lj;
  int newton_pair = force->newton_pair;
  double pairrad;

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  // loop over neighbors of my atoms

  for (ii = 0; ii < inum; ii++) {
    i = ilist[ii];
    itype = type[i];

    iquat = bonus[ellipsoid[i]].quat;
    MathExtra::quat_to_mat_trans(iquat,a1);

    jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      factor_lj = special_lj[sbmask(j)];
      j &= NEIGHMASK;

      // r12 = center to center vector

      r12[0] = x[i][0]-x[j][0];
      r12[1] = x[i][1]-x[j][1];
      r12[2] = x[i][2]-x[j][2];
      rsq = MathExtra::dot3(r12,r12);
      jtype = type[j];
      pairrad = (shape1[type[i]][0] + shape1[type[j]][0])/2;

      // compute if less than cutoff

      if (rsq < cutsq[itype][jtype] && rsq > pairrad) {

        jquat = bonus[ellipsoid[j]].quat;
        MathExtra::quat_to_mat_trans(jquat,a2);
        one_eng = gayberne_analytic(i,j,a1,a2,r12,rsq,fforce,ttor,rtor);

        fforce[0] *= factor_lj;
        fforce[1] *= factor_lj;
        fforce[2] *= factor_lj;
        ttor[0] *= factor_lj;
        ttor[1] *= factor_lj;
        ttor[2] *= factor_lj;

        f[i][0] += fforce[0];
        f[i][1] += fforce[1];
        f[i][2] += fforce[2];
        tor[i][0] += ttor[0];
        tor[i][1] += ttor[1];
        tor[i][2] += ttor[2];

        if (newton_pair || j < nlocal) {
          rtor[0] *= factor_lj;
          rtor[1] *= factor_lj;
          rtor[2] *= factor_lj;
          f[j][0] -= fforce[0];
          f[j][1] -= fforce[1];
          f[j][2] -= fforce[2];
          tor[j][0] += rtor[0];
          tor[j][1] += rtor[1];
          tor[j][2] += rtor[2];
        }

        if (eflag) evdwl = factor_lj*one_eng;

        if (evflag) ev_tally_xyz(i,j,nlocal,newton_pair,
                                 evdwl,0.0,fforce[0],fforce[1],fforce[2],
                                 -r12[0],-r12[1],-r12[2]);
      }
    }
  }

  if (vflag_fdotr) virial_fdotr_compute();
}

/* ----------------------------------------------------------------------
   allocate all arrays
------------------------------------------------------------------------- */

void PairGayBernePatchy::allocate()
{
  allocated = 1;
  int n = atom->ntypes;

  memory->create(setflag,n+1,n+1,"pair:setflag");
  for (int i = 1; i <= n; i++)
    for (int j = i; j <= n; j++)
      setflag[i][j] = 0;

  memory->create(cutsq,n+1,n+1,"pair:cutsq");

  memory->create(eps_ij_all,n+1,n+1,"pair:eps_ij_all");
  memory->create(z_ij_all,n+1,n+1,"pair:z_ij_all");
  memory->create(shape1,n+1,3,"pair:shape1");
  memory->create(shape2,n+1,3,"pair:shape2");
  memory->create(cut,n+1,n+1,"pair:cut");
  memory->create(offset,n+1,n+1,"pair:offset");

  lshape = new double[n+1];
  costheta_m = new double[n+1];
  theta_m = new double[n+1];
}

/* ----------------------------------------------------------------------
   global settings
------------------------------------------------------------------------- */

void PairGayBernePatchy::settings(int narg, char **arg)
{
  if (narg != 1) error->all(FLERR,"Illegal pair_style command");
 
  cut_global = utils::numeric(FLERR,arg[0],false,lmp);

  // reset cutoffs that have been explicitly set

  if (allocated) {
    int i,j;
    for (i = 1; i <= atom->ntypes; i++)
      for (j = i; j <= atom->ntypes; j++)
        if (setflag[i][j]) cut[i][j] = cut_global;
  }
}

/* ----------------------------------------------------------------------
   set coeffs for one or more type pairs
------------------------------------------------------------------------- */

void PairGayBernePatchy::coeff(int narg, char **arg)
{
  if (narg < 6 || narg > 7)
    error->all(FLERR,"Incorrect args for pair coefficients");
  if (!allocated) allocate();

  int ilo,ihi,jlo,jhi;
  utils::bounds(FLERR,arg[0],1,atom->ntypes,ilo,ihi,error);
  utils::bounds(FLERR,arg[1],1,atom->ntypes,jlo,jhi,error);

  double eps_ij = utils::numeric(FLERR,arg[2],false,lmp);
  double z_ij = utils::numeric(FLERR,arg[3],false,lmp);
  double theta_m_i = utils::numeric(FLERR,arg[4],false,lmp);
  double theta_m_j = utils::numeric(FLERR,arg[5],false,lmp);
  
  double cut_one = cut_global;
  if (narg == 7) cut_one = utils::numeric(FLERR,arg[6],false,lmp);

  int count = 0;
  for (int i = ilo; i <= ihi; i++) {
    for (int j = MAX(jlo,i); j <= jhi; j++) {
      eps_ij_all[i][j] = eps_ij;
      z_ij_all[i][j] = z_ij;
      cut[i][j] = cut_one;
      theta_m[i] = theta_m_i;
      theta_m[j] = theta_m_j;
      costheta_m[i] = cos(theta_m_i);
      costheta_m[j] = cos(theta_m_j);
      
      setflag[i][j] = 1;
      count++;
    }
  }

  if (count == 0) error->all(FLERR,"Incorrect args for pair coefficients");
}


/* ----------------------------------------------------------------------
   init specific to this pair style
------------------------------------------------------------------------- */

void PairGayBernePatchy::init_style()
{
  avec = (AtomVecEllipsoid *) atom->style_match("ellipsoid");
  if (!avec) error->all(FLERR,"Pair gayberne requires atom style ellipsoid");

  neighbor->request(this,instance_me);

  // per-type shape precalculations
  // require that atom shapes are identical within each type
  // if shape = 0 for point particle, set shape = 1 as required by Gay-Berne

  for (int i = 1; i <= atom->ntypes; i++) {
    if (!atom->shape_consistency(i,shape1[i][0],shape1[i][1],shape1[i][2]))
      error->all(FLERR,
                 "Pair gayberne requires atoms with same type have same shape");
    if (shape1[i][0] == 0.0)
      shape1[i][0] = shape1[i][1] = shape1[i][2] = 1.0;
    shape2[i][0] = shape1[i][0]*shape1[i][0];
    shape2[i][1] = shape1[i][1]*shape1[i][1];
    shape2[i][2] = shape1[i][2]*shape1[i][2];
    lshape[i] = (shape1[i][0]*shape1[i][1]+shape1[i][2]*shape1[i][2]) *
      sqrt(shape1[i][0]*shape1[i][1]);
  }
}

/* ----------------------------------------------------------------------
   init for one type pair i,j and corresponding j,i
------------------------------------------------------------------------- */

double PairGayBernePatchy::init_one(int i, int j)
{
  offset[i][j] = 0.0;

  eps_ij_all[j][i] = eps_ij_all[i][j];
  z_ij_all[j][i] = z_ij_all[i][j];
  offset[j][i] = offset[i][j];

  return cut[i][j];
}

/* ----------------------------------------------------------------------
   proc 0 writes to restart file
------------------------------------------------------------------------- */

void PairGayBernePatchy::write_restart(FILE *fp)
{
  write_restart_settings(fp);

  int i,j;
  for (i = 1; i <= atom->ntypes; i++) {
    for (j = i; j <= atom->ntypes; j++) {
      fwrite(&setflag[i][j],sizeof(int),1,fp);
      if (setflag[i][j]) {
        fwrite(&eps_ij_all[i][j],sizeof(double),1,fp);
        fwrite(&z_ij_all[i][j],sizeof(double),1,fp);
        fwrite(&cut[i][j],sizeof(double),1,fp);
      }
    }
  }
}

/* ----------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */

void PairGayBernePatchy::read_restart(FILE *fp)
{
  read_restart_settings(fp);
  allocate();

  int i,j;
  int me = comm->me;
  for (i = 1; i <= atom->ntypes; i++) {
    for (j = i; j <= atom->ntypes; j++) {
      if (me == 0) utils::sfread(FLERR,&setflag[i][j],sizeof(int),1,fp,NULL,error);
      MPI_Bcast(&setflag[i][j],1,MPI_INT,0,world);
      if (setflag[i][j]) {
        if (me == 0) {
          utils::sfread(FLERR,&eps_ij_all[i][j],sizeof(double),1,fp,NULL,error);
          utils::sfread(FLERR,&z_ij_all[i][j],sizeof(double),1,fp,NULL,error);
          utils::sfread(FLERR,&cut[i][j],sizeof(double),1,fp,NULL,error);
        }
        MPI_Bcast(&eps_ij_all[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&z_ij_all[i][j],1,MPI_DOUBLE,0,world);
        MPI_Bcast(&cut[i][j],1,MPI_DOUBLE,0,world);
      }
    }
  }
}

/* ----------------------------------------------------------------------
   proc 0 writes to restart file
------------------------------------------------------------------------- */

void PairGayBernePatchy::write_restart_settings(FILE *fp)
{
  fwrite(&cut_global,sizeof(double),1,fp);
}

/* ----------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
------------------------------------------------------------------------- */

void PairGayBernePatchy::read_restart_settings(FILE *fp)
{
  int me = comm->me;
  if (me == 0) {
    utils::sfread(FLERR,&cut_global,sizeof(double),1,fp,NULL,error);
  }
  MPI_Bcast(&cut_global,1,MPI_DOUBLE,0,world);
}

/* ----------------------------------------------------------------------
   proc 0 writes to data file
------------------------------------------------------------------------- */

void PairGayBernePatchy::write_data(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    fprintf(fp,"%d %d %g %g %g\n",i,eps_ij_all[i][i],z_ij_all[i][i]);
}

/* ----------------------------------------------------------------------
   proc 0 writes all pairs to data file
------------------------------------------------------------------------- */

void PairGayBernePatchy::write_data_all(FILE *fp)
{
  for (int i = 1; i <= atom->ntypes; i++)
    for (int j = i; j <= atom->ntypes; j++)
      fprintf(fp,"%d %d %d %g %g %g %g\n",i,j,eps_ij_all[i][j],z_ij_all[i][j],
              cut[i][j]);
}

/* ----------------------------------------------------------------------
   compute analytic energy, force (fforce), and torque (ttor & rtor)
   based on rotation matrices a and precomputed matrices b and g
   if newton is off, rtor is not calculated for ghost atoms
------------------------------------------------------------------------- */

double PairGayBernePatchy::gayberne_analytic(const int i,const int j,double a1[3][3],
                                       double a2[3][3], double *r12,
                                       const double rsq, double *fforce,
                                       double *ttor, double *rtor)
{
  double tempv[3], tempv2[3], a1_nb[3], a2_nb[3];
  double temp[3][3];
  double temp1,temp2,temp3,temp_U,a1_nb_rhat,a2_nb_rhat;

  int *type = atom->type;
  int newton_pair = force->newton_pair;
  int nlocal = atom->nlocal;
  
  double r12hat[3];
  MathExtra::normalize3(r12,r12hat);
  double r = sqrt(rsq);
  double dr, forcelj, one_eng, dexp, r_o;
  int itype, jtype;
  itype = type[i];
  jtype = type[j];
  double n_b[3];
  n_b[0] = n_b[1] = n_b[2] = 0.0;
  
  // CHECK IF PATCHES ARE ALIGNED
  MathExtra::matvec(a1, n_b, a1_nb);
  a1_nb_rhat = MathExtra::dot3(a1_nb,r12hat);
  MathExtra::matvec(a2, n_b, a2_nb);
  a2_nb_rhat = MathExtra::dot3(a2_nb,r12hat);
  
  if ( ((-a1_nb_rhat) > costheta_m[i]) && (a2_nb_rhat > costheta_m[j])){
    // patches are aligned, calculate attractive potential, force
    r = sqrt(rsq);
    r_o = (shape1[itype][0] + shape1[jtype][0])/2;
    forcelj = ((-eps_ij_all[itype][jtype] * (z_ij_all[itype][jtype] * r + r_o) \
              /pow(r,2)) * exp(-z_ij_all[itype][jtype] * (r / r_o - 1)));
    temp_U = eps_ij_all[itype][jtype] - eps_ij_all[itype][jtype] * r_o / r * \
             exp(-z_ij_all[itype][jtype] * (r / r_o - 1)) - offset[itype][jtype];
    fforce[0] = r12[0]*forcelj;
    fforce[1] = r12[1]*forcelj;
    fforce[2] = r12[2]*forcelj;
    ttor[0] = ttor[1] = ttor[2] = 0.0;
    rtor[0] = rtor[1] = rtor[2] = 0.0;
  }

  return temp_U;
}
