#######################################################################
# SOFT SPHER POTENTIAL TABLE GENERATOR                                #
#######################################################################

########################################
# 1. Input interaction parameters
########################################

#=======================================
# Potential amplitude and tail exponent
epsilon = 1
n = 72

# Table settings
# number of points, min point, max point
N = 1000
rlo = 0.7
rhi = 1.4
#=======================================

########################################
# 2. Import functions
########################################

from math import exp,sqrt

########################################
# 3. Main part
########################################

f = open('./r%2d.table' % n,'w')

# Comments at top of morse.table
f.write('# Tabulated r%2d interaction for LAMMPS\n' % n)
f.write('#\n')
f.write('# Potential parameters:\n')
f.write('#   epsilon = %d\n' % epsilon)
f.write('#   n = %d' % n)

dr = (rhi-rlo)/(N-1)

f.write('\n')
f.write('R%2d\n' % n)
f.write('N %i R %f %f\n' % (N,rlo,rhi))
f.write('\n')
for k in range(0,N): # This loop currently redundant but working
    kindex = k+1
    r = rlo + k*dr
    energy = epsilon*pow(r,-n)
    force = n*epsilon*pow(r,-n-1)
    f.write('%i %.9e %.9e %.9e\n' % (kindex,r,energy,force))
f.close()
