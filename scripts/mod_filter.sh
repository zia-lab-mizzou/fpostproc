#!/bin/sh

# power of r, omega, and skip-so-many-cycles
r=$1
w=$2
s=$3

# see which output file match the options
# extract the raw data
for f in *.o*; do
    rf=`head $f | awk '/variable table/ {print $4}'`
    wf=`head $f | awk '/variable omega/ {print $4}'`
    if [ "$rf" == "$r" ]; then
	if [ "$wf" == "$w" ]; then
	    echo $f; log_extract_columns.sh $f "time Temp" 1 3 4 > ${f}.pp
	fi
    fi
done

# extract the moduli
for f in *.pp; do
    echo $f
    moduli.py $f $w $s | tail -n 1
done

# clean up
rm *.pp
