#!/bin/env bash

# Transform a bad eps file made in Igor pro to a legit one

if [ $# -lt 1 ]; then
    echo "Usage: `basename $0` file.eps"
    exit 1
fi

fname=$1
stem=${fname%.eps}

epstopdf $fname
pdftops -eps $stem.pdf
rm -f $stem.pdf
