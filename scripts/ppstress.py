#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
import sys
from math import ceil, floor, pi

input_files = ["stress.xy.out", "stress.xz.out", "stress.yz.out"]
output_files = ["proc.xy", "proc.xz", "proc.yz"]
skip_this_many = 15

class Data:
    """Holds time, stress and strain data and defines operations on it"""
    def __init__(self, filename):

        self.filename = filename

        # Pick stress column based on filename
        if filename == "stress.xy.out":
            self.direction = "xy"
        elif filename == "stress.xz.out":
            self.direction = "xz"
        elif filename == "stress.yz.out":
            self.direction = "yz"
        else:
            print "oops"
            sys.exit()

        # Open file, and read past header, then read in data
        print "Reading the %s file" % self.direction
        fp = open(filename, "r")
        fp.readline()
        fp.readline()
        self.time = []
        self.strain = []
        self.stress = [[],[],[],[],[],[]]
        for line in fp:
            words = line.split()
            self.time.append(float(words[1]))
            self.strain.append(float(words[2]))
            stress = [[float(x)] for x in words[3:9]]
            self.stress = [x+y for x,y in zip(self.stress, stress)]
        fp.close()

        # Calculate dt and omega from file
        self.dt = self.get_dt()
        self.omega = self.get_omega()

        # Read parameter file to provide output filename, etc.
        #self.read_param(paramfile)

    def convert_units(self):
        print "Converting units"
        self.time = [4*x for x in self.time]
        self.stress = [[x/8 for x in y] for y in self.stress]

    # Average over time intervals in file
    def get_dt(self):
        print "Getting dt"
        dt_sum = 0
        old_time = self.time[0]
        for time in self.time[1:]:
            new_time = time
            dt = new_time - old_time
            dt_sum += dt
            old_time = new_time
        count =len(self.time) - 1
        return dt_sum / count

    # Get omega for a file from Fourier transform
    def get_omega(self):
        print "Getting omega"
        transform = np.fft.rfft(self.strain).real
        end_idx = len(transform)/2 + 1
        max_val = transform[:end_idx].max()
        max_idx = np.where(transform[:end_idx].real == max_val)[0][0]
        return max_idx * 2*pi / (len(self.strain)-1) / self.dt

    def get_outfile_name(self, filename):
        pass

    def write(self, outfile):
        print "Writing the file"
        fp = open(outfile, "w")
        for i in xrange(len(self.time)):
            fp.write(str(self.time[i])+ "\t" +
                     str(self.strain[i]) + "\t" +
                     "\t".join([str(x[i]) for x in self.stress]) + "\n")
        fp.close()

    def resampled(self, skip_this_many):
        print "Resampling"
        total_time = self.time[-1] - self.time[0]
        period = 2*pi / self.omega
        ncycles = total_time / period
        if ncycles < skip_this_many:
            print "Error ... skipping too many cycles"
            sys.exit()

        # Get starting time and ending time
        start_time = self.time[0] + skip_this_many * period
        num_per = int(floor((self.time[-1] - start_time)/period))
        end_time = start_time + num_per * period

        # Set a new dt slightly smaller than the old dt for integer # in period
        points_per_period = int(ceil(period / self.dt))
        print points_per_period
        dt_new = period / points_per_period

        # Interpolate based on regridded time list
        # NOTE: outputting final point (would be same as first for periodic)
        time_new = [dt_new*n + start_time \
                        for n in range(points_per_period*num_per + 1)]
        splines = interpolate.splrep(self.time, self.strain, s=0)
        self.strain = interpolate.splev(time_new, splines, der=0)
        for i in range(len(self.stress)):
            splines = interpolate.splrep(self.time, self.stress[i], s=0)
            self.stress[i] = interpolate.splev(time_new, splines, der=0)
        self.time = time_new

    def zero_time(self):
        first_time = self.time[0]
        self.time = [x-first_time for x in self.time]

for infile, outfile in zip(input_files, output_files):
    data = Data(infile)
    dt = data.dt
    print "dt = %f" % dt
    omega = data.omega
    print "omega = %f" % omega
    data.resampled(skip_this_many)
    data.convert_units()
    #data.zero_time()
    print "not zeroing time"
    data.write(outfile)
