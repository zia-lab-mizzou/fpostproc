#!/bin/env bash

if [[ "$#" -ne "1" ]]; then
    echo usage: $0 [file_extension]
    exit 1
fi

str=$1

n=1
for f in `ls -v *.$str`; do
    mv $f $n.$str
    let n++
done
