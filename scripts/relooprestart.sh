#!/bin/sh

# NOT WORKING!

# June 12, 2014
#
# Take restart that doesn't work (could not assign atoms),
# and produce restart that does.  Utilizes awk and LAMMPS.
# Note: assumes Atom style = atomic
# Note: assumes Unit style = lj
# Note: assumes Pair style = table

if [ $# -lt 2 ]; then
    echo "usage `basename $0` restart [lammps_run_command]"
    exit 1
fi

# NOT WORKING
echo This script is not trusted right now
exit 1

restart=$1
shift 1
lammps_run_command=$*

# Use LAMMPS to produce data file
$lammps_run_command -r $restart BAD_LMP_DATA

# Assume restart ends in a period followed by the timestep
Ntimestep=`echo $restart | awk -F '.' '{print $NF}'`

# awk thru the data file, only adjusting x,y,z and ix,iy,iz when x,y,z bad

awk 'BEGIN               { print_normally = 1 }
     NR==3               { natoms = $1 }
     NR==7               { xlo = $1; xhi = $2; lx = $2 - $1 }
     NR==8               { ylo = $1; yhi = $2; ly = $2 - $1 }
     NR==9               { zlo = $1; zhi = $2; lz = $2 - $1 }
     /Atoms/             { first = NR + 2; \
                           last = NR + 2 + natoms - 1 }
     NR>=first&&NR<=last { print_normally = 0; \
                           xu = $3 + $6 * lx; \
                           yu = $4 + $7 * ly; \
                           zu = $5 + $8 * lz; \
                           xmult = (xu - xlo) / lx; \
                           ymult = (yu - ylo) / ly; \
                           zmult = (zu - zlo) / lz; \
                           if (xmult >= 0) ix = int(xmult); \
                           else ix = int(xmult) - 1; \
                           if (ymult >= 0) iy = int(ymult); \
                           else iy = int(ymult) - 1; \
                           if (zmult >= 0) iz = int(zmult); \
                           else iz = int(zmult) - 1; \
                           x = xu - ix * lx; \
                           y = yu - iy * ly; \
                           z = zu - iz * lz; \
                           printf("%d %d %18.16e %18.16e %18.16e %d %d %d\n", \
                                  $1,$2,x,y,z,ix,iy,iz)}
     NR>last             { print_normally = 1 }
     print_normally==1   { print }' BAD_LMP_DATA > GOOD_LMP_DATA

# Use LAMMPS to create a restart file, using above assumptions

cat > in.CREATE_RESTART << EOF
read_data GOOD_LMP_DATA
reset_timestep ${Ntimestep}
atom_modify sort 0 0
run 0
write_restart restart.CORRECTED
EOF

$lammps_run_command -v Ntimestep $Ntimestep < in.CREATE_RESTART

# Clean up

mv restart.CORRECTED $restart.corrected
rm -f in.CREATE_RESTART BAD_LMP_DATA GOOD_LMP_DATA log.lammps
