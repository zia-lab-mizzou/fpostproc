#!/usr/bin/env python2.7

# Offset plot hash

import sys
import numpy
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

infile = sys.argv[1]
title = sys.argv[2]

# These nrows and ncols are for the final output, not the input file
nrows = 2
ncols = 4

# Read data for this snapshot
data = numpy.genfromtxt(infile, dtype=float)
col = []
for i in range(nrows*ncols):
    col.append([row[i] for row in data])

# Layout
# Using matplotlibrc but adjusting the label size
fig = plt.figure()
mpl.rcParams['axes.labelsize'] = 16.0

# Histogram bar appearance
fc = ["black", "gray", "blue", "red", "green", "cyan", "orange", "brown"]
label = ["0","1","2","3","4","5","6","7+"]

for i in range(nrows*ncols):
    
    # Default subplot settings
    sp = fig.add_subplot(nrows,ncols,i+1,axisbg='w')
    sp.axes.set_xlim(-1,13)
    sp.axes.set_ylim(0,1)
    for tick in sp.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)

    # Set titles
    #sp.set_title(titles[i])

    # Make only certain axes display text, improving legibility
    if (i%ncols!=0):
        sp.axes.yaxis.set_ticklabels([])

    #sp.axes.set_ylabel("$p(N_c(\Delta t) | N_c(0))$")
    
    # Create this subplot's bars
    # Offsetting to line up bar center with xtick
    xval = [x-0.5 for x in range(len(col[i]))]
    sp.bar(xval, col[i], facecolor=fc[i])
    sp.set_title(label[i])

plt.tight_layout()

# Shared horizontal and vertical axis labels
fontprop = mpl.font_manager.FontProperties(size=16.0)
plt.figtext(0.5, 0.05, "Contact number, $N_c$",
            horizontalalignment="center",
            fontproperties=fontprop)
plt.figtext(0.05, 0.55, "$p(N_c(\Delta t) | N_c(0))$",
            rotation='vertical',
            verticalalignment="center",
            fontproperties=fontprop)

# Subplot indices and space at bottom
fig.subplots_adjust(bottom=0.2)
fig.subplots_adjust(top=0.9)
fig.subplots_adjust(left=0.15)
fig.subplots_adjust(right=0.9)

plt.savefig(title + ".png")
