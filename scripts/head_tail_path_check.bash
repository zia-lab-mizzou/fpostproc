#!/bin/bash

# head_tail_path_check.sh

# Inspect the first and last timestep listed in paths.txt for Nc problems.
# Output is a table of contact numbers and the minimum and maximum PE for those,
#   divided by half the pair potential minimum.

if [[ "$#" -ne "3" ]]; then
    echo usage: $0 [path] [umin] [rcut]
    exit 1
fi

path=$1
umin=$2
rcut=$3

t1=`awk 'NR==2 {print $2}' $path/paths.txt`
t2=`awk '{t=$3} END {print t}' $path/paths.txt`


echo Nc check for timesteps $t1 and $t2

times="$t1 $t2"
for t in $times; do
    list_nc $path $t $rcut > HTPC_NC
    list_pe $path $t > HTPC_PE
    awk '{print $2}' HTPC_PE > HTPC_PECOL
    paste HTPC_NC HTPC_PECOL > HTPC_COMP

    awk -v u=$umin \
	'BEGIN {for (i=0;i<13;i++) maxes[i]=-999}
               {bin=$2}
               {if ($3>maxes[bin]) maxes[bin]=$3}
               {if ($3<mins[bin]) mins[bin]=$3}
         END   {for (i=0;i<13;i++) printf "%d ", i; printf "\n"; \
                for (i=0;i<13;i++) printf "%f ", -maxes[i]*2/u; printf "\n"; \
                for (i=0;i<13;i++) printf "%f ", -mins[i]*2/u; printf "\n"}' HTPC_COMP
done

rm -f HTPC_NC HTPC_PE HTPC_PECOL HTPC_COMP
