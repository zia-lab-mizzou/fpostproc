#!/bin/env bash

first_step=`awk 'NR==2 {print $2}' paths.txt`

nbatch=23
batch_size=11
rsep=0.1

for i in $(seq 1 $nbatch); do

    cat > $i.slurm <<EOF
#!/bin/env bash

#SBATCH -p normal
#SBATCH -n 1
#SBATCH -t 4:00:00
#SBATCH -A TG-CTS130035
#SBATCH --mail-user=blandrum@princeton.edu --mail-type=begin --mail-type=end

cd \$SLURM_SUBMIT_DIR

EOF

    # First file number for this batch

    start_file=$(( ($i-1)*$batch_size + 1 ))
    for j in $(seq 1 $batch_size); do
	m=$(( $start_file + $j - 1 ))
	h5posf=`awk -v k=$(( $m + 1 )) 'NR==k {print $1}' paths.txt`
	tstart=`awk -v k=$(( $m + 1 )) 'NR==k {print $2}' paths.txt`
	tdur=`awk -v k=$(( $m + 1 )) 'NR==k {print $3-$2}' paths.txt`
	tinc=`awk -v k=$(( $m + 1 )) 'NR==k {print $4}' paths.txt`
	echo list_nc `pwd`/ $tstart $tdur $tinc $rsep $h5posf.nc >> $i.slurm
    done

done
