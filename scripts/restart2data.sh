#!/bin/bash

for i in {1..128}
do
    cd $i
    echo $i
    a=`awk '/TIMESTEP/{getline; print}' dump/\`ls -1v dump | tail -n 1\` | tail -n 1 `
    echo $a
    #prev=`ls -v1 data/ | tail -n 1 | grep -o '[0-9]*$'`
    #echo $prev
    b=$(( a-a%1000000 ))
    echo $b
    c=$(( $b+0 ))
    #echo $c
    #file=`ls -v1 restart/ | tail -1`
    cd restart
    file=`ls -v *.$b | tail -n 1`
    echo $file
    #data_file=`ls -v1 restart/ | tail -1 | sed 's/restart/data/'`
    data_file=`echo $file | sed 's/restart/data/' | sed "s/$b$/$c/"`
    echo $data_file
    cd ..
    mpirun -n 1 lmp_hb -restart2data restart/$file data/$data_file nocoeff nolabelmap
    cd ..
done
