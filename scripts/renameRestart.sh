#!/bin/bash

for i in {1..128}
do
    cd $i
    echo $i
    cd data
    file=`ls -v | grep ".*rep0\.[0-9]*$"`
    #echo $file
    for f in ${file[@]}
    do
        #echo $f
        new_f=`echo $f | sed 's/rep0\./rep0\.0\./'`
        #echo $new_f
        mv $f $new_f
    done
    cd ..
    cd ..
done
