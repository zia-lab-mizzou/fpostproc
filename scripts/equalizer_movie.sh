#!/bin/bash

# equalizer_movie.sh
# Last updated September 20, 2013

# Note: requires relatively modern version of gnuplot to work (looping)?
n=0
for f in "$@"; do
    gnuplot << EOF
set terminal postscript eps size 3.5,2.62 enhanced color font 'Times,9' linewidth 2
set output "$f.eps"
set size 1,1
set origin 0,0
set key font ",16"
set xrange[-1:13]
set yrange[0:1]
set xlabel "{/Times-Italic N}_c" font "Times,16"
set ylabel "{/Times-Italic p}({/Times-Italic N}_c)" font "Times,16" offset 2,0
set grid
set key center tmargin samplen 0
set style fill solid 1.0 noborder
set style line 1 lt 1 lc rgb "#404040"
set style line 2 lt 1 lc rgb "#00ff00"
set style line 3 lt 1 lc rgb "#0000ff"
set style line 4 lt 1 lc rgb "#006600"
set style line 5 lt 1 lc rgb "#990000"
set style line 6 lt 1 lc rgb "#33ffff"
set style line 7 lt 1 lc rgb "#000000"
set multiplot layout 2,4 rowsfirst scale 1.1,1
myplot(f,x,t) = sprintf("plot '%s' u (column(0)):%d:(0.6) w boxes title '%s' ls '%d'",f,x+1,t,x+1)
eval myplot("$f",0,"0")
eval myplot("$f",1,"1")
eval myplot("$f",2,"2")
eval myplot("$f",3,"3")
eval myplot("$f",4,"4")
eval myplot("$f",5,"5")
eval myplot("$f",6,"6+")
unset multiplot
EOF
    convert -geometry 800x600 -density 800 -flatten $f.eps $n.multiplot.png
    rm $f.eps
    n=$(($n+1))
done

# Below, I had to remove a horizontal pixel to get the height divisible
# by 2.  The pixel format avoided outputting a black screen in QuickTime.
# I'm leaving the PNG frames in the directory, in case the ffmpeg command
# didn't work for some reason.
ffmpeg -i %d.multiplot.png -vf "crop=in_w:in_h-1" -f mp4 -vcodec libx264 -pix_fmt yuv420p multiplot.mp4
