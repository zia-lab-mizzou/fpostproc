#!/bin/env bash

# don2lmpdat.bash
#
# Convert Donev's output format (in modified program) to LAMMPS data
# IMPORTANT: Assuming all masses are 1

if [ $# -lt 3 ]; then
    echo USAGE: `basename $0` [donfile] [lmpdat] [scaleon]
    exit 1
fi

donfile=$1
lmpdat=$2
scaleon=$3

natoms=`head $donfile | awk 'NR==2 {print $1}'`
ntypes=`head $donfile | awk 'NR==2 {print $2}'`

# Get box length from the diameter of the species that we 'scaleon'
boxlen=`head $donfile | awk -v f=$scaleon 'NR==4 {printf "%.12g\n", 1/$f}'`

cat <<EOF > $lmpdat
LAMMPS data file from Donev output $donfile

$natoms atoms 

$ntypes atom types

0 $boxlen xlo xhi 
0 $boxlen ylo yhi 
0 $boxlen zlo zhi 

Masses

EOF

for i in $(seq 1 1 $ntypes); do
    echo $i 1 >> $lmpdat
done
echo >> $lmpdat
echo Atoms >> $lmpdat
echo >> $lmpdat

# Assume atoms are in order, assigning them according to the header info

awk -v len=$boxlen\
    'NR==3 {for (i=1; i<=NF; i++) n[i]=$i}
     NR>6  {if (n[t]<1) {
              while (n[t]<1) {
                t+=1;
              }
            }
            
            j+=1
            printf "%d %d %.12g %.12g %.12g\n",j,t,$4*len,$5*len,$6*len;
            n[t]-=1}' \
		$donfile >> $lmpdat
