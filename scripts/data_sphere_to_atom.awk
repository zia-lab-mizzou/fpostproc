#!/bin/awk -f

# Shut off switches if blank line

NF==0 { atoms=0; velocities=0 }

# Add masses line

NR==10 { print;		  \
         print "Masses";  \
	 print "1 1";	  \
	 print "2 1";	  \
	 print "3 1";	  \
	 print "4 1";	  \
	 print "5 1";	  \
	 print ""; next }

# Deal with Atoms part

/Atoms/  { atoms = 1; print;			\
           getline; print; next }
atoms==1 { print $1,$2,$5,$6,$7,$8,$9,$10; next }

# Deal with Velocities part

/Velocities/  { velocities = 1; print;		\
                getline; print; next }
velocities==1 { print $1,$2,$3,$4; next }

# Just print if nothing matched

{print}
