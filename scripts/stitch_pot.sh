#!/bin/bash

if [ $# -lt 2 ]; then
    echo "USAGE `basename $0` {firstFile} {secondFile}"
    exit 1
fi

n_header=6
f1=$1
f2=$2

N1=`awk '(NR==5) {print $2}' $f1`
#echo $N1
inner1=`awk '(NR==5) {print $4}' $f1`
#echo $inner1
outer1=`awk '(NR==5) {print $5}' $f1`
#echo $outer1
N2=`awk '(NR==5) {print $2}' $f2`
#echo $N2
inner2=`awk '(NR==5) {print $4}' $f2`
#echo $inner2
outer2=`awk '(NR==5) {print $5}' $f2`
#echo $outer2

u1=`awk 'END{print $3}' $f1`
echo $u1
u2=`awk -v n_header=$n_header '(NR==n_header+1){print $3}' $f2`
echo $u2
#ud=$(( u1-u2 ))
#echo $ud

awk -v N1="$N1" -v N2="$N2" -v outer2="$outer2" '{if(NR==$n_header+N1) {$3+=0;printf "%8d %- 22.15g %- 22.15g %- 22.15g\n", $1,$2,$3,$4} else if(/N /) {print $1, $2+N2, $3, $4, $5=outer2} else {print}}' $f1 > temp
awk -v n_skip=$n_header -v N1="$N1" -v u1=$u1 -v u2=$u2 'NR>n_skip {$1=$1+N1;$3=$3+u1-u2;printf("%8d %- 22.15g %- 22.15g %- 22.15g\n",$1,$2,$3,$4)}' $f2 >> temp

