#!/bin/awk -f

# mean_nc.awk
#
# Output the mean Nc for population with some original Nc

# Assumes that there is no header
# The first line deals with a contact number of zero bin

NR>1 { v0  += (NR-1)*$1;  \
       v1  += (NR-1)*$2;  \
       v2  += (NR-1)*$3;  \
       v3  += (NR-1)*$4;  \
       v4  += (NR-1)*$5;  \
       v5  += (NR-1)*$6;  \
       v6  += (NR-1)*$7;  \
       v7  += (NR-1)*$8;  \
       v8  += (NR-1)*$9;  \
       v9  += (NR-1)*$10; \
       v10 += (NR-1)*$11; \
       v11 += (NR-1)*$12; \
       v12 += (NR-1)*$13 }
END  { print v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12 }
