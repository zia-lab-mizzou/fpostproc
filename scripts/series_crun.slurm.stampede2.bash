#!/bin/bash

# series_crun.slurm.bash
# Last updated November 20, 2014

# Submit a series of continuation runs that depend on completion of each other
# Continuation runs will launch whether or not last run had exit 0
# Requires the CRUN environmental variable that gives path to crun.slurm
# Dependency jobid is optional
# IMPORTANT: must be submitted from the jobs subdirectory!
# IMPORTANT: not currently able to specify extra LAMMPS flags for executable here
# Added wait time of 10 seconds for job to start to give time for folder preparation (wflag)

if [ $# -lt 9 ]; then
    echo "usage `basename $0` [nickname] [crun] [lammps] [queue] [ntasks] [nnodes] [walltime] [ncruns] [name] [opt:origid]"
    exit 1
fi

set -e

nickname=$1
crun=$2
lmp=$3
queue=$4
ntasks=$5
nnodes=$6
walltime=$7
ncruns=$8
name=$9
origid=${10}

Jflag="-J $name"
tflag="-t $walltime"
Mflag="--mail-user=$EMAILJOB --mail-type=begin --mail-type=end"
wflag='--begin=now+10'

# Switch for different clusters
if [ "$nickname" == "Stampede" ]; then
    Aflag="-A TG-CTS130035"
    nflag="-n $ntasks"
    pflag="-p $queue"
    Nflag="-N $nnodes"
elif [ "$nickname" == "Tiger" ]; then
    nflag="-N $(( $ntasks / 16 )) --ntasks-per-node=16"
elif [ "$nickname" == "Hellbender" ]; then
    Aflag="-A rnzrf3-lab"
    nflag="-n $ntasks"
    pflag="-p $queue"
    Nflag="-N $nnodes"
    cflag="-c $(( 128/$ntasks ))"
else
    echo "nickname $nickname available to `basename $0` not recognized"
    exit 1
fi

# Uses 2 dependencies: one for the original job and the other for the
#   previous job in the chain.
# If this is the original job (blank origid), there are no dependencies.
# If this is the first in a chain, but not the original, set both dependencies
#   to the original job.
# After submitting the job, updated the second dependency for the next job
#   with the returned jobid.
for i in `seq 1 $ncruns`;
do
    if [[ "$i" == "1" && -n "$origid" ]]; then
	depid=$origid
    fi
    if [ $origid ]; then
	dflag="-d afterany:${origid}:${depid}"
    fi
    flags="$wflag $Jflag $pflag $nflag $Nflag $tflag $Aflag $Mflag $dflag $cflag"
    echo sbatch $flags $crun $nickname $lmp > series_crun_tmp
    sbatch $flags $crun $nickname $lmp >> series_crun_tmp
    cat series_crun_tmp >> series_crun_log
    depid=`awk '/Submitted batch job/ {print $4}' series_crun_tmp`
    if [[ "$i" == "1" && -z "$origid" ]]; then
	origid=$depid
    fi
    echo "Submitted batch job $depid"
    echo "LAMMPS run command was $lmp"
    rm series_crun_tmp
done
