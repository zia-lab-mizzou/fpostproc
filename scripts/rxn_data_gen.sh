#!/bin/bash

if [ $# -ne 2 ]
then
    echo "USAGE `basename $0` {run_name} {index}"
    exit 1
fi

run_name=$1
index=$2

for i in {2..128}
do
    data_file=`ls -v1 $i/data/ | tail -n 1`
    echo $data_file
    #prev_file=`ls -v1 $i/data/ | tail -n 2 | head -n 1`
    #echo $prev_file
    step=`echo $data_file | grep -o '[0-9]*$'`
    echo $step
    #prev_step=`echo $prev_file | grep -o '[0-9]*$'`
    #echo $prev_step
    diff_step=$(( $step-0 ))
    echo $diff_step
    filename=`grep -rl "^$diff_step$" $i/dump/*${run_name}.${index}.* | tail -n 1`
    echo $filename
    awk -v laststep="^${diff_step}$" -f ~/src/fpostproc/scripts/reactions_extract.awk $filename > temp
    sort -n temp > temp2
    echo "" >> $i/data/$data_file
    echo "Reactions # rxnc, rxnp, rxnt, rxnd" >> $i/data/$data_file
    echo "" >> $i/data/$data_file
    cat temp2 >> $i/data/$data_file
    cp $i/data/$data_file $i/input/data.translation_gr1.0_rxn_ncog1_rep0_restart$(( $index+1 ))
    rm temp*
done
