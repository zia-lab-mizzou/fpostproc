#!/usr/bin/env python2.6

# generateRun_della.py
#
# AUTHOR: Ben Landrum
# LAST UPDATED: June 5, 2013
# PURPOSE: Submit a series of runs on the Della cluster
#          Fills the 'input' folder with files needed for a job
#          Then, copies the input to the jobs folder and submits to the queue
# INPUT: A 'parambase' file, which includes LAMMPS variable definitions
#        and wildcards (preceding '@' for file variable, '?' for non-file)
#        An 'input' directory, submitted to the cluster
#        A 'file_variables' directory, stores file variables (init restarts)
# USE: Run the file, which prompts for variable values
# NOTES: The script submits the job to get the jobid before the job directory is created.
#        In order to have this ready quickly, I prepare the run in
#          the ./jobs/GENERATE_RUN_TMP directory ahead of time and move this to the
#          correct job directory immediately after I get the jobid.
#        It usually takes some time for a job to run after it's submitted.


# ADD COMPLETION?

import os, sys, re, shutil, itertools
import subprocess as sp

class ParamVariable:
    """A class to store wildcard variable information
    Instantly moves files in ./input to ./file_variables
    Has elements 'type' and 'values'
    'type' is a string, either 'file' or 'non-file'
    'values' SHOULD BE a list, without restriction on element type"""
    def __init__(self, type, values):
        if type == "file":
            for value in values:
                in_input = os.path.isfile("./input/"+value)
                in_file_variables = os.path.isfile("./file_variables/"+value)
                if in_input and in_file_variables:
                    print("Error: %s in ./input and ./file_variables" % value)
                    sys.exit(1)
                elif in_input:
                    shutil.move("./input/"+value, "./file_variables/"+value)
                elif in_file_variables:
                    pass
                else:
                    print("Error: %s neither in ./input nor ./file_variables"\
                          % value)
                    sys.exit(1)
            self.type = "file"
        elif type == "non-file":
            self.type = "non-file"
        else:
            print("Error: type must be 'file' or 'non-file'")
            sys.exit(1)
        self.values = values

    def __repr__(self):
        return "ParamVariable(type=%s, values=[%s])" % \
               (self.type, ", ".join([str(x) for x in self.values]))

    def return_option(self, i):
        """Return ParamVariable instance with a single option in values"""
        if i >= len(self.values):
            print("Error: out of range for values")
            sys.exit(1)
        return ParamVariable(self.type, [self.values[i]])

    def expand_options(self):
        """Output a list of single-valued ParamVariables from a single
        ParamVariable"""
        return [self.return_option(i) for i in xrange(len(self.values))]

def prompt_for_values(name):
    return raw_input("Enter values for %s: " % name).split()

def dict_product(d):
    """Input dictionary of keys with values of class ParamVariable
    Output list of dicts with values that are variable choices"""
    expanded_dict = {}
    for k,v in d.iteritems():
        expanded_dict[k] = v.expand_options()
    product = [x for x in apply(itertools.product, expanded_dict.values())]
    return [dict(zip(expanded_dict.keys(), p)) for p in product]

def write_param_file(parambase, d):
    """Output string containing parambase contents, with wildcards replaced
    by SINGLE values in dict of ParamVariables"""
    f = open("./scripts/"+parambase, "r")
    s = ""
    for line in f:
        words = line.split()
        if len(words) == 4:
            if words[3] == "@" or words[3] == "?":
                name = words[1]
                words[3] = str(d[name].values[0])
        s += " ".join(words) + "\n"
    f.close()
    f = open("./input/param."+parambase[10:]+".0", "w")
    f.write(s)
    f.close()

def move_necessary_files(d_orig, d_single):
    """For each file variable in d_orig
    If variable is not the d_single variable, move that file out of ./input
    If variable is, move that file into ./input
    Reservoir of files is in ./file_variables"""
    for k,v in d_orig.iteritems():
        if v.type == "file":
            desired_file = d_single[k].values[0]
            for filename in v.values:
                if filename == desired_file:
                    try:
                        shutil.move("./file_variables/"+filename, \
                                    "./input/"+filename)
                    except IOError:
                        pass
                else:
                    try:
                        shutil.move("./input/"+filename, \
                                    "./file_variables/"+filename)
                    except IOError:
                        pass

def submit_della_file(della_basename):
    """Submit a job from the jobs directory."""
    os.chdir("./jobs")
    spp = sp.Popen(["qsub", "../scripts/"+della_basename], stdout=sp.PIPE, stderr=sp.STDOUT)
    while True:
        line = spp.stdout.readline()
        if line != "":
            words = line.split(".")
            try:
                int(words[0])
                jobid = int(words[0])
            except:
                break
        else:
            break
    os.chdir("..")
    try:
        return jobid
    except:
        print "Could not get jobid from system. Try manual submission."
        sys.exit()

def prepare_job_directory():
    job_folder = "./jobs/GENERATE_RUN_TMP"
    os.mkdir(job_folder)
    shutil.copytree("./input", job_folder+"/input")
    os.mkdir(job_folder+"/jpg")
    os.mkdir(job_folder+"/log")
    os.mkdir(job_folder+"/dump")
    os.mkdir(job_folder+"/output")
    os.mkdir(job_folder+"/data")
    os.mkdir(job_folder+"/restart")

def move_job_directory(jobid):
    shutil.move("./jobs/GENERATE_RUN_TMP", "./jobs/"+str(jobid))

def move_files_back(d):
    for k,v in d.iteritems():
        if v.type == "file":
            for filename in v.values:
                try:
                    shutil.move("./input/"+filename, \
                                "./file_variables/"+filename)
                except IOError:
                    pass

# Look for parambase.* files in scripts directory
# If there is only one of these, open it
parambase_files = [f for f in os.listdir("./scripts") if \
                   (f.startswith("parambase") and not f.endswith("~"))]
if len(parambase_files) < 1:
    print("Didn't find any parambase files")
    sys.exit()
elif len(parambase_files) > 1:
    print("Found more than one parambase file")
    sys.exit()
parambase = parambase_files[0]
print parambase

# Check if there is only one *.della in ./scripts
della_files = [f for f in os.listdir("./scripts") if \
              (f.endswith(".della") and not f.endswith("~"))]
if len(della_files) < 1:
    print("Didn't find any .della files")
    sys.exit()
elif len(della_files) > 1:
    print("Found more than one .della file")
    sys.exit()
della_basename = della_files[0]

# Read through parambase file, searching for variable wildcards
# Create dictionary with these variable names as keys
f = open("./scripts/"+parambase, "r")
var_dict = {}
for line in f:
    words = line.split()
    if len(words) == 4:
        if words[3] == "@":
            type = "file"
        elif words[3] == "?":
            type = "non-file"
        else:
            continue
        name = words[1]
        values = prompt_for_values(name)
        var_dict[name] = ParamVariable(type, values)

enumerated_options = dict_product(var_dict)

for d in enumerated_options:
    move_necessary_files(var_dict, d)
    write_param_file(parambase, d)
    prepare_job_directory()
    jobid = submit_della_file(della_basename)
    move_job_directory(jobid)
    print "Submitted jobid %d" % jobid

# Move all file variables back into the file_variables directory
move_files_back(var_dict)
