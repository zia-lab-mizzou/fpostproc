#!/bin/awk -f
BEGIN {start=0; np=0}
NR==4 {np=$1}

/ITEM: TIMESTEP/{
    getline;
    if ($0 ~ laststep) {start=NR+8};
}
start>0 && NR>=start && NR<=start+np {print $1, $9, $10, $11, $12}
