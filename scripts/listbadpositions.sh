#!/bin/sh

awk 'NR==3               { natoms = $1 }
     NR==7               { xlo = $1; lx = $2 - $1 }
     NR==8               { ylo = $1; ly = $2 - $1 }
     NR==9               { zlo = $1; lz = $2 - $1 } 
     /Atoms/ { first = NR + 2; last = NR + 2 + natoms - 1 }
     first>0&&NR>=first&&NR<=last {if (($3-xlo > lx) || ($4-ylo > ly) || ($5 - zlo > lz)) print $1,$3,$4,$5; \
                                   else if (($3 < xlo) || ($4 < ylo) || ($5 < zlo)) print $1,$3,$4,$5}' $1
