#!/usr/bin/env bash

if [ $# -lt 1 ]; then
    echo "USAGE `basename $0` [jobid]"
    exit 1
fi

jobid=$1

loglist=`ls -v $jobid/log/log.*.*`
lastlog=`ls -v $jobid/log/log.*.* | tail -n 1`
if [ -s $lastlog ]; then
    :
else
    lastlog=`ls -v $jobid/log/log.*.* | tail -n 2 | head -n 1`
fi

running=1
for l in $loglist; do
    delta=`awk '/Loop time/ {printf "%f\n", b-a}; /Step/ {getline; a=$3}; {b=$3}' $l`
    running=`echo "$delta > 0" | bc -l`
    if [ "$running" -eq "0" ]; then
	strainstop=`awk '/Loop time/ {print b}; {b=$3}' $l`
	break
    fi
done

straincur=`awk '/Loop time/ {print b}; {b=$3}' $lastlog`

echo $jobid $running $strainstop $straincur
