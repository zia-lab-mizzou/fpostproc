#!/opt/apps/python/epd/7.3.2/bin/python

import sys
import os
import random
import numpy as N
from tables import *

if len(sys.argv) < 3:
    sys.exit('Usage: %s HDF5file dumpfile' % sys.argv[0])
if (not os.path.exists(sys.argv[1])) or \
       not os.path.exists(sys.argv[2]):
    sys.exit('ERROR: Check your paths')

hfile = sys.argv[1] # HDF5 file
dfile = sys.argv[2] # dump file

numsteps = 10
def OutFieldsCurrStep(dfile, atom_list, natoms):
    """Read through one step, extracting data for target atoms
    The order is id type xu yu zu vx vy vz pe sts[1-6]"""
    dict = {}
    for i in atom_list:
        dict[i] = []
    atom_list_of_strings = [str(atom) for atom in atom_list]
    for i in xrange(9):
        dfile.readline()
    for i in xrange(natoms):
        line = dfile.readline()
        words = line.split()
        atom_str = words[0]
        if atom_str in atom_list_of_strings:
            atom = int(atom_str)
            dict[atom] += [int(words[1])]
            for word in words[2:]:
                dict[atom] += [float(word)]
    return dict

# Read through first timestep to get fields for atoms in atom_list
d = open(dfile, "r")
d.readline(); d.readline(); d.readline()
natoms = int(d.readline())
d.seek(0)

# Make a random list of atom ids to check
atom_list = []
numcheck = 100
print("Checking %d atoms" % numcheck)
for i in xrange(numcheck):
    atom_list += [random.randrange(1,natoms+1)]

# Open the HDF5 file
h = openFile(hfile, "r")

# Check fields, step one
epos_max = 0
evel_max = 0
epe_max = 0
ests_max = 0
for n in xrange(numsteps):
    atom_dict = OutFieldsCurrStep(d, atom_list, natoms)
    for atom in atom_list:
        dpos = N.array(atom_dict[atom][1:4])
        dvel = N.array(atom_dict[atom][4:7])
        dpe  = N.array([atom_dict[atom][7]])
        dsts = N.array(atom_dict[atom][8:14])
        hpos = h.root.pos[n, atom-1, :]
        hvel = h.root.vel[n, atom-1, :]
        hpe  = h.root.pe [n, atom-1, :]
        hsts = h.root.virialstress[n, atom-1, :]
        epos = N.linalg.norm(N.subtract(dpos,hpos))
        evel = N.linalg.norm(N.subtract(dvel,hvel))
        epe  = N.linalg.norm(N.subtract(dpe ,hpe ))
        ests = N.linalg.norm(N.subtract(dsts,hsts))
        if epos > epos_max:
            epos_max = epos
        if evel> evel_max:
            evel_max = evel
        if epe > epe_max:
            epe_max  = epe
        if ests> ests_max:
            ests_max = ests
    print("Read step %d" % n)

# Print maximum errors:
print("")
print("Maximum errors:------")
print("Position: %g" % epos)
print("Velocity: %g" % evel)
print("PotEng:   %g" % epe)
print("Stress:   %g" % ests)

h.close()
d.close()
