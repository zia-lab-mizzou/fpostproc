#!/usr/bin/env bash

# package_list.sh
#
# See if somebody you're working with has the LaTeX packages, executables, and fonts
# you need in order to compile your document.  Edit the lists below for your needs.
#
# Requires kpsewhich, which comes with TeX distributions.

# List files here (must have certain extensions, according to kpsewhich?)

k=`command -v kpsewhich 2>&1`
if [ -z "$k" ]; then
    echo error: missing kpsewhich executable
    exit 1
fi

#command -v kpsewhic >/dev/null 2>&1 || (echo "error: cannot find executable kpsewhich"; exit 1)

plst="amsmath.sty
      beamer.cls
      biblatex.sty
      booktabs.sty
      cancel.sty
      glossaries.sty
      graphicx.sty
      hyperref.sty
      kvoptions.sty
      libertine.sty
      mhchem.sty
      pst-plot.sty
      pstricks-add.sty
      soul.sty
      subcaption.sty
      subfig.sty
      subfigure.sty
      texgyrepagella-math.otf
      unicode-math.sty
      xcolor.sty
      xspace.sty
      xstring.sty"

# List executables here

elst="biber
      bibtex
      makeglossaries
      xelatex
      xetex"

for f in $plst; do
    n=`kpsewhich $f`
    test $n && echo -e "\033[01;32m FOUND   \033[0m\033[00;36m$f\033[0m" $n || echo -e "\033[01;31m MISSING \033[0m\033[00;36m$f\033[0m"
done

for f in $elst; do
    n=`which $f 2>/dev/null`
    test $n && echo -e "\033[01;32m FOUND   \033[0m\033[00;36m$f\033[0m" $n || echo -e "\033[01;31m MISSING \033[0m\033[00;36m$f\033[0m"
done
