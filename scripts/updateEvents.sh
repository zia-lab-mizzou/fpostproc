#!/bin/bash

if [ $# -ne 1 ]
then
    echo "USAGE `basename $0` {index}"
    exit 1
fi
    
index=$1
prev=$(( $index-1 ))
for i in {1..128}
do
    cd $i
    step=`ls -v data/ | head -n $prev | tail -n 1 | grep -o "[0-9]*$"`
    echo $step
    awk -v step=$step '/reacting/{print $1, $2, $3, $4, $5, $6, $7, $8+step;next}/unbinding/{print $1, $2, $3, $4, $5, $6, $7+step;next}/unbound/{print $1, $2, $3, $4, $5, $6, $7+step;next} {print}' output/out${index}_$i.txt > output/temp
    cp output/out${index}_$i.txt output/z_out2${index}_$i.txt
    mv output/temp output/out${index}_$i.txt
    cd ..
done
     
