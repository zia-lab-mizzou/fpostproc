#!/opt/apps/python/epd/7.3.2/bin/python

# h5trjdir16
# Last updated: November 19, 2013
# Use 16 procs on Stamped to HDF5 a bunch of .lammpstrj files

from os import getcwd, listdir, kill, setsid
from os.path import isfile, join
import signal
import sys
import re
import subprocess as sp

def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]

argc = len(sys.argv)
if argc == 2:
    has_lastninc = False
    stem = sys.argv[1]
elif argc == 3:
    has_lastninc = True
    stem = sys.argv[1]
    lastninc = int(sys.argv[2])
else:
    print "Usage: h5trjdir [stem] [optional: lastninc]"
    sys.exit()

# Preferences
queue = "normal"
timestr = "24:00:00"
acct = "TG-CTS130035"
jobname = "h5trjdir16.slurm"
nprocs = 16

# Make ordered list of files with correct stem
# Sorting by number just in case
pwd = getcwd()
onlyfiles = [f for f in listdir(pwd) if isfile(join(pwd,f))]
trjlist = [f for f in onlyfiles if re.match(stem+".+\.lammpstrj$",f)]
trjlist = sorted(trjlist, key=natural_sort_key)

#dict = {}
#for i in xrange(len(trjlist)):
#    number = [int(s) for s in trjlist[i].split(".") if s.isdigit()][0]
#    dict[number] = trjlist[i]
#tmplist = []
#for key in sorted(dict.iterkeys()):
#    tmplist += [dict[key]]
#trjlist = tmplist

# Get list of first timesteps in order to set last timesteps in executable
tstartlist = []
for f in trjlist:
    fp = open(f, "r")
    fp.readline()
    line = fp.readline()
    tstartlist.append(int(line))
    fp.close()

fp = open(jobname, "w")
fp.write("#!/bin/bash\n")
fp.write("\n")
fp.write("#SBATCH -J "+jobname+"\n")
fp.write("#SBATCH -o "+jobname+".o%j\n")
fp.write("#SBATCH -n "+str(nprocs)+"\n")
fp.write("#SBATCH -p "+queue+"\n")
fp.write("#SBATCH -t "+timestr+"\n")
fp.write("#SBATCH -A "+acct+"\n")
fp.write("#SBATCH --mail-user=blandrum@princeton.edu\n")
fp.write("#SBATCH --mail-type=begin\n")
fp.write("#SBATCH --mail-type=end\n")
fp.write("\n")
fp.write("cd "+pwd+"\n")

for i in xrange(len(trjlist)):
    f = trjlist[i]
    h5file = f[:-9]+"h5"
    tstart = tstartlist[i]
    if i < len(trjlist)-1:
        tninc = tstartlist[i+1]
    else:
        if has_lastninc:
            tninc = lastninc
            if tninc < tstart:
                print "Error: last timestep in last shot before tstart"
                sys.exit()
        else:
            tninc = -1

    if tninc == -1:
        fp.write("dump2h5 "+f+" "+h5file+" &\n")
    else:
        fp.write("dump2h5 "+f+" "+h5file+" ninc "+str(tninc)+" &\n")
    if ((i+1)%nprocs == 0):
        fp.write("wait\n")

fp.write("wait\n")
fp.close()
spp = sp.Popen(["sbatch", jobname], stdout=sp.PIPE, stderr=sp.STDOUT)
spp.communicate()
