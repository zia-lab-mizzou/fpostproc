#!/bin/awk -f

# Scan a LAMMPS dump file, and just output columns with ...
#   step xlo xhi ylo yhi zlo zhi xy xz yz

# NOTE: Will not insert a zero if the box is not tilted!
#       Does not work with non-triclinic boxes!

m==1               { printf "%d ",$1; m=0 }
/ITEM: TIMESTEP/   { m=1 }

n>0&&n<4           { printf "%f %f %f ",$1,$2,$3; n+=1 }
n==4               { printf "\n"; n=0 }
/ITEM: BOX BOUNDS/ { n=1 }
