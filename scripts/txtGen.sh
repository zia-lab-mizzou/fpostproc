#!/bin/bash

get_box_16.sh *.lammpstrj

echo "ITEM: paths tstart tend tinc" > paths.txt
index_dumps.sh -f `ls -v *.lammpstrj` > list
awk '{sub(/lammpstrj/, "h5");print}' list >> paths.txt
rm list

python ~/src/fpostproc/scripts/particleGen.py `ls *.lammpstrj | grep "\.1\.1\."`
