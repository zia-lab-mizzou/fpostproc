#!/bin/bash

# Note: requires relatively modern version of gnuplot to work (looping)?

n=0
for f in "$@"; do
    gnuplot << EOF
set term postscript eps enhanced font "Times"
set output "$f.ps"
set size 1,1
set origin 0,0
set xrange[-10:10]
set yrange[0:1]
set xlabel "{/Symbol D}{/Times-Italic N}_c" font "Times,18"
set ylabel "{/Times-Italic p}({/Symbol D}{/Times-Italic N}_c)" font "Times,18"
set grid
set key center tmargin samplen 0
set style fill solid 1.0 noborder
cswitch(l) = l>16?3:(l<16?1:2)
set style line 1 lt 1 lc rgb "#DC143C"
set style line 2 lt 1 lc rgb "black"
set style line 3 lt 1 lc rgb "#006400"
set multiplot layout 3,3 rowsfirst scale 1,1
zeroplot(f,x,t) = sprintf("plot '%s' u (column(0)-16):%d:(0.6):(cswitch(column(0))) w boxes title '%s' lc variable",f,x+1,t)
eval zeroplot("$f",0,"0")
eval zeroplot("$f",1,"1")
eval zeroplot("$f",2,"2")
eval zeroplot("$f",3,"3")
eval zeroplot("$f",4,"4")
eval zeroplot("$f",5,"5")
eval zeroplot("$f",6,"6+")
set xrange[-1:13]
set yrange[0:0.2]
set xlabel "{/Times-Italic N}_c" font "Times,18"
set ylabel "{/Times-Italic p}({/Times-Italic N}_c)" font "Times,18"
plot "nc_dist" u 1:2:(0.6) w boxes title "{/Times-Italic N}_c distribution" lc 3
unset multiplot
EOF
    n=$(($n+1))
done
