#!/bin/env bash

param=$1
looptotal=$2

mkdir -p input/actual
mv input/$param input/actual/$param
awk -v x=$looptotal \
    ' /^variable looptotal equal/ {print $1,$2,$3,x}
     !/^variable looptotal equal/' input/actual/$param > input/$param
