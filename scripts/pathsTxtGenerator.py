import sys
import subprocess
import time
import os
import math

current_dir = sys.path[0]

if len(sys.argv) < 5:
    print('Usage:')
    print(" python pathsTxtGenerator.py [t_start] [t_end] [t_inc] [stem] [snaps per file")
    sys.exit(0)

## Define Parameters here
file_prefix = 'high-res.1.'
file_ext = '.h5'

t_start = int(sys.argv[1])
t_end = int(sys.argv[2])
t_inc = int(sys.argv[3])
file_prefix = sys.argv[4]
snaps_per_file = int(sys.argv[5])
#t_start = 1000000
#t_end = 2000000
#t_inc = 100000

t_vec = range(t_start, t_end,snaps_per_file*t_inc)
## Write file
fout = open('paths.txt','w')
fout.write('ITEM: path tstart tend tinc\n')
counter = 1
for t_cur in t_vec:
    t_end = t_cur+snaps_per_file*t_inc
    cur_line = file_prefix + str(counter) + file_ext + ' ' + str(t_cur) + ' ' + str(t_end) + ' ' + str(t_inc) + '\n'

    fout.write(cur_line)
    counter = counter + 1
    
fout.close()
