# get_box using 16 procs

if [ $# -lt 1 ]; then
    echo "USAGE `basename $0` {dumplist}"
    exit 1
fi

# create a boxtxt file for each dump in parallel

i=0
for f in $@; do
    echo $f
    i=$(($i+1))
    get_box.sh $f > $f.boxtxt &
    if [ $i -eq 16 ]; then
	wait
	i=0
    fi
done

wait

# combine them and print to a gb16 file

rm -f gb16
for f in $@; do
    cat $f.boxtxt >> gb16
    rm $f.boxtxt
done
sort -n gb16 > gb16.sort

# for old quescent gels, need xy, xz, yz info
awk '{print $1,$2,$3,"0",$4,$5,"0",$6,$7,"0"}' gb16.sort > gb16.triclinic

# for new runs, no modification needed
#cat gb16.sort > gb16.triclinic

echo ITEM: step xlo_bound xhi_bound xy ylo_bound yhi_bound xz zlo_bound zhi_bound yz > box.txt

# prune the gb16 file and output to box.txt

awk 'NR==1 {print; s=$1}; NR>1&&$1>s {print; s=$1}' gb16.triclinic >> box.txt

# remove all boxtxt files and the gb16

rm -f *.boxtxt gb1*
