#!/usr/bin/env python

# updateParam.py
#
# AUTHOR:       Ben Landrum
# LAST UPDATED: December 11, 2012
# PURPOSE:      Update a param file using command-line input
# INPUT:        param.*.i, where i is an integer
# OUTPUT:       param.*.(i+1)

# Structure of target param file might not be same as current param structure,
# so must go through every variable in that file
# Can hit enter to keep the current value

import os, sys

if len(sys.argv) != 2:
    print "USAGE: updateParam.py [param_old_str]"
    sys.exit()

# Assume form param.shear.3, with trailing index
# Update this index
param_old_str = sys.argv[1]
words = os.path.basename(param_old_str).split(".")
str_without_index = ".".join(words[0:-1])
index_old = int(words[-1])
index_new = index_old + 1
param_new_str = os.path.dirname(os.path.abspath(param_old_str)) + "/" + \
    str_without_index + "." + str(index_new)
print param_new_str

param_old = open(param_old_str, "r")
param_new = open(param_new_str, "w")
for line in param_old:
    words = line.split()
    if len(words) == 0:
        param_new.write(line)
        continue
    if words[0] == "#":
        param_new.write(line)
        continue
    val = raw_input("%s %s ? (orig %s): " % (words[1], words[2], words[3]))
    if val == "":
        val = words[3]
    param_new.write("%s %s %s %s\n" % (words[0], words[1], words[2], val))

param_old.close()
param_new.close()
