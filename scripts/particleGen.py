import sys
import subprocess
import time
import os
import math
import numpy as np

current_dir = sys.path[0]

if len(sys.argv) < 2:
    print("Usage:")
    print("  python particleTxtGeneratorPoly.py [file_to_read]")
    sys.exit(0)

## Define Parameters here

file_to_read = sys.argv[1]
## Write file


#Open file to read (lammpstrj)
trj_read = open(file_to_read, 'r')

for i in range(3):
    trj_read.readline()

line = trj_read.readline()
num_particles = int(line)

fout = open('particle.txt','w')
fout.write('ITEM: NUMBER OF PARTICLES\n')
fout.write(str(num_particles)+'\n')

fout.write('ITEM: NUMBER OF TYPES\n')
num_types = 5
fout.write(str(num_types)+'\n')

fout.write('ITEM: SIZES IN TYPE ORDER\n')
#size_types = [0.65, 0.825, 1.00, 1.175, 1.35]
#size_types = [0.70, 0.85, 1.00, 1.15, 1.30]
size_types = [0.90, 0.95, 1.00, 1.05, 1.10]
#size_types = [1.0, 1.0, 1.0, 1.0, 1.0]
for part_size in size_types:
    fout.write(str(part_size) + '\n')
# for i in range(3):
#     print (trj_read.readline(),end='')
# # Read out header
# for i in xrange(9):
#     print trj_read.readline()

#Start writing
for i in range(5):
    trj_read.readline()
fout.write('ITEM: LIST OF TYPES IN LAMMPS ATOM ORDER\n')

atoms = np.zeros((num_particles, 2), dtype = int)
for i in range(num_particles):
    line = trj_read.readline()
    words = line.split()
    atoms[i][0] = int(words[0])
    atoms[i][1] = int(words[1])

atoms = atoms[atoms[:,0].argsort()]

for i in range(num_particles):
    # fout.write(str(atoms[i][0]) + ' ' + str(atoms[i][1]) + '\n')
    fout.write(str(atoms[i][1]) + '\n')
 

fout.close()
