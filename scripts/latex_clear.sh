#!/usr/bin/env sh

rm -rf *.aux *.log *.bbl *.blg *-blx.bib *.log *.out *.glg *.glo *.gls *.glsdefs *.ist *.lof *.lot *.xdv *.xml *.synctex.gz *.bcf *.toc *.html auto
