#!/bin/bash

for i in {1..128}
do
    cd $i
    echo $i
    cd dump
    for f in $(find ./ -newermt "2024-04-23" ! -newermt "2024-04-27")
    do
        #echo $f
        rm $f
    done
    cd ..
    cd ..
done
