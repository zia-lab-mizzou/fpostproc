#!/bin/sh

# Extract thermo data from LAMMPS log
# log_extract_columns.bash [log] "thermo header" c1 c2 ...

if [ $# -lt 3 ]; then
    echo "usage: `basename $0` [log] \"thermo header\" c1 c2 ..."
    exit 1
fi

f=$1
phrase=$2
columns=$3

# Turn columns list into a string listing awk fields

i=0
for var in "$@"
do
    if [[ $i -gt 2 ]]; then
	columns="${columns} $var"
    fi
    i=$(($i+1))
done
columns_awk="\$`echo $columns | sed 's/\ /,\\\$/g'`"

awk "/Loop time of/ { sw=0 }
    sw==1           { print $columns_awk }
    /$phrase/       { sw=1 }" $f
