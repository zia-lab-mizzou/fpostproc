#!/bin/sh

# index_dumps.sh --- Go thru input list of dumps and index timesteps
# Author: Ben Landrum
# index_dumps.sh [dump list]
# Will provide errors if there are missing snapshots
# Assumes equal spacing in all dump files

# See if using first step only (not checking last step)

if [ "$1" = "-f" ]; then
    first_only=1
    shift
else
    first_only=0
fi

# Inspect the first dump for dt

first_step=`awk 'NR==2 {print; exit}' $1`
natoms=`awk 'NR==4 {print; exit}' $1`
second_step_line=$((11 + $natoms))
second_step=`awk -v line=$second_step_line 'NR==line {print; exit}' $1`
dt=$(($second_step - $first_step))

# Inspect all dumps for first and last steps

for dump in $@
do
    awk 'NR==2 {print; exit}' $dump >> INDEX_DUMPS_FIRST
    if [ "$first_only" -eq "0" ]; then
	tail -n $((9 + $natoms)) $dump | \
	    awk 'NR==2 {print; exit}' >> INDEX_DUMPS_LAST
    fi
done

# Generate the index

n=0
for dump in $@; do
    n=$(($n+1))
    printf "%s " "$dump"
    awk -v n=$n 'NR==n {printf "%d ",$1}' INDEX_DUMPS_FIRST

    if [ "$first_only" -eq "0" ]; then

	last_step=`awk -v L=$n 'NR==L {print}' INDEX_DUMPS_LAST`

        # Check if we're not on the last dump
        # If we're not, then see if the next first step is before the
        #   current last step, and use that to set the last step in the dump.
        # If the next first step is well after the current last step, we're
        #   missing stuff.

	if [ "$n" -lt "$#" ]; then
	    next_first_step=`awk -v L=$(($n+1)) 'NR==L {print}' \
		INDEX_DUMPS_FIRST`
	    if [ "$next_first_step" -lt "$last_step" ]; then
		last_step_used=$(($next_first_step - $dt))
	    else
		if [ "$(($next_first_step-$last_step))" -ne "$dt" ]; then
		    echo Disagreement between last and next step
		    echo Last is $last_step
		    echo Next first is $next_first_step
		    echo dt is $dt
		    rm INDEX_DUMPS_FIRST INDEX_DUMPS_LAST
		    exit 1
		fi
		last_step_used=$last_step
	    fi
	    printf "%s " "$last_step_used"
	else
	    printf "%s " "$last_step"
	fi
    else

	# Using first step only
	# Use next dump's first step to set this dump's last step
	# If there is no next dump, use a tail

	if [ "$n" -lt "$#" ]; then
	    next_first_step=`awk -v L=$(($n+1)) 'NR==L {print}' \
		INDEX_DUMPS_FIRST`
	    last_step_used=$(($next_first_step - $dt))
	    printf "%s " "$last_step_used"
	else
	    last_step=`tail -n $((9 + $natoms)) $dump | \
		awk 'NR==2 {print; exit}'`
	    printf "%s " "$last_step"
	fi
    fi
    printf "%s\n" "$dt"
done

# Clean up

rm -f INDEX_DUMPS_FIRST INDEX_DUMPS_LAST
