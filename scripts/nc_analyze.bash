#!/bin/env bash

# Analyze a file that has a time-series of nc-binned values
# One column per Nc
# Assume the first is Nc=0, and that there are up to 18 usable columns
# IMPORTANT: Don't use on files with a header ... need just data in columns

# Output is a series of mean values followed by the standard error for those

# Using the corrected sample standard deviation for the stdev



if [ $# -ne 1 ]
then
    echo "Usage: `basename $0` [input_file]"
    exit $E_BADARGS
fi

input=$1

# Get the number of columns from the first row
num_col=`awk 'NR==1 {print NF; exit}' $input`

# Get array of mean values
mean_str=`awk '     {for (i=1;i<=NF;i++) x[i]+=$i; n+=1} 
               END  {for (i=1;i<=NF;i++) printf "%14g", x[i]/n; \
		     printf "\n"}' $input`
mean_array=($mean_str)

# Get array of estimates of standard error of the mean (SEM)
for (( i=0; i < $num_col; i++ )); do
    sem=`awk -v m=${mean_array[$i]} -v j=$(($i+1)) \
             '    {sqdiff=($j-m)^2; sqdiffsum+=sqdiff; n+=1}
              END {printf "%14g\n", sqrt(sqdiffsum/(n-1)/n)}' $input`
    sem_array[$i]=$sem
done

for (( i=0; i < $num_col; i++ )); do
    printf "%2s %12s %12s\n" \
	"$i" \
	"${mean_array[$i]}" \
	"${sem_array[$i]}"
done
