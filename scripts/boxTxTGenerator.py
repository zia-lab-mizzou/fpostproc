#NOTE: This script has to change when the box size is not constant!

import sys
import subprocess
import time
import os
import math
import linecache


current_dir = sys.path[0]

if len(sys.argv) < 4:
    print("Usage:")
    print("  python boxTxtGenerator.py [t_start] [t_end] [t_inc] [xyz_lo] [xyz_bound]")
    sys.exit(1)

t_start = int(sys.argv[1])
t_end = int(sys.argv[2])
t_inc = int(sys.argv[3])

#print(sys.argv[4])
#line_old = linecache.getline(sys.argv[4], 7)
#line = line_old.split(" ")
#print(line)
xlo = float(sys.argv[4])
ylo = xlo
zlo = xlo

xbound = float(sys.argv[5])
ybound = xbound
zbound = xbound
    
## Define Parameters here
file_prefix = 'high-res.1.'
file_ext = '.h5'
#t_start = 1000000
#t_end = 2000000
#t_inc = 100000
t_vec = range(t_start, t_end+t_inc, t_inc)

#xlo = 0.00
#ylo = 0.00
#zlo = 0.00
#xbound = 24.0
#ybound = 24.0
#zbound = 24.0
xy = 0.00
xz = 0.00
yz = 0.00

## Write file
fout = open('box.txt','w')
fout.write('ITEM: step xlo_bound xhi_bound xy ylo_bound yhi_bound xz zlo_bound zhi_bound yz\n')

for t_cur in t_vec:
    cur_line = str(t_cur)+' '+str(xlo)+' '+str(xbound)+' '+str(xy)+' '+str(ylo)+' '+str(ybound)+' '+str(xz)+' '+str(zlo)+' '+str(zbound)+' '+str(yz)+'\n'
    fout.write(cur_line)
    
fout.close()
