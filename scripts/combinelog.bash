#!/bin/bash
#
# combinelog.bash
#
# Combine series of log files into a master log
# Logs that overlap will be combined to favor the later log
# Warn if the timestep spacing is off

if [ $# -lt 3 ]
then
    echo "Usage: `basename $0` {start_str} {stop_str} {log1} {log2} ..."
    exit $E_BADARGS
fi

start_str=$1
stop_str=$2
shift 2

# Prepare files to write to

out1=COMBINELOG_OUTPUT_1
out2=COMBINELOG_OUTPUT_2

# Loop through list of files

counter=0
for log in "$@"
do
    counter=$(($counter+1))

    # If there is a later file in the list, get its first timestep
    # If not, make the next timestep negative

    if [ "$counter" -lt "$#" ]; then
	counter_plus=$(($counter + 1))
	next_log=${!counter_plus}
	next_start=`awk -v rx=$start_str \
	    '$0 ~ rx {getline; print $1; exit}' $next_log`
    else
	next_start=-1
    fi

    # Step signals the thermo part of the log, and Loop signals the end
    # There may be multiple thermo sections in the same log
    # We will read a line if it's less than the first step of the next log
    # We will also read a line if there is no following log

    awk -v ns=$next_start -v x=$start_str -v y=$stop_str \
	'$0~y        { yesprint = 0 }
         yesprint==1 { if ($1<ns||ns==-1) print }
         $0~x        { yesprint = 1 }' $log >> $out1
done

# Get dt from the first output file (from the first log file)
# Have to check difference between 3rd and 2nd thermo output lines
# First timestep might not be on the normal output schedule

dt=`awk 'NR==2 { t2 = $1 }; NR==3 { t3 = $1 }; END { print t3-t2 }' $out1`

# Filter out timesteps that aren't multiples of this dt

awk -v dt=$dt '($1%dt)==0 { print }' $out1 > $out2

# Check to see that we have all equally spaced timesteps in our master log
# Print output

awk -v dt=$dt \
    'NR>1 { t2 = $1; if (t2-t1!=dt) {print "Bad spacing @"t2; exit} }
          { print; t1 = $1 }' $out2

# Clean up

rm $out1 $out2
