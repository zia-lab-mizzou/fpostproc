#!/usr/bin/env python2.7
# Input argument: python over_histo.py [filenames]*4 Title [subplot titles]*4
# Offset plot hash

import sys                  #import system-specific parameters and functions
import numpy                                      #import the numpy module
import matplotlib as mpl                          #import matplotlib
import matplotlib.pyplot as plt                   #import pyplot library
from matplotlib.backends.backend_pdf import PdfPages #map python output to PDF syntax, particulary to a single plot
#import numpy.ma as ma #import masking package of numpy

# Changes the font to Times New Roman
from matplotlib import rcParams
mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Times New Roman'

# Adjust tick mark size
mpl.rcParams['xtick.major.width'] = 1
mpl.rcParams['xtick.minor.width'] = 1
mpl.rcParams['ytick.major.width'] = 1
mpl.rcParams['ytick.minor.width'] = 1

# CHANGE INPUT ARGUMENTS HERE
files = [sys.argv[i+1] for i in range(8)]
title = sys.argv[9]

nc_list = [0,1,2,3]

# Using matplotlibrc but adjusting the label size
mpl.rcParams['figure.figsize'] = 5, 6
fig = plt.figure()
mpl.rcParams['axes.labelsize'] = 16.0


fontprop = mpl.font_manager.FontProperties(size=20.0)
plt.figtext(0.55, 0.02, "Contact number, $N_c$",
            horizontalalignment="center",
            fontproperties=fontprop)
plt.figtext(0.04, 0.5, "$p(N_c(\Delta t) | N_c(0))$", verticalalignment="center", fontproperties=fontprop, rotation='vertical')

fontprop = mpl.font_manager.FontProperties(size=16.0)
plt.figtext(0.27, 0.93, '$V_0/kT=5$', verticalalignment='center', fontproperties=fontprop)
plt.figtext(0.66, 0.93, '$V_0/kT=6$', verticalalignment='center', fontproperties=fontprop)


plt.figtext(0.82, 0.83, '0.4k', horizontalalignment="left", fontproperties=fontprop)
plt.figtext(0.82, 0.63, '4k', horizontalalignment="left", fontproperties=fontprop)
plt.figtext(0.82, 0.42, '40k', horizontalalignment="left", fontproperties=fontprop)
plt.figtext(0.82, 0.22, '400k', horizontalalignment="left", fontproperties=fontprop)
plt.figtext(0.42, 0.83, '0.4k', horizontalalignment="left", fontproperties=fontprop)
plt.figtext(0.42, 0.63, '4k', horizontalalignment="left", fontproperties=fontprop)
plt.figtext(0.42, 0.42, '40k', horizontalalignment="left", fontproperties=fontprop)
plt.figtext(0.42, 0.22, '200k', horizontalalignment="left", fontproperties=fontprop)



# Subplot indices and space at bottom
fig.subplots_adjust(left=0.18)
fig.subplots_adjust(right=0.95)
fig.subplots_adjust(bottom=0.12)
fig.subplots_adjust(top=0.9)
fig.subplots_adjust(wspace=0.1)
inds = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]

# Hatches and colors for bars in histogram
hatch = ["////", "....", "----", "OO"]
fill = [False, False, False, False]
fc = ["silver", "b", "r", "g", "silver"]
ec = ["k", "b", "r", "g" ]
alpha = [1,1,1,1,1]

i = 0
for f in files:

    # Read data for this subplot
    data = numpy.genfromtxt(f, dtype=float)        
    col = []                                       

    for j in range(len(nc_list)):
        col.append([row[nc_list[j]] for row in data])

    # Default subplot settings
    ind = inds[i]
    # CHANGE NUMBER OF SUBPLOTS
    sp = fig.add_subplot(4,2,ind, axisbg='w') 
    sp.axes.set_xlim(-1,11)
    sp.axes.set_ylim(0,1)
    for tick in sp.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)

    # CHANGE Y LABELS AND TICK MARKS TO APPEAR
    if (ind!=1) and (ind!=3) and (ind!=5) and (ind!=7):
        sp.axes.yaxis.set_ticklabels([])

    # CHANGE X LABELS TO APPEAR
    if (ind<7):
        sp.axes.xaxis.set_ticklabels([])
    
    # CHANGE TITLES ABOVE THE SUB PLOTS
    # Set titles
    sp.set_title(" ")
    
    for j in range(len(col)):
        xval = [x-0.5 for x in range(len(col[j]))]
        sp.bar(xval, col[j], fill=fill[j], edgecolor=ec[j], facecolor=fc[j],
               alpha=alpha[j], hatch=hatch[j])


#    for k in range(0,16):
#        xval_n = k - 0.5;
#        for r in range(len(col)):
#            if (r==0):
#                tb_vals = numpy.array([r,col[r][k]])
#            else:
#                tb_vals = numpy.vstack((tb_vals,[r,col[r][k]]))
#        vals = tb_vals[tb_vals[:,1].argsort()][::-1]
#        for v in range(len(vals)):
#            sp.bar(xval_n, vals[v,1], fill=fill[int(vals[v,0])], edgecolor=ec[int(vals[v,0])], facecolor=fc[int(vals[v,0])], alpha=alpha[int(vals[v,0])], hatch=hatch[int(vals[v,0])])

#        tb_vals = numpy.array([])
    
    i += 1

plt.savefig(title + ".png")
#plt.show()
#pp = PdfPages('multiplot.pdf')
#pp.savefig()
