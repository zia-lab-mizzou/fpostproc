#!/bin/env bash

# get_box.sh
# read a lammpstrj and print to screen the timestep and box dims

if [ $# -ne 1 ]; then
    echo "USAGE `basename $0` {dump}"
    exit 1
fi

dump=$1

# get the (assumed constant) # of atoms from the top of the file
natoms=`head $dump | awk '/ITEM: NUMBER OF ATOMS/ {getline; print}'`
snap_nlines=$(($natoms + 9))

# output the lines in the header of each snapshot
awk -v snl=$snap_nlines '(NR-1)%snl==0 {getline; printf "%s ", $0;
                                        getline; getline; getline;
                                        getline; printf "%s ", $0;
                                        getline; printf "%s ", $0;
                                        getline; printf "%s\n", $0}' $dump
