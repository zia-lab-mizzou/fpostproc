#!/bin/sh

# April 25, 2013
#
# Taking 4-column, sorted lists of "ID xu yu zu", find distance between atoms
# in the two files.  This is useful for seeing if atoms magically warped.
# It prints a list of the top 10 n ($3) atoms that moved the furthest.

awk -v f2=$2 '{ x1 = $2; \
                y1 = $3; \
                z1 = $4; \
                getline < f2; \
                x2 = $2; \
                y2 = $3; \
                z2 = $4; \
                dx = x1 - x2; \
                dy = y1 - y2; \
                dz = z1 - z2; \
                x2 = dx * dx; \
                y2 = dy * dy; \
                z2 = dz * dz; \
                dist = sqrt(x2 + y2 + z2); \
                printf("%d %10.8f\n",NR,dist) }' $1 \
    | sort -n -k2 -r | head -n $3
