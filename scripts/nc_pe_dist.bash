#!/bin/env bash

# nc_pe_dist.bash

if [ $# -ne 2 ]; then
    echo 'USAGE: nc_pe_dist.bash [path] [rsep]'
    exit 1
fi

path=$1
rsep=$2

# Check nc and pe at last timestep in h5 directory

ttarg=`tail -n 1 $path/paths.txt | awk '{print $3}'`
tinc=`tail -n 1 $path/paths.txt | awk '{print $4}'`

list_pe $path $ttarg > CHECK_NC_PE_PRE
list_nc $path $ttarg 0 $tinc $rsep > CHECK_NC_NC_PRE

# Combine output to make one file

awk '{print $2}' CHECK_NC_PE_PRE > CHECK_NC_PE
awk '{print $2}' CHECK_NC_NC_PRE > CHECK_NC_NC
paste CHECK_NC_NC CHECK_NC_PE > CHECK_NC_MASTER

# Show Nc=0..16

for i in {0..16}; do
    awk -v nc=$i '$1==nc {sum+=$2; n+=1}
                     END {if (n>0) printf "%d %g %g\n", nc, n/NR, sum/n;
                          else printf "%d 0 0\n", nc}' \
			 CHECK_NC_MASTER
done

rm -f CHECK_NC_PE CHECK_NC_PE_PRE CHECK_NC_NC CHECK_NC_NC_PRE CHECK_NC_MASTER
