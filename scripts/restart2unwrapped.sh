#!/bin/sh

# April 24, 2013
#
# Use restart2data, awk and sort to output unwrapped coords from restart

restart2data $1 LAMMPSDATA

awk 'NR==3                  { natoms = $1 }
     NR==7                  { lx = $2 - $1 }
     NR==8                  { ly = $2 - $1 }
     NR==9                  { lz = $2 - $1 }
     /Atoms/                { first = NR + 2; \
                              last = NR + 2 + natoms - 1 }
     NR>=first&&NR<=last { xu = $3 + $6 * lx; \
                           yu = $4 + $7 * ly; \
                           zu = $5 + $8 * lz; \
                           printf("%d %10.8f %10.8f %10.8f\n",$1,xu,yu,zu) }' \
	 LAMMPSDATA | sort -n -k1 > restart.coords
