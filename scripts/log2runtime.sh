#!/bin/sh
#
# log2runtime.sh
#
# Output avg hours per 10 million steps from sequence of LAMMPS log files
# Points are averaged from all 'Loop time' lines in each file
# x-axis is the number of the file in the input list

# Leaf through the log files in the command line
# Getting proc number from last in file

for log in "$@"
do
awk '/Loop time of/ { time = $4; \
                      procs = $6; \
                      steps = $9; \
                      time_tot += time * 10000000 / steps; \
                      n += 1 }
     END { print time_tot / n / 3600, procs }' $log
done
