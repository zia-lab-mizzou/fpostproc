#!/usr/bin/env python2.7

# Offset plot hash

import sys
import numpy
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

# Figure settings: files and target Nc
#files = ["0.nc_flow",
#         "6kT-0.1k-9990.nc_flow",
#         "6kT-0.1k-99900.nc_flow",
#         "6kT-0.1k-999000.nc_flow"]

files = [sys.argv[i+1] for i in range(4)]
title = sys.argv[5]

titles = ["0",
          "0.4",
          "4",
          "40"]
nc_list = [0,1,2,3,12]

# Using matplotlibrc but adjusting the label size
fig = plt.figure()
mpl.rcParams['axes.labelsize'] = 16.0
fontprop = mpl.font_manager.FontProperties(size=16.0)
plt.figtext(0.5, 0.05, "Contact number, $N_c$",
            horizontalalignment="center",
            fontproperties=fontprop)

# Subplot indices and space at bottom
fig.subplots_adjust(bottom=0.2)
fig.subplots_adjust(top=0.9)
inds = [141,142,143,144]

# Hatches and colors for bars in histogram
hatch = [None, "///", "\\\\\\", "---", "..."]
fill = [True, False, False, False, False]
fc = ["#00ffff", None, None, None, None]
ec = ["black", "black", "blue", "red", "green"]
alpha = [0.5,1,1,1,1]

i = 0
for f in files:

    # Read data for this subplot
    data = numpy.genfromtxt(f, dtype=float)
    col = []
    for j in range(len(nc_list)):
        col.append([row[nc_list[j]] for row in data])

#    sp.axes.xaxis.set_ticklabels([])
#    sp.axes.set_xticks((0,2,4,6,8,10,12))

    # Default subplot settings
    ind = inds[i]
    sp = fig.add_subplot(ind, axisbg='w')
    sp.axes.set_xlim(-1,13)
    sp.axes.set_ylim(0,1)
    for tick in sp.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)

    # Set titles
    sp.set_title(titles[i])

    # Make only certain axes display text, improving legibility
    if (i==0):
        sp.axes.set_ylabel("$p(N_c(\Delta t) | N_c(0))$")
    else:
        sp.axes.yaxis.set_ticklabels([])

    # Create this subplot's bars
    # Offsetting to line up bar center with xtick
    for j in range(len(col)):
        xval = [x-0.5 for x in range(len(col[j]))]
        sp.bar(xval, col[j], fill=fill[j], hatch=hatch[j], edgecolor=ec[j],
               facecolor=fc[j], alpha=alpha[j])
    i += 1

plt.savefig(title + ".pdf")
plt.show()
#pp = PdfPages('multiplot.pdf')
#pp.savefig()
