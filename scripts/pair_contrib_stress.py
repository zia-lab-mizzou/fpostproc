#!/usr/bin/env python2.7

# Take output from compute pair/local
# bin by the first output in column, the interparticle force
# compute the stress contribution for each pair force,
# sum these into two separate stress tensors
# normalize on the volume to return to pressure or stress units

import numpy as np
from scipy import interpolate
from scipy.optimize import curve_fit
import sys
from math import ceil, cos, exp, floor, pi, sin, sqrt
import pylab as pl


# Input format
# [name of py script] [filename] 

# Current set up: will call this for each file... 
# so that I will have a file: "[script name] [input file] \n [script name]..."

# Set box size
L = 125.419
vol = L*L*L

# Parse input
if (len(sys.argv) < 2):
    print "Usage pair_contrib_stress.py [file]"
    sys.exit()

infile = sys.argv[1]

# Initialize counters
count_attr = 0
count_rep = 0
count_other = 0

# Initialize stress variables
stress_a_xx = 0
stress_a_yy = 0
stress_a_zz = 0
stress_a_xy = 0
stress_a_xz = 0
stress_a_yz = 0

stress_r_xx = 0
stress_r_yy = 0
stress_r_zz = 0
stress_r_xy = 0
stress_r_xz = 0
stress_r_yz = 0

# Initialize Kahan summation variables
#c_a_xx = 0.0
#c_r_xx = 0.0

# Obtain timestep
with open(infile, "r") as ifile:
    for line in ifile:
        if 'TIMESTEP' in line:
            ts = next(ifile, '').strip()
        if 'ENTRIES index' in line:
            for line in ifile:
                pair_info = line.split()
                p_info_array = np.array(map(float, pair_info))
                fx = p_info_array[2]
                fy = p_info_array[3]
                fz = p_info_array[4]
                delx = p_info_array[5]
                dely = p_info_array[6]
                delz = p_info_array[7]
                check_sign = np.sign(p_info_array[1])
                if check_sign < 0:
                    #print(p_info_array[1])
                    #print "attractive"
                    count_attr = count_attr + 1
                    # compute stress as fx*delx ... so on
                    stress_a_xx = stress_a_xx + fx*delx # fx * delx
                    # Kahan summation for a_xx component
                    #y_a_xx = fx*delx - c_a_xx
                    #t_a_xx = stress_a_xx + y_a_xx
                    #c_a_xx = (t_a_xx - stress_a_xx) - y_a_xx
                    #stress_a_xx= t_a_xx
                    stress_a_yy = stress_a_yy + fy*dely # fy * dely
                    stress_a_zz = stress_a_zz + fz*delz # fz * delz
                    stress_a_xy = stress_a_xy + fx*dely # fx * dely
                    stress_a_xz = stress_a_xz + fx*delz # fx * delz
                    stress_a_yz = stress_a_yz + fy*delz # fy * delz
                elif check_sign > 0:
                    #print(p_info_array[1])
                    #print "repulsive"
                    count_rep = count_rep + 1
                    # compute stress as fx*delx ... so on
                    stress_r_xx = stress_r_xx + fx*delx # fx * delx
                    # Kahan summation for a_xx component
                    #y_r_xx = fx*delx - c_r_xx
                    #t_r_xx = stress_r_xx + y_r_xx
                    #c_r_xx = (t_r_xx - stress_r_xx) - y_r_xx
                    #stress_r_xx= t_r_xx
                    stress_r_yy = stress_r_yy + fy*dely # fy * dely
                    stress_r_zz = stress_r_zz + fz*delz # fz * delz
                    stress_r_xy = stress_r_xy + fx*dely # fx * dely
                    stress_r_xz = stress_r_xz + fx*delz # fx * delz
                    stress_r_yz = stress_r_yz + fy*delz # fy * delz 
                else:
                    count_other = count_other + 1
                        

# Average on volume
stress_a_xx = stress_a_xx/vol
stress_a_yy = stress_a_yy/vol
stress_a_zz = stress_a_zz/vol
stress_a_xy = stress_a_xy/vol
stress_a_xz = stress_a_xz/vol
stress_a_yz = stress_a_yz/vol

stress_r_xx = stress_r_xx/vol
stress_r_yy = stress_r_yy/vol
stress_r_zz = stress_r_zz/vol
stress_r_xy = stress_r_xy/vol
stress_r_xz = stress_r_xz/vol
stress_r_yz = stress_r_yz/vol


#print ('ts = {0}, attr number: {1}, rep number: {2}, other: {3}'.format(ts,count_attr,count_rep,count_other))
#print(stress_a_xx, stress_a_yy, stress_a_zz, stress_a_xy, stress_a_xz, stress_a_yz, stress_r_xx, stress_r_yy, stress_r_zz, stress_r_xy, stress_r_xz, stress_r_yz)

# Nicer format
print('{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}').format(ts, stress_a_xx, stress_a_yy, stress_a_zz, stress_a_xy, stress_a_xz, stress_a_yz, stress_r_xx, stress_r_yy, stress_r_zz, stress_r_xy, stress_r_xz, stress_r_yz, count_attr, count_rep)

# Output
# Desired format: timestep [6 comps of attractive stress tensor], 
#     [6 comps of repulsive stress tensor]

