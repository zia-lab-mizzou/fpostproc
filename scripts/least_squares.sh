#!/bin/env bash

f=$1

awk '    { x[NR] = $1; y[NR] = $2;\
           sx += x[NR]; sy += y[NR];\
           sxx += x[NR]*x[NR];\
           sxy += x[NR]*y[NR]; }
     END { det = NR*sxx - sx*sx;\
           a = (NR*sxy - sx*sy)/det;\
           b = (-sx*sxy+sxx*sy)/det;\
           print a,b }' $f
