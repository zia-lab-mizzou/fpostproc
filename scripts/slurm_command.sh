#!/usr/bin/env bash

# If no EMAILJOB variable defined, does not let you know when job
# launches or completes.

if [ $# -ne 3 ]; then
    echo "usage `basename $0` [command] [job_name] [time]"
    exit 1
fi

# Defaults

Aflag='-A TG-CTS130035'
nflag='-n 16'
pflag='-p normal'
sscript=SLURM_COMMAND_TEMPFILE
logfile=sc_log

# Get input variables from command line

cmd_line=$1
job_name=$2
time_limit=$3
jflag="-J $job_name"
tflag="-t $time_limit"
if [ -n "$EMAILJOB" ]; then
    Mflag="--mail-user=$EMAILJOB --mail-type=begin --mail-type=end"
fi
slurm_line="$Aflag $nflag $tflag $pflag $jflag $Mflag"

# Create the temporary script file
echo '#!/usr/bin/env bash' > $sscript
echo $cmd_line >> $sscript

# Submit it

sbatch $slurm_line $sscript

# Continue job submission log file

echo $slurm_line >> $logfile
echo $cmd_line >> $logfile

# Remove temporary script file

rm $sscript
