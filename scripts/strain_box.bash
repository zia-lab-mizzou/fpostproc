#!/bin/env bash

if [ $# -lt 3 ]; then
    echo Usage: `basename $0` [box.txt_file] [plane] [target]
    exit 1
fi

file=$1
plane=$2
target=$3

# step xlo_bound xhi_bound xy ylo_bound yhi_bound xz zlo_bound zhi_bound yz

if [ "$plane" == "xy" ]; then
    c=4
    d=5
elif [ "$plane" == "xz" ]; then
    c=7
    d=8
elif [ "$plane" == "yz" ]; then
    c=10
    d=8
else
    echo Plane must be xy, xz, or yz
    exit 1
fi

awk -v c=$c -v d=$d -v t=$target 'NR>1 { s = $c/($(d+1) - $d);
                                         if (s-ls < -0.5) m+=1;
                                         if (s-ls > 0.5) m-=1;
                                         if ((s+m-t)/t > 0) { n=$1; p=1; exit};
                                         ls = s }
                                  END  { if (p==1) print n, s+m }' $file
