#!/bin/sh

# April 25, 2013
#
# Use awk and sort to output sorted unwrapped coordinates
# Assuming dump file has xu, yu, zu in columns

awk 'NR==4              { natoms = $1 }
     NR>9&&NR<10+natoms { printf("%d %10.8f %10.8f %10.8f\n",$1,$3,$4,$5) }' \
	 $1 | sort -n -k1 > dump.coords
