#!/usr/bin/env python2

import numpy as np
from scipy import interpolate
from scipy.optimize import curve_fit
import sys
from math import ceil, cos, exp, floor, pi, sin, sqrt
import pylab as pl

class Data:
    """Holds time, stress and strain data and defines operations on it"""
    def __init__(self, filename, omega):
        fp = open(filename, "r")
        self.time = []
        self.strain = []
        self.stress = []
        for line in fp:
            words = line.split()
            self.time.append(float(words[0]))
            self.strain.append(float(words[1]))
            self.stress.append(float(words[2]))
        fp.close()

        # Calculate dt, and get omega from user (for now)
        self.dt = self.get_dt()
        self.omega = omega

    def get_amplitude(self):
        """Get the amplitude of the strain wave"""
        integrand = (self.strain-self.strain[0]) * \
                (self.strain-self.strain[0])
        sqmod = np.trapz(integrand,self.time) * \
            self.omega/(pi*self.num_per)
        self.amplitude = sqrt(sqmod)

    def get_dt(self):
        """Get the dt automatically from the file by averaging what's there"""
        t1 = self.time[0]
        t2 = self.time[-1]
        nsamp = len(self.time)
        return (t2-t1) / (nsamp-1)

    def get_moduli(self, cyc_start, cyc_end):
        """
        stress = -Asin(wt+phi)*Gprime - A*w*visc*cos(wt+phi)
        stress = -Asin(wt+phi)*G1 - Acos(wt+phi)*G2
        
        FT real part over A gives one modulus
        """
        w = self.omega
        t = self.time
        stress = self.stress
        N = cyc_end - cyc_start + 1
        A = self.amplitude
        ppp = self.points_per_period

        first_index = (cyc_start-1) * ppp
        last_index = cyc_end * ppp

        integrand = []
        for i in xrange(first_index, last_index+1):
            val = t[i]
            integrand.append(sin(w*val)*stress[i])
        G1 = -np.trapz(integrand,t[first_index:last_index+1])*w / (pi*N*A)
        integrand = []
        for i in xrange(first_index, last_index+1):
            val = t[i]
            integrand.append(cos(w*val)*stress[i])
        G2 = -np.trapz(integrand,t[first_index:last_index+1])*w / (pi*N*A)
        
        return (G1,G2)

    # def get_omega(self):
    #     """Estimate omega by Fourier transform of strain"""
    #     #transform = np.fft.rfft(self.strain).real
    #     #max_val = transform[:end_idx].max()
    #     #max_idx = np.where(transform[:end_idx].real == max_val)[0][0]

    #     # Estimate peak location from modulus of frequency spectrum
    #     transform = np.fft.rfft(self.strain)
    #     end_idx = len(transform)/2 + 1
    #     transformmod = abs(transform)[:end_idx]
    #     max_val = transformmod.max()
    #     max_idx = np.where(transformmod.real == max_val)[0][0]

    #     # Fit a Gaussian to the spectrum to get a refined frequency guess
    #     waxis = []
    #     for i in xrange(end_idx):
    #         waxis.append(2*pi/(len(self.strain)-1)/self.dt)
    #     def gauss_func(x, a, b, c):
    #         return a*x*exp(-0.5/c/c)
    #     popt, pcov = curve_fit(gauss_func, waxis, transformmod)

    #     print max_idx
    #     print end_idx

    #     pl.plot(transformmod)
    #     pl.show()

    #     print transformmod[max_idx-1]
    #     print transformmod[max_idx]
    #     print transformmod[max_idx+1]

    #     print max_idx * 2*pi / (len(self.strain)-1) / self.dt

    #     print "cheating omega=100"
    #     return 100.0
    #     #return max_idx * 2*pi / (len(self.strain)-1) / self.dt

    def resampled(self, cycle_start, cycle_stop):

        total_time = self.time[-1] - self.time[0]
        period = 2.0*pi / self.omega
        cycles_in_file = int(total_time / period)

        # Input error reporting
        # Set cycle stop
        if (cycle_start > cycles_in_file):
            print "Starting cycle bigger than number of cycles in file"
            sys.exit()
        if (cycle_stop != -1):
            if (cycle_stop < cycle_start):
                print "Final cycle less than first cycle"
                sys.exit()
            if (cycle_stop > cycles_in_file):
                print "Final cycle greater than number of cycles in file"
                sys.exit()
        else:
            cycle_stop = cycles_in_file

        # Get starting time and ending time, and store the number of periods
        start_time = self.time[0] + (cycle_start-1) * period
        end_time = self.time[0] + cycle_stop * period
        self.num_per = cycle_stop - cycle_start + 1
        print "start time = %f and end time = %f" % (start_time,end_time)

        # Set a new dt slightly smaller than the old dt for integer # in period
        points_per_period = int(period / self.dt) + 1
        dt_new = period / points_per_period

        # Interpolate based on regridded time list
        # NOTE: outputting final point (would be same as first for periodic)
        time_new = [dt_new*n + start_time \
                        for n in range(points_per_period*self.num_per + 1)]
        interp = interpolate.interp1d(self.time, self.strain, kind='linear')
        strain_new = interp(time_new)
        interp = interpolate.interp1d(self.time, self.stress, kind='linear')
        stress_new = interp(time_new)

        print "isnan stress_new, strain_new"

        if np.isnan(np.sum(strain_new)):
            print "nan in interpolated strain"
            sys.exit
        if np.isnan(np.sum(stress_new)):
            print "nan in interpolated stress"
            sys.exit

        self.time = time_new
        self.dt = dt_new
        self.strain = strain_new
        self.stress = stress_new
        self.points_per_period = points_per_period

# Parse input.
if (len(sys.argv) < 4):
    print "Usage moduli.py [file] [omega] [mode] ([val1] [val2] ...)"
    sys.exit()

infile = sys.argv[1]
omega = float(sys.argv[2])
mode = sys.argv[3]
if (mode == "all"):
    cycle_start = 1
    cycle_stop = -1
    step_inc = -1
elif (mode == "skip"):
    if (len(sys.argv) != 5):
        print "Usage moduli.py [file] [omega] skip [ncyc]"
        sys.exit()
    cycle_start = int(sys.argv[4]) + 1
    if (cycle_start < 1):
        print "error: starting cycle must be > 0"
        sys.exit()
    cycle_stop = -1
    step_inc = -1
elif (mode == "step"):
    if (len(sys.argv) != 5):
        print "Usage moduli.py [file] [omega] step [ncyc]"
        sys.exit()
    cycle_start = 1
    cycle_stop = -1
    step_inc = int(sys.argv[4])
elif (mode == "range"):
    if (len(sys.argv) != 6):
        print "Usage moduli.py [file] [omega] range [cyc_first] [cyc_last]"
        sys.exit()
    cycle_start = int(sys.argv[4])
    if (cycle_start < 1):
        print "error: first cycle must be > 0"
        sys.exit()
    cycle_stop = int(sys.argv[5])
    if (cycle_stop < cycle_start):
        print "error: last cycle must be >= first"
        sys.exit()
    step_inc = -1
elif (mode == "time"):
    if (len(sys.argv) != 5):
        print "Usage moduli.py [file] [omega] time [lmptime_within]"
        sys.exit()
    cycle_start = 1
    cycle_stop = int(float(sys.argv[4]) * omega / (2.0*pi))
    step_inc = -1
else:
    print "[mode] is all, skip, step, range, or time"
    print "all"
    print "skip [number]"
    print "step [every]"
    print "range [first] [last]"
    print "time [lmptime_within]"
    sys.exit()

# Load data.
data = Data(infile, omega)

print "orig dt = %g" % data.dt

# Put samples onto grid by interpolation,
# and get amplitude.
data.resampled(cycle_start, cycle_stop)
data.get_amplitude()

# Output values (input and interpreted) to screen.
omega = data.omega
amplitude = data.amplitude
num_per = data.num_per
dt = data.dt
print "omega = %g" % omega
print "amp = %f" % amplitude
print "num_per = %d" % num_per
print "dt = %g" % dt

# Get moduli, and print to screen.
if (step_inc == -1):
    step_inc = num_per

# TEMPORARILY CONVERTING
print "WARNING: dividing raw output by 8 to get in a rather than (2a) units"

for i in range(num_per/step_inc):
    j = i + 1
    k = j + step_inc - 1
    G1, G2 = data.get_moduli(j,k)
    G1 /= 8
    G2 /= 8
    print "%f %f" % (G1,G2)
