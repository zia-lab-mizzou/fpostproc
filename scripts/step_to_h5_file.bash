paths=$1
step=$2

awk -v s=$step 'NR>1&&s>=$2&&s<=$3 {print $1; exit}' $paths
