#!/usr/bin/env bash

# Script:       cstats.sh
# Author:       Ben Landrum
# Last updated: April 19, 2015
# Usage:        Return min, max, mean, standard deviation, and standard error
#               of a column of data

if [ $# -ne 2 ]; then
    echo "usage: `basename $0` [file] [column]"
    echo "output is min, max, mean, standard deviation, standard error"
    exit 1
fi

f=$1
c=$2

awk -v c=$c 'NR==1 { n=$c; x=$c; M=$c; S=0}
             NR>1  { Ml=M; M=M+($c-M)/NR; S=S+($c-Ml)*($c-M) }
             NR>1  { if ($c<n) n=$c; if ($c>x) x=$c }
             END   { print n, x, M, sqrt(S/(NR-1)), sqrt(S/(NR-1)/NR)}' $f
