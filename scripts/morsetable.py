#######################################################################
# MORSE POTENTIAL TABLE GENERATOR                                     #
#                                                                     #
# Purpose: Makes interaction tables for polydisperse Morse mixture.
#     The output is morse_alpha_D0.table, where alpha is the Morse
#     screening length and D0 is the minimum.  The potentials between
#     particles are named MORSE_x_y, where x is an integer from 1 to n
#     and y is an integer from x to n.  The polydispersity is just in the r0 value in the morse potential.  The screening length is unchanged.  However, I am also shifting the rlo and rhi values for the different particles and keeping the same number of points for each interaction.  The particle sizes are spaced uniformly.
# Author: Ben Landrum                            #
# Last updated: 12/27/2011                       #
##################################################

########################################
# 1. Input interaction parameters
########################################

#=======================================
# Potential amplitude and screening length
D0 = 6
alpha = 60

# Size distribution parameters
# number of sizes, min size, max size
n = 5
a = 0.9
b = 1.1

# Table settings
# number of points, min point, max point
N = 1000
rlo = 0.9
rhi = 1.3
#=======================================

########################################
# 2. Import functions
########################################

from math import exp,sqrt

########################################
# 3. Main part
########################################

f = open('./morse.table','w')

# Comments at top of morse.table
f.write('# Tabulated Morse interaction for LAMMPS\n')
f.write('#\n')
f.write('# Potential parameters:\n')
f.write('#   D0 = %d\n' % D0)
f.write('#   alpha = %d\n' % alpha)
f.write('#   n = %d\n' % n)
f.write('#   a = %f\n' % a)
f.write('#   b = %f\n' % b)

dr = (rhi-rlo)/(N-1)

# Loop over particle size pairs, but put
# all interactions in the same morse.table
# file

# Note: As written, implementing polydispersity by only shifting
#       x0 and the rlo and rhi for each table, the entries
#       for the forces and potentials are actually all the same,
#       just at different positions.  Thus, the loop actually does
#       redundant computations.  I am leaving it as is in case
#       I want to implement the polydispersity differently in the
#       future.
for i in range(0,n):
    iindex = i+1
    idiam = rlo + i*(b-a)/(n-1)
    for j in range(i,n):
        jindex = j+1
        jdiam = rlo + j*(b-a)/(n-1)
        r0 = (idiam+jdiam)/2
        rlos = rlo + (r0-(a+b)/2.0)
        rhis = rhi + (r0-(a+b)/2.0)
        f.write('\n')
        f.write('MORSE_%d_%d\n' % (iindex,jindex))
        f.write('N %i R %f %f\n' % (N,rlos,rhis))
        f.write('\n')
        for k in range(0,N): # This loop currently redundant but working
            kindex = k+1
            r = rlos + k*dr
            x = r-r0
            energy = D0*(exp(-2*alpha*x)-2*exp(-alpha*x))
            force = 2*alpha*D0*(exp(-2*alpha*x)-exp(-alpha*x))
            f.write('%i %.9e %.9e %.9e\n' % (kindex,r,energy,force))

f.close()
