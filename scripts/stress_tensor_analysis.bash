#!/bin/env bash

if [ $# -lt 3]; then
    echo "usage `basename $0` [file] [mode] [alpha]"
    exit 1
fi

set -e

file=$1
mode=$2
alpha=$3

# Modes to output principal stress values

if [ "$mode" == "pri" ]; then
elif [ "$mode" == "ijj" ]; then
fi
