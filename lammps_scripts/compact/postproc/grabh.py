import sys
sys.path.append('')
import os, shutil, numpy as np, glob
# code to read and sort all the h(t) data to text files so that we can export to Igor Pro. Also generate matplotlib plots for quick viewing.a
Lbox = 125.67
zcorr = 0.123471
hoft=[]
for ave in glob.glob('avespatial.*'):
  data = open(ave,'r')
  nbins = 0
  line = data.readlines()
  nlines = sum(1 for x in open(ave))
  c = 3
  while c < nlines:
    header = []
    header = line[c].split()
    nbins = int(header[1])
    time = 4*int(header[0])*0.00001
    height = 1
    for l in range(nbins):
      text = line[c+l+1].split()
      znormal = (float(text[1])+zcorr)/Lbox
      rho = float(text[3])*np.pi/6 # LAMMPS units 4*pi/3 over 8
      press = float(text[4])/8.0 
      if l == nbins-1: # correct for last bin because it is only 0.67 in volume as other bins
        rho = rho/0.67
      if rho < 0.01:
        height = znormal
        break
    hoft += [[time, height]]
    c += nbins+1
  data.close()
h = np.asarray(hoft)
dum, indices = np.unique(h[:,0],return_index=True)
h = h[indices,:]
h = h[h[:,0].argsort(),] # equivalent to a sortrows
outfile = open('hoft.dat','w')
for c in range(h.shape[0]):
  outfile.write('{0:12.6f} {1:8.6f}\n'.format(h[c][0],h[c][1]))
outfile.close()
