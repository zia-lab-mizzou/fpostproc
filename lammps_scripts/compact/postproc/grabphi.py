import sys
sys.path.append('')
import os, shutil, numpy as np, glob
# code to read and sort all the h(t) data to text files so that we can export to Igor Pro. Also generate matplotlib plots for quick viewing.a
timeout = int(sys.argv[1])
strinc = '' #sys.argv[2]
Lbox = 125.67
zcorr = 0.123471
for ave in glob.glob('avespatial.*'):
  data = open(ave,'r')
  nbins = 0
  line = data.readlines()
  nlines = sum(1 for x in open(ave))
  c = 3
  while c < nlines:
    header = []
    header = line[c].split()
    nbins = int(header[1])
    if int(header[0])%timeout == 0: # print if correct frequencya
      f = open('phipress'+str(4*int(header[0])/100000)+strinc+'.dat','w')
      
#      if header[0]==0:
#        print line[c], line[c+1]
      for l in range(nbins):
        text = line[c+l+1].split()
        znormal = (float(text[1])+zcorr)/Lbox
        rho = float(text[3])*np.pi/6 # LAMMPS units 4*pi/3 over 8
        press = float(text[4])/8.0 
        if l == nbins-1: # correct for last bin because it is only 0.67 in volume as other bins
          rho = rho/0.67
        f.write('{0:10.6f} {1:10.6f} {2:10.6f}\n'.format(znormal, rho, press))
      f.close()
    c += nbins+1
  data.close()
