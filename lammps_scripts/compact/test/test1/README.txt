November 28, 2015
Lily

This test is intended to look at the following system:
monodisperse particles
non-interacting
apply gravity, at different Pe settings
output velocity (averaged over every particle, not necessarily time averaged) in the direction of the applied gravity
