December 2, 2015
Lily

In the last test (test2) of compaction, I discovered that the damp parameter was being scaled by particle mass such that all particles sedimented with the same velocity even though the larger particles should sediment faster.  

To try to correct for this, this test will likely be abou the following system:
non interacting particles
polydispersity introduced by different relative masses
type 3 particle is the one that the system is non dimensionalized on
fix gravity is set to act on all particles (applied force is ~m)
fix langevin is applied with a different drag force to each particle type so that it goes like ~sigma instead of ~sigma^3 (so damp is a function of particle size and NOT mass)
