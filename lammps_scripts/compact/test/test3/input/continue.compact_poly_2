# continue.compact_poly_2
# Last updated December 2, 2015

include input/${param}

variable num_size equal 5 # sets number of particle sizes?

units lj
atom_style atomic
atom_modify sort 0 1 # may need for non-interacting particles
variable volume_prefactor equal PI/6
variable reduced_density equal ${vfrac}/${volume_prefactor}
lattice sc ${reduced_density}
region box block 0.0 $l 0.0 $l 0.0 $l
create_box ${num_size} box

# add particles
create_atoms 1 region box

# Replace type 1 atoms with types 2 through 5.
set type 1 type/fraction 2 0.2 11000
set type 1 type/fraction 3 0.25 10301
set type 1 type/fraction 4 0.333333 12002
set type 1 type/fraction 5 0.5 14443

group 1 type 1  # size in sigma: 0.90
group 2 type 2  # size in sigma: 0.95
group 3 type 3  # size in sigma: 1.00
group 4 type 4  # size in sigma: 1.05
group 5 type 5  # size in sigma: 1.10

# create variables for particle sizes
variable size1 equal 0.90
variable size2 equal 0.95
variable size3 equal 1.00
variable size4 equal 1.05
variable size5 equal 1.10

# set all masses to same density
mass 1 ${size1}^3
mass 2 ${size2}^3
mass 3 ${size3}^3
mass 4 ${size4}^3
mass 5 ${size5}^3
velocity all create 1 25252

# timestep information
variable timestep_LJ equal ${timestep}/${damp}
timestep ${timestep_LJ}
variable realtime_LJ equal dt*step
variable realtime equal v_realtime_LJ*${damp}

# pair styles
neighbor 1.0 bin
pair_style none

# Set fixes
fix nve all nve
#fix langevin all langevin ${temp} ${temp} ${damp} ${seed}
variable damp1 equal ${size1}^2*${damp}
variable damp2 equal ${size2}^2*${damp}
variable damp3 equal ${size3}^2*${damp}
variable damp4 equal ${size4}^2*${damp}
variable damp5 equal ${size5}^2*${damp}
fix langevin1 1 langevin ${temp} ${temp} ${damp1} ${seed}
fix langevin2 2 langevin ${temp} ${temp} ${damp2} ${seed}
fix langevin3 3 langevin ${temp} ${temp} ${damp3} ${seed}
fix langevin4 4 langevin ${temp} ${temp} ${damp4} ${seed}
fix langevin5 5 langevin ${temp} ${temp} ${damp5} ${seed}
variable gravity equal 8*PI*${peclet}/3
fix gravity all gravity ${gravity} vector 0 0 -1

# computes
compute compvel_1 1 reduce ave vz
compute compvel_2 2 reduce ave vz
compute compvel_3 3 reduce ave vz
compute compvel_4 4 reduce ave vz
compute compvel_5 5 reduce ave vz

# main run
thermo_style custom step v_realtime pxx pyy pzz pxy pxz pyz temp c_compvel_1 c_compvel_2 c_compvel_3 c_compvel_4 c_compvel_5
thermo ${thermo_increment}
run ${steps}