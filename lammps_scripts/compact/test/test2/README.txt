December 1, 2015
Lily

This test is intended to look at the following system:
polydisperse particles
same mass (not same density!!!!)
non-interacting particles
apply gravity by setting Pe, likely will use just Pe=1 or other
output particle velocity by particle type (averaged over all particles of same type) in the direction of gravity (z direction)


UPDATE:  PARTICLE DENSITY KEPT CONSTANT, NOT PARTICLE MASS
It does not make sense to perform a test where the particles are identified as different types but are still all non interacting (thus no enforcement of particle size, which in other scripts comes from the pair_coeff info).  So, this test WILL be testing particle of different masses.
