# include.shear
#
# Purpose:
#   Oscillate the LOADED system with given amplitude and frequency
#   Do this by remapping coordinates every timestep
#   Thermostat the system with the Langevin thermostat
#   
# Variables:
#   component        = desired shear plane: 4, 5, or 6 for xy, xz, or yz
#   A                = amplitude, in engineering strain
#   w                = angular frequency (omega), in reciprocal Brownian times
#   orig_restart     = restart file for oscillation time=0
#   restart.{xy,xz,yz}.* = most advanced restart file ... created if absent
#   interactions_file = file that describes the interactions
#   num_cycles       = # cycles to oscillate TO, from timestep of first restart
#   timestep         = timestep, in Brownian times
#   temp             = temperature, in LJ units
#   thermo_increment = # of _timesteps_ between thermos
#   dump_per_cycle   = # of times to write to dump file per cycle
#   index            = the index of this run within the series
#
# Output:
#   fix ave/time of Brownian time,strain,stress: output/stress.${cstr}.out
#   log: log/log.${cstr}.${index}
#   (${cstr} is the string for the shear plane: xy, xz or yz)
#   restart file approximately every oscillation cycle, and at the end
#   dump file
#
# Notes:
#   LAMMPS produces restarts and dumps on timestep multiples, that may
#   not line up with the starting/ending time for a run.
#   Thus, in general, the difference in timesteps between the first two
#   and last two outputs will not be the same as those in between.

variable name string shear

# Get first timestep (count oscillations from this point)
# Assuming timestep has not changed since this restart
read_restart input/${orig_restart}
variable first_step equal step
variable first_step equal ${first_step}
if ${component}==4 then &
  "variable tilt_orig equal xy" &
elif ${component}==5 &
  "variable tilt_orig equal xz" &
elif ${component}==6 &
  "variable tilt_orig equal yz" 
variable displace_orig equal ${tilt_orig}
clear

# Switch for oscillation plane
# 4=xy, 5=xz, 6=yz
# Switch variables named after relevant component of LAMMPS pressure vector
if ${component}==4 then &
  "variable cstr string 'xy'" &
  "variable A_dist equal ${A}*ly" &
  "variable strain equal xy/ly" &
  "variable tilt equal xy" &
elif ${component}==5 &
  "variable cstr string 'xz'" &
  "variable A_dist equal ${A}*lz" &
  "variable strain equal xz/lz" &
  "variable tilt equal xz" &
elif ${component}==6 &
  "variable cstr string 'yz'" &
  "variable A_dist equal ${A}*lz" &
  "variable strain equal yz/lz" &
  "variable tilt equal yz" &
else &
  "print 'Error: bad component'" &
  "quit"

# Copy original restart and rename to restart.{xy,xz,yz}.*
# This is overwritten during continuation or extension runs, but avoids errors
shell cp input/${orig_restart} restart/restart.${cstr}.${first_step}

# Read in the most advanced restart file in the desired direction
read_restart restart/restart.${cstr}.*

# Determine time integration parameters and variables
# IMPORTANT: timestep set here!
variable timestep_LJ equal ${timestep}/${damp}
timestep ${timestep_LJ}
variable step_since equal step-${first_step}
variable realtime_LJ equal dt*v_step_since
variable realtime equal v_realtime_LJ*${damp}

# Determine oscillation parameters and variables
variable w_LJ equal ${w}*${damp}
variable step_goal equal ceil(2*PI*${num_cycles}/${w_LJ}/dt+${first_step})
variable step_goal equal ${step_goal}
variable displace_0 equal ${tilt}

# Jump to end of this script (include.shear) if we already met our goal
variable current_step equal step
if ${current_step}>=${step_goal} then "jump input/include.shear skipped"

# Prepare system, and set fixes
change_box all triclinic
include input/${interactions_file}
fix nve all nve
fix langevin all langevin ${temp} ${temp} ${damp} ${seed}
variable displace equal ${A_dist}*sin(${w}*v_realtime)-v_displace_0+v_displace_orig
variable rate equal ${w_LJ}*${A_dist}*cos(${w}*v_realtime)
fix deform all deform 1 ${cstr} variable v_displace v_rate remap x units box

# Thermo output
thermo ${thermo_increment}
thermo_style custom step v_realtime temp press pe v_strain &
  c_thermo_press[${component}]

# Main rheological output
#fix rheo_out all ave/time 1 1 ${thermo_increment} v_step_since v_realtime v_strain c_thermo_press[1] c_thermo_press[2] c_thermo_press[3] c_thermo_press[4] c_thermo_press[5] c_thermo_press[6] file output/stress.${cstr}.out.${index}

# Dump
compute get_pe all pe/atom
compute get_stress all stress/atom NULL virial
variable main_options string "id type xu yu zu vx vy vz &
  c_get_pe c_get_stress[1] c_get_stress[2] c_get_stress[3] &
  c_get_stress[4] c_get_stress[5] c_get_stress[6]"

# Restart
restart ${restart_increment} restart/restart.${cstr}.*

# Sub-crun loop
variable a loop ${segs_per_crun}
label loop

  if ${current_step}>=${step_goal} then "jump input/include.shear skipped"

  log log/log.${cstr}.${index}.${a}
  dump main all custom ${dump_increment} &
    dump/${name}.${index}.${a}.lammpstrj ${main_options}

  variable step_diff equal ${step_goal}-${current_step}  
  if ${step_diff}<${restart_increment} then &
      "run ${step_goal} upto" &
  else &
      "run ${restart_increment}"

  undump main

  variable displace_0 equal ${tilt}

next a
jump input/include.shear loop

# Produce a restart file at the last timestep, used to detect completion
write_restart restart/restart.${cstr}.*

# From if statement above, jump here if we already reached our goal
label skipped
clear
