# include.remap
#
# Purpose:
#   Shift the system to (0,0,0)
#   For earlier cases which were run with "fix deform scale"
#      
# Variables:
#   orig_restart     = restart file for oscillation time=0
#   restart.{name}.* = most advanced restart file ... created if absent
#   index            = the index of this run within the series
#
# Output:
#   restart file after shifting; no particle movement
#
# Notes:
#   LAMMPS produces restarts and dumps on timestep multiples, that may
#   not line up with the starting/ending time for a run.
#   Thus, in general, the difference in timesteps between the first two
#   and last two outputs will not be the same as those in between.

variable name string glass_shift

# Get first timestep (count oscillations from this point)
# Assuming timestep has not changed since this restart
read_restart input/${orig_restart}
variable first_step equal step
variable first_step equal ${first_step}
variable lx0 equal lx
variable ly0 equal ly
variable lz0 equal lz
clear

# Copy original restart and rename to restart.{xy,xz,yz}.*
# This is overwritten during continuation or extension runs, but avoids errors
shell cp input/${orig_restart} restart/restart.${name}.${first_step}

# Read in the most advanced restart file in the desired direction
read_restart restart/restart.${name}.*

# Run 0 to set box to triclinic
atom_modify sort 0 0
run 0
# Move box, it is shifted in space due to fix deform
change_box all x final 0 ${lx0} y final 0 ${ly0} &
           z final 0 ${lz0} remap units box

# Write 0th dump file as a check
# Options for the main dump
compute get_pe all pe/atom
compute get_stress all stress/atom virial
variable main_options string "id type xu yu zu vx vy vz c_get_pe c_get_stress[1] c_get_stress[2] c_get_stress[3] c_get_stress[4] c_get_stress[5] c_get_stress[6]"

dump main all custom 1 dump/init.${name}.lammpstrj &
    ${main_options}

run 0

write_restart restart/restart.${name}
