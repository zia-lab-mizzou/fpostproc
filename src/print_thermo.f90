! print_thermo.f90
!
! PURPOSE: Print out virial pressure and potential energy per particle
! VARIABLES: (optional) starting timestep (to use if got far but crashed)
! AUTHOR: Ben Landrum
! LAST MODIFIED: December 10, 2014

program print_thermo

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_data, only : source_data_t
  implicit none

  integer :: i,ii,j,nparticles,nargs,snap,nsnaps
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory
  integer(int64) :: tstep,tstart
  real :: pe,press,c,y,t
  logical :: yes_tstart

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 1) then
     write(*, '(a)') 'print_thermo [directory] {tstart}'
     stop
  end if
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  yes_tstart = .false.
  if (nargs .eq. 2) then
     call get_command_argument(2, buf); read(buf, *) tstart
     yes_tstart = .true.
  end if

  call source_data%init(trim(adjustl(directory)), particle, domain)
  nsnaps = size(source_data%timestep)
  nparticles = particle%nparticles

  ! check if pressure, pe, velocity data are there

  if (.not. particle%pe_flag) then
     write(*, '(a)') 'Error: no PE data'
     stop
  end if
  if (.not. particle%stress_flag) then
     write(*, '(a)') 'Error: no stress data'
     stop
  end if

  ! check tstart if given

  if (yes_tstart) then
     if (.not. source_data%step_exists(tstart)) then
        write (*, '(a)') 'Error: invalid tstart'
        stop
     end if
     ii = source_data%step_index(tstart)
  else
     ii = 1
  end if

  ! print header

  write(*, '(a)') 'ITEM: step virial_press pe'

  do i = ii, nsnaps

     tstep = source_data%timestep(i)
     snap = source_data%step_index(tstep)

     call source_data%read_snap(particle, domain, snap)

     ! get potential energy using Kahan summation
     ! not sure if the compiler actually uses it or messes it up

     pe = 0.0
     c = 0.0
     do j = 1, nparticles
        y = particle%pe(j) - c
        t = pe + y
        c = (t - pe) - y
        pe = t
     end do
     pe = pe / nparticles

     ! same for (virial) pressure

     press = 0.0
     c = 0.0
     do j = 1, nparticles
        y = sum(particle%stress(1:3,j)) - c
        t = press + y
        c = (t - press) - y
        press = t
     end do
     press = -press / (3.0 * domain%volume())

     ! not determining temperature
     ! would require mass input (not implemented)

     ! print output

     write(*, '(i14,4g14.5)') tstep, press, pe

  end do

end program print_thermo
