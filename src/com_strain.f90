program com_strain
! Define strain as the center of mass of all the particles in the z direction.
! May bin with contact numbers in the future
! May add functionality in the future regarding x and y.
  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,k,nparticles,nargs,snap,nsnaps
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory
  integer, dimension(:), allocatable :: nc
  real :: rsep,minsize, Lbox
  integer(int64) :: tstart,tdur,tinc,tstep
  integer, parameter :: ncmax = 20
  real zcoord
  integer ntrack, id(10000)
  real :: meanz, depth

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 5) then
     write(*, '(A)') 'com_strain [directory] [tstart] [tdur] [tinc] [Lbox] {rsep}'
     stop
  endif
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) tstart
  call get_command_argument(3, buf); read(buf, *) tdur
  call get_command_argument(4, buf); read(buf, *) tinc
  call get_command_argument(5, buf); read(buf, *) Lbox

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles
  allocate(nc(nparticles))

  ! init group and cells

  call group%init(domain)
!  minsize = 2*maxval(particle%typeradius(:)) + rsep
!  call cells%init(group, minsize, .false.)

  ! get initial coordinate
 
  tstep = tstart
  snap = source_data%step_index(tstep)

  ! get contact numbers

!  nc(:) = 0
  call source_data%read_snap(particle, domain, snap)
!  call cells%reset_grid()
!  call cells%assign_particles()
!  call eval_nc(cells, .true., rsep, nc)

  ! loop over snaps

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     ! get contact numbers
!
!     nc(:) = 0
     call source_data%read_snap(particle, domain, snap)
!     call cells%reset_grid()
!     call cells%assign_particles()
!     call eval_nc(cells, .true., rsep, nc)

     ! get the particles that have nc >= 10 at tstart
     meanz = 0
     do j = 1, nparticles
        zcoord = particle%x(3,j)
        meanz = meanz + zcoord
     end do
     meanz = meanz / nparticles
     write(*,*) tstep, meanz

  end do

end program com_strain
