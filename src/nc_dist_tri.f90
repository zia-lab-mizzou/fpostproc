program nc_dist_tri

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,k,nparticles,nargs,snap,nsnaps
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory,mode
  integer, dimension(:), allocatable :: nc
  real :: rsep,minsize,meanval
  integer(int64) :: tstart,tdur,tinc,tstep
  integer, parameter :: ncmax = 20
  real, dimension(ncmax+1) :: ncdist
  logical :: mean_only

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 6) then
     write(*, '(A)') 'nc_dist_tri [mode] [directory] [tstart] [tdur] [tinc] [rsep]'
     stop
  end if
  call get_command_argument(1, buf); mode = trim(adjustl(buf))
  call get_command_argument(2, buf); directory = trim(adjustl(buf))
  call get_command_argument(3, buf); read(buf, *) tstart
  call get_command_argument(4, buf); read(buf, *) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); read(buf, *) rsep

  if (trim(adjustl(mode)) == 'mean') then
     mean_only = .true.
  else if (trim(adjustl(mode)) == 'full') then
     mean_only = .false.
  else if (trim(adjustl(mode)) == 'band') then
     mean_only = .false.
  else
     print *, 'mode must be mean or full or band (full and band)'
     stop
  end if
  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles
  allocate(nc(nparticles))

  ! init group and cells

  call group%init(domain)
  minsize = 2*maxval(particle%typeradius(:)) + rsep
  call cells%init(group, minsize, .false.)

  ! loop over snaps

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     ! get contact numbers

     nc(:) = 0
     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()
     call eval_nc(cells, .true., rsep*2*(sum(particle%typeradius(:))/size(particle%typeradius(:))), nc)

     ! get the distribution

     ncdist(:) = 0.0
     do j = 1, nparticles
        k = nc(j)
        if (k <= ncmax) then
           ncdist(k+1) = ncdist(k+1) + 1
        end if
     end do
     ncdist = ncdist / nparticles

     ! print output

     write(*, '(i10)', advance='no') tstep
     if (.not. mean_only) then
        do j = 0, ncmax
           write(*, '(f8.5)', advance='no') ncdist(j+1)
        end do
        write (*, '(a)') ''
     else
        meanval = 0.0
        do j = 0, ncmax
           meanval = meanval + j*ncdist(j+1)
        end do
        write (*, '(f8.5)') meanval
     end if

  end do

end program nc_dist_tri
