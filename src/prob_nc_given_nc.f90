program prob_nc_given_nc

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,k,nparticles,nargs,snap,nsnaps,combine_from
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory,mode
  integer, dimension(:), allocatable :: nc, nc_init
  real :: rsep,minsize
  integer(int64) :: tstart,tdur,tinc,tstep
  integer, parameter :: ncmax = 20
  real, dimension(ncmax+1) :: nc_count,meanval
  real, dimension(ncmax+1, ncmax+1) :: histo
  logical :: combining,mean_only

  ! Get input arguments

  nargs = command_argument_count()
  if (nargs .lt. 6) then
     write(*, '(A)') 'prob_nc_given_nc [mode] [directory] [tstart] [tdur] [tinc] [rsep] {combine_from--not yet}'
     stop
  end if
  call get_command_argument(1, buf); mode = trim(adjustl(buf))
  call get_command_argument(2, buf); directory = trim(adjustl(buf))
  call get_command_argument(3, buf); read(buf, *) tstart
  call get_command_argument(4, buf); read(buf, *) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); read(buf, *) rsep
  ! call get_command_argument(1, buf); directory = trim(adjustl(buf))
  ! call get_command_argument(2, buf); read(buf, *) tstart
  ! call get_command_argument(3, buf); read(buf, *) tdur
  ! call get_command_argument(4, buf); read(buf, *) tinc
  ! call get_command_argument(5, buf); read(buf, *) rsep
  ! call get_command_argument(6, buf); read(buf, *) ncmax_opt
  combining = .false.
  if (nargs .gt. 6) then
     combining = .true.
     call get_command_argument(7, buf); read(buf, *) combine_from
  end if

  if (trim(adjustl(mode)) == 'mean') then
     mean_only = .true.
  else if (trim(adjustl(mode)) == 'full') then
     mean_only = .false.
  else
     print *, 'mode must be mean or full'
     stop
  end if

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles

  ! allocate places to hold nc data

  allocate(nc(nparticles))
  allocate(nc_init(nparticles))

  snap = source_data%step_index(tstart)
  call source_data%set_box(domain, snap)

  call group%init(domain)  
  minsize = 2*maxval(particle%typeradius(:)) + rsep
  call cells%init(group, minsize, .false.)

  ! determine contact numbers at tstart

  call source_data%read_snap(particle, domain, snap)
  call cells%reset_grid()
  call cells%assign_particles()
  call eval_nc(cells, .true., rsep, nc)

  ! save initial nc values

  nc_init = nc

  ! count how many particles are in each group

  nc_count(:) = 0.0
  do j = 1, nparticles
     k = nc(j)
     if (k <= ncmax) then
        nc_count(k+1) = nc_count(k+1) + 1.0
     end if
  end do

  ! loop over snaps
  ! explicitly calculating t=0 values for sanity

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     call source_data%read_snap(particle, domain, snap)

     ! determine cells

     call cells%reset_grid()
     call cells%assign_particles()
     call eval_nc(cells, .true., rsep, nc)

     ! get histogram
     ! histogram: initial nc (columns) by new nc (rows)

     histo(:,:) = 0.0
     ! ask particle j to record current nc given initial nc in correct bin
     do j = 1, nparticles
        histo(nc(j)+1, nc_init(j)+1) = histo(nc(j)+1, nc_init(j)+1) + 1.0 
     end do

     ! now that we have the histogram for this timestep, normalize??
     ! divide each column by the original population in nc_count per nc
     ! also, make sure not to divide by 0, don't know how to handle 
     ! populations that were 0 initially
     
     do j = 0, ncmax !rows
        do k = 0, ncmax !columns
           if (nc_count(k+1) .gt. 0.0) then
              histo(j+1, k+1) = histo(j+1, k+1)/nc_count(k+1)
           end if
        end do
     end do

     ! print out histogram or mean values

     if (.not. mean_only) then
        write(*, '(i10)', advance='yes') tstep
        do j = 0, ncmax
           do k = 0, ncmax
              write(*, '(f15.5)', advance='no') histo(j+1, k+1)
           end do
           write(*, '(a)') ''
        end do
     else
        write(*, '(i10)', advance='no') tstep
        meanval(:) = 0.0
        do j = 0, ncmax !columns
           do k = 0, ncmax !rows
              meanval(j+1) = meanval(j+1) + histo(k+1, j+1)*k
           end do
        end do
        do j = 0, ncmax
           write(*, '(f15.5)', advance='no') meanval(j+1)
        end do
        write(*, '(a)') ''
     end if


  end do


end program prob_nc_given_nc
