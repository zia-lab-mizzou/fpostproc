! Compare the unmapped position of a particle to what it would have been
! if it flowed there from a position given at an earlier timestep.
! Result should be independent of initial timestep.

program diagnose_particle_flow

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t,propagate_in_flow
  use mod_data, only : source_data_t
  implicit none

  integer :: i,nparticles,nargs,snap_init,snap_fini,id_init,id_fini
  type(particle_t) :: particle_init,particle_flow,particle_fini
  type(domain_t) :: domain_init,domain_fini
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory
  integer(int64) :: tstart,tsep
  integer, dimension(3) :: delta_img
  real :: err
  real, dimension(3) :: coord

  ! read inputs

  print *, 'reading inputs'
  nargs = command_argument_count()
  if (nargs .ne. 5) then
     write(*, '(A)') 'diagnose_particle_flow [directory] [tstart] [tsep] [id_init] [id_fini]'
     stop
  end if
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) tstart
  call get_command_argument(3, buf); read(buf, *) tsep
  call get_command_argument(4, buf); read(buf, *) id_init
  call get_command_argument(5, buf); read(buf, *) id_fini

  ! init source data, which inits particle and domain

  call source_data%init(trim(adjustl(directory)), particle_init, domain_init)
  snap_init = source_data%step_index(tstart)
  snap_fini = source_data%step_index(tstart+tsep)
  nparticles = particle_init%nparticles

  ! check particle ids

  if (id_fini < id_init) then
     print *, 'id_fini must be >= id_init'
     stop
  end if
  if (id_init < 1) then
     print *, 'id_init cannot be less than 1'
     stop
  end if
  if (id_fini > nparticles) then
     print *, 'id_fini cannot be greater than nparticles'
     stop
  end if

  ! check timesteps

  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%step_exists(tstart+tsep)) then
     print *, 'bad tsep value'
     stop
  end if

  ! need two domain types and three particle types
  ! domain_init = box at tstart (initted but not at desired step)
  ! domain_fini = box at tstart+tsep (not initted)
  ! particle_init = configuration at tstart (initted but not at desired step)
  ! particle_fini = configuration at tstart+tsep (not initted)
  ! particle_flow = configuration obtained by mapping
  !                 particle_init from domain_init to domain_fini

  ! read particle_init and domain_init from initial data

  call source_data%read_snap(particle_init, domain_init, snap_init)

  ! init the final particle and domain
  ! then read their data from source data

  call particle_init%copy(particle_fini)
  call domain_fini%init(particle_fini)
  call source_data%read_snap(particle_fini, domain_fini, snap_fini)

  ! copy particle_init to particle_flow, then propagate the particles

  call particle_init%copy(particle_flow)
  delta_img(:) = (/ 0, 0, 0 /)
  do i = snap_init+1, snap_fini
     delta_img(:) = delta_img(:) + source_data%image_flips(:,i)
  end do

  call propagate_in_flow(domain_init, domain_fini, delta_img, particle_flow)

  ! print output

  write(*, '(a)') 'xyz init, xyz fini, xyz flow:-'
  do i = id_init, id_fini
     write(*, '(9g14.6)') particle_init%x(:,i), particle_fini%x(:,i), &
          particle_flow%x(:,i)
  end do

  err = 0.0
  do i = 1, nparticles
     coord = particle_fini%x(:,i) - particle_flow%x(:,i)
     err = err + dot_product(coord,coord)
  end do
  err = err / nparticles
  write(*, '(a,g14.6)') 'MSD: ', err
  write(*, '(a,3i4)') 'BOX FLIPS: ', delta_img(:)

end program diagnose_particle_flow
