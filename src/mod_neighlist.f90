! FEATURES FROM NEIGH_LIST:
!   setup_pages: setup data structures
!   grow: grow maxlocal
!   stencil_allocate: allocate stencil arrays
!   copy_skip: copy skip from a neigh request
!   print_attributes: debug routine
!   get_maxlocal: get for maxatoms



! NEED:
!   go through neighlist creation, checking algorithm and indices
!   specification of group in init?
!   construction of cutneighsq based on uniform dist, radius + delta
!
!
! QUESTIONS:
!   Qhat are nex_group and nex_type?
!     A: # of entries in group and type exclusion lists
!   What are ex1_bit and ex2_bit?
!     A: pairs of group bits to exclude
!   Why no ex1_type,maxex_group,ex1_group, etc.?
!   What is inum?
!     A: number of i atoms neighbors are stored for
!   Why firstneigh?
!     A: pointer to first j value of each i atom
!   What is ilist?
!     A: local indices of i atoms
!   Is ilist necessary?
!   Should neighlist trigger rebuild cells, or should this be done by manip?
!   How do high-level procedures gain access to neighbor list traversing?
!   Does this use the atom reordering?

!
! STATE FLAGS:
!   neighbor_exist (dynamic?)
!   exclude (set-and-forget?)


module mod_neighlist

  use mod_particle, only : particle_t
  use mod_group, only : group_t, excludes_t
  use mod_domain, only : domain_t
  use mod_cells, only : cells_t

  implicit none
  private

  type, public :: neighlist_t

     logical :: neighbor_exist
     logical :: exclude
     logical :: typecutsq_flag

     integer :: includegroup
     integer :: nex_group
     integer :: nex_type
     integer, dimension(:), allocatable :: ex1_bit,ex2_bit
     logical, dimension(:,:), allocatable :: ex_type
 
     real :: delta
     real, dimension(:,:), allocatable :: cutneighsq
     integer, dimension(:), allocatable :: ilist
     integer, dimension(:), allocatable :: numneigh
     integer, dimension(:), allocatable :: firstneigh
     integer :: inum

     ! necessary?
     integer :: nstencil

     type(particle_t), pointer :: particle => null()
     type(cells_t), pointer :: cells => null()
     type(domain_t), pointer :: domain => null()
     type(group_t), pointer :: group => null()

   contains

     procedure, public :: init
     procedure, public :: build
     procedure :: build_cell
     procedure :: build_nocell
     procedure :: exclusion
     
  end type neighlist_t

contains

  ! ----------------------------------------------------------------------
  ! allocate and set parameters
  ! ----------------------------------------------------------------------

  subroutine init(this, delta, includegroup, excludes)
    class(neighlist_t), intent(out) :: this
    real, intent(in) :: delta
    integer, intent(in), optional :: includegroup
    type(excludes_t), dimension(:), intent(in), optional :: excludes
    integer :: i,j,n
    real, dimension(:), pointer :: typeradius
    real :: rsq

    this%delta = delta
    n = this%particle%ntypes

    ! Specify cutoffs based on particle (type) radii + delta
    ! Determine if we're using types to set cutoffs

    if (this%particle%typeradius_flag) then ! types specify radii
       this%typecutsq_flag = .true.
       allocate(this%cutneighsq(n,n))
       this%cutneighsq(:,:) = 0.0
       typeradius => this%particle%typeradius
       do i = 1, n
          do j = 1, n
             this%cutneighsq(i,j) = typeradius(i) + typeradius(j) &
                  + delta
          end do
       end do
    else ! per-particle radius
       this%typecutsq_flag = .false.
    end if

    ! Include a specific group (and none others) if desired

    this%includegroup = 0
    if (present(includegroup)) then
       if (includegroup < 1) then
          print *, 'Invalid group ID for neighbor list building'
          stop
       end if
    else
       this%includegroup = includegroup
    end if

    ! Build exclusion pairs of groups and types
!!$
!!$    this%nex_group = 0
!!$    this%nex_type = 0
!!$    if (present(excludes)) then
!!$
!!$       do i = 1, size(excludes)
!!$          if (excludes(i)%style == 'group') then
!!$             this%nex_group = this%nex_group + 1
!!$          else if (excludes(i)%style == 'type')
!!$             this%nex_type = this%nex_type + 1
!!$          else
!!$             print *, 'Bad style for exclusion'
!!$             stop
!!$          end if
!!$       end do
!!$
!!$       if (nex_type > 0) then
!!$          allocate(ex_type(n,n))
!!$          ex_type(:,:) = .false.
!!$          do i = 1, size(excludes)
!!$             if (excludes(i)%style == 'type') then
!!$                ex_type(excludes(i)%m,excludes(i)%n) = .true.
!!$             end if
!!$          end do
!!$       end if
!!$
!!$       if (nex_group > 0) then
!!$          allocate(ex1_bit(nex_group))
!!$          allocate(ex2_bit(nex_group))
!!$
!!$          do i = 1, nex_group
!!$             ex1_bit(i) = this%group%bitmask(ex1_group(i))
!!$             ex2_bit(i) = this%group%bitmask(ex2_group(i))
!!$          end do
!!$       end if
!!$
!!$    end if

  end subroutine init

  ! ----------------------------------------------------------------------
  ! build a neighbor list, calling more specific procedures
  ! ----------------------------------------------------------------------

  subroutine build(this)
    class(neighlist_t), intent(inout) :: this
    if (this%cells%cells_exist) then
       call this%build_cell()
    else
       call this%build_nocell()
    end if
  end subroutine build

  ! ----------------------------------------------------------------------
  ! build a neighbor list using a cell list
  ! if not current for this time step, build cell list
  ! ----------------------------------------------------------------------

  subroutine build_cell(this)
    class(neighlist_t), intent(inout) :: this
    integer, dimension(:), pointer :: type => null(), mask => null()
    real, dimension(:,:), pointer :: x => null()
    integer :: itype,jtype,i,j,k,n,ibin,inum,nparticles
    real :: xtmp,ytmp,ztmp,delx,dely,delz,rsq

    type => this%particle%type
    x => this%particle%x
    mask => this%particle%mask
    nparticles = this%particle%nparticles

    if (.not. this%cells%current) then
       call this%cells%reset_grid()
       call this%cells%assign_particles()
    end if

    ! loop over each particle, storing neighbors

    do i = 1, nparticles
       n = 0

       itype = type(i)
       xtmp = x(1,i)
       ytmp = x(2,i)
       ztmp = x(3,i)

       ! loop over rest of particles in i's bin

!       j = bins(i)
       do while (j >= 1)

          jtype = type(j)
!!$          if (this%exclude .and. &
!!$               this%group%exclusion(i,j,itype,jtype,mask)) cycle

          delx = xtmp - x(1,j)
          dely = ytmp - x(2,j)
          delz = ztmp - x(3,j)
          rsq = delx*delx + dely*dely + delz*delz

!!$          if (rsq <= cutneighsq(itype,jtype)) then
!!$             ! TODO: WHAT IS THIS?
!!$!             neighptr(n) = j
!!$             n = n + 1
!!$          end if

!          j = bins(j)
       end do

       ! loop over all other particles in stencil

       ibin = this%cells%coord2cell(x(:,i))
       do k = 1, this%nstencil
!          j = binhead(ibin+stencil(k))
          do while (j > 0)
             jtype = type(j)
!!$             if (this%exclude .and. &
!!$                  exclusion(i,j,itype,jtype,mask)) cycle

             delx = xtmp - x(1,j)
             dely = ytmp - x(2,j)
             delz = ztmp - x(3,j)
             rsq = delx*delx + dely*dely + delz*delz

!!$             if (rsq <= cutneighsq(itype,jtype)) then
!!$!                neighptr(n) = j
!!$                n = n + 1
!!$             end if


!             j = bins(j)
          end do

       end do

!       ilist(inum) = i
       inum = inum + 1
       ! TODO WHAT'S THIS?
!       firstneigh(i) = neighptr
!       numneigh(i) = n
       ! ipage point to vgot(n)
!       if (ipage%status()) then
!          print *, 'Neighbor list overflow, boost neigh_modify one'
!          stop
!       end if

    end do

!    list%inum = inum
  end subroutine build_cell

  ! ----------------------------------------------------------------------
  ! build a neighbor list without a cell list
  ! ----------------------------------------------------------------------

  subroutine build_nocell(this)
    class(neighlist_t), intent(inout) :: this
    integer :: nparticles, bitmask, n
    integer, dimension(:), pointer :: type => null()
    integer, dimension(:), pointer :: mask => null()
    real, dimension(:,:), pointer :: x => null()

    real :: xtmp,ytmp,ztmp,delx,dely,delz,rsq
    integer :: i,j,itype,jtype

    x => this%particle%x
    type => this%particle%type
    mask => this%particle%mask

    if (this%includegroup > 0) then
       nparticles = this%particle%nparticles
    else
       nparticles = this%particle%nfirst
       bitmask = this%group%bitmask(this%includegroup)
    end if

    do i = 1, nparticles ! nlocal?
       n = 0
       
       itype = type(i)
       xtmp = x(1,i)
       ytmp = x(2,i)
       ztmp = x(3,i)

       ! loop over remaining particles
       ! check atom nparticles nlocal etc

       do j = i+1, nparticles
          if ((this%includegroup > 0) .and. &
              (iand(mask(j),bitmask)==0)) cycle
          jtype = type(j)
          if (this%exclude .and. &
              this%exclusion(i,j,itype,jtype,mask)) cycle

          delx = xtmp - x(1,j)
          dely = ytmp - x(2,j)
          delz = ztmp - x(3,j)
          call this%domain%minimum_image(delx,dely,delz)
          rsq = delx*delx + dely*dely + delz*delz

          if (rsq <= this%cutneighsq(itype,jtype)) then
             ! update the neighbor list
             n = n + 1
          end if
       end do ! j

       ! update ilist
       ! update firstneigh
       this%numneigh(i) = n
       ! update inum

    end do ! i

  end subroutine build_nocell

  ! ----------------------------------------------------------------------
  ! test if particle pair i,j is excluded from neighbor list
  ! due to type or group settings
  ! return .true. if should be excluded, .false. if included
  ! ----------------------------------------------------------------------

  function exclusion(this, i, j, itype, jtype, mask)
    logical :: exclusion
    class(neighlist_t), intent(in) :: this
    integer, intent(in) :: i,j,itype,jtype
    integer, dimension(:), intent(in) :: mask
    integer :: m,nex_type,nex_group

    nex_type = this%nex_type
    nex_group = this%nex_group

    if ((nex_type > 0) .and. this%ex_type(itype,jtype)) then
       exclusion = .true.
       return
    end if

    if (nex_group > 0) then
       do m = 1, nex_group
          if ((iand(mask(i),this%ex1_bit(m)) /= 0) .and. &
              (iand(mask(j),this%ex2_bit(m)) /= 0)) then
             exclusion = .true.
             return
          end if
          if ((iand(mask(i),this%ex2_bit(m)) /= 0) .and. &
              (iand(mask(j),this%ex1_bit(m)) /= 0)) then
             exclusion = .true.
             return
          end if
       end do
    end if

    exclusion = .false.

  end function exclusion

end module mod_neighlist
