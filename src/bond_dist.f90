program bond_dist

  ! ---------------------------------------------------------------------------
  ! DATE: September 4, 2015
  ! Made by: Lily
  ! 
  ! This executable should be able to do both mean and binned bond distance,
  ! the mean functionality is already working and the binned function is
  ! being added.
  ! ---------------------------------------------------------------------------

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_mean_bond_dist,eval_bin_bond_dist,eval_mean_bond_dist_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,nparticles,nargs,snap,nsnaps
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory,mode
  real :: rsep,minsize,mean_bond_dist
  real :: bin_lo,bin_hi
  real, dimension(:), allocatable :: bin_bond_dist_output
  integer, parameter :: ncmax = 20
  real, dimension(ncmax+1) :: mean_bond_dist_nc
  integer :: nbins,bond_sum
  integer(int64) :: tstart,tdur,tinc,tstep
  integer, dimension(:), allocatable :: bin_bond_dist

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 5) then
     write(*, '(a)') 'bond_dist [mode] [directory] [tstart] [tdur] &
          &[tinc] [rsep] {nbins} {bin_lo} {bin_hi}'
     stop
  end if
  call get_command_argument(1, buf); mode = trim(adjustl(buf))
  call get_command_argument(2, buf); directory = trim(adjustl(buf))
  call get_command_argument(3, buf); read(buf, *) tstart
  call get_command_argument(4, buf); read(buf, *) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); read(buf, *) rsep
  if ((mode /= 'mean') .and. (mode /= 'nc') .and. (mode /= 'binned')) then
     write(*, '(a)') 'mode must be mean, nc, or binned'
     stop
  end if
  if (mode == 'binned') then
     if (nargs .lt. 9) then
        write(*, '(a)') 'binned mode requires values for nbins, bin_lo, bin_hi'
        stop
     else 
        call get_command_argument(7, buf); read(buf, *) nbins
        call get_command_argument(8, buf); read(buf, *) bin_lo
        call get_command_argument(9, buf); read(buf, *) bin_hi
        if (bin_hi .le. bin_lo) then
           write(*, '(a)') 'bin_hi must be greater than bin_lo'
           stop
        end if
     end if
  end if

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles

  ! init group and cells

  call group%init(domain)
  minsize = 2*maxval(particle%typeradius(:)) + rsep
  call cells%init(group, minsize, .false.)

  ! set up arrays for binned case
  if (mode == 'binned') then
     allocate(bin_bond_dist(nbins))
     allocate(bin_bond_dist_output(nbins))
  end if

  ! loop over snaps

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     ! get snap info

     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()
     
     if (mode == 'mean') then
        call eval_mean_bond_dist(cells, .true., rsep, mean_bond_dist)
        
        ! print output

        write(*, '(i16)', advance='no') tstep
        write(*, '(2X,f16.8)') mean_bond_dist

     end if

     if (mode == 'nc') then
        call eval_mean_bond_dist_nc(cells, .true., rsep, mean_bond_dist_nc)

        print *, 'ran eval_mean_bond_dist_nc'

        ! print output
        
        write(*, '(i16)', advance='no') tstep
        do j = 1, ncmax
           write(*, '(2X,f16.8)', advance='no') mean_bond_dist_nc(j)
        end do
        write(*, '(2X,f16.8)') mean_bond_dist_nc(ncmax+1)

     end if

     if (mode == 'binned') then
!        allocate(bin_bond_dist(nbins))
        call eval_bin_bond_dist(cells, .true., rsep, nbins, bin_lo, bin_hi, bin_bond_dist)
        
        ! normalize the output
        bond_sum = 0
        do j = 1, nbins
           bond_sum = bond_sum + bin_bond_dist(j)
        end do
!        allocate(bin_bond_dist_output(nbins))
        bin_bond_dist_output(:) = 0.0
        do j = 1, nbins
           bin_bond_dist_output(j) = real(bin_bond_dist(j))/real(bond_sum)
        end do

        ! print output
        write(*, '(i16)', advance = 'no') tstep
        do j = 1, nbins - 1
           write(*, '(2X,f16.8)', advance = 'no') bin_bond_dist_output(j)
        end do
        write(*, '(2X,f16.8)') bin_bond_dist_output(nbins)

     end if

  end do

end program bond_dist
