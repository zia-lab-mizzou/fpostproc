program cell_stress_distribution

  use, intrinsic :: iso_fortran_env
!  use, intrinsic :: iso_c_binding
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_data, only : source_data_t
  use mod_cells, only : cells_t
  use mod_statistics, only : running_stat_t
  use mod_stat_func, only : cell_stress
  use mod_image, only : render_array
  implicit none

  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  type(running_stat_t), dimension(:), allocatable :: rs
  integer(int64) :: tstart,tdur,tinc,tstep
  integer :: i,j,k,l,n,nargs,nsnaps,nparticles,ncellx,ncells,nbins,bin,snap
  real :: alpha,minstress,maxstress,binwidth,val,histavg,imgavg,cellavg
  real, dimension(:), allocatable :: histo
  real, dimension(:,:), allocatable :: cellstress
  real(real64), dimension(:,:), allocatable :: img_array
  character(len=2) :: str,imgplane
  character(len=300) :: fname,buf,directory

  nargs = command_argument_count()
  if (nargs /= 11) then
     write(*, '(a)') 'cell_stress_distribution [directory] &
          &[tstart] [tdur] [tinc] [stress] [ncellx] [alpha] &
          &[minstress] [maxstress] [nbins] [imgplane]'
     stop
  end if
  call get_command_argument( 1, buf); directory = trim(adjustl(buf))
  call get_command_argument( 2, buf); read(buf, *) tstart
  call get_command_argument( 3, buf); read(buf, *) tdur
  call get_command_argument( 4, buf); read(buf, *) tinc
  call get_command_argument( 5, buf); str = trim(adjustl(buf))
  call get_command_argument( 6, buf); read(buf, *) ncellx
  call get_command_argument( 7, buf); read(buf, *) alpha
  call get_command_argument( 8, buf); read(buf, *) minstress
  call get_command_argument( 9, buf); read(buf, *) maxstress
  call get_command_argument(10, buf); read(buf, *) nbins
  call get_command_argument(11, buf); imgplane = trim(adjustl(buf))
  if (modulo(tdur,tinc) /= 0) then
     write(*,'(a)') 'tdur is not a multiple of tinc'
     stop
  end if
  if ((str /= 'pr') .and. (str /= 'vm') .and. &
      (str /= 'xx') .and. (str /= 'yy') .and. &
      (str /= 'zz') .and. (str /= 'yz') .and. &
      (str /= 'xz') .and. (str /= 'xy')) then
     write(*,'(a)') 'stress must be pr, vm, xx, yy, zz, yz, xz, or xy'
     stop
  end if
  if (ncellx < 3) then
     write(*,'(a)') 'ncellx must be >= 3'
     stop
  end if
  if ((alpha <= 0.0) .or. (alpha > 1.0)) then
     write(*,'(a)') 'alpha must be > 0.0 and <= 1.0'
     stop
  end if
  if (minstress >= maxstress) then
     write(*,'(a)') 'minstress must be less than maxstress'
     stop
  end if
  if (nbins < 1) then
     write(*,'(a)') 'nbins must be >= 1'
     stop
  end if
  if ((imgplane /= 'yz') .and. &
      (imgplane /= 'xz') .and. &
      (imgplane /= 'xy')) then
     write(*,'(a)') 'imgplane must be yz, xz, or xy'
     stop
  end if

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     write(*,'(a)') 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     write(*,'(a)') 'hdf5 timesteps appear irregular'
     write(*,'(a)') 'not supporting nonzero tdur for irregular'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles
  call group%init(domain)

  ! init cells with a bogus target_size for now
  ! will be specifying the number of cells in the main loop
  ! using optional arguments in reset_grid to keep the cell ids constant
  ! if box flips, will zero the running stats and issue a warning
  ! since cells change discontinuously then

  call cells%init(group, 1.0, .true.)
  ncells = ncellx*ncellx*ncellx
  allocate(cellstress(6,ncells))

  ! init the running stats, and set to exponential moving average mode

  allocate(rs(ncells))
  do i = 1, ncells
     call rs(i)%init()
     call rs(i)%set_ema(real(alpha,real64))
  end do

  allocate(histo(nbins))
  allocate(img_array(ncellx,ncellx))
  binwidth = (maxstress-minstress) / nbins

  ! print info line

  write(*, '(a)') 'ITEM: type of stress'
  write(*, '(a)') str
  write(*, '(a)') 'ITEM: stress of each bin'
  do j = 1, nbins
     write(*, '(g14.5)', advance='no') minstress + (j-0.5)*binwidth
  end do
  write(*, '(a)') ''
  write(*, '(a)') 'ITEM: stress distribution'

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     call source_data%read_snap(particle,domain,snap)

     ! box flip check, warning, and adjustment of averages

     if (sum(abs(source_data%image_flips(:,snap))) /= 0) then
        write (*, '(a)') 'box flip ... stopping'
        stop
     end if

     call cells%reset_grid(ncellx,ncellx,ncellx)
     call cells%assign_particles()
     call cell_stress(cells, cellstress)
     do j = 1, ncells
        select case (str)
        case ('pr')
           val = -sum(cellstress(1:3,j))/3.0
           call rs(j)%push(real(val,real64))
        case ('vm')
           val = (cellstress(1,j) - cellstress(2,j))**2
           val = val + (cellstress(2,j) - cellstress(3,j))**2
           val = val + (cellstress(3,j) - cellstress(1,j))**2
           val = val + &
                6*(cellstress(4,j)**2 + cellstress(5,j)**2 + cellstress(6,j)**2)
           val = 0.5*val
           val = sqrt(val)
           call rs(j)%push(real(val,real64))
        case ('xx')
           call rs(j)%push(real(cellstress(1,j),real64))
        case ('yy')
           call rs(j)%push(real(cellstress(2,j),real64))
        case ('zz')
           call rs(j)%push(real(cellstress(3,j),real64))
        case ('yz')
           call rs(j)%push(real(cellstress(4,j),real64))
        case ('xz')
           call rs(j)%push(real(cellstress(5,j),real64))
        case ('xy')
           call rs(j)%push(real(cellstress(6,j),real64))
        end select
     end do

     ! put into histogram and output

     histo(:) = 0.0
     do j = 1, ncells
        val = real(rs(j)%mean())
        bin = int((val-minstress)/binwidth) + 1
        if (bin < 1) cycle
        if (bin > nbins) cycle
        histo(bin) = histo(bin) + 1.0
     end do
     histo(:) = histo(:) / ncells
     do j = 1, nbins
        write(*, '(g14.5)', advance='no') histo(j)
     end do
     write (*,'(a)') ''

     ! TO-REMOVE
     ! find average stress from histo

     histavg = 0.0
     do j = 1, nbins
        val = minstress + (j-0.5)*binwidth
        histavg = val * histo(j)
     end do
     cellavg = 0.0
     do j = 1, ncells
        cellavg = cellavg + rs(j)%mean()
     end do
     cellavg = cellavg / ncells

     ! accumulate values for image and output

     img_array(:,:) = 0.0
     do j = 1, ncellx
        do k = 1, ncellx
           do l = 1, ncellx
              if (imgplane == 'yz') then
                 n = cells%triplet2cell( (/ j, k, l /) )
              else if (imgplane == 'yz') then
                 n = cells%triplet2cell( (/ k, j, l /) )
              else
                 n = cells%triplet2cell( (/ k, l, j /) )
              end if
              img_array(j,k) = img_array(j,k) + rs(n)%mean()
           end do
        end do
     end do
     img_array(:,:) = img_array(:,:) / ncellx

     write(buf,'(i10)') tstep
     write(fname,'(5a)') 'stress-', str, '-', trim(adjustl(buf)), '.ppm'
     call render_array(img_array, real(minstress,real64), &
          & real(maxstress,real64), .false., 'jt', fname)
     print *, 'histavg ', histavg, ' cellavg ', cellavg
  end do

end program cell_stress_distribution
