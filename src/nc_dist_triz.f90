program nc_dist_triz
! modified to read height.dat so that bins can be made for the current gel and
! we calculate nc in 21 bins. 
! FUNCTIONALITY TO ADD: numbers of bins as desired

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,k,nparticles,nargs,snap,nsnaps
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory,mode
  integer, dimension(:), allocatable :: nc
  real :: rsep,minsize,meanval
  integer(int64) :: tstart,tdur,tinc,tstep,tcorr
  real :: treal,t_in
  integer, parameter :: ncmax = 20
!  real, dimension(ncmax+1) :: ncdist
  real, dimension(:,:), allocatable :: ncdist
  real :: Lbox_z, zcoord, zcorr, height
  integer zbin
  integer, parameter :: nbins = 21 ! may change this later
  logical :: mean_only, zbins_only

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 7) then
     write(*, '(A)') 'nc_dist_triz [mode] [directory] [tstart] [tdur] [tinc] [rsep] [Lbox-z] [z-corr] [t-corr]'
     stop
  end if
  call get_command_argument(1, buf); mode = trim(adjustl(buf))
  call get_command_argument(2, buf); directory = trim(adjustl(buf))
  call get_command_argument(3, buf); read(buf, *) tstart
  call get_command_argument(4, buf); read(buf, *) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); read(buf, *) rsep
  call get_command_argument(7, buf); read(buf, *) Lbox_z
  call get_command_argument(8, buf); read(buf, *) zcorr
  call get_command_argument(9, buf); read(buf, *) tcorr

  zbins_only = .false.

  if (trim(adjustl(mode)) == 'mean') then
     mean_only = .true.
     allocate(ncdist(ncmax+1,1))
  else if (trim(adjustl(mode)) == 'full') then
     mean_only = .false.
     allocate(ncdist(ncmax+1,1))
  else if (trim(adjustl(mode)) == 'band') then
     mean_only = .false.
     zbins_only = .true.
     allocate(ncdist(ncmax+1,100))
  else if (trim(adjustl(mode)) == 'zband') then
     mean_only = .false.
     zbins_only = .true.
     allocate(ncdist(ncmax+1,100)) ! gotta come up with a way to also
!characterize Lbox
  else
     print *, 'mode must be mean or full or band (full and band) or zband (which creates a two dimensional array)'
     stop
  end if
  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles
  allocate(nc(nparticles))

  ! init group and cells

  call group%init(domain)
  minsize = 2*maxval(particle%typeradius(:)) + rsep
  call cells%init(group, minsize, .false.)

  ! read h(t) data
  open(unit=10, file = 'height.dat', action = 'read')

  ! loop over snaps
 
  t_in = 0
  height = 1.0

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     treal = (tstep-tcorr) * 0.00004 
     snap = source_data%step_index(tstep)
   
     ! read height
     do while (t_in < treal)
        read(10,*) t_in, height
        t_in = (t_in-tcorr) * 0.00004
     end do

     ! get contact numbers

     nc(:) = 0
     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()
     call eval_nc(cells, .true., rsep, nc)

     ! get the distribution

     ncdist(:,:) = 0.0
     do j = 1, nparticles
        k = nc(j)
        if (trim(adjustl(mode)) == 'zband' .or. trim(adjustl(mode)) == 'band') then
           zcoord = particle%x(3,j)
     ! normalize zcoord from 0 to 1
           zcoord = (zcoord-zcorr)/Lbox_z - floor((zcoord-zcorr)/Lbox_z) 
     ! bin it within the height of the sedimenting bed
           zbin = ceiling(((zcoord)/height)*nbins)
           if (k <= ncmax .and. zbin > 0 .and. zbin <= nbins) then
              ncdist(k+1,zbin) = ncdist(k+1,zbin) + 1
           end if
        else 
           if (k <= ncmax) then
              ncdist(k+1,1) = ncdist(k+1,1) + 1
            end if
        end if
     end do
     ncdist = ncdist / nparticles

     ! print output

     write(*, '(f10.2)', advance='no') treal
     write(*, '(f8.5)', advance='no') height
     if (zbins_only) then
        if (trim(adjustl(mode)) == 'zband') then
           write(*, '(a)') ''
           do j = 0, ncmax
              do k = 1, nbins
                 write(*, '(f8.5)', advance='no') ncdist(j+1,k)
              end do
              write (*, '(a)') ''
           end do
           write (*, '(a)') ''
        else if (trim(adjustl(mode)) == 'band') then
           do k = 1, nbins
              meanval = 0.0
              do j = 0, ncmax
                 meanval = meanval + j*ncdist(j+1,k)
              end do
              write(*, '(f10.5)', advance='no') meanval*nbins
           end do
           write (*, '(a)') ''
        endif 
     else if (.not. mean_only) then
        do j = 0, ncmax
           write(*, '(f8.5)', advance='no') ncdist(j+1,1)
        end do
        write (*, '(a)') ''
     else
        meanval = 0.0
        do j = 0, ncmax
           meanval = meanval + j*ncdist(j+1,1)
        end do
        write (*, '(f8.5)') meanval
     end if

  end do

end program nc_dist_triz
