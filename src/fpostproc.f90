module fpostproc

  type, public :: fpostproc_t

     class(atom_t), pointer :: atom
     class(neighbor_t), pointer :: neighbor
     class(domain_t), pointer :: domain

  end type fpostproc_t

end module fpostproc
