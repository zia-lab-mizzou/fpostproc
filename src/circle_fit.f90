program main

  use iso_fortran_env

  real(real64), dimension(2,8) :: gruntz_data

  gruntz_data(:,1) = (/  0.7_real64,  4.0_real64 /)
  gruntz_data(:,2) = (/  3.3_real64,  4.7_real64 /)
  gruntz_data(:,3) = (/  5.6_real64,  4.0_real64 /)
  gruntz_data(:,4) = (/  7.5_real64,  1.3_real64 /)
  gruntz_data(:,5) = (/  6.4_real64, -1.1_real64 /)
  gruntz_data(:,6) = (/  4.4_real64, -3.0_real64 /)
  gruntz_data(:,7) = (/  0.3_real64, -2.5_real64 /)
  gruntz_data(:,8) = (/ -1.1_real64,  1.3_real64 /)

contains

end program main
