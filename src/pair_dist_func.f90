program pair_dist_func

  use, intrinsic :: iso_fortran_env
  use RCImageIO
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_rdf, eval_pair_dist_func
  use mod_data, only : source_data_t
  use mod_statistics, only : running_stat_t
  use mod_image, only : render_array
  implicit none

  character(len=300) :: buf,directory,subtr_str,subtr_g0_str
  integer(int64) :: tstart,tdur,tinc,tstep,g0
  real :: rmax,minsize,rval,rgrid
  real(real64) :: gmin,gmax
  integer :: nbin,i,j,k,m,n,nsnaps,nargs,snap,nparticles,type_a,type_b,group_a,group_b
  character(len=2) :: plane
  character(len=3) :: mode
  character(len=300) :: fname
  type(source_data_t) :: source_data
  type(particle_t) :: particle
  type(group_t) :: group
  type(domain_t) :: domain
  type(cells_t) :: cells
  type(running_stat_t), dimension(:), allocatable :: rs_array_rad,rs0_array_rad
  type(running_stat_t), dimension(:,:,:), allocatable :: rs_array_xyz,rs0_array_xyz
  real, dimension(:), allocatable :: g_rad,g0_rad
  real, dimension(:,:,:), allocatable :: g_xyz,g0_xyz
  real(real64), dimension(:,:), allocatable :: g_xyz_flat
  logical, dimension(:), allocatable :: group_ids
  logical :: subtr,subtr_g0

  subtr = .false.
  subtr_g0 = .false.

  ! read inputs

  nargs = command_argument_count()
  if (nargs < 8) then
     write(*, '(a)') 'pair_dist_func [mode] [directory] [tstart] &
                     &[tdur] [tinc] [rmax] [nbin] [subtr] {gmin} {gmax} &
                     &{plane} {subtr_g0} {g0} {type_a} {type_b}'
     stop
  end if
  call get_command_argument(1, buf); mode = trim(adjustl(buf))
  call get_command_argument(2, buf); directory = trim(adjustl(buf))
  call get_command_argument(3, buf); read(buf, *) tstart
  call get_command_argument(4, buf); read(buf, *) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); read(buf, *) rmax
  call get_command_argument(7, buf); read(buf, *) nbin
  call get_command_argument(8, buf); subtr_str = trim(adjustl(buf))
  if (subtr_str == 'yes') then
     subtr = .true.
  else if (subtr_str == 'no') then
     subtr = .false.
  else
     write(*, '(a)') 'subtr must be yes or no'
     stop
  end if
  if ((mode /= 'xyz') .and. (mode /= 'rad')) then
     write(*, '(a)') 'mode must be xyz or rad'
     stop
  end if   
  if (rmax <= 0.0) then
     write(*, '(a)') 'rmax must be > 0.0'
     stop
  end if
  ! TODO does 1 actually work?
  if (nbin < 1) then
     write(*, '(a)') 'nbin must be >= 1'
     stop
  end if
  if ((mode == 'xyz') .and. (mod(nbin,2) == 0)) then
     write(*, '(a)') 'must have odd nbin for mode xyz'
     stop
  end if
  print *, "read first set of input arguments"
  if (nargs > 8) then
     call get_command_argument(9, buf); read(buf, *) gmin
     call get_command_argument(10, buf); read(buf, *) gmax
     call get_command_argument(11, buf); read(buf, *) plane
     ! Expected input for subtr_g0 is timestep of 
     !  snap present in same folder
     call get_command_argument(12, buf); subtr_g0_str = trim(adjustl(buf))
     print *, "read in g0 information"
     if (gmax <= 0.0) then
        write(*, '(a)') 'requiring gmax be > 0.0'
        stop
     end if
     if ((plane /= 'xy') .and. (plane /= 'xz') .and. (plane /= 'yz')) then
        write(*, '(a)') 'plane must be xy, xz, or yz'
        stop
     end if
     if (subtr_g0_str == 'yes') then
        subtr_g0 = .true.
        call get_command_argument(13, buf); read(buf, *) g0
     else if (subtr_g0_str == 'no') then
        subtr_g0 = .false.
     else
        write(*, '(a)') 'subtr_g0 must be yes or no'
        stop
     end if
  else
     if (mode == 'xyz') then
        write(*, '(a)') 'must supply gmax and plane arguments for mode xyz'
        stop
     end if
  end if
  type_a = 0
  type_b = 0
  print *, "sucessfully read in all inputs up to type inputs"
  print *, subtr_g0
  if (nargs > 12) then
     if ((subtr_g0) .and. (nargs == 15)) then
        print *, "read subtr_g0 variable as true"
        call get_command_argument(14, buf); read(buf, *) type_a
        call get_command_argument(15, buf); read(buf, *) type_b
     elseif ((.not. subtr_g0) .and. (nargs == 14)) then
        call get_command_argument(13, buf); read(buf, *) type_a
        call get_command_argument(14, buf); read(buf, *) type_b   
     else
        write(*, '(a)') 'must input correct number of arguments at this time'
     end if
     if ((type_a == 0) .neqv. (type_b == 0)) then
        write(*, '(a)') 'only both types a and b can be zero, not one'
        stop
     end if
  end if
  print *, "read in all inputs"

  ! get bin spacing
  ! TODO does this get things right with center square?
  
  if (mode == 'xyz') then
     rgrid = 2 * rmax / nbin
  else
     rgrid = rmax / nbin
  end if

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     write(*, '(a)') 'bad tstart value'
     stop
  end if
  print *, "read in timestep for measurement, now trying to read in g0"
  if (subtr_g0) then ! check to see if subtr_g0 is used
     print *, "trying to see if g0 data exists"
     if (.not. source_data%step_exists(g0)) then
        write(*, '(a)') 'g0 value'
        stop
     end if
  end if
  if (.not. source_data%regular) then
     write(*, '(a)') 'hdf5 timesteps appear irregular'
     write(*, '(a)') 'not supporting nonzero tdur for irregular'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles

  ! init group
  ! using all-all for group pair in pair distribution function

  call group%init(domain)
  if (type_a /= 0) then
     if (type_a > particle%ntypes) then
        write(*, '(a)') 'type a must be <= ntypes'
        stop
     end if
     if (type_b > particle%ntypes) then
        write(*, '(a)') 'type b must be <= ntypes'
        stop
     end if
     if (type_a < 1) then
        write(*, '(a)') 'type a must be >= 1'
        stop
     end if
     if (type_b < 1) then
        write(*, '(a)') 'type b must be >= 1'
        stop
     end if
     allocate(group_ids(nparticles))
     group_ids(:) = .false.
     do i = 1, nparticles
        if (particle%type(i) == type_a) group_ids(i) = .true.  
     end do
     group_a = group%create(group_ids(:))
     if (type_b /= type_a) then
        group_ids(:) = .false.
        do i = 1, nparticles
           if (particle%type(i) == type_b) group_ids(i) = .true.  
        end do
        group_b = group%create(group_ids(:))
     else
        group_b = group_a
     end if
  else
     group_a = 1
     group_b = 1
  end if

  ! TODO is this an appropriate minsize?

  minsize = sqrt(3.0) * rmax
  call cells%init(group, minsize, .false.)

  ! allocate arrays for statistical functions

  if (mode == 'xyz') then
     allocate(g_xyz(nbin,nbin,nbin))
     allocate(rs_array_xyz(nbin,nbin,nbin))
     allocate(g_xyz_flat(nbin,nbin))
     if (subtr_g0) then
        allocate(g0_xyz(nbin,nbin,nbin))
        allocate(rs0_array_xyz(nbin,nbin,nbin))
     end if
  else
     allocate(g_rad(nbin))
     allocate(rs_array_rad(nbin))
     if (subtr_g0) then
        allocate(g0_rad(nbin))
        allocate(rs0_array_rad(nbin))
     end if
  end if

  ! loop over snaps, accumulating to average

  print *, group_a, group_b
  print *, 'looping over snaps'
  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()

     if (mode == 'xyz') then
        print *, 'eval pdf snap ', i
        call eval_pair_dist_func(cells, group, group_a, group_b, rmax, nbin, g_xyz)
        do j = 1, nbin
           do k = 1, nbin
              do m = 1, nbin
                 call rs_array_xyz(j,k,m)%push(real(g_xyz(j,k,m), real64))
              end do
           end do
        end do
     else
        call eval_rdf(cells, group, group_a, group_b, rmax, nbin, g_rad)
        do j = 1, nbin
           call rs_array_rad(j)%push(real(g_rad(j), real64))
        end do
     end if

  end do

  ! perform calculations on timestep selected for g0 if called
 
  if (subtr_g0) then
     print *, 'calculating g0'
     
     snap = source_data%step_index(g0)

     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()

     if (mode == 'xyz') then
        print *, 'eval pdf snap ', i
        call eval_pair_dist_func(cells, group, group_a, group_b, rmax, nbin, g0_xyz)
        do j = 1, nbin
           do k = 1, nbin
              do m = 1, nbin
                 call rs0_array_xyz(j,k,m)%push(real(g0_xyz(j,k,m), real64))
              end do
           end do
        end do
     else
        call eval_rdf(cells, group, group_a, group_b, rmax, nbin, g0_rad)
        do j = 1, nbin
           call rs0_array_rad(j)%push(real(g_rad(j), real64))
        end do
     end if
  end if

  ! output data
  ! make sure to use averaged results

  ! output data if subtr_g0 is present properly
 
  if (subtr_g0) then
     if (mode == 'xyz') then
        do k = 1, nbin
           do j = 1, nbin
              do i = 1, nbin
                 g_xyz(i,j,k) = real(rs_array_xyz(i,j,k)%mean())-real(rs0_array_xyz(i,j,k)%mean())
              end do
           end do
        end do
        n = size(g_xyz, 1)
        m = (n + 1) / 2
        if (plane == 'xy') then
           do i = 1, n
              do j = 1, n
                 g_xyz_flat(i,j) = real(g_xyz(i,j,m))
              end do
           end do
        else if (plane == 'xz') then
           do i = 1, n
              do j = 1, n
                 g_xyz_flat(i,j) = real(g_xyz(i,m,j))
              end do
           end do
        else
           do i = 1, n
              do j = 1, n
                 g_xyz_flat(i,j) = real(g_xyz(m,i,j))
              end do
           end do
        end if
        write(buf,'(i10)') tstep
        write(fname,'(5a)') 'pdf-', plane, '-', trim(adjustl(buf)), '.ppm'
        call render_array(g_xyz_flat(:,:),gmin,gmax,.false.,'jt',fname)

     else
        write(*, '(a)') 'r, g(r)'
        do i = 1, nbin
           g_rad(i) = real(rs_array_rad(i)%mean())-real(rs0_array_rad(i)%mean())
           rval = (real(i)-0.5) * rgrid
           write(*, '(2g14.5)') rval, g_rad(i)
        end do
     end if
  else 
     if (mode == 'xyz') then
        do k = 1, nbin
           do j = 1, nbin
              do i = 1, nbin
                 g_xyz(i,j,k) = real(rs_array_xyz(i,j,k)%mean())
              end do
           end do
        end do
        n = size(g_xyz, 1)
        m = (n + 1) / 2
        if (plane == 'xy') then
           do i = 1, n
              do j = 1, n
                 g_xyz_flat(i,j) = real(g_xyz(i,j,m))
              end do
           end do
        else if (plane == 'xz') then
           do i = 1, n
              do j = 1, n
                 g_xyz_flat(i,j) = real(g_xyz(i,m,j))
              end do
           end do
        else
           do i = 1, n
              do j = 1, n
                 g_xyz_flat(i,j) = real(g_xyz(m,i,j))
              end do
           end do
        end if
        write(buf,'(i10)') tstep
        write(fname,'(5a)') 'pdf-', plane, '-', trim(adjustl(buf)), '.ppm'
        call render_array(g_xyz_flat(:,:),gmin,gmax,.false.,'jt',fname)

     else
        write(*, '(a)') 'r, g(r)'
        do i = 1, nbin
           g_rad(i) = real(rs_array_rad(i)%mean())
           rval = (real(i)-0.5) * rgrid
           write(*, '(2g14.5)') rval, g_rad(i)
        end do
     end if
  end if
  
  

contains

  ! ----------------------------------------------------------------------
  ! return rgb triple for input pair distribution value
  ! map [0,1] in density to [(0,0,0),(0,0,255)], or black to blue
  ! map [1,1+(gmax-1)/2] in density to [(0,0,255),(0,255,0)],
  !    gradient between blue and green (no red)
  ! map [1+(gmax-1)/2,gmax] in density to [(0,255,0),(255,0,0)],
  !    gradient between green and red (no blue)
  ! if < 0 or > gmax, saturate to black or red
  ! NOTE: assumes gmax > 1.0
  ! ----------------------------------------------------------------------

  subroutine val_to_rgb(val, maxval, color)

    real, intent(in) :: val,maxval
    type(rgb), intent(out) :: color
    integer :: c

    c = int(val/maxval*64)
    select case (c)
    case (:-1)
       color%red = 0
       color%green = 0
       color%blue = 101
    case (0)
       color%red = 0
       color%green = 0
       color%blue = 143
    case (1)
       color%red = 0
       color%green = 0
       color%blue = 159
    case (2)
       color%red = 0
       color%green = 0
       color%blue = 175
    case (3)
       color%red = 0
       color%green = 0
       color%blue = 191
    case (4)
       color%red = 0
       color%green = 0
       color%blue = 207
    case (5)
       color%red = 0
       color%green = 0
       color%blue = 223
    case (6)
       color%red = 0
       color%green = 0
       color%blue = 239
    case (7)
       color%red = 0
       color%green = 0
       color%blue = 255
    case (8)
       color%red = 0
       color%green = 15
       color%blue = 255
    case (9)
       color%red = 0
       color%green = 31
       color%blue = 255
    case (10)
       color%red = 0
       color%green = 47
       color%blue = 255
    case (11)
       color%red = 0
       color%green = 63
       color%blue = 255
    case (12)
       color%red = 0
       color%green = 79
       color%blue = 255
    case (13)
       color%red = 0
       color%green = 95
       color%blue = 255
    case (14)
       color%red = 0
       color%green = 111
       color%blue = 255
    case (15)
       color%red = 0
       color%green = 127
       color%blue = 255
    case (16)
       color%red = 0
       color%green = 143
       color%blue = 255
    case (17)
       color%red = 0
       color%green = 159
       color%blue = 255
    case (18)
       color%red = 0
       color%green = 175
       color%blue = 255
    case (19)
       color%red = 0
       color%green = 191
       color%blue = 255
    case (20)
       color%red = 0
       color%green = 207
       color%blue = 255
    case (21)
       color%red = 0
       color%green = 223
       color%blue = 255
    case (22)
       color%red = 0
       color%green = 239
       color%blue = 255
    case (23)
       color%red = 0
       color%green = 255
       color%blue = 255
    case (24)
       color%red = 15
       color%green = 255
       color%blue = 239
    case (25)
       color%red = 31
       color%green = 255
       color%blue = 223
    case (26)
       color%red = 47
       color%green = 255
       color%blue = 207
    case (27)
       color%red = 63
       color%green = 255
       color%blue = 191
    case (28)
       color%red = 79
       color%green = 255
       color%blue = 175
    case (29)
       color%red = 95
       color%green = 255
       color%blue = 159
    case (30)
       color%red = 111
       color%green = 255
       color%blue = 143
    case (31)
       color%red = 127
       color%green = 255
       color%blue = 127
    case (32)
       color%red = 143
       color%green = 255
       color%blue = 111
    case (33)
       color%red = 159
       color%green = 255
       color%blue = 95
    case (34)
       color%red = 175
       color%green = 255
       color%blue = 79
    case (35)
       color%red = 191
       color%green = 255
       color%blue = 63
    case (36)
       color%red = 207
       color%green = 255
       color%blue = 47
    case (37)
       color%red = 223
       color%green = 255
       color%blue = 31
    case (38)
       color%red = 239
       color%green = 255
       color%blue = 15
    case (39)
       color%red = 255
       color%green = 255
       color%blue = 0
    case (40)
       color%red = 255
       color%green = 239
       color%blue = 0
    case (41)
       color%red = 255
       color%green = 223
       color%blue = 0
    case (42)
       color%red = 255
       color%green = 207
       color%blue = 0
    case (43)
       color%red = 255
       color%green = 191
       color%blue = 0
    case (44)
       color%red = 255
       color%green = 175
       color%blue = 0
    case (45)
       color%red = 255
       color%green = 159
       color%blue = 0
    case (46)
       color%red = 255
       color%green = 143
       color%blue = 0
    case (47)
       color%red = 255
       color%green = 127
       color%blue = 0
    case (48)
       color%red = 255
       color%green = 111
       color%blue = 0
    case (49)
       color%red = 255
       color%green = 95
       color%blue = 0
    case (50)
       color%red = 255
       color%green = 79
       color%blue = 0
    case (51)
       color%red = 255
       color%green = 63
       color%blue = 0
    case (52)
       color%red = 255
       color%green = 47
       color%blue = 0
    case (53)
       color%red = 255
       color%green = 31
       color%blue = 0
    case (54)
       color%red = 255
       color%green = 15
       color%blue = 0
    case (55)
       color%red = 255
       color%green = 0
       color%blue = 0
    case (56)
       color%red = 239
       color%green = 0
       color%blue = 0
    case (57)
       color%red = 223
       color%green = 0
       color%blue = 0
    case (58)
       color%red = 207
       color%green = 0
       color%blue = 0
    case (59)
       color%red = 191
       color%green = 0
       color%blue = 0
    case (60)
       color%red = 175
       color%green = 0
       color%blue = 0
    case (61)
       color%red = 159
       color%green = 0
       color%blue = 0
    case (62)
       color%red = 143
       color%green = 0
       color%blue = 0
    case (63:)
       color%red = 127
       color%green = 0
       color%blue = 0
    end select

!!$    if (val < 1.0) then
!!$       level = max(0, floor(val * 256))
!!$       color%red = 0
!!$       color%green = 0
!!$       color%blue = level
!!$    else
!!$       greenpoint = 0.5 * (gmax + 1.0)
!!$       invwidth = 1.0 / (greenpoint - 1.0)
!!$       if (val < greenpoint) then
!!$          level = floor((val-1.0) * invwidth * 256)
!!$          level = min(level, 255)
!!$          color%red = 0
!!$          color%green = level
!!$          color%blue = 255-level
!!$       else
!!$          level = min(255, floor((val-greenpoint) * invwidth * 255))
!!$          level = max(level, 0)
!!$          color%red = level
!!$          color%green = 255-level
!!$          color%blue = 0
!!$       end if
!!$    end if

  end subroutine val_to_rgb

  ! ----------------------------------------------------------------------
  ! Create plane with two axes, setting the third coord to zero
  ! Plane must be xy, xz, or yz
  ! First coord is the horizontal axis
  ! Second coord is the vertical axis
  ! gmax goes into the 'val_2_rgb' function for coloring
  ! fname set to the timestep and the plane value in above
  ! NOTE: Assuming pair distribution function input is a cube
  !       and that there are an odd number of elements per side
  ! ----------------------------------------------------------------------

  subroutine plot_pair_dist_func(g_xyz, plane, gmin, gmax, subtr, tstep)

!    use mod_image, only : val2rgb_gs

    real, dimension(:,:,:), intent(inout) :: g_xyz
    character(len=2), intent(in) :: plane
    real, intent(in) :: gmin,gmax
    logical, intent(in) :: subtr
    integer(int64), intent(in) :: tstep
    integer :: m,n
    type(rgb) :: color
    character(len=300) :: fname
    type(rgbimage) :: img
 
    n = size(g_xyz, 1)
    m = (n + 1) / 2

    if (subtr) call subtract_equidistant(g_xyz)

    call init_img(img)
    call alloc_img(img,n,n)

    if (plane == 'xy') then
       do i = 1, n
          do j = 1, n
             ! missing line to put color info
             call put_pixel(img, i-1, n-j, color)
          end do
       end do
    else if (plane == 'xz') then
       do i = 1, n
          do j = 1, n
             ! missing line to put color info
             call put_pixel(img, i-1, n-j, color)
          end do
       end do
    else
       do i = 1, n
          do j = 1, n
             ! missing line to put color info
             call put_pixel(img, i-1, n-j, color)
          end do
       end do
    end if

    write(buf, '(i10)') tstep
    write(fname, '(5a)') 'pdf-', plane, '-', trim(adjustl(buf)), '.ppm'
    open(8, file=trim(adjustl(fname)), action='write')
    call output_ppm(8, img)
    call free_img(img)
    close(8)

  end subroutine plot_pair_dist_func

  ! ----------------------------------------------------------------------
  ! Subtract averages over equidistant pixels
  ! Assumes equal grid spacing and odd number of pixels
  ! ----------------------------------------------------------------------

  subroutine subtract_equidistant(g_xyz)

    real, dimension(:,:,:), intent(inout) :: g_xyz
    integer :: n,m,rsq,i,j,k,ii,jj,kk,imax,jmax,kmax
    real :: v

    print *, 'averaging'

    n = (size(g_xyz,1)-1)/2
    do rsq = 1, 3*n*n
       v = 0
       m = 0
       kmax = int(sqrt(real(rsq)))
       do k = -kmax, kmax
          kk = k + n + 1
          jmax = int(sqrt(real(rsq - k*k)))
          do j = -jmax, jmax
             jj = j + n + 1
             imax = int(sqrt(real(rsq - j*j - k*k)))
             do i = -imax, imax
                ii = i + n + 1
                if (i*i+j*j+k*k == rsq) then
                   if (abs(i) > n) cycle
                   if (abs(j) > n) cycle
                   if (abs(k) > n) cycle
                   v = v + g_xyz(ii,jj,kk)
                   m = m + 1
                end if
             end do
          end do
       end do
       if (m > 0) then
          v = v / m
       else
          cycle
       end if
       kmax = int(sqrt(real(rsq)))
       do k = -kmax, kmax
          kk = k + n + 1
          jmax = int(sqrt(real(rsq - k*k)))
          do j = -jmax, jmax
             jj = j + n + 1
             imax = int(sqrt(real(rsq - j*j - k*k)))
             do i = -imax, imax
                ii = i + n + 1
                if (i*i+j*j+k*k == rsq) then
                   if (abs(i) > n) cycle
                   if (abs(j) > n) cycle
                   if (abs(k) > n) cycle
                   g_xyz(ii,jj,kk) = g_xyz(ii,jj,kk) - v
                end if
             end do
          end do
       end do
    end do
       
  end subroutine subtract_equidistant

end program pair_dist_func
