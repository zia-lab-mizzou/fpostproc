! fourier.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    May 20, 2013
! PURPOSE:         Contains everything needed to operate in Fourier space and the recipes for some statistical functions.
! CONTAINS:        subroutines: ft_dens, get_isf, get_ssf, get_ssf_each, inner_product, read_modes
!                  types: fmode, fmodemap
!                  globals: modes, modemap, trippath
!                  FFT not implemented yet.

module mod_ft_old

   use mod_particle, only : particle_t
   use mod_constants, only : pi
   implicit none

   type fmode ! used to store Fourier modes
      integer :: id ! id of the Fourier mode
      integer, dimension(3) :: m ! vector for the mode
      integer :: msq ! squared magnitude
      integer :: bin ! id for the mode (all modes with same msq have same bin)
   end type fmode

   type fmodemap ! obsolete?
      integer :: msq, multip
   end type fmodemap

   ! Global variables:
   type(fmode), dimension(:), allocatable :: modes ! array storing the relevant Fourier modes
   type(fmodemap), dimension(:), allocatable :: modemap ! obsolete?

!**********************************************************************
contains
!**********************************************************************

   subroutine ft_dens(particle, rhoq, qval)
   ! Fourier transform the density for given part (already allocated!)
   ! does this for all modes that we got already from file
   ! the modes could be from zero or not

      type(particle_t), intent(in)                    :: particle
      complex, dimension(size(modes))                 :: rhoq
      real, dimension(3, size(modes))                 :: qvecs
      real, intent(in)                                :: qval
      integer                                         :: i, j

      ! conversion factors for box, assume cubic
      !qval = 2.0 * pi / (cells%boxhi(1)-cells%boxlo(1))
      do i = 1, size(modes)
         qvecs(:, i) = -qval * modes(i)%m(:) ! neg sign for FT added here
      end do

      rhoq = 0
      do i = 1, size(modes)
         do j = 1, particle%nparticles
            rhoq(i) = rhoq(i) + &
                      exp(cmplx(0.0, dot_product(particle%x(:,j), qvecs(:, i))))
         end do
      end do

   end subroutine ft_dens

!**********************************************************************

   subroutine get_isf(fullorself, from, to, qval, subtr_tf, isf)
   ! IMPORTANT: when using, loaded modes array must have identical msq
   ! currently ass-backwards, input q is provided by the loaded msq
   ! and the box variables

      character(len=4), intent(in) :: fullorself
      !type(dumpfile), dimension(:), intent(in) :: dumps
      !integer*8, dimension(:), intent(in) :: times
      logical, intent(in) :: subtr_tf
      complex,  intent(out) :: isf
      type(particle_t), intent(inout) :: to
      type(particle_t), intent(in) :: from
      complex, dimension(size(modes)) :: rhoq1, rhoq2
      complex, dimension(size(modes)) :: isfeach
      real, intent(in) :: qval
      real, dimension(3) :: mean_origin, mean_disp
      integer :: msq, i, j, nparticles

      ! check for consistency
      nparticles = from%nparticles
      if (to%nparticles /= nparticles) then
         write(*, '(a)') 'cannot get isf between snaps with different nparticles'
         stop
      end if

      ! Check if modes in modes array have identical squared (integer) values.
      msq = modes(1)%msq
      do i = 2, size(modes)
         if (modes(i)%msq /= msq) then
            write(*, '(A)') 'Error: modes array for ISF has multiple mags'
            stop
         end if
      end do

      if (fullorself == 'full') then

         ! Get origin value of full correlator.

         call ft_dens(from, rhoq1, qval)
         !ssfeach(:) = inner_product(rhoq1, rhoq1) / nparticles

         ! Get the starting position in unwrapped coordinates.
         ! This is used when subtracting the mean motion.
         if (subtr_tf) then
            mean_origin(1) = sum(from%x(1,:)) / nparticles !get_mean_pos(part1, mean_origin)
            mean_origin(2) = sum(from%x(2,:)) / nparticles
            mean_origin(3) = sum(from%x(3,:)) / nparticles
            mean_disp(1) = sum(to%x(1,:)) / nparticles !get_mean_pos(part2, mean_disp)
            mean_disp(2) = sum(to%x(2,:)) / nparticles
            mean_disp(3) = sum(to%x(3,:)) / nparticles
            mean_disp = mean_disp - mean_origin
            do i = 1, nparticles
              to%x(:,i) = to%x(:,i) - mean_disp !call subtract_displacement(part2, mean_disp)
            end do
         end if

         ! Get FT at the current timestep, and take inner product with FT at the
         !   inital timestep

         call ft_dens(to, rhoq2, qval)
         isfeach(:) = inner_product(rhoq2, rhoq1) / nparticles

      else if (fullorself == 'self') then

         isfeach(:) = 1.0

         ! Get mean position (if subtracting mean motion.
         if (subtr_tf) then
            mean_origin(1) = sum(from%x(1,:)) / nparticles !get_mean_pos(part1, mean_origin)
            mean_origin(2) = sum(from%x(2,:)) / nparticles
            mean_origin(3) = sum(from%x(3,:)) / nparticles
            mean_disp(1) = sum(to%x(1,:)) / nparticles !get_mean_pos(part2, mean_disp)
            mean_disp(2) = sum(to%x(2,:)) / nparticles
            mean_disp(3) = sum(to%x(3,:)) / nparticles
            mean_disp = mean_disp - mean_origin
            do i = 1, nparticles
              to%x(:,i) = to%x(:,i) - mean_disp !call subtract_displacement(part2, mean_disp)
            end do
         end if

         ! Get particle displacements to FT.
         to%x = to%x-from%x

         call ft_dens(to, rhoq2, qval)
         isfeach(:) = rhoq2 / nparticles

      else

         write(*, '(A)') 'Error: should be full or self'
         stop

      end if

      ! Average over the modes, all the same wavenumber (magnitude).
      isf = 0
      !do i = 1, size(times)
         do j = 1, size(modes)
            isf = isf + isfeach(j) / size(modes)
         end do
      !end do

      ! Normalize if using full ISF (already normalized if self ISF).
      !if (fullorself == 'full') then
      !   normval = real(ssf)
      !   isf = isf / normval
      !end if

   end subroutine get_isf

!**********************************************************************

   !subroutine get_ssf(part, qmax, nbin, qvec, ssf)
   !! get the static structure factor over all loaded modes
   !! bin the output to get a function
   !! uses: global box and modes
   !! note: should load appropriate modes in calling program,
   !!          no more and no less than needed!

   !   type(particle), dimension(:), intent(in) :: part
   !   real, intent(in)                         :: qmax
   !   integer, intent(in)                      :: nbin
   !   real, dimension(nbin)                    :: qvec, ssf
   !   complex, dimension(size(modes))          :: rhoq
   !   real, dimension(size(modes))             :: ssf_each, qvec_each
   !   real                                     :: qval, delq

   !   call get_ssf_each(part, qmax, qvec_each, ssf_each)

   !   ! bin to get the ssf averaged over different modes
   !   call bin_avg_real(qvec_each, ssf_each, 0.0, qmax, nbin, qvec, ssf)

   !end subroutine get_ssf

!**********************************************************************

   !subroutine get_ssf_each(part, qmax, qvec_each, ssf_each)
   !! get the static structure factor over all loaded modes,
   !! outputting an SSF that isn't bin_avg'd
   !! uses: global box and modes
   !! note: should load appropriate modes in calling program,
   !!          no more and no less than needed!

   !   type(particle), dimension(:), intent(in) :: part
   !   real, intent(in)                         :: qmax
   !   complex, dimension(size(modes))          :: rhoq
   !   real, dimension(size(modes))             :: ssf_each, qvec_each
   !   real                                     :: qval, delq

   !   qval = 2.0 * pi / (box%hi(1) - box%lo(1))

   !   ! get fourier transform and ssf contributions
   !   call ft_dens(part, rhoq) ! over all wavevectors loaded, given global box

   !   ssf_each = inner_product(rhoq, rhoq) / size(part)

   !   ! bin to get the ssf averaged over different modes
   !   qvec_each = qval * sqrt(real(modes(:)%msq))

   !end subroutine get_ssf_each

!**********************************************************************

   function inner_product(a, b) result (c)
   ! for complex arrays a and b, take complex conjugate of elements
   ! of a and multiply elementwise by b to get c
   ! EXAMPLE: inner_product(a(t),b(0)) = <a*(t) b(0)> = c
   ! used for correlation functions in q space

      complex, dimension(:) :: a, b
      complex, dimension(size(a)) :: c
      integer :: i

      do i = 1, size(a)
         c(i) = conjg(a(i)) * b(i)
      end do

   end function inner_product

!**********************************************************************

   subroutine read_modes(directory, oneormax, msqtarg, msqact, limit)
   ! reads the modes from a modes.txt file
   ! stores the modes necessary for the current problem (by msqmax) in memory

     character(len=*), intent(in)  :: directory
     character(len=3), intent(in)  :: oneormax ! load one msq or from 0 to max
     integer, intent(in)           :: msqtarg
     integer, intent(out)          :: msqact
     integer, intent(in), optional :: limit ! maximum number of modes per msq
     integer                       :: msqmaxfile, msq, multip
     integer                       :: modemin, modemax, nmode
     integer                       :: i, dummy, bin_old, bin_new
     character(len=1)              :: testchar

     ! Variables for optional mode limitation:
     integer :: j, randpos, counter, nbin, bin_pos, nmode_new
     integer, dimension(:, :), allocatable :: mode_points
     type(fmode), dimension(:), allocatable :: modes_new
     type(fmode) :: temp_mode
     real :: r

     open(7, file=trim(directory)//'triples.txt', status='old')
     read(7, *) ! TRIPLES FOR BOX FOURIER TRANSFORM
     read(7, *) ! ITEM: max_msq
     read(7, *) msqmaxfile

     ! Check if inputs make sense and if we are able to provide output.
     if (msqtarg > msqmaxfile) then
        write(*, '(A)') 'Error: modes file does not have enough modes'
        stop
     end if
     if (msqtarg < 0) then
        write(*, '(A)') 'Error: msqtarg must be greater than zero'
        stop
     end if

     ! Read past the 'multiplicity by bin_num' part.
     read(7, *)

     ! Get mode numbers of first mode to use and last to use
     ! for single msq, getting a group of q with same msq.
     if (oneormax == 'max') then
        modemin = 1; modemax = 0
        do
           read(7, *) dummy, msq, multip
           modemax = modemax + multip
           if (msq >= msqtarg) then
              msqact = msq
              exit
           end if
        end do
     else if (oneormax == 'one') then
        modemin = 1; modemax = 0
        do ! get first position and last position
           read(7, *) dummy, msq, multip
           modemax = modemax + multip
           if (msq >= msqtarg) then
              msqact = msq
              exit
           end if
           modemin = modemin + multip
        end do
     else
        write(*, '(A)') 'Error: need to select one or max when reading modes'
        stop
     end if
        
     nmode = modemax - modemin + 1

     ! Read to 'ITEM: ...' to get us to where the mode triples start.
     do
        read(7, '(A1)') testchar
        if (testchar == 'I') exit
     end do

     allocate(modes(nmode))

     ! Read past modes before modemin.
     do i = 1, modemin-1
        read(7, *)
     end do

     ! Read in the modes.
     do i = 1, nmode
        modes(i)%id = modemin-1 + i
        read(7, *) modes(i)%m(:), modes(i)%msq, modes(i)%bin
     end do
     close(7)

     ! If using a ceiling on the number of modes with same magnitude,
     ! then copy the modes array with the modes to use and then replace
     ! the global modes array with the new (smaller) one.

!     print*, 'Original modes up to 15 (id, msq, bin):-'
!     if (nmode .gt. 15) then
!        do i = 1, 15
!           print*, modes(i)%id, modes(i)%msq, modes(i)%bin
!        end do
!     else
!        do i = 1, nmode
!           print*, modes(i)%id, modes(i)%msq, modes(i)%bin
!        end do
!     end if

     if (present(limit)) then
        if (limit .le. 0) then
           write(*, '(A)') 'Limit in mode number must be > 0'
           stop
        end if

        ! Get the number of modes for each bin.
        nbin = modes(size(modes))%bin - modes(1)%bin + 1 ! number of bins
        allocate(mode_points(2, nbin)) ! keep track of initial and final modes in bins
        mode_points = 0
        bin_old = 0
        bin_pos = 0
        do i = 1, nmode
           bin_new = modes(i)%bin
           if (bin_new .gt. bin_old) then
              bin_pos = bin_pos + 1
              mode_points(1, bin_pos) = i
              mode_points(2, bin_pos) = i
           else
              mode_points(2, bin_pos) = mode_points(2, bin_pos) + 1
           end if
           bin_old = bin_new
        end do

        ! If number of modes in a bin is greater than the cutoff, shuffle all,
        ! then take modes from the beginning of the bin to use.
        ! I would shuffle all modes in all bins, but only shuffling the
        ! well populated bins makes debugging easier (ordered, small-pop bins).

        ! Using Durstenfeld algorithm to shuffle.
        ! NOTE: random_seed() might be compiler dependent.
        ! Check to see if generating different random numbers every time.
        call random_seed()
        do i = 1, nbin
           if (mode_points(2, i) - mode_points(1, i) + 1 .gt. limit) then ! shuffle
              do j = mode_points(2, i), mode_points(1, i)+1, -1
                 call random_number(r)
                 randpos = int(r * (j - mode_points(1, i) + 1)) &
                      + mode_points(1, i)
                 temp_mode = modes(j)
                 modes(j) = modes(randpos)
                 modes(randpos) = temp_mode
              end do
           end if
        end do

        ! Determine the size of the new modes array, and allocate it.
        nmode_new = 0
        do i = 1, nbin
           counter = mode_points(2, i) - mode_points(1, i)
           if (counter .lt. limit) then
              nmode_new = nmode_new + mode_points(2, i) - mode_points(1, i) + 1
           else
              nmode_new = nmode_new + limit
           end if
        end do
        allocate(modes_new(nmode_new))

        ! Fill in the new modes array.
        counter = 0
        do i = 1, nbin
           if (mode_points(2, i) - mode_points(1, i) + 1 .le. limit) then
              do j = mode_points(1, i), mode_points(2, i) 
                 counter = counter + 1
                 modes_new(counter) = modes(j)
              end do
           else
              do j = mode_points(1, i), mode_points(1, i) + limit - 1
                 counter = counter + 1
                 modes_new(counter) = modes(j)
              end do
           end if
        end do

        ! Replace the modes array with the new one.
        deallocate(modes)
        allocate(modes(nmode_new))
        modes = modes_new
        deallocate(modes_new)

     end if

!     print*, 'Final modes up to 15 (id, msq, bin):-'
!     if (nmode_new .gt. 15) then
!        do i = 1, 15
!           print*, modes(i)%id, modes(i)%msq, modes(i)%bin
!        end do
!     else
!        do i = 1, nmode_new
!           print*, modes(i)%id, modes(i)%msq, modes(i)%bin
!        end do
!     end if

!     write(*, '(A,I10,A,I10,A)') 'Read ', nmode, ' modes and will use ', nmode_new, &
!          ' modes'

   end subroutine read_modes

!**********************************************************************
end module mod_ft_old
!**********************************************************************

! SCRAP CODE

!   function fft_dens_lattice(lattice) result(ft)

!      size_array = (/ size(lattice, 1), size(lattice, 2), size(lattice, 3) /)

!      status = DftiCreateDescriptor(Desc_Handle, DFTI_SINGLE, DFTI_COMPLEX, &
!                                    3, size_array)
!      status = DftiCommitDescriptor(Desc_Handle)


!**********************************************************************
