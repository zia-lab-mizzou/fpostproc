! TODO add group selection and exclusion

module mod_stat_func

  use, intrinsic :: iso_fortran_env
  use, intrinsic :: iso_c_binding
  use mod_constants, only : pi, twopi
  use mod_particle, only : particle_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  implicit none
  private

  public :: eval_vfrac
  public :: init_ffts, close_ffts, eval_fabric, eval_fabric_part
  public :: eval_rdf, eval_nc, eval_ssf_3d, tidy_ssf, tidy_ssf_rad
  public :: eval_pair_dist_func, symmetrize_3d
  public :: get_mean_pos, get_displ_var, get_displ_var_sq, get_displ_var_cr
  public :: cell_stress, eval_cluster_id, eval_order_parameter, eval_order_parameterq4
  public :: eval_mean_bond_dist,eval_bin_bond_dist,eval_mean_bond_dist_nc
  public :: eval_isf_self_3d,eval_isf_full_3d,eval_fft_dens_3d

contains

  ! ----------------------------------------------------------------------
  ! Calculate the volume fraction of the system being studied
  ! Should work for rectangular and triclinic boxes
  ! ----------------------------------------------------------------------
  
  subroutine eval_vfrac(cells, vfrac)

    type(cells_t), intent(inout) :: cells
    real, intent(inout) :: vfrac
    integer :: i,nparticles,ntypes,type_tmp
    logical :: typeradius_flag
    real :: xprd,yprd,zprd,v_part,v_box
    real, dimension(:), pointer :: typeradius
    integer, dimension(:), pointer :: type
    real, dimension(:), allocatable :: v_ntype

    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes

    ! set up volume of each type, if types are present
    if (typeradius_flag) then
       typeradius => cells%particle%typeradius
       type => cells%particle%type
       allocate(v_ntype(ntypes))
       v_ntype(:) = 4*pi*(typeradius(:)**3)/3
    end if

    ! simulation box volume
    xprd = cells%domain%xprd
    yprd = cells%domain%yprd
    zprd = cells%domain%zprd
    v_box = xprd*yprd*zprd

    ! initialize particle volume counter
    v_part = 0.0

    ! use cell lists to loop over all particles in the system
    ! WHAT HAPPENS FOR POINT PARTICLES?
    do i = 1, nparticles
       type_tmp = type(i)
       v_part = v_part + v_ntype(type_tmp)
    end do

    ! calculate the final volume fraction
    vfrac = v_part/v_box


  end subroutine eval_vfrac



  ! ----------------------------------------------------------------------
  ! Calculate RDF in periodic/nonperiodic and rectangular/triclinic boxes
  ! TODO add group specification
  ! ----------------------------------------------------------------------

  subroutine eval_rdf(cells, group, agroup, bgroup, rcut, nbin, rdf)

    type(cells_t), intent(inout) :: cells
    type(group_t), intent(in), target :: group
    real, intent(in) :: rcut
    integer, intent(in) :: agroup,bgroup,nbin
    real, dimension(:), intent(out) :: rdf
    integer(int64), dimension(nbin) :: hist
    real :: rsq,rcutsq,normfac,rgrid,inv_rgrid,delx,dely,delz,xtmp,ytmp,ztmp
    integer :: i,j,c,d,n,groupbit_a,groupbit_b,othergroup,neighcell,na,nb
    integer :: nparticles
    integer, dimension(:), pointer :: mask => null()
    logical :: groups_identical

    ! If group ids are not equal, cannot have a particle in both groups.
    ! TODO insert this conditional with a new group function

    na = group%count(agroup)
    nb = group%count(bgroup)
    if (na < 1) then
       write(*, '(a)') 'group A in pair distribution function has bad count'
       stop
    end if
    if (nb < 1) then
       write(*, '(a)') 'group B in pair distribution function has bad count'
       stop
    end if

    groups_identical = .false.
    if (agroup == bgroup) groups_identical = .true.

    groupbit_a = group%bitmask(agroup)
    groupbit_b = group%bitmask(bgroup)
    mask => group%mask
    rcutsq = rcut * rcut
    rgrid = rcut / nbin
    inv_rgrid = 1.0 / rgrid
    hist(:) = 0

    ! check if particle coordinates need wrapping, and wrap if they do
    ! can do this here, because no reason to be in lamda coords

    if (.not. cells%domain%particles_inside()) then
       call cells%domain%remap()
    end if

    ! if cells are functional, then use them to calculate RDF
    ! if not, loop over particle pairs

    if_cells: if (cells%cells_exist) then

       cell_loop: do c = 1, cells%ncells

          ! below, all in the ith cell
          ! loop over all particles i,j in this cell c
          ! cellhead(c) is the first i particle
          ! the first (distinct) j is the next of i

          i = cells%cellhead(c)
          self_loop: do while (i > 0)

             if (iand(mask(i),groupbit_a) /= 0) then
                othergroup = groupbit_b
             else if (iand(mask(i),groupbit_b) /= 0) then
                othergroup = groupbit_a
             else
                i = cells%next(i)
                cycle
             end if

             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             j = cells%next(i)
             do while (j > 0)
                if (iand(mask(j),othergroup) == 0) then
                   j = cells%next(j)
                   cycle
                end if
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                if (rsq < rcutsq) then
                   n = floor(sqrt(rsq) * inv_rgrid) + 1
                   n = min(n,nbin)
                   hist(n) = hist(n) + 1
                end if
                j = cells%next(j)
             end do
             i = cells%next(i)
          end do self_loop

          ! loop over distinct cells
          ! should I make this part of previous loop?

          i = cells%cellhead(c)
          distinct_loop: do while (i > 0)

             if (iand(mask(i),groupbit_a) /= 0) then
                othergroup = groupbit_b
             else if (iand(mask(i),groupbit_b) /= 0) then
                othergroup = groupbit_a
             else
                i = cells%next(i)
                cycle
             end if

             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do d = 1, cells%nstencil
                neighcell = cells%stencil_neighbor(c,d)
                if (neighcell == 0) cycle
                j = cells%cellhead(neighcell)
                do while (j > 0)
                   if (iand(mask(j),othergroup) == 0) then
                      j = cells%next(j)
                      cycle
                   end if
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   if (rsq < rcutsq) then
                      n = floor(sqrt(rsq) * inv_rgrid) + 1
                      n = min(n,nbin)
                      hist(n) = hist(n) + 1
                   end if
                   j = cells%next(j)
                end do
             end do
             i = cells%next(i)
          end do distinct_loop
       end do cell_loop

    else ! not using cells, so loop over all pairs

       nparticles = cells%particle%nparticles
       do i = 1, nparticles-1

          if (iand(mask(i),groupbit_a) /= 0) then
             othergroup = groupbit_b
          else if (iand(mask(i),groupbit_b) /= 0) then
             othergroup = groupbit_a
          else
             cycle
          end if

          xtmp = cells%particle%x(1,i)
          ytmp = cells%particle%x(2,i)
          ztmp = cells%particle%x(3,i)
          do j = i+1, nparticles
             if (iand(mask(j),othergroup) == 0) cycle
             delx = cells%particle%x(1,j) - xtmp
             dely = cells%particle%x(2,j) - ytmp
             delz = cells%particle%x(3,j) - ztmp
             call cells%domain%minimum_image(delx, dely, delz)
             rsq = delx*delx + dely*dely + delz*delz
             if (rsq < rcutsq) then
                n = floor(sqrt(rsq) * inv_rgrid) + 1
                n = min(n,nbin)
                hist(n) = hist(n) + 1
             end if
          end do
       end do

    end if if_cells

    ! Normalize the histogram.

    normfac = 3 * product(cells%domain%prd(:)) &
         / (4 * pi * rgrid*rgrid*rgrid * na * nb)
    if (groups_identical) normfac = normfac * 2
    forall (i = 1:nbin) rdf(i) = hist(i) * normfac / (3*i*i - 3*i + 1)

  end subroutine eval_rdf

  ! ----------------------------------------------------------------------
  ! Calculate pair distribution function
  ! nbin is the # of bins in one direction (used for all)
  ! rmax is the maximum distance along one axis
  ! in reality, the maximum distance is sqrt(3)*rmax in corner of cube
  ! agroup is the group of the center particle
  ! bgroup is the group of surrounding particles
  ! ----------------------------------------------------------------------

  subroutine eval_pair_dist_func(cells, group, agroup, bgroup, rmax, nbin, g)

    class(cells_t), intent(inout) :: cells
    class(group_t), intent(in), target :: group
    real, intent(in) :: rmax
    integer, intent(in) :: agroup,bgroup,nbin
    real, dimension(:,:,:), intent(out) :: g
    integer(int64), dimension(nbin,nbin,nbin) :: hist
    real :: normfac,delx,dely,delz,xtmp,ytmp,ztmp,rgrid,inv_rgrid
    integer :: i,j,c,d,xbin,ybin,zbin,neighcell,na,nb,groupbit_a,groupbit_b
    integer :: nparticles,othergroup,vec_sign
    logical :: groups_identical
    integer, dimension(:), pointer :: mask => null()

    if (mod(nbin,2) == 0) then
       write(*, '(a)') 'nbin must be odd in eval_pair_dist_func'
       stop
    end if

    na = group%count(agroup)
    nb = group%count(bgroup)
    if (na < 1) then
       write(*, '(a)') 'group A in pair distribution function has bad count'
       stop
    end if
    if (nb < 1) then
       write(*, '(a)') 'group B in pair distribution function has bad count'
       stop
    end if

    groups_identical = .false.
    if (agroup == bgroup) groups_identical = .true.
    mask => group%mask
    hist(:,:,:) = 0_int64
    rgrid = 2 * rmax / nbin
    inv_rgrid = 1.0 / rgrid

    ! check if particle coordinates need wrapping, and wrap if they do
    ! can do this here, because no reason to be in lamda coords

    if (.not. cells%domain%particles_inside()) then
       call cells%domain%remap()
    end if

    ! if cells are functional, then use them to calculate RDF
    ! if not, loop over particle pairs

    groupbit_a = group%bitmask(agroup)
    groupbit_b = group%bitmask(bgroup)

    if_cells: if (cells%cells_exist) then

       cell_loop: do c = 1, cells%ncells

          ! below, all in the ith cell
          ! loop over all particles i,j in this cell c
          ! cellhead(c) is the first i particle
          ! the first (distinct) j is the next of i

          i = cells%cellhead(c)
          self_loop: do while (i > 0)

             if (iand(mask(i),groupbit_a) /= 0) then
                vec_sign = 1
                othergroup = groupbit_b
             else if (iand(mask(i),groupbit_b) /= 0) then
                vec_sign = -1
                othergroup = groupbit_a
             else
                i = cells%next(i)
                cycle
             end if

             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)

             j = cells%next(i)
             do while (j > 0)

                if (iand(mask(j),othergroup) == 0) then
                   j = cells%next(j)
                   cycle
                end if

                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                delx = vec_sign * delx
                dely = vec_sign * dely
                delz = vec_sign * delz

                call cells%domain%minimum_image(delx, dely, delz)

                xbin = ceiling( (delx+rmax) * inv_rgrid )
                ybin = ceiling( (dely+rmax) * inv_rgrid )
                zbin = ceiling( (delz+rmax) * inv_rgrid )

                if ((xbin < 1) .or. (xbin > nbin)) then
                   j = cells%next(j)
                   cycle
                end if
                if ((ybin < 1) .or. (ybin > nbin)) then
                   j = cells%next(j)
                   cycle
                end if
                if ((zbin < 1) .or. (zbin > nbin)) then
                   j = cells%next(j)
                   cycle
                end if
                
                hist(xbin,ybin,zbin) = hist(xbin,ybin,zbin) + 1
                j = cells%next(j)
             end do
             i = cells%next(i)
          end do self_loop

          ! loop over distinct cells
          ! should I make this part of previous loop?

          i = cells%cellhead(c)
          distinct_loop: do while (i > 0)

             if (iand(mask(i),groupbit_a) /= 0) then
                vec_sign = 1
                othergroup = groupbit_b
             else if (iand(mask(i),groupbit_b) /= 0) then
                vec_sign = -1
                othergroup = groupbit_a
             else
                i = cells%next(i)
                cycle
             end if

             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)

             do d = 1, cells%nstencil
                neighcell = cells%stencil_neighbor(c,d)
                if (neighcell == 0) cycle
                j = cells%cellhead(neighcell)
                do while (j > 0)

                   if (iand(mask(j),othergroup) == 0) then
                      j = cells%next(j)
                      cycle
                   end if

                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   delx = vec_sign * delx
                   dely = vec_sign * dely
                   delz = vec_sign * delz
                   call cells%domain%minimum_image(delx, dely, delz)

                   xbin = ceiling( (delx+rmax) * inv_rgrid )
                   ybin = ceiling( (dely+rmax) * inv_rgrid )
                   zbin = ceiling( (delz+rmax) * inv_rgrid )

                   if ((xbin < 1) .or. (xbin > nbin)) then
                      j = cells%next(j)
                      cycle
                   end if
                   if ((ybin < 1) .or. (ybin > nbin)) then
                      j = cells%next(j)
                      cycle
                   end if
                   if ((zbin < 1) .or. (zbin > nbin)) then
                      j = cells%next(j)
                      cycle
                   end if
                
                   hist(xbin,ybin,zbin) = hist(xbin,ybin,zbin) + 1

                   j = cells%next(j)
                end do
             end do
             i = cells%next(i)
          end do distinct_loop
       end do cell_loop

    else ! not using cells, so loop over all pairs

       nparticles = cells%particle%nparticles
       do i = 1, nparticles-1

          if (iand(mask(i),groupbit_a) /= 0) then
             vec_sign = 1
             othergroup = groupbit_b
          else if (iand(mask(i),groupbit_b) /= 0) then
             vec_sign = -1
             othergroup = groupbit_a
          else
             cycle
          end if

          xtmp = cells%particle%x(1,i)
          ytmp = cells%particle%x(2,i)
          ztmp = cells%particle%x(3,i)
          do j = i+1, nparticles
             if (iand(mask(j),othergroup) == 0) cycle
             delx = cells%particle%x(1,j) - xtmp
             dely = cells%particle%x(2,j) - ytmp
             delz = cells%particle%x(3,j) - ztmp
             delx = vec_sign * delx
             dely = vec_sign * dely
             delz = vec_sign * delz
             call cells%domain%minimum_image(delx, dely, delz)

             xbin = ceiling( (delx+rmax) * inv_rgrid )
             ybin = ceiling( (dely+rmax) * inv_rgrid )
             zbin = ceiling( (delz+rmax) * inv_rgrid )

             if (xbin < 1) cycle
             if (ybin < 1) cycle
             if (zbin < 1) cycle
             if (xbin > nbin) cycle
             if (ybin > nbin) cycle
             if (zbin > nbin) cycle
                
             hist(xbin,ybin,zbin) = hist(xbin,ybin,zbin) + 1

          end do
       end do

    end if if_cells

    ! Normalize the histogram.

    normfac = product(cells%domain%prd(:)) &
         / (rgrid*rgrid*rgrid * na * nb)
    if (groups_identical) normfac = 2 * normfac
    g(:,:,:) = normfac * hist(:,:,:)
    if (groups_identical) call symmetrize_3d(g(:,:,:))

  end subroutine eval_pair_dist_func

  ! ----------------------------------------------------------------------
  ! Calculate contact numbers, with/without cells in
  ! rectangular/triclinic box in periodic/nonperiodic boundaries
  ! Can apply a cutoff based on a_i+a_j+delta (TRUE) or a uniform
  ! cutoff (FALSE) in the yes_delta argument.  The distance r that follows
  ! is either the delta or the uniform cutoff.
  ! Get the number of bins from the dimensions of the ncdist array
  ! Assuming that r>0 if yes_delta is false
  ! And that r+a_i+a_j > 0 for all i,j if yes_delta is true
  ! ----------------------------------------------------------------------

  subroutine eval_nc(cells, yes_delta, r, nc)

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    integer, dimension(:), intent(inout) :: nc
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp
    real, dimension(:,:), allocatable :: rsqmat

    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes

    ! default rcutsq for yes_delta is false
    rcutsq = r*r

    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                         + cells%particle%typeradius(j) &
                         + r
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if

    nc(:) = 0

    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff

    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_uniform

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch

  end subroutine eval_nc

  ! ----------------------------------------------------------------------
  ! Find the fabric tensor, as defined in Eqn 14 in Aarons, Sun,
  ! & Sundaresan 2010
  ! Code here is based on loops in the eval_nc section
  ! ----------------------------------------------------------------------

  subroutine eval_fabric(cells, yes_delta, r, fabric)

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    real, dimension(6), intent(inout) :: fabric
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp
    real :: Rxx,Ryy,Rzz,Ryz,Rxz,Rxy
    real :: nx,ny,nz,norm,vfrac
    real :: c_xx,c_yy,c_zz,c_yz,c_xz,c_xy
    real :: y_xx,y_yy,y_zz,y_yz,y_xz,y_xy
    real :: t_xx,t_yy,t_zz,t_yz,t_xz,t_xy
    real, dimension(:,:), allocatable :: rsqmat

    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes

    ! default rcutsq for yes_delta is false
    rcutsq = r*r

    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                         + cells%particle%typeradius(j) &
                         + r
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if

    fabric(:) = 0.0
    Rxx = 0.0
    Ryy = 0.0
    Rzz = 0.0
    Ryz = 0.0
    Rxz = 0.0
    Rxy = 0.0
    
    ! Kahan summation variables
    c_xx = 0.0
    c_yy = 0.0
    c_zz = 0.0
    c_yz = 0.0
    c_xz = 0.0
    c_xy = 0.0

    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff
    
    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   if (rsq < rcutsq) then
                      norm = sqrt(rsq)
                      nx = delx/norm
                      ny = dely/norm
                      nz = delz/norm
                      ! Kahan summation for xx component
                      y_xx = nx*nx - c_xx
                      t_xx = Rxx + y_xx
                      c_xx = (t_xx - Rxx) - y_xx
                      Rxx = t_xx
                      ! Kahan summation for yy component
                      y_yy = ny*ny - c_yy
                      t_yy = Ryy + y_yy
                      c_yy = (t_yy - Ryy) - y_yy
                      Ryy = t_yy
                      ! Kahan summation for zz component
                      y_zz = nz*nz - c_zz
                      t_zz = Rzz + y_zz
                      c_zz = (t_zz - Rzz) - y_zz
                      Rzz = t_zz
                      ! Kahan summation for yz component
                      y_yz = ny*nz - c_yz
                      t_yz = Ryz + y_yz
                      c_yz = (t_yz - Ryz) - y_yz
                      Ryz = t_yz
                      ! Kahan summation for xz component
                      y_xz = nx*nz - c_xz
                      t_xz = Rxz + y_xz
                      c_xz = (t_xz - Rxz) - y_xz
                      Rxz = t_xz
                      ! Kahan summation for xy component  
                      y_xy = nx*ny - c_xy
                      t_xy = Rxy + y_xy
                      c_xy = (t_xy - Rxy) - y_xy
                      Rxy = t_xy

!                      Rxx = nx*nx + Rxx
!                      Ryy = ny*ny + Ryy
!                      Rzz = nz*nz + Rzz
!                      Ryz = ny*nz + Ryz
!                      Rxz = nx*nz + Rxz
!                      Rxy = nx*ny + Rxy
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop? 

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      if (rsq < rcutsq) then
                         norm = sqrt(rsq)
                         nx = delx/norm
                         ny = dely/norm
                         nz = delz/norm
                         ! Kahan summation for xx component
                         y_xx = nx*nx - c_xx
                         t_xx = Rxx + y_xx
                         c_xx = (t_xx - Rxx) - y_xx
                         Rxx = t_xx
                         ! Kahan summation for yy component
                         y_yy = ny*ny - c_yy
                         t_yy = Ryy + y_yy
                         c_yy = (t_yy - Ryy) - y_yy
                         Ryy = t_yy
                         ! Kahan summation for zz component
                         y_zz = nz*nz - c_zz
                         t_zz = Rzz + y_zz
                         c_zz = (t_zz - Rzz) - y_zz
                         Rzz = t_zz
                         ! Kahan summation for yz component
                         y_yz = ny*nz - c_yz
                         t_yz = Ryz + y_yz
                         c_yz = (t_yz - Ryz) - y_yz
                         Ryz = t_yz
                         ! Kahan summation for xz component
                         y_xz = nx*nz - c_xz
                         t_xz = Rxz + y_xz
                         c_xz = (t_xz - Rxz) - y_xz
                         Rxz = t_xz
                         ! Kahan summation for xy component
                         y_xy = nx*ny - c_xy
                         t_xy = Rxy + y_xy
                         c_xy = (t_xy - Rxy) - y_xy
                         Rxy = t_xy

!                         Rxx = nx*nx + Rxx
!                         Ryy = ny*ny + Ryy
!                         Rzz = nz*nz + Rzz
!                         Ryz = ny*nz + Ryz
!                         Rxz = nx*nz + Rxz
!                         Rxy = nx*ny + Rxy
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_uniform

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                if (rsq < rcutsq) then
                   norm = sqrt(rsq)
                   nx = delx/norm
                   ny = dely/norm
                   nz = delz/norm
                   ! Kahan summation for xx component
                   y_xx = nx*nx - c_xx
                   t_xx = Rxx + y_xx
                   c_xx = (t_xx - Rxx) - y_xx
                   Rxx = t_xx
                   ! Kahan summation for yy component
                   y_yy = ny*ny - c_yy
                   t_yy = Ryy + y_yy
                   c_yy = (t_yy - Ryy) - y_yy
                   Ryy = t_yy
                   ! Kahan summation for zz component
                   y_zz = nz*nz - c_zz
                   t_zz = Rzz + y_zz
                   c_zz = (t_zz - Rzz) - y_zz
                   Rzz = t_zz
                   ! Kahan summation for yz component
                   y_yz = ny*nz - c_yz
                   t_yz = Ryz + y_yz
                   c_yz = (t_yz - Ryz) - y_yz
                   Ryz = t_yz
                   ! Kahan summation for xz component
                   y_xz = nx*nz - c_xz
                   t_xz = Rxz + y_xz
                   c_xz = (t_xz - Rxz) - y_xz
                   Rxz = t_xz
                   ! Kahan summation for xy component
                   y_xy = nx*ny - c_xy
                   t_xy = Rxy + y_xy
                   c_xy = (t_xy - Rxy) - y_xy
                   Rxy = t_xy
                   
!                   Rxx = nx*nx + Rxx
!                   Ryy = ny*ny + Ryy
!                   Rzz = nz*nz + Rzz
!                   Ryz = ny*nz + Ryz
!                   Rxz = nx*nz + Rxz
!                   Rxy = nx*ny + Rxy
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs 

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      norm = sqrt(rsq)
                      nx = delx/norm
                      ny = dely/norm
                      nz = delz/norm
                      ! Kahan summation for xx component
                      y_xx = nx*nx - c_xx
                      t_xx = Rxx + y_xx
                      c_xx = (t_xx - Rxx) - y_xx
                      Rxx = t_xx
                      ! Kahan summation for yy component
                      y_yy = ny*ny - c_yy
                      t_yy = Ryy + y_yy
                      c_yy = (t_yy - Ryy) - y_yy
                      Ryy = t_yy
                      ! Kahan summation for zz component
                      y_zz = nz*nz - c_zz
                      t_zz = Rzz + y_zz
                      c_zz = (t_zz - Rzz) - y_zz
                      Rzz = t_zz
                      ! Kahan summation for yz component
                      y_yz = ny*nz - c_yz
                      t_yz = Ryz + y_yz
                      c_yz = (t_yz - Ryz) - y_yz
                      Ryz = t_yz
                      ! Kahan summation for xz component
                      y_xz = nx*nz - c_xz
                      t_xz = Rxz + y_xz
                      c_xz = (t_xz - Rxz) - y_xz
                      Rxz = t_xz
                      ! Kahan summation for xy component
                      y_xy = nx*ny - c_xy
                      t_xy = Rxy + y_xy
                      c_xy = (t_xy - Rxy) - y_xy
                      Rxy = t_xy

!                      Rxx = nx*nx + Rxx
!                      Ryy = ny*ny + Ryy
!                      Rzz = nz*nz + Rzz
!                      Ryz = ny*nz + Ryz
!                      Rxz = nx*nz + Rxz
!                      Rxy = nx*ny + Rxy
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         norm = sqrt(rsq)
                         nx = delx/norm
                         ny = dely/norm
                         nz = delz/norm
                         ! Kahan summation for xx component
                         y_xx = nx*nx - c_xx
                         t_xx = Rxx + y_xx
                         c_xx = (t_xx - Rxx) - y_xx
                         Rxx = t_xx
                         ! Kahan summation for yy component
                         y_yy = ny*ny - c_yy
                         t_yy = Ryy + y_yy
                         c_yy = (t_yy - Ryy) - y_yy
                         Ryy = t_yy
                         ! Kahan summation for zz component
                         y_zz = nz*nz - c_zz
                         t_zz = Rzz + y_zz
                         c_zz = (t_zz - Rzz) - y_zz
                         Rzz = t_zz
                         ! Kahan summation for yz component
                         y_yz = ny*nz - c_yz
                         t_yz = Ryz + y_yz
                         c_yz = (t_yz - Ryz) - y_yz
                         Ryz = t_yz
                         ! Kahan summation for xz component
                         y_xz = nx*nz - c_xz
                         t_xz = Rxz + y_xz
                         c_xz = (t_xz - Rxz) - y_xz
                         Rxz = t_xz
                         ! Kahan summation for xy component
                         y_xy = nx*ny - c_xy
                         t_xy = Rxy + y_xy
                         c_xy = (t_xy - Rxy) - y_xy
                         Rxy = t_xy
                         

!                         Rxx = nx*nx + Rxx
!                         Ryy = ny*ny + Ryy
!                         Rzz = nz*nz + Rzz
!                         Ryz = ny*nz + Ryz
!                         Rxz = nx*nz + Rxz
!                         Rxy = nx*ny + Rxy
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   norm = sqrt(rsq)
                   nx = delx/norm
                   ny = dely/norm
                   nz = delz/norm
                   ! Kahan summation for xx component
                   y_xx = nx*nx - c_xx
                   t_xx = Rxx + y_xx
                   c_xx = (t_xx - Rxx) - y_xx
                   Rxx = t_xx
                   ! Kahan summation for yy component
                   y_yy = ny*ny - c_yy
                   t_yy = Ryy + y_yy
                   c_yy = (t_yy - Ryy) - y_yy
                   Ryy = t_yy
                   ! Kahan summation for zz component
                   y_zz = nz*nz - c_zz
                   t_zz = Rzz + y_zz
                   c_zz = (t_zz - Rzz) - y_zz
                   Rzz = t_zz
                   ! Kahan summation for yz component
                   y_yz = ny*nz - c_yz
                   t_yz = Ryz + y_yz
                   c_yz = (t_yz - Ryz) - y_yz
                   Ryz = t_yz
                   ! Kahan summation for xz component
                   y_xz = nx*nz - c_xz
                   t_xz = Rxz + y_xz
                   c_xz = (t_xz - Rxz) - y_xz
                   Rxz = t_xz
                   ! Kahan summation for xy component
                   y_xy = nx*ny - c_xy
                   t_xy = Rxy + y_xy
                   c_xy = (t_xy - Rxy) - y_xy
                   Rxy = t_xy

!                   Rxx = nx*nx + Rxx
!                   Ryy = ny*ny + Ryy
!                   Rzz = nz*nz + Rzz
!                   Ryz = ny*nz + Ryz
!                   Rxz = nx*nz + Rxz
!                   Rxy = nx*ny + Rxy
                end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      norm = sqrt(rsq)
                      nx = delx/norm
                      ny = dely/norm
                      nz = delz/norm
                      ! Kahan summation for xx component
                      y_xx = nx*nx - c_xx
                      t_xx = Rxx + y_xx
                      c_xx = (t_xx - Rxx) - y_xx
                      Rxx = t_xx
                      ! Kahan summation for yy component
                      y_yy = ny*ny - c_yy
                      t_yy = Ryy + y_yy
                      c_yy = (t_yy - Ryy) - y_yy
                      Ryy = t_yy
                      ! Kahan summation for zz component
                      y_zz = nz*nz - c_zz
                      t_zz = Rzz + y_zz
                      c_zz = (t_zz - Rzz) - y_zz
                      Rzz = t_zz
                      ! Kahan summation for yz component
                      y_yz = ny*nz - c_yz
                      t_yz = Ryz + y_yz
                      c_yz = (t_yz - Ryz) - y_yz
                      Ryz = t_yz
                      ! Kahan summation for xz component
                      y_xz = nx*nz - c_xz
                      t_xz = Rxz + y_xz
                      c_xz = (t_xz - Rxz) - y_xz
                      Rxz = t_xz
                      ! Kahan summation for xy component
                      y_xy = nx*ny - c_xy
                      t_xy = Rxy + y_xy
                      c_xy = (t_xy - Rxy) - y_xy
                      Rxy = t_xy

!                      Rxx = nx*nx + Rxx
!                      Ryy = ny*ny + Ryy
!                      Rzz = nz*nz + Rzz
!                      Ryz = ny*nz + Ryz
!                      Rxz = nx*nz + Rxz
!                      Rxy = nx*ny + Rxy
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         norm = sqrt(rsq)
                         nx = delx/norm
                         ny = dely/norm
                         nz = delz/norm
                         ! Kahan summation for xx component
                         y_xx = nx*nx - c_xx
                         t_xx = Rxx + y_xx
                         c_xx = (t_xx - Rxx) - y_xx
                         Rxx = t_xx
                         ! Kahan summation for yy component
                         y_yy = ny*ny - c_yy
                         t_yy = Ryy + y_yy
                         c_yy = (t_yy - Ryy) - y_yy
                         Ryy = t_yy
                         ! Kahan summation for zz component
                         y_zz = nz*nz - c_zz
                         t_zz = Rzz + y_zz
                         c_zz = (t_zz - Rzz) - y_zz
                         Rzz = t_zz
                         ! Kahan summation for yz component
                         y_yz = ny*nz - c_yz
                         t_yz = Ryz + y_yz
                         c_yz = (t_yz - Ryz) - y_yz
                         Ryz = t_yz
                         ! Kahan summation for xz component
                         y_xz = nx*nz - c_xz
                         t_xz = Rxz + y_xz
                         c_xz = (t_xz - Rxz) - y_xz
                         Rxz = t_xz
                         ! Kahan summation for xy component
                         y_xy = nx*ny - c_xy
                         t_xy = Rxy + y_xy
                         c_xy = (t_xy - Rxy) - y_xy
                         Rxy = t_xy

!                         Rxx = nx*nx + Rxx
!                         Ryy = ny*ny + Ryy
!                         Rzz = nz*nz + Rzz
!                         Ryz = ny*nz + Ryz
!                         Rxz = nx*nz + Rxz
!                         Rxy = nx*ny + Rxy
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   norm = sqrt(rsq)
                   nx = delx/norm
                   ny = dely/norm
                   nz = delz/norm
                   ! Kahan summation for xx component
                   y_xx = nx*nx - c_xx
                   t_xx = Rxx + y_xx
                   c_xx = (t_xx - Rxx) - y_xx
                   Rxx = t_xx
                   ! Kahan summation for yy component
                   y_yy = ny*ny - c_yy
                   t_yy = Ryy + y_yy
                   c_yy = (t_yy - Ryy) - y_yy
                   Ryy = t_yy
                   ! Kahan summation for zz component
                   y_zz = nz*nz - c_zz
                   t_zz = Rzz + y_zz
                   c_zz = (t_zz - Rzz) - y_zz
                   Rzz = t_zz
                   ! Kahan summation for yz component
                   y_yz = ny*nz - c_yz
                   t_yz = Ryz + y_yz
                   c_yz = (t_yz - Ryz) - y_yz
                   Ryz = t_yz
                   ! Kahan summation for xz component
                   y_xz = nx*nz - c_xz
                   t_xz = Rxz + y_xz
                   c_xz = (t_xz - Rxz) - y_xz
                   Rxz = t_xz
                   ! Kahan summation for xy component
                   y_xy = nx*ny - c_xy
                   t_xy = Rxy + y_xy
                   c_xy = (t_xy - Rxy) - y_xy
                   Rxy = t_xy
                   
!                   Rxx = nx*nx + Rxx
!                   Ryy = ny*ny + Ryy
!                   Rzz = nz*nz + Rzz
!                   Ryz = ny*nz + Ryz
!                   Rxz = nx*nz + Rxz
!                   Rxy = nx*ny + Rxy
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch


    ! Normalize the output based on the volume fraction and 
    ! number of particles
    ! Be very careful with the order of this output!!!
    ! [Rxx, Ryy, Rzz, Ryz, Rxz, Rxy]
    ! Factor of 2 may not be necessary???
    call eval_vfrac(cells, vfrac)
    Rxx = 2*vfrac*Rxx/nparticles
    Ryy = 2*vfrac*Ryy/nparticles
    Rzz = 2*vfrac*Rzz/nparticles
    Ryz = 2*vfrac*Ryz/nparticles
    Rxz = 2*vfrac*Rxz/nparticles
    Rxy = 2*vfrac*Rxy/nparticles

    fabric(1) = Rxx
    fabric(2) = Ryy
    fabric(3) = Rzz
    fabric(4) = Ryz
    fabric(5) = Rxz
    fabric(6) = Rxy

  end subroutine eval_fabric

  ! ----------------------------------------------------------------------
  ! Find the fabric tensor per particle
  ! Code here is based on loops in the eval_fabric section
  !   which computes overall fabric tensor
  ! ----------------------------------------------------------------------

  subroutine eval_fabric_part(cells, yes_delta, r, fabric_part)

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    real, dimension(:,:), intent(inout) :: fabric_part
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp
    real :: nx,ny,nz,norm,vfrac
    real, dimension(:,:), allocatable :: rsqmat

    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes

    ! default rcutsq for yes_delta is false
    rcutsq = r*r

    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                         + cells%particle%typeradius(j) &
                         + r
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if

    fabric_part(:,:) = 0.0
    
    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff
    
    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   if (rsq < rcutsq) then
                      norm = sqrt(rsq)
                      nx = delx/norm
                      ny = dely/norm
                      nz = delz/norm

                      fabric_part(1,i) = nx*nx + fabric_part(1,i)
                      fabric_part(2,i) = ny*ny + fabric_part(2,i)
                      fabric_part(3,i) = nz*nz + fabric_part(3,i)
                      fabric_part(4,i) = ny*nz + fabric_part(4,i)
                      fabric_part(5,i) = nx*nz + fabric_part(5,i)
                      fabric_part(6,i) = nx*ny + fabric_part(6,i)
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop? 

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      if (rsq < rcutsq) then
                         norm = sqrt(rsq)
                         nx = delx/norm
                         ny = dely/norm
                         nz = delz/norm

                         fabric_part(1,i) = nx*nx + fabric_part(1,i)
                         fabric_part(2,i) = ny*ny + fabric_part(2,i)
                         fabric_part(3,i) = nz*nz + fabric_part(3,i)
                         fabric_part(4,i) = ny*nz + fabric_part(4,i)
                         fabric_part(5,i) = nx*nz + fabric_part(5,i)
                         fabric_part(6,i) = nx*ny + fabric_part(6,i)
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_uniform

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                if (rsq < rcutsq) then
                   norm = sqrt(rsq)
                   nx = delx/norm
                   ny = dely/norm
                   nz = delz/norm
                   
                   fabric_part(1,i) = nx*nx + fabric_part(1,i)
                   fabric_part(2,i) = ny*ny + fabric_part(2,i)
                   fabric_part(3,i) = nz*nz + fabric_part(3,i)
                   fabric_part(4,i) = ny*nz + fabric_part(4,i)
                   fabric_part(5,i) = nx*nz + fabric_part(5,i)
                   fabric_part(6,i) = nx*ny + fabric_part(6,i)
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs 

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      norm = sqrt(rsq)
                      nx = delx/norm
                      ny = dely/norm
                      nz = delz/norm

                      fabric_part(1,i) = nx*nx + fabric_part(1,i)
                      fabric_part(2,i) = ny*ny + fabric_part(2,i)
                      fabric_part(3,i) = nz*nz + fabric_part(3,i)
                      fabric_part(4,i) = ny*nz + fabric_part(4,i)
                      fabric_part(5,i) = nx*nz + fabric_part(5,i)
                      fabric_part(6,i) = nx*ny + fabric_part(6,i)
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         norm = sqrt(rsq)
                         nx = delx/norm
                         ny = dely/norm
                         nz = delz/norm

                         fabric_part(1,i) = nx*nx + fabric_part(1,i)
                         fabric_part(2,i) = ny*ny + fabric_part(2,i)
                         fabric_part(3,i) = nz*nz + fabric_part(3,i)
                         fabric_part(4,i) = ny*nz + fabric_part(4,i)
                         fabric_part(5,i) = nx*nz + fabric_part(5,i)
                         fabric_part(6,i) = nx*ny + fabric_part(6,i)
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   norm = sqrt(rsq)
                   nx = delx/norm
                   ny = dely/norm
                   nz = delz/norm

                   fabric_part(1,i) = nx*nx + fabric_part(1,i)
                   fabric_part(2,i) = ny*ny + fabric_part(2,i)
                   fabric_part(3,i) = nz*nz + fabric_part(3,i)
                   fabric_part(4,i) = ny*nz + fabric_part(4,i)
                   fabric_part(5,i) = nx*nz + fabric_part(5,i)
                   fabric_part(6,i) = nx*ny + fabric_part(6,i)
                 end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      norm = sqrt(rsq)
                      nx = delx/norm
                      ny = dely/norm
                      nz = delz/norm

                      fabric_part(1,i) = nx*nx + fabric_part(1,i)
                      fabric_part(2,i) = ny*ny + fabric_part(2,i)
                      fabric_part(3,i) = nz*nz + fabric_part(3,i)
                      fabric_part(4,i) = ny*nz + fabric_part(4,i)
                      fabric_part(5,i) = nx*nz + fabric_part(5,i)
                      fabric_part(6,i) = nx*ny + fabric_part(6,i)
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         norm = sqrt(rsq)
                         nx = delx/norm
                         ny = dely/norm
                         nz = delz/norm

                         fabric_part(1,i) = nx*nx + fabric_part(1,i)
                         fabric_part(2,i) = ny*ny + fabric_part(2,i)
                         fabric_part(3,i) = nz*nz + fabric_part(3,i)
                         fabric_part(4,i) = ny*nz + fabric_part(4,i)
                         fabric_part(5,i) = nx*nz + fabric_part(5,i)
                         fabric_part(6,i) = nx*ny + fabric_part(6,i)
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   norm = sqrt(rsq)
                   nx = delx/norm
                   ny = dely/norm
                   nz = delz/norm

                   fabric_part(1,i) = nx*nx + fabric_part(1,i)
                   fabric_part(2,i) = ny*ny + fabric_part(2,i)
                   fabric_part(3,i) = nz*nz + fabric_part(3,i)
                   fabric_part(4,i) = ny*nz + fabric_part(4,i)
                   fabric_part(5,i) = nx*nz + fabric_part(5,i)
                   fabric_part(6,i) = nx*ny + fabric_part(6,i)
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch


    ! Normalize the output based on the volume fraction and 
    ! number of particles
    ! Be very careful with the order of this output!!!
    ! [Rxx, Ryy, Rzz, Ryz, Rxz, Rxy]
    ! Factor of 2 may not be necessary???
    !call eval_vfrac(cells, vfrac)
    !fabric_part(:,:) = 2*vfrac/nparticles*fabric_part(:,:)

  end subroutine eval_fabric_part

  ! ----------------------------------------------------------------------
  ! initialize and close ffts
  ! ----------------------------------------------------------------------

  subroutine init_ffts()
    use fftw3
    integer(c_int) :: err
    err = fftw_init_threads()
  end subroutine init_ffts

  subroutine close_ffts()
    use fftw3
    call fftw_cleanup_threads()
  end subroutine close_ffts

  ! ----------------------------------------------------------------------
  ! Evaluate FT of density using FFTs
  ! I can't figure out the in-place transform
  ! Should eventually adjust number of cells to be multiples of small primes
  ! ----------------------------------------------------------------------

  subroutine eval_fft_dens_3d(cells, dens, ftdens)

    use mod_omp
    use fftw3

    type(cells_t), intent(inout) :: cells
    real(c_double), dimension(:,:,:), intent(inout), allocatable :: dens
    complex(c_double_complex), dimension(:,:,:), intent(inout), &
         allocatable :: ftdens
    type(c_ptr) :: plan = c_null_ptr
    integer :: ncells,nx,ny,nz,i,c,ix,iy,iz

    nx = cells%ncellx
    ny = cells%ncelly
    nz = cells%ncellz
    ncells = nx*ny*nz
    allocate(dens(nx,ny,nz))
    allocate(ftdens(nx/2+1,ny,nz))
    dens(:,:,:) = 0.0

    ! place particles into density bins (reset grid?)
    ! lazily using assign_particles rather than directly binning
    ! this calls for x2lamda if necessary and then puts back
    
    call cells%assign_particles()

    do iz = 1, nz
       do iy = 1, ny
          do ix = 1, nx
             c = (iz-1) * ny * nx &
               + (iy-1) * nx &
               + ix
             i = cells%cellhead(c)
             do while (i > 0)
                dens(ix,iy,iz) = dens(ix,iy,iz) + 1.0
                i = cells%next(i)
             end do
          end do
       end do
    end do

    ! out-of-place real-to-complex transform
    ! number of elements must be reversed, going between Fortran and C

    call fftw_plan_with_nthreads(omp_get_max_threads())
    plan = fftw_plan_dft_r2c_3d(nz,ny,nx,dens,ftdens,FFTW_ESTIMATE)

    call fftw_execute_dft_r2c(plan,dens,ftdens)
    call fftw_destroy_plan(plan)

  end subroutine eval_fft_dens_3d

  ! ----------------------------------------------------------------------
  ! Evaluate the static structure factor
  ! ----------------------------------------------------------------------

  subroutine eval_ssf_3d(cells, ssf)

    type(cells_t), intent(inout) :: cells
    real(c_double), dimension(:,:,:), allocatable :: dens
    complex(c_double_complex), dimension(:,:,:), allocatable :: ftdens
    real, dimension(:,:,:), allocatable :: ssf
    integer :: nx,ny,nz,nparticles

    call eval_fft_dens_3d(cells, dens, ftdens)

    nx = size(ftdens,1)
    ny = size(ftdens,2)
    nz = size(ftdens,3)
    nparticles = cells%particle%nparticles

    allocate(ssf(nx,ny,nz))
    ssf(:,:,:) = real(ftdens(:,:,:)*conjg(ftdens(:,:,:))) / nparticles

  end subroutine eval_ssf_3d

  ! ----------------------------------------------------------------------
  ! Tidy the static structure factor for presentation and determine
  ! wavevectors
  ! nq is the number of q-points per dimension to plot
  ! qmax is not strictly the maximum q but the maximum q in a principal
  !   direction (diagonals may have larger q)
  ! tidy_ssf is the binned, reoriented ssf, so that the (worthless)
  !   0-point (DC) is in the middle
  ! qmax must be positive
  ! usually want nq to be odd, so we can plot planes through the origin
  ! ----------------------------------------------------------------------

  subroutine tidy_ssf(cells, ssf_in, nq, qmax, ssf_out)

    type(cells_t), intent(in) :: cells
    real, dimension(:,:,:), intent(in) :: ssf_in
    integer, intent(in) :: nq
    real, intent(in) :: qmax
    real, dimension(:,:,:), allocatable, intent(inout) :: ssf_out
    integer, dimension(:,:,:), allocatable :: counts
    real, dimension(3) :: qvec
    real, dimension(6) :: h_inv
    integer :: nx,ny,nz,i,j,k,m,ii,jj,kk,xbin,ybin,zbin
    real :: inv_qgrid,ssf_val

    h_inv(:) = cells%domain%h_inv(:)

    ! nx,ny,nz are dimensions of the input ssf
    ! nx is about half of ncellx
    ! delq is the bin width

    nx = size(ssf_in,1)
    ny = size(ssf_in,2)
    nz = size(ssf_in,3)

    inv_qgrid = 0.5 * nq / qmax

    ! nq is the number of q-points in output image
    ! qmax is the maximum value of q in the
    ! principal directions

    allocate(ssf_out(nq,nq,nq))
    allocate(counts(nq,nq,nq))
    ssf_out(:,:,:) = 0.0
    counts(:,:,:) = 0

    ! loop over all points in the input ssf
    ! note that in input, the 1,1,1 index is q=0
    ! we want q=0 to be in the middle of the array
    ! have to correct for the fact that large y and
    ! z wavenumbers are actually also negative ones

    do k = 1, nz
       kk = modulo(k-1+nz/2,nz) - nz/2
       do j = 1, ny
          jj = modulo(j-1+ny/2,ny) - ny/2
          do i = 1, nx
             ii = i-1

             ! cycle if at 'DC' (q=0,0,0)
             ! otherwise get nonsense

             if (ii == 0) then
                if (jj == 0) then
                   if (kk == 0) then
                      cycle
                   end if
                end if
             end if

             ! get the qvec in 'lamda' coords, then transform to
             ! qvecs in real coords using (H^T)^(-1) = (h_inv)^T

             qvec = (/ real(ii), real(jj), real(kk) /)
             qvec(3) = h_inv(5)*qvec(1) + h_inv(4)*qvec(2) + h_inv(3)*qvec(3)
             qvec(2) = h_inv(6)*qvec(1) + h_inv(2)*qvec(2)
             qvec(1) = h_inv(1)*qvec(1)
             qvec(:) = twopi * qvec(:)

             ssf_val = ssf_in(i,j,k)

             ! once for the positive qvec and once for the negative qvec

             do m = 1, 2

                ! determine the bin to drop the ssf value into
                ! if 5 bins in each dimension, q=0 should
                ! go into the 3rd bin, and q=-4/5*qmax
                ! should go into the first bin

                xbin = ceiling( (qvec(1)+qmax) * inv_qgrid )
                ybin = ceiling( (qvec(2)+qmax) * inv_qgrid )
                zbin = ceiling( (qvec(3)+qmax) * inv_qgrid )

                ! flip the qvec before second and final cycle
                ! qvec not needed in rest of current loop

                qvec(:) = -qvec(:)

                if (xbin < 1) cycle
                if (ybin < 1) cycle
                if (zbin < 1) cycle
                if (xbin > nq) cycle
                if (ybin > nq) cycle
                if (zbin > nq) cycle

                ssf_out(xbin,ybin,zbin) = ssf_out(xbin,ybin,zbin) &
                     + ssf_val
                counts(xbin,ybin,zbin) = counts(xbin,ybin,zbin) + 1

             end do
          end do
       end do
    end do

    ! normalize by the counts in each bin
    ! if no counts, make the bin value 1.0 (for no structural correlation)

    do k = 1, nq
       do j = 1, nq
          do i = 1, nq
             m = counts(i,j,k)
             if (m /= 0) then
                ssf_out(i,j,k) = ssf_out(i,j,k) / m
             else
                ssf_out(i,j,k) = -1.0
             end if
          end do
       end do
    end do

    ! get sqrt(<q^2>)
    ! weight S(q)-1.0 by q^2, then integrate
    ! subtracting 1.0 so integral converges over infinite domain
    ! then divide by integral of S(q)-1.0 and take square root
    ! no need to multiply by delq

!    rt2mom = 0.0
!    denom = 0.0
!    do k = 1, nq
!       do j = 1, nq
!          do i = 1, nq
!             qvec(:) = (/ real(i), real(j), real(k) /)
!             qvec(:) = qvec(:) - 0.5
!             qvec(:) = delq * qvec(:)
!             qvec(:) = qvec(:) - qmax
!             qsq = dot_product(qvec,qvec)
!             ssf_val = ssf_out(i,j,k) - 1.0
!             rt2mom = rt2mom + qsq*ssf_val
!             denom = denom + ssf_val
!          end do
!       end do
!    end do
!    rt2mom = rt2mom / denom
!    rt2mom = sqrt(rt2mom)

  end subroutine tidy_ssf

  ! ----------------------------------------------------------------------
  ! Perform radial averaging over array with 0 in the center
  ! ----------------------------------------------------------------------

  subroutine tidy_ssf_rad(cells, ssf_in, nq, qmax, ssf_out)

    type(cells_t), intent(in) :: cells
    real, dimension(:,:,:), intent(in) :: ssf_in
    integer, intent(in) :: nq
    real, intent(in) :: qmax
    real, dimension(:), allocatable, intent(inout) :: ssf_out
    integer, dimension(:), allocatable :: counts
    real, dimension(3) :: qvec
    real, dimension(6) :: h_inv
    integer :: nx,ny,nz,i,j,k,m,ii,jj,kk,bin
    real :: inv_qgrid,qval

    h_inv(:) = cells%domain%h_inv(:)

    ! nx,ny,nz are dimensions of the input ssf
    ! nx is about half of ncellx
    ! delq is the bin width

    nx = size(ssf_in,1)
    ny = size(ssf_in,2)
    nz = size(ssf_in,3)

    inv_qgrid = nq / qmax

    ! nq is the number of q-points in output image
    ! qmax is the maximum value of q

    allocate(ssf_out(nq))
    allocate(counts(nq))
    ssf_out(:) = 0.0
    counts(:) = 0

    ! loop over all points in the input ssf
    ! note that in input, the 1,1,1 index is q=0
    ! we want q=0 to be in the middle of the array
    ! have to correct for the fact that large y and
    ! z wavenumbers are actually also negative ones

    do k = 1, nz
       kk = modulo(k-1+nz/2,nz) - nz/2
       do j = 1, ny
          jj = modulo(j-1+ny/2,ny) - ny/2
          do i = 1, nx
             ii = i-1

             if (ii == 0) then
                if (jj == 0) then
                   if (kk == 0) then
                      cycle
                   end if
                end if
             end if

             ! get the qvec in 'lamda' coords, then transform to
             ! qvecs in real coords using (H^T)^(-1) = (h_inv)^T

             qvec = (/ real(ii), real(jj), real(kk) /)
             qvec(3) = h_inv(5)*qvec(1) + h_inv(4)*qvec(2) + h_inv(3)*qvec(3)
             qvec(2) = h_inv(6)*qvec(1) + h_inv(2)*qvec(2)
             qvec(1) = h_inv(1)*qvec(1)
             qvec(:) = twopi * qvec(:)
             qval = dot_product(qvec,qvec)
             qval = sqrt(qval)

             ! determine the bin to drop the ssf value into

             bin = ceiling( qval * inv_qgrid )
             if (bin < 1) cycle
             if (bin > nq) cycle

             ssf_out(bin) = ssf_out(bin) + ssf_in(i,j,k)
             counts(bin) = counts(bin) + 1

          end do
       end do
    end do

    ! normalize by the counts in each bin
    ! if no counts, make the bin value 1.0 (for no structural correlation)

    do i = 1, nq
       m = counts(i)
       if (m /= 0) then
          ssf_out(i) = ssf_out(i) / m
       else
          ssf_out(i) = 1.0
       end if
    end do

    ! calculate sqrt(<q^2>)
    ! no need to multiply and then divide by delq (from integration)

!    rt2mom = 0.0
!    denom = 0.0
!    do i = 1, nq
!       qval = (real(i) - 0.5)*delq
!       ssf_val = ssf_out(i) - 1.0
!       denom = denom + ssf_val
!       rt2mom = rt2mom + qval*qval*ssf_val
!    end do

    ! not working great for now
    ! need to implement Gaussian fitting (otherwise sensitive to range)
    ! originally an output variable, but leaving here for now

!    rt2mom = rt2mom / denom
!    rt2mom = sqrt(rt2mom)

  end subroutine tidy_ssf_rad

  ! ----------------------------------------------------------------------
  ! Assign particles the lowest id of all the particles in the same
  ! cluster.  E.g. if particles 2--10 are all in the same cluster,
  ! they would all get the number 2.
  ! ----------------------------------------------------------------------

  subroutine eval_cluster_id(cells, yes_delta, r, cluster_id)

    type(cells_t), intent(in) :: cells
    integer, dimension(:), intent(inout) :: cluster_id
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    integer :: c,d,i,j,idi,idj,itype,jtype,neighcell
    integer :: ncells,nparticles,ntypes,cut_type
    real :: rsq,rcutsq,xtmp,ytmp,ztmp,delx,dely,delz,ri,rj
    logical :: done,typeradius_flag
    real, dimension(:,:), allocatable :: rsqmat

    ntypes = cells%particle%ntypes
    ncells = cells%ncells
    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag

    if (size(cluster_id) .ne. nparticles) then
       write(*, '(a)') 'cluster_id size must be nparticles'
       stop
    end if

    if (.not. cells%cells_exist) then
       write(*, '(a)') 'eval_cluster_id currently requires cells'
       stop
    end if

    ! select rcutsq method by what's loaded

    rcutsq = r*r
    cut_type = 1
    if (yes_delta) then
       if (typeradius_flag) then
          cut_type = 2
          allocate(rsqmat(ntypes,ntypes))
          do i = 1, ntypes
             do j = 1, ntypes
                rsqmat(i,j) = cells%particle%typeradius(i) &
                            + cells%particle%typeradius(j) &
                            + r
             end do
          end do
          rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
       else
          cut_type = 3
          write(*, '(a)') 'currently not supporting clusters and yes_delta w/o typeradius_flag'
          stop
       end if
    end if

    ! all particles start with own cluster id

    do i = 1, nparticles
       cluster_id(i) = i
    end do

    done = .false.
    progress_loop: do while (.not. done)
       done = .true.
       cell_c: do c = 1, ncells

          ! self loop over all particles in cell

          i = cells%cellhead(c)
          self_i: do while (i > 0)

             itype = cells%particle%type(i)
             idi = cluster_id(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)

             j = cells%next(i)
             self_j: do while (j > 0)

                idj = cluster_id(j)

                ! if particles already in same cluster, cycle

                if (idi .eq. idj) then
                   j = cells%next(j)
                   cycle
                end if

                ! check if contacting

                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz

                select case (cut_type)
                case(1)
                   continue
                case(2)
                   jtype = cells%particle%type(j)
                   rcutsq = rsqmat(itype,jtype)
                case(3)
                   ri = cells%particle%radius(i)
                   rj = cells%particle%radius(j)
                   rcutsq = (ri + rj + r)
                   rcutsq = rcutsq * rcutsq
                end select

                ! if outside of range, go to next j

                if (rsq >= rcutsq) then
                   j = cells%next(j)
                   cycle
                end if

                ! pick new cluster ids from minimum id in pair
                ! flag that we're not done yet
                ! need a cycle without updates to be done

                cluster_id(i) = min(idi,idj)
                cluster_id(j) = cluster_id(i)
                j = cells%next(j)
                done = .false.

             end do self_j
             i = cells%next(i)
          end do self_i

          ! distinct neighbor loop

          i = cells%cellhead(c)
          distinct_i: do while (i > 0)

             itype = cells%particle%type(i)
             idi = cluster_id(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)

             cell_d: do d = 1, cells%nstencil

                neighcell = cells%stencil_neighbor(c,d)
                if (neighcell == 0) cycle
                j = cells%cellhead(neighcell)

                distinct_j: do while (j > 0)

                   idj = cluster_id(j)

                   ! if particles already in same cluster, cycle

                   if (idi .eq. idj) then
                      j = cells%next(j)
                      cycle
                   end if

                   ! check if contacting

                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz

                   select case (cut_type)
                   case(1)
                      continue
                   case(2)
                      jtype = cells%particle%type(j)
                      rcutsq = rsqmat(itype,jtype)
                   case(3)
                      ri = cells%particle%radius(i)
                      rj = cells%particle%radius(j)
                      rcutsq = (ri + rj + r)
                      rcutsq = rcutsq * rcutsq
                   end select

                   ! if outside of range, go to next j

                   if (rsq >= rcutsq) then
                      j = cells%next(j)
                      cycle
                   end if

                   ! pick new cluster ids
                   ! flag that we're not done yet

                   cluster_id(i) = min(idi,idj)
                   cluster_id(j) = cluster_id(i)
                   j = cells%next(j)
                   done = .false.

                end do distinct_j
             end do cell_d
             i = cells%next(i)
          end do distinct_i
       end do cell_c
       if (done) exit
    end do progress_loop

  end subroutine eval_cluster_id

  ! ----------------------------------------------------------------------
  ! Get the mean position
  ! Used for subtracting mean drift from displacement
  ! ----------------------------------------------------------------------

  subroutine get_mean_pos(part, mean_pos)

    type(particle_t), intent(in) :: part
    real, dimension(3), intent(out) :: mean_pos
    integer :: nparticles, i

    nparticles = part%nparticles

    mean_pos = 0
    do i = 1, nparticles
       mean_pos = mean_pos + part%x(:,i)
    end do
    mean_pos = mean_pos / nparticles

  end subroutine get_mean_pos

  ! ----------------------------------------------------------------------
  ! Get symmetric tensor of self-displacement variance
  ! Used for getting MSD
  ! IMPORTANT: assumes unwrapped coords
  ! ----------------------------------------------------------------------

  subroutine get_displ_var(from, to, group, var, subtr_mean, igroup)

    type(particle_t), intent(in) :: from,to
    real, dimension(6), intent(out) :: var
    type(group_t), intent(in), target :: group
    logical, intent(in) :: subtr_mean
    integer, intent(in), optional :: igroup
    integer :: nparticles,i,groupsize,jgroup,groupbit
    real, dimension(3) :: mean_pos_from, mean_pos_to, mean_disp
    real :: delx,dely,delz
    integer, dimension(:), pointer :: mask => null()

    mask => group%mask

    ! check for consistency

    nparticles = from%nparticles
    if (to%nparticles /= nparticles) then
       write(*, '(a)') 'cannot get displ. var. between snaps with different nparticles'
       stop
    end if

    ! check if group exists (if present)
    ! exit with error if not
    ! if no group specified, group is all (group 1)

    if (.not. present(igroup)) then
       jgroup = 1
    else
       jgroup = igroup
    end if
    if (jgroup > group%ngroup) then
       write(*, '(a)') 'group ', jgroup, ' not found'
       stop
    end if
    if (jgroup < 1) then
       write(*, '(a)') 'group ', jgroup, ' not found'
       stop
    end if
    groupbit = group%bitmask(jgroup)

    ! loop over particles
    ! subtract mean motion if desired

    call get_mean_pos(from, mean_pos_from)
    call get_mean_pos(to, mean_pos_to)
    mean_disp = mean_pos_to - mean_pos_from

    var(:) = 0.0
    do i = 1, nparticles
       if (iand(mask(i),groupbit) /= 0) then
          delx = to%x(1,i) - from%x(1,i)
          dely = to%x(2,i) - from%x(2,i)
          delz = to%x(3,i) - from%x(3,i)
          if (subtr_mean) then
             delx = delx - mean_disp(1)
             dely = dely - mean_disp(2)
             delz = delz - mean_disp(3)
          end if
          var(1) = var(1) + delx*delx
          var(2) = var(2) + dely*dely
          var(3) = var(3) + delz*delz
          var(4) = var(4) + dely*delz
          var(5) = var(5) + delx*delz
          var(6) = var(6) + delx*dely
       end if
    end do

    ! normalize by size of group

    groupsize = group%count(jgroup)

    if (groupsize > 0) var(:) = var(:) / groupsize

  end subroutine get_displ_var

  ! ----------------------------------------------------------------------
  ! Get symmetric tensor of self-displacement 4th moment
  ! Used for getting non-gaussian parameter
  ! IMPORTANT: assumes unwrapped coords
  ! ----------------------------------------------------------------------

  subroutine get_displ_var_sq(from, to, group, var_sq, subtr_mean, igroup)

    type(particle_t), intent(in) :: from,to
    real, dimension(6), intent(out) :: var_sq
    type(group_t), intent(in), target :: group
    logical, intent(in) :: subtr_mean
    integer, intent(in), optional :: igroup
    integer :: nparticles,i,groupsize,jgroup,groupbit
    real, dimension(3) :: mean_pos_from, mean_pos_to, mean_disp
    real :: delx,dely,delz
    integer, dimension(:), pointer :: mask => null()

    mask => group%mask

    ! check for consistency

    nparticles = from%nparticles
    if (to%nparticles /= nparticles) then
       write(*, '(a)') 'cannot get displ. var. between snaps with different nparticles'
       stop
    end if

    ! check if group exists (if present)
    ! exit with error if not
    ! if no group specified, group is all (group 1)

    if (.not. present(igroup)) then
       jgroup = 1
    else
       jgroup = igroup
    end if
    if (jgroup > group%ngroup) then
       write(*, '(a)') 'group ', jgroup, ' not found'
       stop
    end if
    if (jgroup < 1) then
       write(*, '(a)') 'group ', jgroup, ' not found'
       stop
    end if
    groupbit = group%bitmask(jgroup)

    ! loop over particles
    ! subtract mean motion if desired

    call get_mean_pos(from, mean_pos_from)
    call get_mean_pos(to, mean_pos_to)
    mean_disp = mean_pos_to - mean_pos_from

    var_sq(:) = 0.0
    do i = 1, nparticles
       if (iand(mask(i),groupbit) /= 0) then
          delx = to%x(1,i) - from%x(1,i)
          dely = to%x(2,i) - from%x(2,i)
          delz = to%x(3,i) - from%x(3,i)
          if (subtr_mean) then
             delx = delx - mean_disp(1)
             dely = dely - mean_disp(2)
             delz = delz - mean_disp(3)
          end if
          var_sq(1) = var_sq(1) + delx*delx*delx*delx
          var_sq(2) = var_sq(2) + dely*dely*dely*dely
          var_sq(3) = var_sq(3) + delz*delz*delz*delz
          var_sq(4) = var_sq(4) + dely*delz*dely*delz
          var_sq(5) = var_sq(5) + delx*delz*delx*delz
          var_sq(6) = var_sq(6) + delx*dely*delx*dely
       end if
    end do

    ! normalize by size of group

    groupsize = group%count(jgroup)

    if (groupsize > 0) var_sq(:) = var_sq(:) / groupsize

  end subroutine get_displ_var_sq

  ! ----------------------------------------------------------------------
  ! Get symmetric tensor of self-displacement variance
  ! Used for getting cage-relative(cr) MSD
  ! IMPORTANT: assumes unwrapped coords
  ! ----------------------------------------------------------------------

  subroutine get_displ_var_cr(from, to, group, cells, yes_delta, r, var_cr, igroup)

    type(particle_t), intent(in) :: from,to
    real, dimension(6), intent(out) :: var_cr
    type(group_t), intent(in), target :: group
    integer, intent(in), optional :: igroup
    integer :: groupsize,jgroup,groupbit
    real, dimension(3) :: mean_pos_from_neighbor, mean_pos_to_neighbor
    real :: delx,dely,delz,delxx,delyy,delzz
    integer, dimension(:), pointer :: mask => null()

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes,nc
    logical :: typeradius_flag
    real :: rcutsq,rsq,xtmp,ytmp,ztmp
    real, dimension(:,:), allocatable :: rsqmat

    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes

    ! default rcutsq for yes_delta is false
    rcutsq = r*r

    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                         + cells%particle%typeradius(j) &
                         + r
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if

    mask => group%mask

    ! check for consistency

    nparticles = from%nparticles
    if (to%nparticles /= nparticles) then
       write(*, '(a)') 'cannot get displ. var. between snaps with different nparticles'
       stop
    end if

    ! check if group exists (if present)
    ! exit with error if not
    ! if no group specified, group is all (group 1)

    if (.not. present(igroup)) then
       jgroup = 1
    else
       jgroup = igroup
    end if
    if (jgroup > group%ngroup) then
       write(*, '(a)') 'group ', jgroup, ' not found'
       stop
    end if
    if (jgroup < 1) then
       write(*, '(a)') 'group ', jgroup, ' not found'
       stop
    end if
    groupbit = group%bitmask(jgroup)

    ! loop over particles
    ! subtract nearest-neighbor motion

    var_cr(:) = 0.0
    do i = 1, nparticles
       if (iand(mask(i),groupbit) /= 0) then
          delx = to%x(1,i) - from%x(1,i)
          dely = to%x(2,i) - from%x(2,i)
          delz = to%x(3,i) - from%x(3,i)

          xtmp = to%x(1,i)
          ytmp = to%x(2,i)
          ztmp = to%x(3,i)

          nc = 0
          mean_pos_to_neighbor(:) = 0.0
          mean_pos_from_neighbor(:) = 0.0

          ! keeping logical evaluation out of inner loop
          ! first case: uniform cutoff

          option_switch: if (.not. yes_delta) then

             if_cells_uniform: if (cells%cells_exist) then

                cell_loop_uniform: do c = 1, cells%ncells

                   j = cells%cellhead(c)
                   do while (j > 0)
                      if (j==i) cycle
                      delxx = cells%particle%x(1,j) - xtmp
                      delyy = cells%particle%x(2,j) - ytmp
                      delzz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delxx, delyy, delzz)
                      rsq = delxx*delxx + delyy*delyy + delzz*delzz
                      if (rsq < rcutsq) then
                         mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                         mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                         nc = nc + 1
                      end if
                      j = cells%next(j)
                   end do

                   ! loop over distinct cells
                   ! should I make this part of previous loop?

                   j = cells%cellhead(c)
                   do d = 1, cells%nstencil
                      neighcell = cells%stencil_neighbor(c,d)
                      if (neighcell == 0) cycle
                      j = cells%cellhead(neighcell)
                      do while (j > 0)
                         if (j==i) cycle
                         delxx = cells%particle%x(1,j) - xtmp
                         delyy = cells%particle%x(2,j) - ytmp
                         delzz = cells%particle%x(3,j) - ztmp
                         call cells%domain%minimum_image(delxx, delyy, delzz)
                         rsq = delxx*delxx + delyy*delyy + delzz*delzz
                         if (rsq < rcutsq) then
                            mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                            mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                            nc = nc + 1
                         end if
                         j = cells%next(j)
                      end do
                   end do
                         
                end do cell_loop_uniform

             else ! not using cells, so loop over all pairs

                do j = 1, nparticles
                   if (j==i) cycle
                   delxx = cells%particle%x(1,j) - xtmp
                   delyy = cells%particle%x(2,j) - ytmp
                   delzz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delxx, delyy, delzz)
                   rsq = delxx*delxx + delyy*delyy + delzz*delzz
                   if (rsq < rcutsq) then
                      mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                      mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                      nc = nc + 1
                   end if
                end do

             end if if_cells_uniform

          else if (typeradius_flag) then ! can use matrix cutoffs

             if_cells_typeradius: if (cells%cells_exist) then

                cell_loop_typeradius: do c = 1, cells%ncells

                   ! below, all in the ith cell
                   ! loop over all particles i,j in this cell c
                   ! cellhead(c) is the first i particle
                   ! the first (distinct) j is the next of i

                   j = cells%cellhead(c)
                   itype = cells%particle%type(i)
                   do while (j > 0)
                      if (j==i) cycle
                      jtype = cells%particle%type(j)
                      delxx = cells%particle%x(1,j) - xtmp
                      delyy = cells%particle%x(2,j) - ytmp
                      delzz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delxx, delyy, delzz)
                      rsq = delxx*delxx + delyy*delyy + delzz*delzz
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                         mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                         nc = nc + 1
                      end if
                      j = cells%next(j)
                   end do

                   ! loop over distinct cells
                   ! should I make this part of previous loop?

                   j = cells%cellhead(c)
                   itype = cells%particle%type(i)
                   do d = 1, cells%nstencil
                      neighcell = cells%stencil_neighbor(c,d)
                      if (neighcell == 0) cycle
                      j = cells%cellhead(neighcell)
                      do while (j > 0)
                         if (j==i) cycle
                         jtype = cells%particle%type(j)
                         delxx = cells%particle%x(1,j) - xtmp
                         delyy = cells%particle%x(2,j) - ytmp
                         delzz = cells%particle%x(3,j) - ztmp
                         call cells%domain%minimum_image(delxx, delyy, delzz)
                         rsq = delxx*delxx + delyy*delyy + delzz*delzz
                         rcutsq = rsqmat(itype,jtype)
                         if (rsq < rcutsq) then
                            mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                            mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                            nc = nc + 1
                         end if
                         j = cells%next(j)
                      end do
                   end do
                         
                end do cell_loop_typeradius

             else ! not using cells, so loop over all pairs

                itype = cells%particle%type(i)
                do j = 1, nparticles
                   if (j==i) cycle
                   jtype = cells%particle%type(j)
                   delxx = cells%particle%x(1,j) - xtmp
                   delyy = cells%particle%x(2,j) - ytmp
                   delzz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delxx, delyy, delzz)
                   rsq = delxx*delxx + delyy*delyy + delzz*delzz
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                      mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                      nc = nc + 1
                   end if
                end do

             end if if_cells_typeradius

          else ! have to use per-particle cutoff based on delta

             if_cells_perparticle: if (cells%cells_exist) then

                cell_loop_parparticle: do c = 1, cells%ncells

                   j = cells%cellhead(c)
                   do while (j > 0)
                      if (j==i) cycle
                      delxx = cells%particle%x(1,j) - xtmp
                      delyy = cells%particle%x(2,j) - ytmp
                      delzz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delxx, delyy, delzz)
                      rsq = delxx*delxx + delyy*delyy + delzz*delzz
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                         mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                         nc = nc + 1
                      end if
                      j = cells%next(j)
                   end do

                   ! loop over distinct cells
                   ! should I make this part of previous loop?

                   j = cells%cellhead(c)
                   do d = 1, cells%nstencil
                      neighcell = cells%stencil_neighbor(c,d)
                      if (neighcell == 0) cycle
                      j = cells%cellhead(neighcell)
                      do while (j > 0)
                         if (j==i) cycle
                         delxx = cells%particle%x(1,j) - xtmp
                         delyy = cells%particle%x(2,j) - ytmp
                         delzz = cells%particle%x(3,j) - ztmp
                         call cells%domain%minimum_image(delxx, delyy, delzz)
                         rsq = delxx*delxx + delyy*delyy + delzz*delzz
                         rcutsq = cells%particle%radius(i)
                         rcutsq = rcutsq + cells%particle%radius(j)
                         rcutsq = rcutsq + r
                         rcutsq = rcutsq*rcutsq
                         if (rsq < rcutsq) then
                            mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                            mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                            nc = nc + 1
                         end if
                         j = cells%next(j)
                      end do
                   end do
                         
                end do cell_loop_parparticle

             else ! not using cells, so loop over all pairs

                do j = 1, nparticles
                   if (j==i) cycle
                   delxx = cells%particle%x(1,j) - xtmp
                   delyy = cells%particle%x(2,j) - ytmp
                   delzz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delxx, delyy, delzz)
                   rsq = delxx*delxx + delyy*delyy + delzz*delzz
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      mean_pos_to_neighbor(:) = mean_pos_to_neighbor(:) + to%x(:,j)
                      mean_pos_from_neighbor(:) = mean_pos_from_neighbor(:) + from%x(:,j)
                      nc = nc + 1
                   end if
                end do

             end if if_cells_perparticle

          end if option_switch

          delx = delx-(mean_pos_from_neighbor(1)-mean_pos_to_neighbor(1))/nc
          dely = dely-(mean_pos_from_neighbor(2)-mean_pos_to_neighbor(2))/nc
          delz = delz-(mean_pos_from_neighbor(3)-mean_pos_to_neighbor(3))/nc

          var_cr(1) = var_cr(1) + delx*delx
          var_cr(2) = var_cr(2) + dely*dely
          var_cr(3) = var_cr(3) + delz*delz
          var_cr(4) = var_cr(4) + dely*delz
          var_cr(5) = var_cr(5) + delx*delz
          var_cr(6) = var_cr(6) + delx*dely
       end if
    end do

    ! normalize by size of group

    groupsize = group%count(jgroup)

    if (groupsize > 0) var_cr(:) = var_cr(:) / groupsize

  end subroutine get_displ_var_cr

  ! ----------------------------------------------------------------------
  ! Add a 3D array g to a version of itself g',
  ! where g'(i,j,k) = g(xmax+1-i,ymax+1-j,zmax+1-k),
  ! and divide by 2, to make a version that is symmetric.
  ! Avoids allocating a copy of the original array.
  ! ----------------------------------------------------------------------

  subroutine symmetrize_3d(g)

    real, dimension(:,:,:), intent(inout) :: g
    integer :: i,j,k,imax,jmax,kmax,imid,jmid,kmid

    imax = size(g,1)
    jmax = size(g,2)
    kmax = size(g,3)
    imid = imax / 2
    jmid = jmax / 2
    kmid = kmax / 2

    ! Divide everything by 2

    g(:,:,:) = 0.5 * g(:,:,:)

    ! Go through elements on the side of the array with small
    ! k-indices, adding elements on the opposide side of the
    ! the array, through its center.  E.g. (imax,jmax,kmax)
    ! added to (1,1,1).  Only go up to kmid = kmax / 2, in
    ! order to avoid double-counting.  If kmax is odd, say
    ! 5, only go up to the rounded-down half, say 2.  This
    ! leaves the middle plane, which must be dealt with below
    ! in a similar fashion.

    do k = 1, kmid
       do j = 1, jmax
          do i = 1, imax
             g(i,j,k) = g(i,j,k) + g(imax+1-i,jmax+1-j,kmax+1-k)
          end do
       end do
    end do

    ! Copy the small-k elements to the large-k elements.

    do k = 1, kmid
        do j = 1, jmax
          do i = 1, imax
             g(imax+1-i,jmax+1-j,kmax+1-k) = g(i,j,k)
          end do
       end do
    end do

    ! If kmax is even, we're done.
    ! If kmax is odd, fix the skipped middle plane.

    if (mod(kmax,2) == 0) return

    do j = 1, jmid
       do i = 1, imax
          g(i,j,kmid+1) = g(i,j,kmid+1) + g(imax+1-i,jmax+1-j,kmid+1)
       end do
    end do

    do j = 1, jmid
       do i = 1, imax
          g(imax+1-i,jmax+1-j,kmid+1) = g(i,j,kmid+1)
       end do
    end do

    ! If jmax is even, we're done.
    ! If jmax is odd, fix the skipped middle line.

    if (mod(jmax,2) == 0) return

    do i = 1, imid
       g(i,jmid+1,kmid+1) = g(i,jmid+1,kmid+1) + g(imax+1-i,jmid+1,kmid+1)
    end do

    do i = 1, imid
       g(imax+1-i,jmid+1,kmid+1) = g(i,jmid+1,kmid+1)
    end do

    ! If imax is even, we're done.
    ! If imax is odd, fix the center point.

    if (mod(imax,2) == 0) return

    g(imid+1,jmid+1,kmid+1) = 2 * g(imid+1,jmid+1,kmid+1)

  end subroutine symmetrize_3d

  ! ----------------------------------------------------------------------
  ! Get the per-cell stress tensor.
  ! Need to time-average values in most cases.
  ! The output is dimension 6xncells
  ! ----------------------------------------------------------------------

  subroutine cell_stress(cells, stress)

    type(cells_t), intent(in) :: cells
    real, dimension(:,:), intent(inout) :: stress
    integer :: i,c

    if (.not. cells%cells_exist) then
       write(*, '(a)') 'cell_stress requires cells'
       stop
    end if
    if (.not. cells%particle%stress_flag) then
       write(*, '(a)') 'cell_stress requires stress_flag to be true'
       stop
    end if
    if (size(stress,1) /= 6) then
       write(*, '(a)') 'first dimension of stress array must be size 6'
       stop
    end if
    if (size(stress,2) /= cells%ncells) then
       write(*, '(a)') 'second dimension of stress array must be size ncells'
       stop
    end if

    ! check if particle coordinates need wrapping, and wrap if they do
    ! can do this here, because no reason to be in lamda coords

    if (.not. cells%domain%particles_inside()) then
       call cells%domain%remap()
    end if

    ! accumulate particle stress-energy tensor values

    stress(:,:) = 0.0
    do concurrent (c = 1:cells%ncells)
       i = cells%cellhead(c)
       do while (i > 0)
          stress(:,c) = stress(:,c) + cells%particle%stress(:,i)
          i = cells%next(i)
       end do
    end do
    
    ! divide by the per-cell volume (currently one value for all cells)

    stress(:,:) = stress(:,:) * (cells%ncells / cells%domain%volume())

  end subroutine cell_stress

  ! ----------------------------------------------------------------------
  ! calculate the average or binned bond distance
  ! may be two different functions
  ! ----------------------------------------------------------------------

  subroutine eval_mean_bond_dist(cells, yes_delta, r, mean_bond_dist)
    
    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    real, intent(inout) :: mean_bond_dist
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes,nbonds
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp
    real :: c_sum,y_sum,t_sum,bond_dist_sum,bond_dist
    real, dimension(:,:), allocatable :: rsqmat
    real, dimension(:,:), allocatable :: rskin
    
    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes
    
    ! default rcutsq for yes_delta is false
    rcutsq = r*r
    
    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       allocate(rskin(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                  + cells%particle%typeradius(j) &
                  + r
             rskin(i,j) = cells%particle%typeradius(i) &
                  + cells%particle%typeradius(j)
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if
    
    nbonds = 0
    bond_dist_sum = 0.0
    c_sum = 0.0

    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff

    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   if (rsq < rcutsq) then
                      nbonds = nbonds + 1
                      bond_dist = sqrt(rsq)
                      y_sum = bond_dist - c_sum
                      t_sum = bond_dist_sum + y_sum
                      c_sum = (t_sum - bond_dist_sum) - y_sum
                      bond_dist_sum = t_sum
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      if (rsq < rcutsq) then
                         nbonds = nbonds + 1
                         bond_dist = sqrt(rsq)
                         y_sum = bond_dist - c_sum
                         t_sum = bond_dist_sum + y_sum
                         c_sum = (t_sum - bond_dist_sum) - y_sum
                         bond_dist_sum = t_sum
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_uniform
          
       else ! not using cells, so loop over all pairs
          
          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                if (rsq < rcutsq) then
                   nbonds = nbonds + 1
                   bond_dist = sqrt(rsq)
                   y_sum = bond_dist - c_sum
                   t_sum = bond_dist_sum + y_sum
                   c_sum = (t_sum - bond_dist_sum) - y_sum
                   bond_dist_sum = t_sum
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      nbonds = nbonds + 1
                      bond_dist = sqrt(rsq) - rskin(itype,jtype)
                      y_sum = bond_dist - c_sum
                      t_sum = bond_dist_sum + y_sum
                      c_sum = (t_sum - bond_dist_sum) - y_sum
                      bond_dist_sum = t_sum
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         nbonds = nbonds + 1
                         bond_dist = sqrt(rsq) - rskin(itype,jtype)
                         y_sum = bond_dist - c_sum
                         t_sum = bond_dist_sum + y_sum
                         c_sum = (t_sum - bond_dist_sum) - y_sum
                         bond_dist_sum = t_sum
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   nbonds = nbonds + 1
                   bond_dist = sqrt(rsq) - rskin(itype,jtype)
                   y_sum = bond_dist - c_sum
                   t_sum = bond_dist_sum + y_sum
                   c_sum = (t_sum - bond_dist_sum) - y_sum
                   bond_dist_sum = t_sum
                end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      nbonds = nbonds + 1
                      bond_dist = sqrt(rsq) - cells%particle%radius(i)&
                           - cells%particle%radius(j)
                      y_sum = bond_dist - c_sum
                      t_sum = bond_dist_sum + y_sum
                      c_sum = (t_sum - bond_dist_sum) - y_sum
                      bond_dist_sum = t_sum
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         nbonds = nbonds + 1
                         bond_dist = sqrt(rsq) - cells%particle%radius(i)&
                              - cells%particle%radius(j)
                         y_sum = bond_dist - c_sum
                         t_sum = bond_dist_sum + y_sum
                         c_sum = (t_sum - bond_dist_sum) - y_sum
                         bond_dist_sum = t_sum
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   nbonds = nbonds + 1
                   bond_dist = rsq - cells%particle%typeradius(i) &
                        - cells%particle%typeradius(j)
                   y_sum = bond_dist - c_sum
                   t_sum = bond_dist_sum + y_sum
                   c_sum = (t_sum - bond_dist_sum) - y_sum
                   bond_dist_sum = t_sum
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch
    
    ! use integer number of bonds to average the bond length
    mean_bond_dist = bond_dist_sum/nbonds

  end subroutine eval_mean_bond_dist

  ! ----------------------------------------------------------------------
  ! binned bond distance
  ! user must input nbins and bin range
  ! ----------------------------------------------------------------------

  subroutine eval_bin_bond_dist(cells, yes_delta, r, nbins, bin_lo, bin_hi, bin_bond_dist)  

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r,bin_lo,bin_hi
    integer, intent(in) :: nbins
    integer, dimension(:), intent(inout) :: bin_bond_dist
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes,nbonds
    integer :: bond_dist_int
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp,bond_dist
    real, dimension(:,:), allocatable :: rsqmat
    real, dimension(:,:), allocatable :: rskin
    
    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes
    
    ! default rcutsq for yes_delta is false
    rcutsq = r*r
    
    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       allocate(rskin(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                  + cells%particle%typeradius(j) &
                  + r
             rskin(i,j) = cells%particle%typeradius(i) &
                  + cells%particle%typeradius(j)
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if
    
    nbonds = 0
    bin_bond_dist(:) = 0

    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff

    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   if (rsq < rcutsq) then
                      nbonds = nbonds + 1
                      bond_dist = sqrt(rsq)
                      bond_dist_int = ceiling((bond_dist - bin_lo)/&
                           (bin_hi - bin_lo)*nbins) + 1
                      bin_bond_dist(bond_dist_int) =&
                           bin_bond_dist(bond_dist_int) + 1
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      if (rsq < rcutsq) then
                         nbonds = nbonds + 1
                         bond_dist = sqrt(rsq)
                         bond_dist_int = ceiling((bond_dist - bin_lo)/&
                              (bin_hi - bin_lo)*nbins) + 1
                         bin_bond_dist(bond_dist_int) =&
                              bin_bond_dist(bond_dist_int) + 1
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_uniform
          
       else ! not using cells, so loop over all pairs
          
          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                if (rsq < rcutsq) then
                   nbonds = nbonds + 1
                   bond_dist = sqrt(rsq)
                   bond_dist_int = ceiling((bond_dist - bin_lo)/&
                        (bin_hi - bin_lo)*nbins) + 1
                   bin_bond_dist(bond_dist_int) = bin_bond_dist(bond_dist_int)&
                        + 1
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      nbonds = nbonds + 1
                      bond_dist = sqrt(rsq) - rskin(itype,jtype)
                      bond_dist_int = ceiling((bond_dist - bin_lo)/&
                           (bin_hi - bin_lo)*nbins) + 1
                      !bin_bond_dist(bond_dist_int) =&
                      !     bin_bond_dist(bond_dist_int) + 1
                      if (bond_dist_int.le.nbins) then
                         if (bond_dist_int.ge.1) then
                            bin_bond_dist(bond_dist_int) =&
                                 bin_bond_dist(bond_dist_int) + 1
                         else
                            print *, "value below bins"
                            print *, bond_dist_int
                         end if
                      else 
                         print *, "value above bins"
                         print *, bond_dist_int
                      end if
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         nbonds = nbonds + 1
                         bond_dist = sqrt(rsq) - rskin(itype,jtype)
                         bond_dist_int = ceiling((bond_dist - bin_lo)/&
                              (bin_hi - bin_lo)*nbins) + 1
                         if (bond_dist_int.le.nbins) then
                            if (bond_dist_int.ge.1) then
                               bin_bond_dist(bond_dist_int) =&
                                    bin_bond_dist(bond_dist_int) + 1
                            else
                               print *, "value below bins"
                               print *, bond_dist_int
                            end if
                         else 
                            print *, "value above bins"
                            print *, bond_dist_int
                         end if
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   nbonds = nbonds + 1
                   bond_dist = sqrt(rsq) - rskin(itype,jtype)
                   bond_dist_int = ceiling((bond_dist - bin_lo)/&
                        (bin_hi - bin_lo)*nbins) + 1
                   bin_bond_dist(bond_dist_int) = bin_bond_dist(bond_dist_int)&
                        + 1
                end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      write(*, '(f14.8)', advance = 'no') rsq
                      nbonds = nbonds + 1
                      write(*, '(i14)', advance = 'no') nbonds
                      bond_dist = sqrt(rsq) - cells%particle%radius(i)&
                           - cells%particle%radius(j)
                      write(*, '(f14.8)', advance = 'no') bond_dist 
                      bond_dist_int = ceiling((bond_dist - bin_lo)/&
                           (bin_hi - bin_lo)*nbins) + 1
                      write(*, '(i14)') bond_dist_int
                      bin_bond_dist(bond_dist_int) =&
                           bin_bond_dist(bond_dist_int) + 1
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         nbonds = nbonds + 1
                         bond_dist = sqrt(rsq) - cells%particle%radius(i)&
                              - cells%particle%radius(j)
                         bond_dist_int = ceiling((bond_dist - bin_lo)/&
                              (bin_hi - bin_lo)*nbins) + 1
                         bin_bond_dist(bond_dist_int) =&
                              bin_bond_dist(bond_dist_int) + 1
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   nbonds = nbonds + 1
                   bond_dist = rsq - cells%particle%typeradius(i) &
                        - cells%particle%typeradius(j)
                   bond_dist_int = ceiling((bond_dist - bin_lo)/&
                        (bin_hi - bin_lo)*nbins) + 1
                   bin_bond_dist(bond_dist_int) = bin_bond_dist(bond_dist_int)&
                        + 1
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch
    
  end subroutine eval_bin_bond_dist

  ! ---------------------------------------------------------------------------
  ! Now that we can measure the average distance of all bonds, and the
  ! binned bond distance of all bonds, it may be interesting to see if Nc
  ! plays any role in how long bonds are.
  ! First, idenify contact numbers via eval_nc, which records the Nc
  ! of each of N particles in an array of size N.
  ! Second, use the original main loop in the other bond dist calcs
  ! and when the distance between particles i and j is found,
  ! add it to an array of Nc specific bond distances.
  ! In order to later normalize/average the bond distances by Nc,
  ! keep another array of the integer number of bonds placed
  ! in the bond distance info array.
  ! ---------------------------------------------------------------------------

  subroutine eval_mean_bond_dist_nc(cells, yes_delta, r, mean_bond_dist_nc)

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    real, dimension(:), intent(inout) :: mean_bond_dist_nc
    integer, dimension(:), allocatable :: nc
    integer, parameter :: ncmax = 20
    integer, dimension(ncmax+1) :: nc_bonds
    real, dimension(ncmax+1) :: nc_sum_bond_dist
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes!,nbonds
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp,bond_dist
    real, dimension(ncmax+1) :: c_sum,y_sum,t_sum
    real, dimension(:,:), allocatable :: rsqmat
    real, dimension(:,:), allocatable :: rskin
    
    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes
    
    allocate(nc(nparticles))
!    allocate(mean_bond_dist_nc(ncmax+1))
    nc_bonds(:) = int(0)
    nc_sum_bond_dist(:) = 0.0
    mean_bond_dist_nc(:) = 0.0

    call eval_nc(cells, yes_delta, r, nc)

    ! default rcutsq for yes_delta is false
    rcutsq = r*r
    
    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       allocate(rskin(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                  + cells%particle%typeradius(j) &
                  + r
             rskin(i,j) = cells%particle%typeradius(i) &
                  + cells%particle%typeradius(j)
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if
    
    c_sum(:) = 0.0

    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff

    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   if (rsq < rcutsq) then
                      ! add the bond found to the "bond count" by Nc
                      nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                      nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                      
                      bond_dist = sqrt(rsq)

                      y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                      t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                      c_sum(nc(i)) = (t_sum(nc(i)) - nc_sum_bond_dist(nc(i))) &
                           - y_sum(nc(i))
                      nc_sum_bond_dist(nc(i)) = t_sum(nc(i))

                      y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                      t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                      c_sum(nc(j)) = (t_sum(nc(j)) - nc_sum_bond_dist(nc(j))) &
                           - y_sum(nc(j))
                      nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      if (rsq < rcutsq) then
                         nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                         nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                         
                         bond_dist = sqrt(rsq)
                         
                         y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                         t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                         c_sum(nc(i)) = (t_sum(nc(i)) &
                              - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                         nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                         
                         y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                         t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                         c_sum(nc(j)) = (t_sum(nc(j)) &
                              - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                         nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_uniform
          
       else ! not using cells, so loop over all pairs
          
          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                if (rsq < rcutsq) then
                   nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                   nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                   
                   bond_dist = sqrt(rsq)
                   
                   y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                   t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                   c_sum(nc(i)) = (t_sum(nc(i)) &
                        - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                   nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                   
                   y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                   t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                   c_sum(nc(j)) = (t_sum(nc(j)) &
                        - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                   nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      bond_dist = sqrt(rsq) - rskin(itype,jtype)

                      nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                      nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                      
                      y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                      t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                      c_sum(nc(i)) = (t_sum(nc(i)) &
                           - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                      nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                      
                      y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                      t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                      c_sum(nc(j)) = (t_sum(nc(j)) &
                           - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                      nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         bond_dist = sqrt(rsq) - rskin(itype,jtype)
                         
                         nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                         nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                         
                         y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                         t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                         c_sum(nc(i)) = (t_sum(nc(i)) &
                              - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                         nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                         
                         y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                         t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                         c_sum(nc(j)) = (t_sum(nc(j)) &
                              - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                         nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   bond_dist = sqrt(rsq) - rskin(itype,jtype)
                   
                   nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                   nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                   
                   y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                   t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                   c_sum(nc(i)) = (t_sum(nc(i)) &
                        - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                   nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                   
                   y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                   t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                   c_sum(nc(j)) = (t_sum(nc(j)) &
                        - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                   nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                   
                end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      bond_dist = sqrt(rsq) - cells%particle%radius(i)&
                           - cells%particle%radius(j)

                      nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                      nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                      
                      y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                      t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                      c_sum(nc(i)) = (t_sum(nc(i)) &
                           - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                      nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                      
                      y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                      t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                      c_sum(nc(j)) = (t_sum(nc(j)) &
                           - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                      nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         bond_dist = sqrt(rsq) - cells%particle%radius(i)&
                              - cells%particle%radius(j)

                         nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                         nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                         
                         y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                         t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                         c_sum(nc(i)) = (t_sum(nc(i)) &
                              - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                         nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                         
                         y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                         t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                         c_sum(nc(j)) = (t_sum(nc(j)) &
                              - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                         nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                         
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do
                   
          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   bond_dist = rsq - cells%particle%typeradius(i) &
                        - cells%particle%typeradius(j)

                   nc_bonds(nc(i)) = nc_bonds(nc(i)) + 1
                   nc_bonds(nc(j)) = nc_bonds(nc(j)) + 1
                   
                   y_sum(nc(i)) = bond_dist - c_sum(nc(i))
                   t_sum(nc(i)) = nc_sum_bond_dist(nc(i)) + y_sum(nc(i))
                   c_sum(nc(i)) = (t_sum(nc(i)) &
                        - nc_sum_bond_dist(nc(i))) - y_sum(nc(i))
                   nc_sum_bond_dist(nc(i)) = t_sum(nc(i))
                   
                   y_sum(nc(j)) = bond_dist - c_sum(nc(j))
                   t_sum(nc(j)) = nc_sum_bond_dist(nc(j)) + y_sum(nc(j))
                   c_sum(nc(j)) = (t_sum(nc(j)) &
                        - nc_sum_bond_dist(nc(j)))  - y_sum(nc(j))
                   nc_sum_bond_dist(nc(j)) = t_sum(nc(j))
                   
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch
    
    ! use integer number of bonds to average the bond length
    do i = 1, ncmax+1
       if (nc_bonds(i).gt.0) then
          mean_bond_dist_nc(i) = real(nc_sum_bond_dist(i)/nc_bonds(i))
       end if
    end do

  end subroutine eval_mean_bond_dist_nc

  ! ---------------------------------------------------------------------------
  ! Evaluate self-intermediate scattering function
  ! input: cells = current cells
  !        cells0 = initial cells
  !        isf = output scattering function array
  !        subtr_tf = whether to subtract mean displacement 
  ! ---------------------------------------------------------------------------

  subroutine eval_isf_self_3d(cells, cells0, isf, subtr_tf)

    type(cells_t), intent(inout) :: cells
    type(cells_t), intent(in) :: cells0
    logical, intent(in) :: subtr_tf
    real(c_double), dimension(:,:,:), allocatable :: dens
    complex(c_double_complex), dimension(:,:,:), allocatable :: ftdens
    complex(c_double_complex), dimension(:,:,:), allocatable :: isf
    real, dimension(3) :: mean_origin, mean_disp
    integer :: nx,ny,nz,nparticles,i

    nparticles = cells%particle%nparticles

    ! Get the starting position in unwrapped coordinates.
    ! This is used when subtracting the mean motion.
    if (subtr_tf) then
       mean_origin(1) = sum(cells0%particle%x(1,:)) / nparticles !get_mean_pos(part1, mean_origin)
       mean_origin(2) = sum(cells0%particle%x(2,:)) / nparticles
       mean_origin(3) = sum(cells0%particle%x(3,:)) / nparticles
       mean_disp(1) = sum(cells%particle%x(1,:)) / nparticles !get_mean_pos(part2, mean_disp)
       mean_disp(2) = sum(cells%particle%x(2,:)) / nparticles
       mean_disp(3) = sum(cells%particle%x(3,:)) / nparticles
       mean_disp = mean_disp - mean_origin
       do i = 1, nparticles
         cells%particle%x(:,i) = cells%particle%x(:,i) - mean_disp !call subtract_displacement(part2, mean_disp)
       end do
    end if

    ! Get particle displacements to FT.
    cells%particle%x = cells%particle%x - cells0%particle%x

    call eval_fft_dens_3d(cells, dens, ftdens)
    deallocate(dens)
    nx = size(ftdens,1)
    ny = size(ftdens,2)
    nz = size(ftdens,3)
    allocate(isf(nx,ny,nz))
    isf = ftdens / nparticles

  end subroutine eval_isf_self_3d

  ! ---------------------------------------------------------------------------
  ! Evaluate coherent-intermediate scattering function
  ! input: cells = current cells
  !        cells0 = initial cells (only needed for subtracting mean motion)
  !        ftdens0 = FFT of intial cells; calculate only once outside function
  !        isf = output scattering function array
  !        subtr_tf = whether to subtract mean displacement 
  ! ---------------------------------------------------------------------------

  subroutine eval_isf_full_3d(cells, cells0, ftdens0, isf, subtr_tf)

    type(cells_t), intent(inout) :: cells
    type(cells_t), intent(in) :: cells0
    logical, intent(in) :: subtr_tf
    real(c_double), dimension(:,:,:), allocatable :: dens
    complex(c_double_complex), dimension(:,:,:), allocatable :: ftdens
    complex(c_double_complex), dimension(:,:,:), intent(in) :: ftdens0
    complex(c_double_complex), dimension(:,:,:), allocatable :: isf
    real, dimension(3) :: mean_origin, mean_disp
    integer :: nx,ny,nz,nparticles,i

    nparticles = cells%particle%nparticles

    ! Get the starting position in unwrapped coordinates.
    ! This is used when subtracting the mean motion.
    if (subtr_tf) then
       mean_origin(1) = sum(cells0%particle%x(1,:)) / nparticles !get_mean_pos(part1, mean_origin)
       mean_origin(2) = sum(cells0%particle%x(2,:)) / nparticles
       mean_origin(3) = sum(cells0%particle%x(3,:)) / nparticles
       mean_disp(1) = sum(cells%particle%x(1,:)) / nparticles !get_mean_pos(part2, mean_disp)
       mean_disp(2) = sum(cells%particle%x(2,:)) / nparticles
       mean_disp(3) = sum(cells%particle%x(3,:)) / nparticles
       mean_disp = mean_disp - mean_origin
       do i = 1, nparticles
         cells%particle%x(:,i) = cells%particle%x(:,i) - mean_disp !call subtract_displacement(part2, mean_disp)
       end do
    end if

    ! Get current value of full correlator.

    call eval_fft_dens_3d(cells, dens, ftdens)
    deallocate(dens)
    nx = size(ftdens0,1)
    ny = size(ftdens0,2)
    nz = size(ftdens0,3)
    allocate(isf(nx,ny,nz))
    isf = real(ftdens(:,:,:)*conjg(ftdens0(:,:,:))) / nparticles

  end subroutine eval_isf_full_3d

  subroutine eval_order_parameter(cells, yes_delta, r, q6)

    ! Declare local constant Pi
    real, parameter :: pi = 3.1415927

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    integer, dimension(:), allocatable :: nc
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp
    real, dimension(:,:), allocatable :: rsqmat
    ! parameters related to calculation of Q6
    real, dimension(:), intent(inout) :: q6
    complex, dimension(:), allocatable :: qbar66, qbar65, qbar64, qbar63, qbar62
    complex, dimension(:), allocatable :: qbar61, qbar60, qbar6m6, qbar6m5
    complex, dimension(:), allocatable :: qbar6m4, qbar6m3, qbar6m2, qbar6m1
    real :: thetai, phii, thetaj, phij, rsqroot
    ! helping variables
    complex, dimension(:), allocatable :: y66,y65,y64,y63,y62,y61,y60,y6m1,y6m2
    complex, dimension(:), allocatable :: y6m3,y6m4, y6m5,y6m6
    real :: yhelp
    complex, dimension(:), allocatable :: q66, q65, q64, q63, q62, q61
    complex, dimension(:), allocatable :: q60, q6m6, q6m5, q6m4, q6m3
    complex, dimension(:), allocatable :: q6m2, q6m1
    integer, dimension(:,:), allocatable :: nbmat
    integer :: nbindex

    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes

    ! default rcutsq for yes_delta is false
    rcutsq = r*r

    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                         + cells%particle%typeradius(j) &
                         + r
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if
    ! allocate memory
    allocate(qbar66(nparticles),qbar65(nparticles),qbar64(nparticles),&
             qbar63(nparticles),qbar62(nparticles),qbar61(nparticles),&
             qbar60(nparticles),qbar6m1(nparticles),qbar6m2(nparticles),&
             qbar6m3(nparticles),qbar6m4(nparticles),qbar6m5(nparticles),&
             qbar6m6(nparticles))
    allocate(q66(nparticles),q65(nparticles),q64(nparticles),q63(nparticles),&
             q62(nparticles),q61(nparticles),q60(nparticles),q6m1(nparticles),&
             q6m2(nparticles),q6m3(nparticles),q6m4(nparticles),&
             q6m5(nparticles),q6m6(nparticles))
    allocate(y66(nparticles),y65(nparticles),y64(nparticles),y63(nparticles),&
             y62(nparticles),y61(nparticles),y60(nparticles),y6m1(nparticles),&
             y6m2(nparticles),y6m3(nparticles),y6m4(nparticles),&
             y6m5(nparticles),y6m6(nparticles))
    allocate(nbmat(nparticles,20))
    allocate(nc(nparticles))
    !allocate(q6(nparticles))


    nc(:) = 0
    ! initialize variables for q6
    q6(:) = 0.0
    nbindex = 0
    nbmat(:,:) = 0
!    qbar66 = (0.0, 0.0)
!    qbar65 = (0.0, 0.0)
!    qbar64 = (0.0, 0.0)
!    qbar63 = (0.0, 0.0)
!    qbar62 = (0.0, 0.0)
!    qbar61 = (0.0, 0.0)
!    qbar60 = (0.0, 0.0)
!    qbar6m1 = (0.0, 0.0)
!    qbar6m2 = (0.0, 0.0)
!    qbar6m3 = (0.0, 0.0)
!    qbar6m4 = (0.0, 0.0)
!    qbar6m5 = (0.0, 0.0)
!    qbar6m6 = (0.0, 0.0)
    q66 = (0.0, 0.0)
    q65 = (0.0, 0.0)
    q64 = (0.0, 0.0)
    q63 = (0.0, 0.0)
    q62 = (0.0, 0.0)
    q61 = (0.0, 0.0)
    q60 = (0.0, 0.0)
    q6m1 = (0.0, 0.0)
    q6m2 = (0.0, 0.0)
    q6m3 = (0.0, 0.0)
    q6m4 = (0.0, 0.0)
    q6m5 = (0.0, 0.0)
    q6m6 = (0.0, 0.0)
    ! reset helping parameters
    y66  = (0.0, 0.0)
    y65  = (0.0, 0.0)
    y64  = (0.0, 0.0)
    y63  = (0.0, 0.0)
    y62  = (0.0, 0.0)
    y61  = (0.0, 0.0)
    y60  = (0.0, 0.0)
    y6m1  = (0.0, 0.0)
    y6m2  = (0.0, 0.0)
    y6m3  = (0.0, 0.0)
    y6m4  = (0.0, 0.0)
    y6m5  = (0.0, 0.0)
    y6m6  = (0.0, 0.0)
    yhelp = 0.0

    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff

    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rsqroot = rsq**0.5
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                      ! keep track of neighbor indeces
                      nbmat(i,nc(i)) = j
                      nbmat(j,nc(j)) = i
                      ! calculate angles
                      thetai = acos(delz/rsqroot)
                      phii   = atan2(dely,delx)
                      thetaj = acos(-delz/rsqroot)
                      phij   = atan2(-dely,-delx)
                      ! calculate Ylm
                      !for l = 6
                      ! m = 6 and -6
                      yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                      y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                      y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                      yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                      y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                      y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                      ! m = 5 and -5
                      yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                              *(sin(thetai))**5.0
                      y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                      y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                      yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                              *(sin(thetaj))**5.0
                      y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                      y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                      ! m = 4 and -4
                      yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                             *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                      y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                      y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                      yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                             *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                      y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                      y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                      ! m = 3 and -3
                      yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                             *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                      y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                      y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                      yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                             *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                      y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                      y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                      ! m = 2 and -2
                      yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                            *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                            *(sin(thetai))**2.0
                      y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                      y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                      yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                            *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                            *(sin(thetaj))**2.0
                      y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                      y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                      ! m = 1 and -1
                      yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                             *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                             *(sin(thetai))
                      y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                      y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                      yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                             *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                             *(sin(thetaj))
                      y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                      y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                      ! m = 0
                      yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                            *(-5.0 + 105.0*(cos(thetai))**2.0&
                            - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                      y60(i) = y60(i) + yhelp

                      yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                            *(-5.0 + 105.0*(cos(thetaj))**2.0&
                            - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                      y60(j) = y60(j) + yhelp
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do
             ! calculate q_lm (Eq. (36) Zakhari.et.al.2017 PRL)
             q66(:) = y66(:)/nc(:)
             q65(:) = y65(:)/nc(:)

             ! calculate qbar_lm (Eq. (35) Zakhari.et.al.2017 PRL)
             i = cells%cellhead(c)
             do while (i > 0)
                j = cells%next(i)
                do while (j > 0)

                  j = cells%next(j)
                end do
               i = cells%next(i)
             end do


             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rsqroot = rsq**0.5
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                         ! keep track of neighbor indeces
                         nbmat(i,nc(i)) = j
                         nbmat(j,nc(j)) = i
                         ! calculate angles
                         thetai = acos(delz/rsqroot)
                         phii   = atan2(dely,delx)
                         thetaj = acos(-delz/rsqroot)
                         phij   = atan2(-dely,-delx)
                         ! calculate Ylm
                         !for l = 6
                         ! m = 6 and -6
                         yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                         y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                         y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                         yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                         y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                         y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                         ! m = 5 and -5
                         yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                                 *(sin(thetai))**5.0
                         y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                         y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                         yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                                 *(sin(thetaj))**5.0
                         y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                         y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                         ! m = 4 and -4
                         yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                                *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                         y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                         y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                         yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                                *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                         y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                         y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                         ! m = 3 and -3
                         yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                                *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                         y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                         y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                         yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                                *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                         y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                         y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                         ! m = 2 and -2
                         yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                               *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                               *(sin(thetai))**2.0
                         y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                         y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                         yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                               *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                               *(sin(thetaj))**2.0
                         y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                         y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                         ! m = 1 and -1
                         yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                                *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                                *(sin(thetai))
                         y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                         y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                         yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                                *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                                *(sin(thetaj))
                         y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                         y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                         ! m = 0
                         yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                               *(-5.0 + 105.0*(cos(thetai))**2.0&
                               - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                         y60(i) = y60(i) + yhelp

                         yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                               *(-5.0 + 105.0*(cos(thetaj))**2.0&
                               - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                         y60(j) = y60(j) + yhelp
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_uniform

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rsqroot = rsq**0.5
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                   ! keep track of neighbor indeces
                   nbmat(i,nc(i)) = j
                   nbmat(j,nc(j)) = i
                   ! calculate angles
                   thetai = acos(delz/rsqroot)
                   phii   = atan2(dely,delx)
                   thetaj = acos(-delz/rsqroot)
                   phij   = atan2(-dely,-delx)
                   ! calculate Ylm
                   !for l = 6
                   ! m = 6 and -6
                   yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                   y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                   y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                   yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                   y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                   y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                   ! m = 5 and -5
                   yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                           *(sin(thetai))**5.0
                   y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                   y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                   yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                           *(sin(thetaj))**5.0
                   y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                   y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                   ! m = 4 and -4
                   yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                          *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                   y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                   y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                   yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                          *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                   y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                   y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                   ! m = 3 and -3
                   yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                          *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                   y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                   y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                   yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                          *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                   y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                   y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                   ! m = 2 and -2
                   yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                         *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                         *(sin(thetai))**2.0
                   y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                   y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                   yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                         *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                         *(sin(thetaj))**2.0
                   y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                   y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                   ! m = 1 and -1
                   yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                          *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                          *(sin(thetai))
                   y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                   y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                   yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                          *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                          *(sin(thetaj))
                   y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                   y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                   ! m = 0
                   yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                         *(-5.0 + 105.0*(cos(thetai))**2.0&
                         - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                   y60(i) = y60(i) + yhelp

                   yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                         *(-5.0 + 105.0*(cos(thetaj))**2.0&
                         - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                   y60(j) = y60(j) + yhelp
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rsqroot = rsq**0.5
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                      ! keep track of neighbor indeces
                      nbmat(i,nc(i)) = j
                      nbmat(j,nc(j)) = i
                      ! calculate angles
                      thetai = acos(delz/rsqroot)
                      phii   = atan2(dely,delx)
                      thetaj = acos(-delz/rsqroot)
                      phij   = atan2(-dely,-delx)
                      ! calculate Ylm
                      !for l = 6
                      ! m = 6 and -6
                      yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                      y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                      y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                      yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                      y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                      y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                      ! m = 5 and -5
                      yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                              *(sin(thetai))**5.0
                      y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                      y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                      yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                              *(sin(thetaj))**5.0
                      y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                      y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                      ! m = 4 and -4
                      yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                             *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                      y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                      y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                      yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                             *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                      y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                      y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                      ! m = 3 and -3
                      yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                             *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                      y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                      y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                      yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                             *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                      y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                      y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                      ! m = 2 and -2
                      yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                            *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                            *(sin(thetai))**2.0
                      y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                      y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                      yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                            *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                            *(sin(thetaj))**2.0
                      y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                      y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                      ! m = 1 and -1
                      yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                             *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                             *(sin(thetai))
                      y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                      y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                      yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                             *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                             *(sin(thetaj))
                      y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                      y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                      ! m = 0
                      yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                            *(-5.0 + 105.0*(cos(thetai))**2.0&
                            - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                      y60(i) = y60(i) + yhelp

                      yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                            *(-5.0 + 105.0*(cos(thetaj))**2.0&
                            - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                      y60(j) = y60(j) + yhelp
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rsqroot = rsq**0.5
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                         ! keep track of neighbor indeces
                         nbmat(i,nc(i)) = j
                         nbmat(j,nc(j)) = i
                         ! calculate angles
                         thetai = acos(delz/rsqroot)
                         phii   = atan2(dely,delx)
                         thetaj = acos(-delz/rsqroot)
                         phij   = atan2(-dely,-delx)
                         ! calculate Ylm
                         !for l = 6
                         ! m = 6 and -6
                         yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                         y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                         y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                         yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                         y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                         y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                         ! m = 5 and -5
                         yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                                 *(sin(thetai))**5.0
                         y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                         y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                         yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                                 *(sin(thetaj))**5.0
                         y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                         y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                         ! m = 4 and -4
                         yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                                *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                         y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                         y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                         yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                                *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                         y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                         y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                         ! m = 3 and -3
                         yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                                *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                         y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                         y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                         yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                                *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                         y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                         y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                         ! m = 2 and -2
                         yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                               *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                               *(sin(thetai))**2.0
                         y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                         y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                         yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                               *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                               *(sin(thetaj))**2.0
                         y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                         y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                         ! m = 1 and -1
                         yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                                *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                                *(sin(thetai))
                         y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                         y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                         yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                                *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                                *(sin(thetaj))
                         y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                         y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                         ! m = 0
                         yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                               *(-5.0 + 105.0*(cos(thetai))**2.0&
                               - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                         y60(i) = y60(i) + yhelp

                         yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                               *(-5.0 + 105.0*(cos(thetaj))**2.0&
                               - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                         y60(j) = y60(j) + yhelp
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rsqroot = rsq**0.5
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                   ! keep track of neighbor indeces
                   nbmat(i,nc(i)) = j
                   nbmat(j,nc(j)) = i
                   ! calculate angles
                   thetai = acos(delz/rsqroot)
                   phii   = atan2(dely,delx)
                   thetaj = acos(-delz/rsqroot)
                   phij   = atan2(-dely,-delx)
                   ! calculate Ylm
                   !for l = 6
                   ! m = 6 and -6
                   yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                   y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                   y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                   yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                   y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                   y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                   ! m = 5 and -5
                   yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                           *(sin(thetai))**5.0
                   y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                   y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                   yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                           *(sin(thetaj))**5.0
                   y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                   y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                   ! m = 4 and -4
                   yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                          *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                   y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                   y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                   yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                          *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                   y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                   y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                   ! m = 3 and -3
                   yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                          *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                   y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                   y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                   yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                          *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                   y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                   y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                   ! m = 2 and -2
                   yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                         *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                         *(sin(thetai))**2.0
                   y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                   y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                   yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                         *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                         *(sin(thetaj))**2.0
                   y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                   y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                   ! m = 1 and -1
                   yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                          *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                          *(sin(thetai))
                   y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                   y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                   yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                          *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                          *(sin(thetaj))
                   y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                   y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                   ! m = 0
                   yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                         *(-5.0 + 105.0*(cos(thetai))**2.0&
                         - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                   y60(i) = y60(i) + yhelp

                   yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                         *(-5.0 + 105.0*(cos(thetaj))**2.0&
                         - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                   y60(j) = y60(j) + yhelp
                end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rsqroot = rsq**0.5
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                      ! keep track of neighbor indeces
                      nbmat(i,nc(i)) = j
                      nbmat(j,nc(j)) = i
                      ! calculate angles
                      thetai = acos(delz/rsqroot)
                      phii   = atan2(dely,delx)
                      thetaj = acos(-delz/rsqroot)
                      phij   = atan2(-dely,-delx)
                      ! calculate Ylm
                      !for l = 6
                      ! m = 6 and -6
                      yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                      y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                      y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                      yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                      y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                      y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                      ! m = 5 and -5
                      yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                              *(sin(thetai))**5.0
                      y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                      y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                      yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                              *(sin(thetaj))**5.0
                      y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                      y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                      ! m = 4 and -4
                      yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                             *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                      y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                      y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                      yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                             *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                      y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                      y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                      ! m = 3 and -3
                      yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                             *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                      y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                      y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                      yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                             *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                      y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                      y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                      ! m = 2 and -2
                      yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                            *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                            *(sin(thetai))**2.0
                      y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                      y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                      yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                            *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                            *(sin(thetaj))**2.0
                      y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                      y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                      ! m = 1 and -1
                      yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                             *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                             *(sin(thetai))
                      y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                      y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                      yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                             *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                             *(sin(thetaj))
                      y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                      y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                      ! m = 0
                      yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                            *(-5.0 + 105.0*(cos(thetai))**2.0&
                            - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                      y60(i) = y60(i) + yhelp

                      yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                            *(-5.0 + 105.0*(cos(thetaj))**2.0&
                            - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                      y60(j) = y60(j) + yhelp
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rsqroot = rsq**0.5
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                         ! keep track of neighbor indeces
                         nbmat(i,nc(i)) = j
                         nbmat(j,nc(j)) = i
                         ! calculate angles
                         thetai = acos(delz/rsqroot)
                         phii   = atan2(dely,delx)
                         thetaj = acos(-delz/rsqroot)
                         phij   = atan2(-dely,-delx)
                         ! calculate Ylm
                         !for l = 6
                         ! m = 6 and -6
                         yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                         y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                         y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                         yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                         y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                         y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                         ! m = 5 and -5
                         yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                                 *(sin(thetai))**5.0
                         y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                         y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                         yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                                 *(sin(thetaj))**5.0
                         y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                         y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                         ! m = 4 and -4
                         yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                                *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                         y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                         y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                         yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                                *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                         y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                         y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                         ! m = 3 and -3
                         yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                                *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                         y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                         y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                         yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                                *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                         y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                         y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                         ! m = 2 and -2
                         yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                               *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                               *(sin(thetai))**2.0
                         y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                         y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                         yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                               *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                               *(sin(thetaj))**2.0
                         y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                         y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                         ! m = 1 and -1
                         yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                                *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                                *(sin(thetai))
                         y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                         y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                         yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                                *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                                *(sin(thetaj))
                         y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                         y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                         ! m = 0
                         yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                               *(-5.0 + 105.0*(cos(thetai))**2.0&
                               - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                         y60(i) = y60(i) + yhelp

                         yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                               *(-5.0 + 105.0*(cos(thetaj))**2.0&
                               - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                         y60(j) = y60(j) + yhelp
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rsqroot = rsq**0.5
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                   ! keep track of neighbor indeces
                   nbmat(i,nc(i)) = j
                   nbmat(j,nc(j)) = i
                   ! calculate angles
                   thetai = acos(delz/rsqroot)
                   phii   = atan2(dely,delx)
                   thetaj = acos(-delz/rsqroot)
                   phij   = atan2(-dely,-delx)
                   ! calculate Ylm
                   !for l = 6
                   ! m = 6 and -6
                   yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetai))**6.0
                   y66(i)  = y66(i)  + yhelp*cmplx(cos(6*phii),sin(6*phii))
                   y6m6(i) = y6m6(i) + yhelp*cmplx(cos(-6*phii),sin(-6*phii))

                   yhelp = (1.0/64.0)*sqrt(3003.0/(pi))*(sin(thetaj))**6.0
                   y66(j)  = y66(j)  + yhelp*cmplx(cos(6*phij),sin(6*phij))
                   y6m6(j) = y6m6(j) + yhelp*cmplx(cos(-6*phij),sin(-6*phij))
                   ! m = 5 and -5
                   yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetai))&
                           *(sin(thetai))**5.0
                   y65(i)  = y65(i)  + yhelp*cmplx(cos(5*phii),sin(5*phii))
                   y6m5(i) = y6m5(i) - yhelp*cmplx(cos(-5*phii),sin(-5*phii))

                   yhelp = -(3.0/32.0)*sqrt(1001.0/(pi))*(cos(thetaj))&
                           *(sin(thetaj))**5.0
                   y65(j)  = y65(j)  + yhelp*cmplx(cos(5*phij),sin(5*phij))
                   y6m5(j) = y6m5(j) - yhelp*cmplx(cos(-5*phij),sin(-5*phij))
                   ! m = 4 and -4
                   yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                          *(-1+11*(cos(thetai))**2)*(sin(thetai))**4.0
                   y64(i)  = y64(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                   y6m4(i) = y6m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                   yhelp = (3.0/32.0)*SQRT(91.0/(2*pi))&
                          *(-1+11*(cos(thetaj))**2)*(sin(thetaj))**4.0
                   y64(j)  = y64(j)  + yhelp*cmplx(cos(4*phij),sin(4*phij))
                   y6m4(j) = y6m4(j) + yhelp*cmplx(cos(-4*phij),sin(-4*phij))
                   ! m = 3 and -3
                   yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                          *(-3+11*(cos(thetai))**2.0)*(sin(thetai))**3.0
                   y63(i)  = y63(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                   y6m3(i) = y6m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                   yhelp = -(1.0/32.0)*SQRT(1365.0/(pi))&
                          *(-3+11*(cos(thetaj))**2.0)*(sin(thetaj))**3.0
                   y63(j)  = y63(j)  + yhelp*cmplx(cos(3*phij),sin(3*phij))
                   y6m3(j) = y6m3(j) - yhelp*cmplx(cos(-3*phij),sin(-3*phij))
                   ! m = 2 and -2
                   yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                         *(1.0-18.0*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                         *(sin(thetai))**2.0
                   y62(i)  = y62(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                   y6m2(i) = y6m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                   yhelp = (1.0/64.0)*SQRT(1365.0/(pi))&
                         *(1.0-18.0*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                         *(sin(thetaj))**2.0
                   y62(j)  = y62(j)  + yhelp*cmplx(cos(2*phij),sin(2*phij))
                   y6m2(j) = y6m2(j) + yhelp*cmplx(cos(-2*phij),sin(-2*phij))
                   ! m = 1 and -1
                   yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                          *(5-30*(cos(thetai))**2.0+33*(cos(thetai))**4.0)&
                          *(sin(thetai))
                   y61(i)  = y61(i)  + yhelp*cmplx(cos(phii),sin(phii))
                   y6m1(i) = y6m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                   yhelp = -(1.0/16.0)*SQRT(273.0/(2*pi))&
                          *(5-30*(cos(thetaj))**2.0+33*(cos(thetaj))**4.0)&
                          *(sin(thetaj))
                   y61(j)  = y61(j)  + yhelp*cmplx(cos(phij),sin(phij))
                   y6m1(j) = y6m1(j) - yhelp*cmplx(cos(-phij),sin(-phij))
                   ! m = 0
                   yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                         *(-5.0 + 105.0*(cos(thetai))**2.0&
                         - 315.0*(cos(thetai))**4.0 +231.0*(cos(thetai))**6.0)
                   y60(i) = y60(i) + yhelp

                   yhelp = ((1.0/32.0)/SQRT(13.0/pi))&
                         *(-5.0 + 105.0*(cos(thetaj))**2.0&
                         - 315.0*(cos(thetaj))**4.0 +231.0*(cos(thetaj))**6.0)
                   y60(j) = y60(j) + yhelp
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch

    ! calculate q_lm (Eq. (36) Zakhari.et.al.2017 PRL)
    do i = 1, nparticles
        if (nc(i).ne.0) then
           q66(i) = y66(i)/nc(i)
           q65(i) = y65(i)/nc(i)
           q64(i) = y64(i)/nc(i)
           q63(i) = y63(i)/nc(i)
           q62(i) = y62(i)/nc(i)
           q61(i) = y61(i)/nc(i)
           q60(i) = y60(i)/nc(i)
           q6m1(i) = y6m1(i)/nc(i)
           q6m2(i) = y6m2(i)/nc(i)
           q6m3(i) = y6m3(i)/nc(i)
           q6m4(i) = y6m4(i)/nc(i)
           q6m5(i) = y6m5(i)/nc(i)
           q6m6(i) = y6m6(i)/nc(i)
        ! else they are all set to zero anyway
       end if
    end do

    ! calculate qbar_lm (Eq. (35) Zakhari.et.al.2017 PRL)
    !! initialize qbar_lm values to be euqal to qlm that of particle i
    qbar66(:) = q66(:)
    qbar65(:) = q65(:)
    qbar64(:) = q64(:)
    qbar63(:) = q63(:)
    qbar62(:) = q62(:)
    qbar61(:) = q61(:)
    qbar60(:) = q60(:)
    qbar6m1(:) = q6m1(:)
    qbar6m2(:) = q6m2(:)
    qbar6m3(:) = q6m3(:)
    qbar6m4(:) = q6m4(:)
    qbar6m5(:) = q6m5(:)
    qbar6m6(:) = q6m6(:)
    do i = 1, nparticles
       do j = 1,20
         nbindex = nbmat(i,j)
         if (nbindex.ne.0) then
           qbar66(i) = qbar66(i) + q66(nbindex)
           qbar65(i) = qbar65(i) + q65(nbindex)
           qbar64(i) = qbar64(i) + q64(nbindex)
           qbar63(i) = qbar63(i) + q63(nbindex)
           qbar62(i) = qbar62(i) + q62(nbindex)
           qbar61(i) = qbar61(i) + q61(nbindex)
           qbar60(i) = qbar60(i) + q60(nbindex)
           qbar6m1(i) = qbar6m1(i) + q6m1(nbindex)
           qbar6m2(i) = qbar6m2(i) + q6m2(nbindex)
           qbar6m3(i) = qbar6m3(i) + q6m3(nbindex)
           qbar6m4(i) = qbar6m4(i) + q6m4(nbindex)
           qbar6m5(i) = qbar6m5(i) + q6m5(nbindex)
           qbar6m6(i) = qbar6m6(i) + q6m6(nbindex)
         end if
       end do
    end do
    !! divide by nc+1
    qbar66(:) = qbar66(:)/(nc(:)+1)
    qbar65(:) = qbar65(:)/(nc(:)+1)
    qbar64(:) = qbar64(:)/(nc(:)+1)
    qbar63(:) = qbar63(:)/(nc(:)+1)
    qbar62(:) = qbar62(:)/(nc(:)+1)
    qbar61(:) = qbar61(:)/(nc(:)+1)
    qbar60(:) = qbar60(:)/(nc(:)+1)
    qbar6m1(:) = qbar6m1(:)/(nc(:)+1)
    qbar6m2(:) = qbar6m2(:)/(nc(:)+1)
    qbar6m3(:) = qbar6m3(:)/(nc(:)+1)
    qbar6m4(:) = qbar6m4(:)/(nc(:)+1)
    qbar6m5(:) = qbar6m5(:)/(nc(:)+1)
    qbar6m6(:) = qbar6m6(:)/(nc(:)+1)

    ! calculate qbar_l (Eq. (34) Zakhari.et.al.2017 PRL)
    do i = 1, nparticles
      !! calculate sum
      q6(i) =  (abs(qbar66(i)))**2 + (abs(qbar65(i)))**2 + (abs(qbar64(i)))**2 &
            +  (abs(qbar63(i)))**2 + (abs(qbar62(i)))**2 + (abs(qbar61(i)))**2 &
            +  (abs(qbar60(i)))**2 &
            +  (abs(qbar6m1(i)))**2 +(abs(qbar6m2(i)))**2+(abs(qbar6m3(i)))**2 &
            +  (abs(qbar6m4(i)))**2 +(abs(qbar6m5(i)))**2+(abs(qbar6m6(i)))**2
      !! multiply by factor and taking the sqrt
      q6(i) = sqrt((4*pi/13.0)*q6(i))
    end do

  end subroutine eval_order_parameter

  subroutine eval_order_parameterq4(cells, yes_delta, r, q4)

    ! Declare local constant Pi
    real, parameter :: pi = 3.1415927

    type(cells_t), intent(inout) :: cells
    logical, intent(in) :: yes_delta
    real, intent(in) :: r
    integer, dimension(:), allocatable :: nc
    integer :: c,d,i,j,neighcell,itype,jtype,nparticles,ntypes
    logical :: typeradius_flag
    real :: rcutsq,rsq,delx,dely,delz,xtmp,ytmp,ztmp
    real, dimension(:,:), allocatable :: rsqmat
    ! parameters related to calculation of Q4
    real, dimension(:), intent(inout) :: q4
    complex, dimension(:), allocatable :: qbar44, qbar43, qbar42, qbar41, qbar40
    complex, dimension(:), allocatable :: qbar4m4, qbar4m3, qbar4m2, qbar4m1
    real :: thetai, phii, thetaj, phij, rsqroot
    ! helping variables
    complex, dimension(:), allocatable :: y44,y43,y42,y41,y40
    complex, dimension(:), allocatable :: y4m4,y4m3, y4m2,y4m1
    real :: yhelp
    complex, dimension(:), allocatable :: q44, q43, q42, q41, q40
    complex, dimension(:), allocatable :: q4m4, q4m3, q4m2, q4m1
    integer, dimension(:,:), allocatable :: nbmat
    integer :: nbindex

    nparticles = cells%particle%nparticles
    typeradius_flag = cells%particle%typeradius_flag
    ntypes = cells%particle%ntypes

    ! default rcutsq for yes_delta is false
    rcutsq = r*r

    if (yes_delta .and. typeradius_flag) then
       allocate(rsqmat(ntypes,ntypes))
       do i = 1, ntypes
          do j = 1, ntypes
             rsqmat(i,j) = cells%particle%typeradius(i) &
                         + cells%particle%typeradius(j) &
                         + r
          end do
       end do
       rsqmat(:,:) = rsqmat(:,:)*rsqmat(:,:)
    end if
    ! allocate memory
    allocate(qbar44(nparticles),qbar43(nparticles),qbar42(nparticles),&
             qbar41(nparticles),qbar40(nparticles),&
             qbar4m4(nparticles),qbar4m3(nparticles),&
             qbar4m2(nparticles),qbar4m1(nparticles))
    allocate(q44(nparticles),q43(nparticles),q42(nparticles),q41(nparticles),&
             q40(nparticles),q4m4(nparticles),&
             q4m3(nparticles),q4m2(nparticles),q4m1(nparticles))
    allocate(y44(nparticles),y43(nparticles),y42(nparticles),y41(nparticles),&
             y40(nparticles),y4m4(nparticles),y4m3(nparticles),y4m2(nparticles),&
             y4m1(nparticles))
    allocate(nbmat(nparticles,20))
    allocate(nc(nparticles))
    !allocate(q4(nparticles))


    nc(:) = 0
    ! initialize variables for q4
    q4(:) = 0.0
    nbindex = 0
    nbmat(:,:) = 0
    q44 = (0.0, 0.0)
    q43 = (0.0, 0.0)
    q42 = (0.0, 0.0)
    q41 = (0.0, 0.0)
    q40 = (0.0, 0.0)
    q4m1 = (0.0, 0.0)
    q4m2 = (0.0, 0.0)
    q4m3 = (0.0, 0.0)
    q4m4 = (0.0, 0.0)
    ! reset helping parameters
    y44  = (0.0, 0.0)
    y43  = (0.0, 0.0)
    y42  = (0.0, 0.0)
    y41  = (0.0, 0.0)
    y40  = (0.0, 0.0)
    y4m1  = (0.0, 0.0)
    y4m2  = (0.0, 0.0)
    y4m3  = (0.0, 0.0)
    y4m4  = (0.0, 0.0)
    yhelp = 0.0

    ! keeping logical evaluation out of inner loop
    ! first case: uniform cutoff

    option_switch: if (.not. yes_delta) then

       if_cells_uniform: if (cells%cells_exist) then

          cell_loop_uniform: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rsqroot = rsq**0.5
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                      ! keep track of neighbor indeces
                      nbmat(i,nc(i)) = j
                      nbmat(j,nc(j)) = i
                      ! calculate angles
                      thetai = acos(delz/rsqroot)
                      phii   = atan2(dely,delx)
                      thetaj = acos(-delz/rsqroot)
                      phij   = atan2(-dely,-delx)
                      ! calculate Ylm
                      !for l = 4
                      ! m = 4 and -4
                      yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                      y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                      y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                      ! m = 3 and -3
                      yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                      y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                      y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                      ! m = 2 and -2
                      yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                      y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                      y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                      ! m = 1 and -1
                      yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                      y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                      y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                      ! m = 0
                      yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                      y40(i) = y40(i) + yhelp
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do
             ! calculate q_lm (Eq. (36) Zakhari.et.al.2017 PRL)
             q44(:) = y44(:)/nc(:)
             q43(:) = y43(:)/nc(:)

             ! calculate qbar_lm (Eq. (35) Zakhari.et.al.2017 PRL)
             i = cells%cellhead(c)
             do while (i > 0)
                j = cells%next(i)
                do while (j > 0)

                  j = cells%next(j)
                end do
               i = cells%next(i)
             end do


             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rsqroot = rsq**0.5
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                         ! keep track of neighbor indeces
                         nbmat(i,nc(i)) = j
                         nbmat(j,nc(j)) = i
                         ! calculate angles
                         thetai = acos(delz/rsqroot)
                         phii   = atan2(dely,delx)
                         thetaj = acos(-delz/rsqroot)
                         phij   = atan2(-dely,-delx)
                         ! calculate Ylm
                         !for l = 4
                         ! m = 4 and -4
                         yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                         y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                         y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                         ! m = 3 and -3
                         yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                         y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                         y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                         ! m = 2 and -2
                         yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                         y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                         y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                         ! m = 1 and -1
                         yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                         y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                         y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                         ! m = 0
                         yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                         y40(i) = y40(i) + yhelp
 
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_uniform

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rsqroot = rsq**0.5
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                   ! keep track of neighbor indeces
                   nbmat(i,nc(i)) = j
                   nbmat(j,nc(j)) = i
                   ! calculate angles
                   thetai = acos(delz/rsqroot)
                   phii   = atan2(dely,delx)
                   thetaj = acos(-delz/rsqroot)
                   phij   = atan2(-dely,-delx)
                   ! calculate Ylm
                   !for l = 4
                   ! m = 4 and -4
                   yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                   y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                   y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                   ! m = 3 and -3
                   yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                   y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                   y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                   ! m = 2 and -2
                   yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                   y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                   y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                   ! m = 1 and -1
                   yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                   y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                   y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                   ! m = 0
                   yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                   y40(i) = y40(i) + yhelp
                end if
             end do
          end do

       end if if_cells_uniform

    else if (typeradius_flag) then ! can use matrix cutoffs

       if_cells_typeradius: if (cells%cells_exist) then

          cell_loop_typeradius: do c = 1, cells%ncells

             ! below, all in the ith cell
             ! loop over all particles i,j in this cell c
             ! cellhead(c) is the first i particle
             ! the first (distinct) j is the next of i

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   jtype = cells%particle%type(j)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rsqroot = rsq**0.5
                   rcutsq = rsqmat(itype,jtype)
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                      ! keep track of neighbor indeces
                      nbmat(i,nc(i)) = j
                      nbmat(j,nc(j)) = i
                      ! calculate angles
                      thetai = acos(delz/rsqroot)
                      phii   = atan2(dely,delx)
                      thetaj = acos(-delz/rsqroot)
                      phij   = atan2(-dely,-delx)
                      ! calculate Ylm
                      !for l = 4
                      ! m = 4 and -4
                      yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                      y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                      y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                      ! m = 3 and -3
                      yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                      y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                      y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                      ! m = 2 and -2
                      yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                      y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                      y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                      ! m = 1 and -1
                      yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                      y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                      y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                      ! m = 0
                      yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                      y40(i) = y40(i) + yhelp
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                itype = cells%particle%type(i)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      jtype = cells%particle%type(j)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rsqroot = rsq**0.5
                      rcutsq = rsqmat(itype,jtype)
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                         ! keep track of neighbor indeces
                         nbmat(i,nc(i)) = j
                         nbmat(j,nc(j)) = i
                         ! calculate angles
                         thetai = acos(delz/rsqroot)
                         phii   = atan2(dely,delx)
                         thetaj = acos(-delz/rsqroot)
                         phij   = atan2(-dely,-delx)
                         ! calculate Ylm
                         !for l = 4
                         ! m = 4 and -4
                         yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                         y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                         y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                         ! m = 3 and -3
                         yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                         y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                         y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                         ! m = 2 and -2
                         yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                         y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                         y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                         ! m = 1 and -1
                         yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                         y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                         y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                         ! m = 0
                         yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                         y40(i) = y40(i) + yhelp
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_typeradius

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             itype = cells%particle%type(i)
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                jtype = cells%particle%type(j)
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rsqroot = rsq**0.5
                rcutsq = rsqmat(itype,jtype)
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                   ! keep track of neighbor indeces
                   nbmat(i,nc(i)) = j
                   nbmat(j,nc(j)) = i
                   ! calculate angles
                   thetai = acos(delz/rsqroot)
                   phii   = atan2(dely,delx)
                   thetaj = acos(-delz/rsqroot)
                   phij   = atan2(-dely,-delx)
                   ! calculate Ylm
                   !for l = 4
                   ! m = 4 and -4
                   yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                   y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                   y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                   ! m = 3 and -3
                   yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                   y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                   y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                   ! m = 2 and -2
                   yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                   y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                   y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                   ! m = 1 and -1
                   yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                   y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                   y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                   ! m = 0
                   yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                   y40(i) = y40(i) + yhelp
                end if
             end do
          end do

       end if if_cells_typeradius

    else ! have to use per-particle cutoff based on delta

       if_cells_perparticle: if (cells%cells_exist) then

          cell_loop_parparticle: do c = 1, cells%ncells

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                j = cells%next(i)
                do while (j > 0)
                   delx = cells%particle%x(1,j) - xtmp
                   dely = cells%particle%x(2,j) - ytmp
                   delz = cells%particle%x(3,j) - ztmp
                   call cells%domain%minimum_image(delx, dely, delz)
                   rsq = delx*delx + dely*dely + delz*delz
                   rsqroot = rsq**0.5
                   rcutsq = cells%particle%radius(i)
                   rcutsq = rcutsq + cells%particle%radius(j)
                   rcutsq = rcutsq + r
                   rcutsq = rcutsq*rcutsq
                   if (rsq < rcutsq) then
                      nc(i) = nc(i) + 1
                      nc(j) = nc(j) + 1
                      ! keep track of neighbor indeces
                      nbmat(i,nc(i)) = j
                      nbmat(j,nc(j)) = i
                      ! calculate angles
                      thetai = acos(delz/rsqroot)
                      phii   = atan2(dely,delx)
                      thetaj = acos(-delz/rsqroot)
                      phij   = atan2(-dely,-delx)
                      ! calculate Ylm
                      !for l = 4
                      ! m = 4 and -4
                      yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                      y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                      y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                      ! m = 3 and -3
                      yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                      y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                      y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                      ! m = 2 and -2
                      yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                      y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                      y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                      ! m = 1 and -1
                      yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                      y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                      y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                      ! m = 0
                      yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                      y40(i) = y40(i) + yhelp
                   end if
                   j = cells%next(j)
                end do
                i = cells%next(i)
             end do

             ! loop over distinct cells
             ! should I make this part of previous loop?

             i = cells%cellhead(c)
             do while (i > 0)
                xtmp = cells%particle%x(1,i)
                ytmp = cells%particle%x(2,i)
                ztmp = cells%particle%x(3,i)
                do d = 1, cells%nstencil
                   neighcell = cells%stencil_neighbor(c,d)
                   if (neighcell == 0) cycle
                   j = cells%cellhead(neighcell)
                   do while (j > 0)
                      delx = cells%particle%x(1,j) - xtmp
                      dely = cells%particle%x(2,j) - ytmp
                      delz = cells%particle%x(3,j) - ztmp
                      call cells%domain%minimum_image(delx, dely, delz)
                      rsq = delx*delx + dely*dely + delz*delz
                      rsqroot = rsq**0.5
                      rcutsq = cells%particle%radius(i)
                      rcutsq = rcutsq + cells%particle%radius(j)
                      rcutsq = rcutsq + r
                      rcutsq = rcutsq*rcutsq
                      if (rsq < rcutsq) then
                         nc(i) = nc(i) + 1
                         nc(j) = nc(j) + 1
                         ! keep track of neighbor indeces
                         nbmat(i,nc(i)) = j
                         nbmat(j,nc(j)) = i
                         ! calculate angles
                         thetai = acos(delz/rsqroot)
                         phii   = atan2(dely,delx)
                         thetaj = acos(-delz/rsqroot)
                         phij   = atan2(-dely,-delx)
                         ! calculate Ylm
                         !for l = 4
                         ! m = 4 and -4
                         yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                         y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                         y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                         ! m = 3 and -3
                         yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                         y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                         y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                         ! m = 2 and -2
                         yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                         y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                         y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                         ! m = 1 and -1
                         yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                         y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                         y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                         ! m = 0
                         yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                         y40(i) = y40(i) + yhelp
                      end if
                      j = cells%next(j)
                   end do
                end do
                i = cells%next(i)
             end do

          end do cell_loop_parparticle

       else ! not using cells, so loop over all pairs

          do i = 1, nparticles-1
             xtmp = cells%particle%x(1,i)
             ytmp = cells%particle%x(2,i)
             ztmp = cells%particle%x(3,i)
             do j = i+1, nparticles
                delx = cells%particle%x(1,j) - xtmp
                dely = cells%particle%x(2,j) - ytmp
                delz = cells%particle%x(3,j) - ztmp
                call cells%domain%minimum_image(delx, dely, delz)
                rsq = delx*delx + dely*dely + delz*delz
                rsqroot = rsq**0.5
                rcutsq = cells%particle%radius(i)
                rcutsq = rcutsq + cells%particle%radius(j)
                rcutsq = rcutsq + r
                rcutsq = rcutsq*rcutsq
                if (rsq < rcutsq) then
                   nc(i) = nc(i) + 1
                   nc(j) = nc(j) + 1
                   ! keep track of neighbor indeces
                   nbmat(i,nc(i)) = j
                   nbmat(j,nc(j)) = i
                   ! calculate angles
                   thetai = acos(delz/rsqroot)
                   phii   = atan2(dely,delx)
                   thetaj = acos(-delz/rsqroot)
                   phij   = atan2(-dely,-delx)
                   ! calculate Ylm
                   !for l = 4
                   ! m = 4 and -4
                   yhelp = (3.0/16.0)*SQRT(35.0/(2*pi))*(sin(thetai))**4.0
                   y44(i)  = y44(i)  + yhelp*cmplx(cos(4*phii),sin(4*phii))
                   y4m4(i) = y4m4(i) + yhelp*cmplx(cos(-4*phii),sin(-4*phii))

                   ! m = 3 and -3
                   yhelp = -(3.0/8.0)*SQRT(35.0/(pi))*(cos(thetai))*(sin(thetai))**3.0
                   y43(i)  = y43(i)  + yhelp*cmplx(cos(3*phii),sin(3*phii))
                   y4m3(i) = y4m3(i) - yhelp*cmplx(cos(-3*phii),sin(-3*phii))

                   ! m = 2 and -2
                   yhelp = (3.0/8.0)*SQRT(5.0/(2*pi))*(-1.0+7.0*(cos(thetai))**2.0)*(sin(thetai))**2.0
                   y42(i)  = y42(i)  + yhelp*cmplx(cos(2*phii),sin(2*phii))
                   y4m2(i) = y4m2(i) + yhelp*cmplx(cos(-2*phii),sin(-2*phii))

                   ! m = 1 and -1
                   yhelp = -(3.0/8.0)*SQRT(5.0/(pi))*(-3.0+7.0*(cos(thetai))**2.0)*(sin(thetai))
                   y41(i)  = y41(i)  + yhelp*cmplx(cos(phii),sin(phii))
                   y4m1(i) = y4m1(i) - yhelp*cmplx(cos(-phii),sin(-phii))

                   ! m = 0
                   yhelp = ((3.0/16.0)/SQRT(pi))*(3.0 - 30.0*(cos(thetai))**2.0+35.0*(cos(thetai))**4.0)
                   y40(i) = y40(i) + yhelp
                end if
             end do
          end do

       end if if_cells_perparticle

    end if option_switch

    ! calculate q_lm (Eq. (36) Zakhari.et.al.2017 PRL)
    do i = 1, nparticles
        if (nc(i).ne.0) then
           q44(i) = y44(i)/nc(i)
           q43(i) = y43(i)/nc(i)
           q42(i) = y42(i)/nc(i)
           q41(i) = y41(i)/nc(i)
           q40(i) = y40(i)/nc(i)
           q4m1(i) = y4m1(i)/nc(i)
           q4m2(i) = y4m2(i)/nc(i)
           q4m3(i) = y4m3(i)/nc(i)
           q4m4(i) = y4m4(i)/nc(i)
        ! else they are all set to zero anyway
       end if
    end do

    ! calculate qbar_lm (Eq. (35) Zakhari.et.al.2017 PRL)
    !! initialize qbar_lm values to be euqal to qlm that of particle i
    qbar44(:) = q44(:)
    qbar43(:) = q43(:)
    qbar42(:) = q42(:)
    qbar41(:) = q41(:)
    qbar40(:) = q40(:)
    qbar4m1(:) = q4m1(:)
    qbar4m2(:) = q4m2(:)
    qbar4m3(:) = q4m3(:)
    qbar4m4(:) = q4m4(:)
    do i = 1, nparticles
       do j = 1,20
         nbindex = nbmat(i,j)
         if (nbindex.ne.0) then
           qbar44(i) = qbar44(i) + q44(nbindex)
           qbar43(i) = qbar43(i) + q43(nbindex)
           qbar42(i) = qbar42(i) + q42(nbindex)
           qbar41(i) = qbar41(i) + q41(nbindex)
           qbar40(i) = qbar40(i) + q40(nbindex)
           qbar4m1(i) = qbar4m1(i) + q4m1(nbindex)
           qbar4m2(i) = qbar4m2(i) + q4m2(nbindex)
           qbar4m3(i) = qbar4m3(i) + q4m3(nbindex)
           qbar4m4(i) = qbar4m4(i) + q4m4(nbindex)
         end if
       end do
    end do
    !! divide by nc+1
    qbar44(:) = qbar44(:)/(nc(:)+1)
    qbar43(:) = qbar43(:)/(nc(:)+1)
    qbar42(:) = qbar42(:)/(nc(:)+1)
    qbar41(:) = qbar41(:)/(nc(:)+1)
    qbar40(:) = qbar40(:)/(nc(:)+1)
    qbar4m1(:) = qbar4m1(:)/(nc(:)+1)
    qbar4m2(:) = qbar4m2(:)/(nc(:)+1)
    qbar4m3(:) = qbar4m3(:)/(nc(:)+1)
    qbar4m4(:) = qbar4m4(:)/(nc(:)+1)

    ! calculate qbar_l (Eq. (34) Zakhari.et.al.2017 PRL)
    do i = 1, nparticles
      !! calculate sum
      q4(i) = (abs(qbar44(i)))**2 +  (abs(qbar43(i)))**2 + (abs(qbar42(i)))**2 &
            + (abs(qbar41(i)))**2 +  (abs(qbar40(i)))**2 &
            +  (abs(qbar4m1(i)))**2 +(abs(qbar4m2(i)))**2+(abs(qbar4m3(i)))**2 &
            +  (abs(qbar4m4(i)))**2
      !! multiply by factor and taking the sqrt
      q4(i) = sqrt((4*pi/9.0)*q4(i))
    end do

  end subroutine eval_order_parameterq4

end module mod_stat_func
