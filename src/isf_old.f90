program isf_old

  use, intrinsic :: iso_fortran_env
  use, intrinsic :: iso_c_binding
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t, propagate_in_flow
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  !use mod_statistics, only : running_stat_t
  !use mod_stat_func, only : eval_isf_self_3d,eval_isf_full_3d,eval_fft_dens_3d
  use mod_data, only : source_data_t
  use mod_ft_old, only : read_modes, get_isf
  use mod_constants, only : pi
  !use mod_image, only : render_array,symmetrize_halfspace_rot_3d_cmplx,&
  !                      corner_to_center_3d_cmplx,affine_transform,&
  !                      affine_invert,affine_interpolate
  implicit none

  integer :: i,j,k,n,m,nparticles,nargs,snap,nsnaps,nq,qbin,nsnaps_init ! indices
  type(particle_t) :: particle,particle_init,particle_flow
  type(domain_t) :: domain,domain_init
  type(group_t) :: group,group_init
  type(cells_t) :: cells,cells_init
  type(source_data_t) :: source_data
  !complex(c_double_complex), dimension(:), allocatable :: rs_isf_rad
  !integer, dimension(:), allocatable :: counts
  !type(running_stat_t) :: rs_isf_rad_min
  character(len=300) :: buf,directory,fullorself,&
                        my_fmt_qxbin,my_fmt_isf,&
                        fname,tstart_str,tdur_str,qtarg_str,&
                        tinit_str,subtr_yn
  logical :: subtr_tf
  real(real64) :: qx,qxsq,isf_rad_min
  real :: target_size,qmax,delq,qmin,qtarg,qact,qval
  integer(int64) :: tstart,tinit,tdur,tinc,tstep
  integer :: msqtarg,msqact,mlimit
  integer, dimension(3) :: delta_img
  real, parameter :: nyquist_multiplier = 2.0
  ! not using 3d array here to save memory
  ! real(real64), dimension(:,:,:), allocatable :: fin 
  complex :: isf, ssf
  !real(c_double), dimension(:,:,:), allocatable :: dens
  !complex(c_double_complex), dimension(:,:,:), allocatable :: ftdens0
  !complex(c_double_complex), dimension(:,:), allocatable :: isf_all
  !real(real64), dimension(6) :: h_inv
  !real(real64), dimension(12) :: mcomp
  !complex(c_double_complex), dimension(:,:,:), allocatable :: sym
  real(real64), dimension(:), allocatable :: qx_bin
  real(real64) :: dx,dy,dz,scale,kx,ky,kz

  ! read inputs

  nargs = command_argument_count()
  if (nargs .ne. 9) then
     write(*, '(a)') 'isf_old [directory] [tstart] [tinit] [tdur] [tinc] &
                               &[fullorself] [qtarg] [mlimit] [subtr_yn]'
     stop
  end if
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) tstart
  call get_command_argument(3, buf); read(buf, *) tinit
  write(tstart_str,*) tstart
  write(tinit_str,*) tinit
  call get_command_argument(4, buf); read(buf, *) tdur
  write(tdur_str,*) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); fullorself = trim(adjustl(buf))
  call get_command_argument(7, buf); read(buf, *) qtarg
  write(qtarg_str,*) qtarg
  call get_command_argument(8, buf); read(buf, *) mlimit
  call get_command_argument(9, buf); read(buf, *) subtr_yn

  write(fname,'(8a)') 'isf-self-qtarg-', trim(adjustl(qtarg_str)), '-', trim(adjustl(tstart_str)), '-', &
                      &trim(adjustl(tinit_str)), '-', trim(adjustl(tdur_str))
  open(8, file=trim(adjustl(fname)), action='write')

  if (modulo(tdur,tinc) /= 0) then
     write(*,'(a)') 'tdur is not a multiple of tinc'
     stop
  end if
  if (trim(adjustl(fullorself)) /= 'full' .and. trim(adjustl(fullorself)) /= 'self') then
     write(*,'(a)') 'fullorself must be full or self'
     stop
  end if
  if (qtarg <= 0.0) then
     write(*,'(a)') 'qmax must be > 0.0'
     stop
  end if
  if (mlimit < 1) then
     write(*,'(a)') 'nq must be > 0'
     stop
  end if
  if (trim(adjustl(subtr_yn)) .eq. 'yes') then
     subtr_tf = .true.
  else if (trim(adjustl(subtr_yn)) .eq. 'no') then
     subtr_tf = .false.
  else
     write(*, '(a)') '[subtr_yn] must be yes or no'
  end if
  
  ! Init the key classes.

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     write(*,'(a)') 'bad tstart value'
     stop
  end if
  if ((.not. source_data%step_exists(tinit)) .or. (tinit < tstart)) then
     write(*,'(a)') 'bad tinit value'
     stop
  end if
  if (.not. source_data%regular) then
     write(*,'(a)') 'hdf5 timesteps appear irregular'
     write(*,'(a)') 'not supporting nonzero tdur for irregular'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nsnaps_init = int((tinit-tstart)/tinc) + 1
  nparticles = particle%nparticles

  call domain%init(particle)
  call group%init(domain)

  ! need two domain types and two particle types
  ! one for original coords and box
  ! one for time-advanced coords and box
  ! init by copying data read from data files

  call particle%copy(particle_init)
  call particle%copy(particle_flow)
  call domain_init%init(particle_init)
  snap = source_data%step_index(tstart)
  call source_data%set_box(domain_init, snap)

  ! init group, dynam

  call group%init(domain_init)
  call source_data%read_snap(particle_init, domain_init, snap)

  ! unmap to make sure all coordinates are unwrapped

  call domain_init%unmap()

  ! read in modes, calculate target q
  qval = 2*pi/(domain_init%boxhi(1)-domain_init%boxlo(1))
  msqtarg = nint((qtarg * (domain_init%boxhi(1)-domain_init%boxlo(1)) / (2*pi))**2)
  call read_modes(directory, 'one', msqtarg, msqact, mlimit)
  qact = sqrt(real(msqact)) * 2 * pi / (domain_init%boxhi(1)-domain_init%boxlo(1))
  write(8, '(a,f14.6)') 'q*sigma =', qact
  write(8, '(a,i10)') 'Wait time = ', tstart
  write(8, '(a)') 'Offset timesteps, isf_real, isf_imag'

  ! calculate SSF, for normalizing isf later
  if (trim(adjustl(fullorself)) == 'full') then
     call get_isf(trim(adjustl(fullorself)), particle_init, particle_init, qval, subtr_tf, ssf)
  end if

  ! Determine real-space sampling required to resolve maximum wavenumber.
  ! In 1D, need sample increment to be less than (1/2)*(2*pi/qmax).
  ! In 3D, seems okay to find the maximum triclinic sub-cell that fits
  ! inside of a sphere of diameter of (1/2)*(2*pi/qmax).
  ! The .true. option specifies to use the maximizing procedure noted above.

  ! In this code we will fix nyquist_multiplier to 1; To reduce the aliasing
  ! effect the user need to specify a proper qmax.
  ! For a reference, qmax=8.0, lx=125.419 and no-flip box, the fft nodes are 
  ! 452x452x452, which takes 452x452x452x8/1024/1024=704 MB (since here fft 
  ! is out-of-place, there might be a factor of 2. This is the biggest system
  ! I can do on my virtual machine b/c I only allocate 2 GB to RAM.)

  !target_size = pi / 10.0 / nyquist_multiplier! / sqrt(2.0) 
  !call cells%init(group, target_size, .true.)

  !! get initial cells information
  !! needed because isf is a correlation w.r.t. the initial time
  !call cells_init%init(group_init, target_size, .true.)
  !call cells_init%reset_grid()
  !print*,  'setting up fft for initial cells ...'
  !write(*,'(a,3i6)') 'fft nodes:', cells_init%ncellx, cells_init%ncelly, cells_init%ncellz
  !call cells_init%assign_particles()

  ! Loop over snaps.

  delta_img(:)=0
  do n = nsnaps_init, nsnaps

     tstep = tstart + (n-1)*tinc
     snap = source_data%step_index(tstep)

     call source_data%read_snap(particle,domain,snap)
     !call cells%reset_grid()
     !!qmin = 2.0*qmax/cells%ncellx*nyquist_multiplier!*sqrt(2.0)
     print*,  'tstep', tstep
     !write(*,'(a,3i6)') 'fft nodes:', cells%ncellx, cells%ncelly, cells%ncellz
     !call cells%assign_particles()

     ! update flow-advanced mean displacement particle type

     delta_img(:) = delta_img(:) + source_data%image_flips(:,n)
     call propagate_in_flow(domain_init, domain, delta_img, particle_flow)

     ! calculate intermediate scattering function isf

     call get_isf(trim(adjustl(fullorself)), particle_flow, particle, qval, subtr_tf, isf)
     print*,  'done ft'

     ! full ISF scale with SSF
     if (trim(adjustl(fullorself)) == 'full') then
        isf = isf / real(ssf)
     end if
     
     print*,  'done calculating isf, outputting...' 
     ! print output

     write(8, '(i10,2f10.6)') tstep-tstart, real(isf), aimag(isf)
     
  end do

  close(8)

end program isf_old
