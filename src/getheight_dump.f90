! AUTHOR:          Poornima Padmanabhan
! PURPOSE:         Read through a LAMMPS trajectory and calculate height of
! several x-y bins
! INPUT VARIABLES: filename, Lbox, number of frames to read, previous height
! cutoff
! Not sure if we want more constraints for rho = 0.15.

program getheight_dump

  use, intrinsic :: iso_fortran_env
  implicit none

  character(len=300) :: dumppath, buf

  integer :: i,j,k,nparticles,nargs,nsnaps
  integer(int64) :: tsnap
  real zcoord, xcoord, ycoord
  integer ntrack, id, ptype
  real :: meanz0, meanz
  real :: rho(20, 20, 100), Lbox, height, prevh, rhoz(100)
  integer :: a,b,c,h,xbin,ybin,zbin
  xbin = 10
  ybin = 10
  zbin = 100

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 4) then
     write(*, '(A)') 'geth [filename] [Lbox] [nframes] [prevh]'
     stop
  endif
  call get_command_argument(1, buf); dumppath = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) Lbox
  call get_command_argument(3, buf); read(buf, *) nsnaps
  call get_command_argument(4, buf); read(buf, *) prevh
  !================================================================
  ! Main program

  ! get npart from file
  open(7, file=trim(dumppath), action='read')
  read(7, *) 
  read(7, *) 
  read(7, *) ! ITEM: NUMBER OF ATOMS
  read(7, *) nparticles

  close(7)

  prevh = zbin*prevh
  ! loop over snaps in file
  open(7, file=trim(dumppath), action='read')
  do i = 1, nsnaps
     ! initialize rho
     rho(:,:,:) = 0
     rhoz(:) = 0
     height = xbin*ybin ! start with height = 1
     read(7, *)
     read(7, *) tsnap
     do j = 3, 9
        read(7, *)
     end do
     do j = 1, nparticles
        read(7, *) id, ptype, xcoord, ycoord, zcoord ! and other data
        a = floor(xbin*xcoord/Lbox)+1
        b = floor(ybin*ycoord/Lbox)+1
        c = floor(zbin*zcoord/Lbox)+1
        if (a .gt. 0 .and. b .gt. 0 .and. c .gt. 0) then
        if  (a .le. xbin .and. b .le. ybin .and. c .le. zbin) then
          rho(a,b,c) = rho(a,b,c) + 1
          rhoz(c) = rhoz(c) + 1
        end if
        end if
     end do
     rho(:,:,:) = rho(:,:,:)*3.14159*xbin*ybin*zbin/(6*Lbox**3)
     rhoz(:) = rhoz(:)*3.14159*zbin/(6*Lbox**3)
     ! find average height. But first, to only account for interfacial and not
     ! bulk noise, we have one or two constraints:
     ! 1. ensure we are not too far from prevh
     ! 2. look for rho(z) < 0.15 and then search for height
     do h = 1,zbin
       if (rhoz(h) < 0.15 .and. (prevh - h) < 0.1*zbin ) then
!       if ((prevh - h) < 0.1*zbin ) then
         do a = 1,xbin
         do b = 1,ybin
         do c = h,zbin
         if (rho(a,b,c) < 0.1) then ! can possible change this constraint later
!         if (i .eq. 1) then
!           write(*,*) 'Some bin already starts with low density. Exiting...'
!           stop
!         end if 
           height = height - (1- float(c)/float(zbin))
           exit ! get out of this loop
         end if
         enddo
         enddo
         enddo
         exit
       end if
     end do
     height = height / (xbin*ybin)
     prevh = floor(zbin*height)
     write(*,*) tsnap, height
  end do

  close(7)

end program getheight_dump
