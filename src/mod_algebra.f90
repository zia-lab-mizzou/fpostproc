module mod_algebra

  use, intrinsic :: iso_fortran_env
  implicit none
  private

contains

  ! ----------------------------------------------------------------------

  pure function det_voigt_dp(m)
    real(dp), dimension(6), intent(in) :: m
    real(dp) :: det
    det = m(1)*(m(2)*m(3) - m(4)*m(4)) &
         - (m(2)*m(5)*m(5) + m(3)*m(6)*m(6)) &
         + 2*m(4)*m(5)*m(6)
  end function det_voigt_dp

  ! ----------------------------------------------------------------------
  ! Get eigenvalues using Cardano's method on a symmetric matrix
  ! b = 2*cos[(1/3)*arccos{det(B)/2} + 2*k*pi/3], for k=0,1,2
  ! p = trace[(A-qI)^2 / 6]^(1/2)
  ! q = trace(A)/3
  ! Eigenvalues a = p*b + q
  ! ----------------------------------------------------------------------

  pure subroutine eigvals_voigt_dp(m, vals)
    real(dp), dimension(6), intent(in) :: m
    real(dp), dimension(3), intent(out) :: vals
    real(dp), dimension(6) :: b
    real(dp) :: p1,p2,p,q,r,phi
    p1 = m(4)*m(4) + m(5)*m(5) + m(6)*m(6)
    if (p1 == 0) then
       vals(:) = m(1:3)
    else
       q = THIRD_DP * (m(1)+m(2)+m(3))
       p2 = (m(1)-q)^2 + (m(2)-q)^2 + (m(3)-q)^2 + 2*p1
       p = sqrt(SIXTH_DP * p2)
       b(1:3) = m(1:3) - q
       b(4:6) = m(4:6)
       r = 0.5_dp * det_voigt_dp(B)

       if (r <= -1) then
          phi = THIRD_PI_DP
       else if (r >= 1) then
          phi = 0
       else
          phi = THIRD_DP * acos(r)
       end if

       ! vals ordered as eig3 <= eig2 <= eig1

       vals(1) = q + 2*p*cos(phi)
       vals(3) = q + 2*p*cos(phi + TWO_THIRD_PI_DP)
       vals(2) = 3*q - (vals(1) + vals(2))
    end if

  end subroutine eigvals_voigt_dp

  ! ----------------------------------------------------------------------

  pure subroutine eigvecs_voigt_dp(m, vecs, vals)
    real(dp), dimension(6), intent(in) :: m
    real(dp), dimension(3,3), intent(out) :: vecs
    real(dp), dimension(3), intent(out) :: vals
    real(dp) :: det
  end subroutine eigvecs_voigt_dp

  ! ----------------------------------------------------------------------

end module mod_algebra
