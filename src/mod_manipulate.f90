!
! manipulate particle, domain, cells, and neighlist consistently to
!   perform certain actions
!

module mod_manipulate

  use mod_particle, only : particle_t
  use mod_group, only : group_t
  use mod_domain, only : domain_t
  use mod_cells, only : cells_t
  use mod_neighlist, only : neighlist_t
  use, intrinsic :: iso_fortran_env
  implicit none
  private

  ! TODO, multiple instances?  arrays?

  type, public :: manipulate_t
     type(particle_t), pointer :: particle => null()
     type(group_t), pointer :: group => null()
     type(domain_t), pointer :: domain => null()
     type(cells_t), pointer :: cells => null()
     type(neighlist_t), pointer :: neighlist => null()
   contains
     procedure, public :: displace_particles
     procedure, public :: deform
  end type manipulate_t

contains

  ! ----------------------------------------------------------------------
  ! displace particles simultaneously
  ! perform periodic wrapping if required (check if particles left the box)
  ! adjust cells and neighlist if necessary
  ! ----------------------------------------------------------------------

  subroutine displace_particles(this, group, delta, remap)
    class(manipulate_t), intent(inout) :: this
    integer, intent(in) :: group
    real, dimension(3), intent(in) :: delta
    logical, intent(in), optional :: remap
    integer :: i, groupbit

    if (group > this%group%ngroup) then
       print *, 'invalid group'
       stop
    end if

    groupbit = this%group%bitmask(group)

    do i = 1, this%particle%nparticles
       if (iand(this%particle%mask(i),groupbit) /= 0) then
          this%particle%x(:,i) = this%particle%x(:,i) + delta(:)
       end if
    end do

    ! if remap option is absent or is present and true, apply pbc (default)
    ! if present and false, do not
    ! check if the remap keeps all particles in the box

    if (present(remap)) then
       if (.not. remap) continue
    else
       call this%domain%remap()
       if (.not. this%domain%particles_inside()) then
          print *, 'displace_particles lost some particles'
          stop
       end if
    end if

    ! the cells will have changed, but the neighbor list does not if the
    ! group of particles is 'all'

    print *, 'displace particles need to add cell and neighlist adjustment'

  end subroutine displace_particles

  ! ----------------------------------------------------------------------
  ! apply an affine deformation to the box and the entire particle system
  ! the arguments are finite (engineering?) strains, Voigt notation
  ! adjust cells and (but should not require) neighlist if necessary
  ! for example, to double the box volume w/o shear, the strain should be
  ! (/ 2.0**(1.0/3.0), 2.0**(1.0/3.0), 2.0**(1.0/3.0), 0, 0, 0 /)
  ! not the most computationally efficient for shear (too many transformations)
  ! ----------------------------------------------------------------------

  ! TODO should I check for lost particles?

  subroutine deform(this, strain)
    class(manipulate_t), intent(inout) :: this
    real, dimension(6), intent(in) :: strain

    ! determine new boundaries from input finite strain

    ! TODO
!!$
!!$
!!$    ! transform to lamda coordinates for particles
!!$
!!$    call this%domain%x2lamda()
!!$
!!$    ! scale box coordinates
!!$
!!$    call this%domain%set_global_box(boxlo,boxhi,xy,xz,yz)
!!$
!!$    ! transform back to x coordinates
!!$
!!$    call this%domain%lamda2x()

  end subroutine deform

end module mod_manipulate
