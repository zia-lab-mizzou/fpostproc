program dynamicbinPE
! calculate average PE of particles that are in layers of the gel
  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,k,nparticles,nargs,snap,nsnaps
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory
  integer(int64) :: tstart,tdur,tinc,tstep,t_in
  real zcoord, height, treal
  integer zbin
  real :: Lbox,  zcorr, tcorr
  integer, parameter :: nbins = 21 ! may change this later
  integer :: ninbins(21)
  real :: meanpe(21)

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 7) then
     write(*, '(A)') 'dynamicbinPE [directory] [tstart] [tdur] [tinc] [Lbox] [z-corr] [t-corr]'
     stop
  endif
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) tstart
  call get_command_argument(3, buf); read(buf, *) tdur
  call get_command_argument(4, buf); read(buf, *) tinc
  call get_command_argument(5, buf); read(buf, *) Lbox
  call get_command_argument(6, buf); read(buf, *) zcorr
  call get_command_argument(7, buf); read(buf, *) tcorr

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles

  ! init group and cells

  call group%init(domain)

  ! get initial coordinate
 
  tstep = tstart
  snap = source_data%step_index(tstep)

  ! get contact numbers

  call source_data%read_snap(particle, domain, snap)

  ! read h(t) data
  open(unit=10, file = '../dump/height.dat', action = 'read')

  ! loop over snaps
  t_in = 0
  height = 1.0

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     treal = (tstep-tcorr) * 0.00004 
     snap = source_data%step_index(tstep)
   
     ! read height
     do while (t_in < treal)
        read(10,*) t_in, height
        t_in = (t_in-tcorr) * 0.00004
     end do

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     ! read snap

     call source_data%read_snap(particle, domain, snap)
     
     meanpe = 0
     ninbins(:) = 0
     do j = 1, nparticles
       zcoord = particle%x(3,j)
       ! normalize zcoord from 0 to 1
       zcoord = zcoord/Lbox
!       zcoord = (zcoord-zcorr)/Lbox - floor((zcoord-zcorr)/Lbox) 
       ! bin it within the height of the sedimenting bed
       zbin = ceiling(((zcoord)/height)*nbins)
       if (zbin > 0 .and. zbin <= nbins) then
         meanpe(zbin) = meanpe(zbin) + particle%pe(j)
         ninbins(zbin) = ninbins(zbin) + 1
       end if
     end do
     do j = 1,nbins
       meanpe(j) = meanpe(j) / ninbins(j)
     end do
     write(*, '(f10.2)', advance='no') treal
     write(*, '(f8.5)', advance='no') height
     do j = 1, nbins
        write(*, '(f10.5)', advance='no') meanpe(j)
!        write(*,*) meanpe(j)
     end do
        write (*, '(a)') ''

  end do

end program dynamicbinPE
