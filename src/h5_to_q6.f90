module output_h5

  use hdf5
  implicit none

contains

  subroutine prepare_h5out(nsnap, npart, h5out, h5f, h5d, h5fs, h5ms, h5p)

    integer, intent(in) :: nsnap, npart
    character(len=*), intent(in) :: h5out
    integer(hid_t), intent(out) :: h5f, h5d, h5fs, h5ms, h5p
    integer(hid_t) :: h5t
    integer :: hdferr
    integer, parameter :: chunkbytes = 256
    integer(hsize_t), dimension(2) :: dims, count, chunk

    dims = (/ npart, nsnap /)
    chunk = (/ int(real(chunkbytes)/4), 1 /)
    count = (/ npart, 1 /)

    call h5fcreate_f(trim(h5out), H5F_ACC_TRUNC_F, h5f, hdferr)
    call h5screate_simple_f(2, count, h5ms, hdferr)
    call h5screate_simple_f(2, dims, h5fs, hdferr)
    call h5pcreate_f(H5P_DATASET_CREATE_F, h5p, hdferr)
    call h5pset_deflate_f(h5p, 9, hdferr)
    call h5pset_chunk_f(h5p, 2, chunk, hdferr)
    call h5tcopy_f(H5T_STD_U32LE, h5t, hdferr)
    call h5dcreate_f(h5f, "q6", h5t, h5fs, h5d, hdferr, h5p)

  end subroutine prepare_h5out

  subroutine write_q6_snap_h5(nneigh, snap, h5d, h5fs, h5ms)

    real, dimension(:) :: nneigh
    integer, intent(in) :: snap
    integer(hid_t), intent(in) :: h5d, h5fs, h5ms
    integer(hsize_t), dimension(2) :: start, count
    integer :: hdferr

    start = (/ 0, snap-1 /)
    count = (/ size(nneigh), 1 /)
    
    call h5sselect_hyperslab_f(h5fs, H5S_SELECT_SET_F, start, count, hdferr)
    call h5dwrite_f(h5d, H5T_NATIVE_INTEGER, nneigh, count, hdferr, &
         file_space_id=h5fs, mem_space_id=h5ms)

  end subroutine write_q6_snap_h5

  subroutine close_h5out(h5f, h5d, h5fs, h5ms, h5p)

    integer(hid_t), intent(in) :: h5f, h5d, h5fs, h5ms, h5p
    integer :: hdferr

    call h5pclose_f(h5p, hdferr)
    call h5sclose_f(h5ms, hdferr)
    call h5sclose_f(h5fs, hdferr)
    call h5dclose_f(h5d, hdferr)
    call h5fclose_f(h5f, hdferr)

  end subroutine close_h5out

end module output_h5

! MAIN PROGRAM

program h5_to_q6

  use output_h5
  use mod_particle
  use mod_domain
  use mod_group
  use mod_cells
  use mod_stat_func
  use mod_data
  implicit none

  integer :: h5index,i,nsnaps,nparticles,first_snap,nargs,snap
  integer(HID_T) :: lh5f, lh5d, lh5fs, lh5ms, lh5p
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory,h5out
  real, dimension(:), allocatable :: q6
  real :: delta,minsize

  ! read inputs

  nargs = command_argument_count()
  if (nargs /= 3) then
     write(*, '(A)') 'h5_to_q6 [directory] [h5index] [delta]'
     stop
  end if
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) h5index
  call get_command_argument(3, buf); read(buf, *) delta
  call get_command_argument(4, buf)

  print *, 'source data initializing'
  call source_data%init(trim(adjustl(directory)), particle, domain)

  ! check if we have a valid hdf5 file, and get info

  print *, 'validate the file'
  if ((h5index > source_data%ndumps) .or. (h5index < 1)) then
     print *, 'h5index must be between 1 and ', source_data%ndumps
     stop
  end if
  h5out = trim(adjustl(source_data%dumps(h5index)%path))//'.q6'
  print *, 'h5out is ', trim(adjustl(h5out))

  first_snap = 1
  do i = 1, h5index-1
     first_snap = first_snap + source_data%dump_nsnaps(i)
  end do
  nsnaps = source_data%dump_nsnaps(h5index)

  ! get ready for contact number calculation

  print *, 'prepare for q6 calc'
  call group%init(domain)
  nparticles = particle%nparticles
  allocate(q6(nparticles))
  minsize = 2*maxval(particle%typeradius(:)) + delta
  call cells%init(group, minsize, .false.)
  call prepare_h5out(nsnaps, nparticles, h5out, lh5f, lh5d, lh5fs, lh5ms, lh5p)

  ! run on all snaps listed in a 'dump' or hdf5 file

  print *, 'running on snaps'
  do i = 1, nsnaps
     print *, i, '/', nsnaps
     snap = first_snap + i - 1
     q6(:) = 0.0
     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()
     call eval_order_parameter(cells, .true., delta*(sum(particle%typeradius(:))/size(particle%typeradius(:)))*2.0,q6)
     call write_q6_snap_h5(q6, i, lh5d, lh5fs, lh5ms)
  end do

  call close_h5out(lh5f,lh5d,lh5fs,lh5ms,lh5p)

end program h5_to_q6
