! TODO add xperiodic checks for minimum_image (should not wrap to find distance)

program run_tests

  use, intrinsic :: iso_fortran_env
  use, intrinsic :: iso_c_binding
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t, image_flip
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_data, only : source_data_t
  use mod_stat_func
  use mod_image
  use mod_constants, only : pi
  implicit none
  
  character(len=300) :: buf
  character(len=4) :: mode
  logical :: yes_rand
  integer :: nargs
  type(particle_t) :: particle

  nargs = command_argument_count()
  yes_rand = .false.
  if (nargs .gt. 0) then
     call get_command_argument(1, buf); mode = trim(adjustl(buf))
     if (mode == 'rand') then
        yes_rand = .true.
     else
        write(*, '(a)') 'Error: only allowing for rand option'
        stop
     end if
  end if

  ! particle procedure tests
  call test_particle_construct(particle, 1000000)
  call test_particle_destruct(particle)
  call test_particle_can_set_fields()

  ! domain procedure tests
  ! TODO need a test on rectangular-to-triclinic legitimacy

  call test_domain_construct_rect()
  call test_domain_construct_tri()
  call test_rect_to_tri()
  call test_remap_unmap_rect()
  call test_remap_unmap_tri()
  call test_coord_shift_rect()
  call test_coord_shift_tri()
  call test_domain_swap_particle_pointer()
  call test_image_flip()

  ! group tests

  call test_group_construct()
  call test_group_create()

  ! cells procedure tests

  call test_cells_construct()
  call test_cells_assign_rect()
  call test_data_procedures()

  ! statistical functions

  call test_eval_vfrac()
  call test_eval_fabric()
  call test_symmetrize_3d()
  call test_analyt_rdf_rect()
  if (yes_rand) call test_random_rdf()
  call test_analyt_pair_dist_func_rect()
!  call test_random_pair_dist_func_rect()
  call test_analyt_nc_rect()
  call test_analyt_nc_tri()
!  call test_tidy_ssf()
!  call test_analyt_ssf_rect()
  if (yes_rand) call test_random_ssf()
  call test_cluster_id()
  call test_get_flow_unmapped()
  call test_displ_var()
  call test_eval_mean_bond_dist()
  call test_eval_bin_bond_dist()

  ! affine operations
  call test_affine_compose()
  call test_affine_invert()
  call test_affine_interpolate()
  ! call test_affine_transform()
  call test_symmetrize_halfspace_rot_3d()
  call test_centercorner_shift()
contains

  ! ----------------------------------------------------------------------
  ! Print test results (only says passed or failed)
  ! ----------------------------------------------------------------------

  subroutine print_test_result_pf(test, pf)

    character(len=*), intent(in) :: test
    logical, intent(in)          :: pf
    character(len=6)             :: pfstr
    if (pf) then
       pfstr = 'PASSED'
    else
       pfstr = 'FAILED'
    end if
    write(*,'(a)') pfstr//' test '//test

  end subroutine print_test_result_pf

  ! ----------------------------------------------------------------------
  ! Can we construct the particle type?
  ! ----------------------------------------------------------------------

  subroutine test_particle_construct(particle, nparticles)

    type(particle_t), intent(inout) :: particle
    integer, intent(in)         :: nparticles
    character(len=*), parameter :: test_name = 'particle_construct'
    call particle%init(nparticles)

    ! test nparticles is correct and specialized flags are false (default)

    if ( &
         (particle%nparticles == nparticles) .and. &
         (.not. particle%omega_flag) .and. &
         (.not. particle%pe_flag) .and. &
         (.not. particle%stress_flag) &
       ) then
       call print_test_result_pf(test_name, .true.)
       return
    end if

    call print_test_result_pf(test_name, .false.)

  end subroutine test_particle_construct

  ! ----------------------------------------------------------------------
  ! Can we free the memory of the particle type?
  ! ----------------------------------------------------------------------

  subroutine test_particle_destruct(particle)

    type(particle_t), intent(inout) :: particle
    character(len=*), parameter :: test_name = 'particle_destruct'
    call particle%free
    call print_test_result_pf(test_name, .true.)

  end subroutine test_particle_destruct

  ! ----------------------------------------------------------------------
  ! Can we manipulate the particle type components?
  ! ----------------------------------------------------------------------

  subroutine test_particle_can_set_fields()

    type(particle_t) :: particle
    integer, parameter :: nparticles = 10
    integer :: i
    real :: err = 0.
    real, parameter :: tol = 0.000001
    character(len=*), parameter :: test_name = 'particle_can_set_fields'

    call particle%init(nparticles,omega_flag=.true.,pe_flag=.true.,stress_flag=.true.)
    do i = 1, nparticles
       particle%x(:,i) = (/ 1., 1.-i, 1. /)
       particle%v(:,i) = (/ -2., -2.*i, -2. /)
       particle%radius(i) = i/0.95
       particle%omega(:,i) = (/ 0.5-i, 2., 4. /)
       particle%pe(i) = i - 500.
       particle%stress(:,i) = (/ 0.01*i, 2.*i, 3.*i, 4.*i, 5.*i, 6.*i /)
    end do
    do i = 1, nparticles
       err = err + sum(particle%x(:,i) - (/ 1., 1.-i, 1. /))
       err = err + sum(particle%v(:,i) - (/ -2., -2.*i, -2. /))
       err = err + particle%radius(i) - i/0.95
       err = err + sum(particle%omega(:,i) - (/ 0.5-i, 2., 4. /))
       err = err + particle%pe(i) - (i - 500.)
       err = err + &
            sum(particle%stress(:,i) - (/ 0.01*i, 2.*i, 3.*i, 4.*i, 5.*i, 6.*i /))
    end do
    call particle%free
    if (abs(err) < tol) then
       call print_test_result_pf(test_name, .true.)
    else
       call print_test_result_pf(test_name, .false.)
    end if

  end subroutine test_particle_can_set_fields

  ! ----------------------------------------------------------------------
  ! Can we construct a rectangular domain?
  ! ----------------------------------------------------------------------

  subroutine test_domain_construct_rect()

    type(particle_t) :: particle
    type(domain_t) :: domain
    integer, parameter :: nparticles = 10
    real, dimension(3), parameter :: boxlo = (/ 0.2, 3.4, 1.8 /)
    real, dimension(3), parameter :: boxhi = (/ 0.8, 12.6, 11.7 /)
    character(len=*), parameter :: test_name = 'domain_construct_rect'

    ! create the particle and domain types

    call particle%init(nparticles)
    call domain%init(particle)

    ! verify that the box does not "exist" yet and that it is rectangular

    if (domain%triclinic .or. domain%box_exist) then
       call print_test_result_pf(test_name, .false.)
    end if

    ! set the boundaries
    ! box should now exist and still be rectangular

    call domain%set_global_box(boxlo,boxhi)
    if (domain%triclinic .or. (.not. domain%box_exist)) then
       call print_test_result_pf(test_name, .false.)
    else
       call print_test_result_pf(test_name, .true.)
    end if

  end subroutine test_domain_construct_rect

  ! ----------------------------------------------------------------------
  ! Can we construct a triclinic domain?
  ! ----------------------------------------------------------------------

  subroutine test_domain_construct_tri()

    type(particle_t) :: particle
    type(domain_t) :: domain
    integer, parameter :: nparticles = 10
    real, dimension(3), parameter :: boxlo = (/ 0.2, 3.4, 1.8 /)
    real, dimension(3), parameter :: boxhi = (/ 0.8, 12.6, 11.7 /)
    real, parameter :: xy = 0.04
    real, parameter :: xz = 0.02
    real, parameter :: yz = -0.01
    real, dimension(6), parameter :: h_rate = (/ 0.4, 1.0, 4.2, 0.1, -0.5, 6.0 /)
    character(len=*), parameter :: test_name = 'domain_construct_tri'

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi,xy,xz,yz,h_rate)

    ! test that the box does not "exist" and that it is triclinic

    ! TODO test that the box is triclinic

    call print_test_result_pf(test_name, .true.)

  end subroutine test_domain_construct_tri

  ! ----------------------------------------------------------------------
  ! Is conversion from rectangular to trivially triclinic correct?
  ! ----------------------------------------------------------------------

  subroutine test_rect_to_tri()

    type(particle_t) :: particle
    type(domain_t) :: domain
    integer, parameter :: nparticles = 10
    real, dimension(3), parameter :: boxlo = (/ 0.2, 3.4, 1.8 /)
    real, dimension(3), parameter :: boxhi = (/ 0.8, 12.6, 11.7 /)
    real, parameter :: xy = 0.0
    real, parameter :: xz = -0.0
    real, parameter :: yz = 0.0
    character(len=*), parameter :: test_name = 'rect_to_tri'

    ! create rectangular box (by default)

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)

    ! verify not triclinic

    if (domain%triclinic) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call domain%set_global_box(xy=xy,xz=xz,yz=yz)

    ! verify is now triclinic

    if (domain%triclinic) then
       call print_test_result_pf(test_name, .true.)
    else
       call print_test_result_pf(test_name, .false.)
    end if

  end subroutine test_rect_to_tri

  ! ----------------------------------------------------------------------
  ! Is remapping and unmapping reversible for rectangular boxes?
  ! ----------------------------------------------------------------------

  subroutine test_remap_unmap_rect()

    type(particle_t), target :: particle
    type(domain_t) :: domain
    integer, parameter :: nparticles = 4
    real, dimension(3), parameter :: boxlo = (/ 0.2, 3.4, -8.1 /)
    real, dimension(3), parameter :: boxhi = (/ 0.8, 12.6, 1.8 /)
    real, dimension(:,:), pointer :: x => null()
    character(len=*), parameter :: test_name = 'remap_unmap_rect'
    real, dimension(3,4) :: xo,xpbc
    real :: err
    real, parameter :: tol = 0.0001

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)

    ! original (unmapped) coordinates

    xo(:,1) = (/-27.0   , 32.3, 91.8     /)
    xo(:,2) = (/  0.0   ,  0.0,  0.0     /)
    xo(:,3) = (/  0.1999,  3.1,  1.80001 /)
    xo(:,4) = (/  1.0   ,  1.0,  1.0     /)

    ! coordinates when properly remapped into the periodic box

    xpbc(:,1) = (/ 0.6   ,  4.7, -7.2     /)
    xpbc(:,2) = (/ 0.6   ,  9.2,  0.0     /)
    xpbc(:,3) = (/ 0.7999, 12.3, -8.09999 /)
    xpbc(:,4) = (/ 0.4   , 10.2,  1.0     /)

    ! set the particle coordinates to xo

    x => particle%x
    x(:,:) = xo(:,:)

    ! remap the coordinates using the particle type procedures
    ! test to see if the remapped coordinates are what they should be

    call domain%remap()
    err = sum(x-xpbc)
    if (abs(err) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! unmap the coordinates, hopefully recovering their original values

    call domain%unmap()
    err = sum(abs(x-xo))
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! twice more for good luck

    call domain%remap()
    call domain%unmap()
    call domain%remap()
    call domain%unmap()
    err = sum(abs(x-xo))
    if (err > tol) then
       print *, err
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_remap_unmap_rect

  ! ----------------------------------------------------------------------
  ! Is remapping and unmapping reversible for triclinic boxes?
  ! ----------------------------------------------------------------------

  subroutine test_remap_unmap_tri()

    type(particle_t), target :: particle
    type(domain_t) :: domain
    integer, parameter :: nparticles = 4
    real, dimension(3), parameter :: boxlo = (/ 0.0, -100.0, 1.5 /)
    real, dimension(3), parameter :: boxhi = (/ 10.0, -6.0, 11.5 /)
    real, parameter :: xy = 0.5
    real, parameter :: xz = -0.2
    real, parameter :: yz = -0.3
    real, dimension(:,:), pointer :: x => null()
    real, dimension(3,nparticles) :: xo,xpbc
    real, dimension(3) :: delta
    integer :: i
    real :: err
    real, parameter :: tol = 0.0001
    character(len=*), parameter :: test_name = 'remap_unmap_tri'

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi,xy,xz,yz)

    ! original (unmapped) coordinates

    xo(:,1) = (/ -27.0,  99.7, 31.2 /)
    xo(:,2) = (/   0.0,   0.0,  0.0 /)
    xo(:,3) = (/   2.3, -50.6, -1.8 /)
    xo(:,4) = (/   1.0,   3.0,  1.0 /)

    ! coordinates when properly remapped into the periodic box

    xpbc(:,1) = (/ 2.4, -87.7, 11.2 /)
    xpbc(:,2) = (/ 9.3, -94.3, 10.0 /)
    xpbc(:,3) = (/ 2.1, -50.9,  8.2 /)
    xpbc(:,4) = (/ 0.3, -91.3, 11.0 /)

    ! set the particle coordinates to xo

    x => particle%x
    x(:,:) = xo(:,:)

    ! remap the coordinates using the particle type procedures
    ! test to see if the remapped coordinates are what they should be

    call domain%remap()
    do i = 1, 4
       delta = x(:,i) - xpbc(:,i)
       err = sqrt(0.25*dot_product(delta,delta))
    end do
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! unmap the coordinates, hopefully recovering their original values

    call domain%unmap()
    err = sum(abs(x-xo))
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! twice more for good luck

    call domain%remap()
    call domain%unmap()
    call domain%remap()
    call domain%unmap()
    err = sum(abs(x-xo))
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_remap_unmap_tri

  ! ----------------------------------------------------------------------
  ! Can we shift coordinates in the box and respect the PBC (rectangular)?
  ! Test by finding minimum-image DISPLACEMENT
  ! TODO: Add coord-shifting function?
  ! TODO: Add particle-adding function?
  ! ----------------------------------------------------------------------

  subroutine test_coord_shift_rect()

    type(particle_t), target :: particle
    type(domain_t) :: domain
    integer, parameter :: nparticles = 4
    real, dimension(3), parameter :: boxlo = (/ 0.0, -100.0, 1.5 /)
    real, dimension(3), parameter :: boxhi = (/ 10.0, -6.0, 11.5 /)
    real, dimension(:,:), pointer :: x => null()
    real, dimension(3,nparticles) :: xo,xshft
    real, dimension(3) :: x12,x13,x14,x23,x24,x34
    real :: err
    real, dimension(3) :: delta
    real, parameter :: tol = 0.00001
    character(len=*), parameter :: test_name = 'coord_shift_rect'

    ! original coordinates (all inside box)
    xo(:,1) = (/ 2.4, -87.7, 11.2 /)
    xo(:,2) = (/ 9.3, -94.3, 10.0 /)
    xo(:,3) = (/ 2.1,  -6.0,  8.2 /)
    xo(:,4) = (/ 0.3, -91.3,  1.0 /)

    ! shifted coordinates
    xshft(1,:) = xo(1,:) + 27.3
    xshft(2,:) = xo(2,:) - 200.1
    xshft(3,:) = xo(3,:) - 0.001

    ! what the displacements should be: with and without shift
    x12 = (/ -3.1,  -6.6, -1.2 /)
    x13 = (/ -0.3, -12.3, -3.0 /)
    x14 = (/ -2.1,  -3.6, -0.2 /)
    x23 = (/  2.8,  -5.7, -1.8 /)
    x24 = (/  1.0,   3.0,  1.0 /)
    x34 = (/ -1.8,   8.7,  2.8 /)

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)

    ! set to original coordinates first, and verify displacements
    x => particle%x
    x(:,:) = xo(:,:)
    err = 0.0

    delta(:) = x(:,2) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x12(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x13(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x14(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x23(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x24(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,3)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x34(:)
    err = err + sum(delta(:)*delta(:))

    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! set to shifted coordinates, and determine vectors

    x(:,:) = xshft(:,:)
    call domain%remap()

    delta(:) = x(:,2) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x12(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x13(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x14(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x23(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x24(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,3)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x34(:)
    err = err + sum(delta(:)*delta(:))

    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
    else
       call print_test_result_pf(test_name, .true.)
    end if

  end subroutine test_coord_shift_rect

  ! ----------------------------------------------------------------------
  ! Can we shift coordinates in the box and respect the PBC (rectangular)?
  ! TODO: Add coord-shifting function?
  ! ----------------------------------------------------------------------

  subroutine test_coord_shift_tri()

    type(particle_t), target :: particle
    type(domain_t) :: domain
    integer, parameter :: nparticles = 4
    real, dimension(3), parameter :: boxlo = (/ 0.0, -100.0, 1.5 /)
    real, dimension(3), parameter :: boxhi = (/ 10.0, -6.0, 11.5 /)
    real, parameter :: xy = 0.5
    real, parameter :: xz = -0.2
    real, parameter :: yz = -0.3
    real, dimension(:,:), pointer :: x => null()
    real, dimension(3,nparticles) :: xo,xshft
    real, dimension(3) :: x12,x13,x14,x23,x24,x34
    real :: err
    real, dimension(3) :: delta
    real, parameter :: tol = 0.00001
    character(len=*), parameter :: test_name = 'coord_shift_tri'

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi,xy,xz,yz)

    ! coordinates all in box
    xo(:,1) = (/ 2.4, -87.7, 11.2 /)
    xo(:,2) = (/ 9.3, -94.3, 10.0 /)
    xo(:,3) = (/ 2.1, -50.9,  8.2 /)
    xo(:,4) = (/ 0.3, -91.3, 11.0 /)

    ! shifted coordinates
    xshft(1,:) = xo(1,:) + 27.3
    xshft(2,:) = xo(2,:) - 200.1
    xshft(3,:) = xo(3,:) - 0.001

    ! what the displacements should be: with and without shift
    x12 = (/ -3.1,  -6.6, -1.2 /)
    x13 = (/ -0.3,  36.8, -3.0 /)
    x14 = (/ -2.1,  -3.6, -0.2 /)
    x23 = (/  2.8,  43.4, -1.8 /)
    x24 = (/  1.0,   3.0,  1.0 /)
    x34 = (/ -1.8, -40.4,  2.8 /)

    ! set to original coordinates first, and verify displacements
    x => particle%x
    x(:,:) = xo(:,:)
    err = 0.0

    delta(:) = x(:,2) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x12(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x13(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x14(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x23(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x24(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,3)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x34(:)
    err = err + sum(delta(:)*delta(:))

    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! set to shifted coordinates, and determine vectors

    x(:,:) = xshft(:,:)
    call domain%remap()

    delta(:) = x(:,2) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x12(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x13(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,1)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x14(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,3) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x23(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,2)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x24(:)
    err = err + sum(delta(:)*delta(:))

    delta(:) = x(:,4) - x(:,3)
    call domain%minimum_image(delta(1),delta(2),delta(3))
    delta(:) = delta(:) - x34(:)
    err = err + sum(delta(:)*delta(:))

    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
    else
       call print_test_result_pf(test_name, .true.)
    end if

  end subroutine test_coord_shift_tri
  
  ! ----------------------------------------------------------------------
  ! Can we construct a group type?
  ! TODO: add default verification
  ! ----------------------------------------------------------------------
  
  subroutine test_group_construct()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    integer, parameter :: nparticles = 10
    character(len=*), parameter :: test_name = 'group_construct'

    call particle%init(nparticles)
    call domain%init(particle)
    call group%init(domain)
    call print_test_result_pf(test_name, .true.)

  end subroutine test_group_construct

  ! ----------------------------------------------------------------------
  ! Can we construct a cells type?
  ! TODO: add default verification
  ! ----------------------------------------------------------------------

  subroutine test_cells_construct()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    integer, parameter :: nparticles = 100
    integer :: i,j,k,id
    real, parameter :: target_size = 2.0
    character(len=*), parameter :: test_name = 'cells_construct'
    real, dimension(3), parameter :: boxlo = (/ 0.0, 0.0, 0.0 /)
    real, dimension(3), parameter :: boxhi = (/ 10.0, 10.0, 10.0 /)

    call particle%init(nparticles)

    do i = 1, 10
       do j = 1, 10
          do k = 1, 10
             id = (i-1)*100 + (j-1)*10 + k
             particle%x(:,i) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group, target_size, .false.)
    call print_test_result_pf(test_name, .true.)
  end subroutine test_cells_construct

  ! ----------------------------------------------------------------------
  ! Are particles assigned to the correct cells (rectangular)?
  ! Test 4x3x2 cell
  ! ----------------------------------------------------------------------

  subroutine test_cells_assign_rect()

    type(particle_t), target :: particle
    type(domain_t) :: domain
    real, dimension(3), parameter :: boxlo = (/ -1.0, 0.5, -99.0 /)
    real, dimension(3), parameter :: boxhi = (/  3.0, 3.5, -96.0 /)
    type(group_t) :: group
    type(cells_t), target :: cells
    integer, parameter :: nparticles = 3
    real, parameter :: target_size = 0.99
    real, dimension(:,:), pointer :: x => null()
    integer, dimension(:), pointer :: cellhead => null()
    integer, dimension(nparticles) :: correct_cell
    integer :: i
    character(len=*), parameter :: test_name = 'cells_assign_rect'

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group, target_size, .false.)

    ! verify ncells

    if ((cells%ncellx /= 4) .or. &
        (cells%ncelly /= 3) .or. &
        (cells%ncellz /= 3)) then
       call print_test_result_pf(test_name, .false.)
       print *, 'bad ncells'
    end if

    x => particle%x

    x(:,1) = (/ -0.5,  2.0, -97.5 /)
    x(:,2) = (/  1.5, 0.75, -98.9 /)
    x(:,3) = (/  2.9,  3.4, -97.9 /)

    ! first particle is in 1,2,2 = 12 + 4 + 1 = 17
    ! second particle is in 3,1,1 = 0 + 0 + 3 = 3
    ! third particle is in 4,3,2 = 12 + 8 + 4 = 24

    correct_cell = (/ 17, 3, 24 /)

    call cells%assign_particles()

    cellhead => cells%cellhead
    do i = 1, nparticles
       if (cellhead(correct_cell(i)) /= i) then
          call print_test_result_pf(test_name, .false.)
       end if
    end do
    call print_test_result_pf(test_name, .true.)
  end subroutine test_cells_assign_rect

  ! ----------------------------------------------------------------------
  ! Are particles assigned to the correct cells (triclinic)?
  ! ----------------------------------------------------------------------

!!$  subroutine test_cells_assign_tri()
!!$    type(particle_t) :: particle
!!$    type(domain_t) :: domain
!!$    type(group_t) :: group
!!$    type(cells_t) :: cells
!!$    integer, parameter :: nparticles = 10
!!$    real, parameter :: target_size = 2.0
!!$    character(len=*), parameter :: test_name = 'cells_assign_tri'
!!$
!!$    call particle%init(nparticles)
!!$    call domain%init(particle)
!!$    call group%init(domain)
!!$    call cells%init(group, target_size)
!!$    call print_test_result_pf(test_name, .false.)
!!$  end subroutine test_cells_assign_tri

  ! ----------------------------------------------------------------------
  ! Do the procedures in mod_data work as intended?
  ! ----------------------------------------------------------------------

  subroutine test_data_procedures()

    type(source_data_t) :: source_data
    character(len=*), parameter :: test_name = 'data_procedures'
    integer(int64), dimension(5) :: steps
    integer :: i,dump,snap

    source_data%ndumps = 3
    allocate(source_data%dumps(3))
    allocate(source_data%dump_nsnaps(3))

    ! first try regular with no overhang

    source_data%dumps(1)%tstart = -120
    source_data%dumps(1)%tend = 90
    source_data%dumps(1)%tinc = 35
    source_data%dumps(2)%tstart = 125
    source_data%dumps(2)%tend = 160
    source_data%dumps(2)%tinc = 35
    source_data%dumps(3)%tstart = 195
    source_data%dumps(3)%tend = 895
    source_data%dumps(3)%tinc = 35

    call source_data%get_timesteps()

    if (.not. source_data%regular) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (source_data%nsnaps /= 30) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! next try regular with (regular) overhangs
    ! need to deallocate timesteps

    deallocate(source_data%timestep)

    source_data%dumps(1)%tstart = -120
    source_data%dumps(1)%tend = 125
    source_data%dumps(1)%tinc = 35
    source_data%dumps(2)%tstart = 125
    source_data%dumps(2)%tend = 230
    source_data%dumps(2)%tinc = 35
    source_data%dumps(3)%tstart = 195
    source_data%dumps(3)%tend = 895
    source_data%dumps(3)%tinc = 35

    call source_data%get_timesteps()
    if (.not. source_data%regular) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (source_data%nsnaps /= 30) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! run get_dump_snap on these data

    do i = 1, 7
       call source_data%get_dump_snap(i, dump, snap)
       if ((dump /= 1) .or. (snap /= i)) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do
    do i = 1, 2
       call source_data%get_dump_snap(i+7, dump, snap)
       if ((dump /= 2) .or. (snap /= i)) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do
    do i = 1, 21
       call source_data%get_dump_snap(i+9, dump, snap)
       if ((dump /= 3) .or. (snap /= i)) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! next try irregular
    ! need to deallocate timestep

    deallocate(source_data%timestep)

    source_data%dumps(1)%tstart = 0
    source_data%dumps(1)%tend = 22
    source_data%dumps(1)%tinc = 2
    source_data%dumps(2)%tstart = 21
    source_data%dumps(2)%tend = 21
    source_data%dumps(2)%tinc = 1
    source_data%dumps(3)%tstart = 83
    source_data%dumps(3)%tend = 93
    source_data%dumps(3)%tinc = 5

    call source_data%get_timesteps()

    if (source_data%regular) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    if (source_data%nsnaps /= 15) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    steps = (/ 93, 21, 0, 20, 88 /)
    do i = 1, 5
       if (.not. source_data%step_exists(steps(i))) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    steps = (/ -10, 22, 98, 82, -1 /)
    do i = 1, 5
       if (source_data%step_exists(steps(i))) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

    ! test the set_box procedure in order to see if we can
    ! align the domain as new steps are read

  end subroutine test_data_procedures

  ! ----------------------------------------------------------------------
  ! Can we open the test dataset?
  ! ----------------------------------------------------------------------

  subroutine test_open_dataset()

    type(source_data_t) :: source_data
    type(particle_t) :: particle
    type(domain_t) :: domain
    real :: err
    real, dimension(3,3) :: boxlo_bound,boxhi_bound,boxlo,boxhi,prd
    real, dimension(3) :: xy,xz,yz,xlo,xhi,ylo,yhi,zlo,zhi
    real, dimension(6,3) :: h
    character(len=*), parameter :: directory = 'tests/open_dataset/'
    integer :: i,j
    real, parameter :: tol = 0.000001
    character(len=*), parameter :: test_name = 'open_dataset'

    ! box dims that should match box.txt in test file

    boxlo_bound(:,1) = (/ 0.0, 0.0, 0.0 /)
    boxlo_bound(:,2) = (/ 0.0, 0.0, 0.0 /)
    boxlo_bound(:,3) = (/ -2.75647, 0.0, 0.0 /)

    boxhi_bound(:,1) = (/ 13.7823, 13.7823, 13.7823 /)
    boxhi_bound(:,2) = (/ 19.2953, 13.7823, 13.7823 /)
    boxhi_bound(:,3) = (/ 13.7823, 13.7823, 13.7823 /)

    xy(:) = (/ 0.0, 5.51293, -2.75647 /)
    xz(:) = (/ 0.0, 0.0, 0.0 /)
    yz(:) = (/ 0.0, 0.0, 0.0 /)

    xlo(:) = min(0.0,xy(:))
    xlo(:) = min(xlo(:),xz(:))
    xlo(:) = min(xlo(:),xy(:)+xz(:))
    xlo(:) = boxlo_bound(1,:) - xlo(:)
    xhi(:) = max(0.0,xy(:))
    xhi(:) = max(xhi(:),xz(:))
    xhi(:) = max(xhi(:),xy(:)+xz(:))
    xhi(:) = boxhi_bound(1,:) - xhi(:)
    ylo(:) = min(0.0,yz)
    ylo(:) = boxlo_bound(2,:) - ylo(:)
    yhi(:) = max(0.0,yz)
    yhi(:) = boxhi_bound(2,:) - yhi(:)
    zlo(:) = boxlo_bound(3,:)
    zhi(:) = boxhi_bound(3,:)

    do i = 1, 3
       boxlo(:,i) = (/ xlo(i), ylo(i), zlo(i) /)
       boxhi(:,i) = (/ xhi(i), yhi(i), zhi(i) /)
    end do

    do i = 1, 3
       prd(:,i) = boxhi(:,i) - boxlo(:,i)
    end do

    do i = 1, 3
       h(1:3,i) = prd(:,i)
       h(4,i) = yz(i)
       h(5,i) = xz(i)
       h(6,i) = xy(i)
    end do

    ! construct particle and domain types using source data

    call source_data%init(directory, particle, domain)

    ! try reading all of the snaps
    ! there should be 4 in the source data

    if (source_data%ndumps /= 3) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    if (.not. source_data%regular) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    if (source_data%nsnaps /= 3) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    do i = 1, 3
       call source_data%read_snap(particle, domain, i)

       ! verify periodic by default

       if (.not. domain%xperiodic) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       if (.not. domain%yperiodic) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       if (.not. domain%zperiodic) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       do j = 1, 3
          if (.not. domain%periodicity(j)) then
             call print_test_result_pf(test_name, .false.)
             return
          end if
       end do

       ! verify boxlo_bound, boxhi_bound

       err = sum(abs(boxlo_bound(:,i) - domain%boxlo_bound(:)))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if

       ! verify xy,xy,yz

       err = abs(xy(i)-domain%xy) &
           + abs(xz(i)-domain%xz) &
           + abs(yz(i)-domain%yz)
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if

       ! verify prd values

       err = abs(prd(1,i)-domain%xprd) &
           + abs(prd(2,i)-domain%yprd) &
           + abs(prd(3,i)-domain%zprd)
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       err = sum(abs(prd(:,i)-domain%prd(:)))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if

       ! verify h

       err = sum(abs(h(:,i)-domain%h(:)))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if

    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_open_dataset

!!$  ! ----------------------------------------------------------------------
!!$  ! Do the cell neighbor finding functions work?
!!$  ! ----------------------------------------------------------------------
!!$
!!$  subroutine test_cell_neighbor_rect()
!!$    type(particle_t) :: particle
!!$    type(domain_t) :: domain
!!$    type(group_t) :: group
!!$    type(cells_t) :: cells    
!!$    real, parameter :: rcut = 1.44
!!$    integer :: i,j,k,id
!!$    real, dimension(3) :: boxlo,boxhi
!!$    integer, parameter :: nx = 5
!!$    integer, parameter :: ny = 5
!!$    integer, parameter :: nz = 5
!!$    integer, parameter :: nparticles = nx*ny*nz
!!$    integer, dimension(13,27) :: neighs
!!$    character(len=*), parameter :: test_name = 'cell_neighbor_rect'
!!$
!!$    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
!!$    boxhi(:) = (/ real(nx), real(ny), real(nz) /)
!!$
!!$    neighs(:,1)  = (/  2,  4, 10,  5, * 11, 13, 14 /)
!!$    neighs(:,2)  = (/  3,  5, 11,  6,   12, 14, 15 /)
!!$    neighs(:,3)  = (/  1,  6, 12,  4,   10, 15, 13 /)
!!$    neighs(:,4)  = (/  5,  7, 13,  8,   14, 16, 17 /)
!!$    neighs(:,5)  = (/  6,  8, 14,  9,   15, 17, 18 /)
!!$    neighs(:,6)  = (/  4,  9, 15,  7,   13, 18, 16 /)
!!$    neighs(:,7)  = (/  8,  1, 16,  2,   17, 10, 11 /)
!!$    neighs(:,8)  = (/  9,  2, 17,  3,   18, 11, 12 /)
!!$    neighs(:,9)  = (/  7,  3, 18,  1,   16, 12, 10 /)
!!$    neighs(:,10) = (/ 11, 13, 19, 14,   20, 22, 23 /)
!!$    neighs(:,11) = (/ 12, 14, 20, 15,   21, 23, 24 /)
!!$    neighs(:,12) = (/ 10, 15, 21, 13,   19, 24, 22 /)
!!$    neighs(:,13) = (/ 14, 16, 22, 17,   23, 25, 26 /)
!!$    neighs(:,14) = (/ 15, 17, 23, 18,   24, 26, 27 /)
!!$    neighs(:,15) = (/ 13, 18, 24, 16,   22, 27, 25 /)
!!$    neighs(:,16) = (/ 17, 10, 25, 11,   26, 19, 20 /)
!!$    neighs(:,17) = (/ 18, 11, 26, 12,   27, 20, 21 /)
!!$    neighs(:,18) = (/ 16, 12, 27, 10,   25, 21, 19 /)
!!$    neighs(:,19) = (/ 20, 22,  1, 23,    2,  4,  5 /)
!!$    neighs(:,20) = (/ 21, 23,  2, 24,    3,  5,  6 /)
!!$    neighs(:,21) = (/ 19, 24,  3, 22,    1,  6,  4 /)
!!$    neighs(:,22) = (/ 23, 25,  4, 26,    5,  7,  8 /)
!!$    neighs(:,23) = (/ 24, 26,  5, 27,    6,  8,  9 /)
!!$    neighs(:,24) = (/ 22, 27,  6, 25,    4,  9,  7 /)
!!$    neighs(:,25) = (/ 26, 19,  7, 20,    8,  1,  2 /)
!!$    neighs(:,26) = (/ 27, 20,  8, 21,    9,  2,  3 /)
!!$    neighs(:,27) = (/ 25, 21,  9, 19,    7,  3,  1 /)
!!$
!!$    call particle%init(nparticles)
!!$
!!$    do i = 1, nx
!!$       do j = 1, ny
!!$          do k = 1, nz
!!$             id = (k-1)*nx*ny + (j-1)*nx + i
!!$             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
!!$          end do
!!$       end do
!!$    end do
!!$
!!$    call domain%init(particle)
!!$    call domain%set_global_box(boxlo,boxhi)
!!$    call group%init(domain)
!!$    call cells%init(group,rcut)
!!$
!!$    ! verify that ncells is 27
!!$
!!$    if (cells%ncells /= 27) then
!!$       call print_test_result_pf(test_name, .false.)
!!$       return
!!$    end if
!!$
!!$    do i = 1, 27
!!$       do j = 1, 7
!!$          if (neighs(j, i) /= cells%stencil_neighbor(i, j)) then
!!$             print *, i
!!$             write(*, '(7i3)') neighs(:,i)
!!$             do k = 1, 7
!!$                write(*, '(i3)', advance='no') cells%stencil_neighbor(i,k)
!!$             end do
!!$             write(*, '(a)') ''
!!$             call print_test_result_pf(test_name, .false.)
!!$             return
!!$          end if
!!$       end do
!!$    end do
!!$
!!$    ! try the same but non-periodic in y
!!$
!!$    domain%yperiodic = .false.
!!$
!!$    neighs(:,1)  = (/  2,  4, 10,  5, 11, 13, 14 /)
!!$    neighs(:,2)  = (/  3,  5, 11,  6, 12, 14, 15 /)
!!$    neighs(:,3)  = (/  1,  6, 12,  4, 10, 15, 13 /)
!!$    neighs(:,4)  = (/  5,  7, 13,  8, 14, 16, 17 /)
!!$    neighs(:,5)  = (/  6,  8, 14,  9, 15, 17, 18 /)
!!$    neighs(:,6)  = (/  4,  9, 15,  7, 13, 18, 16 /)
!!$    neighs(:,7)  = (/  8,  0, 16,  0, 17,  0,  0 /)
!!$    neighs(:,8)  = (/  9,  0, 17,  0, 18,  0,  0 /)
!!$    neighs(:,9)  = (/  7,  0, 18,  0, 16,  0,  0 /)
!!$    neighs(:,10) = (/ 11, 13, 19, 14, 20, 22, 23 /)
!!$    neighs(:,11) = (/ 12, 14, 20, 15, 21, 23, 24 /)
!!$    neighs(:,12) = (/ 10, 15, 21, 13, 19, 24, 22 /)
!!$    neighs(:,13) = (/ 14, 16, 22, 17, 23, 25, 26 /)
!!$    neighs(:,14) = (/ 15, 17, 23, 18, 24, 26, 27 /)
!!$    neighs(:,15) = (/ 13, 18, 24, 16, 22, 27, 25 /)
!!$    neighs(:,16) = (/ 17,  0, 25,  0, 26,  0,  0 /)
!!$    neighs(:,17) = (/ 18,  0, 26,  0, 27,  0,  0 /)
!!$    neighs(:,18) = (/ 16,  0, 27,  0, 25,  0,  0 /)
!!$    neighs(:,19) = (/ 20, 22,  1, 23,  2,  4,  5 /)
!!$    neighs(:,20) = (/ 21, 23,  2, 24,  3,  5,  6 /)
!!$    neighs(:,21) = (/ 19, 24,  3, 22,  1,  6,  4 /)
!!$    neighs(:,22) = (/ 23, 25,  4, 26,  5,  7,  8 /)
!!$    neighs(:,23) = (/ 24, 26,  5, 27,  6,  8,  9 /)
!!$    neighs(:,24) = (/ 22, 27,  6, 25,  4,  9,  7 /)
!!$    neighs(:,25) = (/ 26,  0,  7,  0,  8,  0,  0 /)
!!$    neighs(:,26) = (/ 27,  0,  8,  0,  9,  0,  0 /)
!!$    neighs(:,27) = (/ 25,  0,  9,  0,  7,  0,  0 /)
!!$          
!!$    do i = 1, 27
!!$       do j = 1, 7
!!$          if (neighs(j, i) /= cells%stencil_neighbor(i, j)) then
!!$             print *, i
!!$             write(*, '(7i3)') neighs(:,i)
!!$             do k = 1, 7
!!$                write(*, '(i3)', advance='no') cells%stencil_neighbor(i,k)
!!$             end do
!!$             write(*, '(a)') ''
!!$             call print_test_result_pf(test_name, .false.)
!!$             return
!!$          end if
!!$       end do
!!$    end do
!!$
!!$    call print_test_result_pf(test_name, .true.)
!!$
!!$  end subroutine test_cell_neighbor_rect

  ! ----------------------------------------------------------------------
  ! Does the rdf work well in periodic and nonperiodic rectangular boxes?
  ! Cell and non-cell versions should produce identical results.
  ! ----------------------------------------------------------------------

  subroutine test_analyt_rdf_rect()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    real, parameter :: rcut = 1.44
    integer, parameter :: nbin = 4
    real, dimension(nbin) :: rdf, ans
    character(len=*), parameter :: test_name = 'analyt_rdf_rect'
    integer :: i,j,k,val,id,group_a,group_b
    integer, parameter :: nx = 6
    integer, parameter :: ny = 6
    integer, parameter :: nz = 6
    integer, parameter :: nparticles = nx*ny*nz
    real :: delr,normfac,err
    real, parameter :: tol = 0.00001
    real, dimension(3) :: boxlo,boxhi
    logical, dimension(nparticles) :: group_ids_a,group_ids_b
    
    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(nx), real(ny), real(nz) /)

    call particle%init(nparticles)

    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,rcut,.false.)
    call cells%assign_particles

    call eval_rdf(cells,group,1,1,rcut,nbin,rdf)

    delr = rcut / nbin
    normfac = 3 * product(cells%domain%prd(:)) &
         / (4 * pi * delr*delr*delr * nparticles * nparticles)
    normfac = normfac * 2

    ! in PBC, there should be 6 NN per particle at r=1
    ! and 12 NNN at r=1.414
    ! we should have 4 bins, spaced 0.36 apart
    ! NN is in bin 3, and NNN is in bin 4
    ! normfac above assumes we counted only unordered pairs, so divide both numbers by 2

    ans(:) = (/ 0.0, 0.0, real(3*nparticles), real(6*nparticles) /)
    forall (i = 1:nbin) ans(i) = ans(i) * normfac / (3*i*i - 3*i + 1)

    do i = 1, nbin
       err = abs(ans(i)-rdf(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! try the same but without using cells

    cells%cells_exist = .false.

    call eval_rdf(cells,group,1,1,rcut,nbin,rdf)

    do i = 1, nbin
       err = abs(ans(i)-rdf(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! now try making half of the particles group_a and half group_b
    ! in a 3D checkerboard pattern
    ! in PBC, there should now be 6 NN per particle at r=1
    ! and 0 (!) NNN at r=1.414
    ! first check using cells, then without cells

    ans(3) = 2 * ans(3)
    ans(4) = 0
    cells%cells_exist = .true.

    group_ids_a(:) = .false.
    group_ids_b(:) = .false.
    do k = 1, nz
       do j = 1, ny
          do i = 1, nx
             id = (k-1)*nx*ny + (j-1)*nx + i
             val = i + j + k
             group_ids_a(id) = (mod(val,2) == 0)
             group_ids_b(id) = (mod(val,2) /= 0)
          end do
       end do
    end do
    group_a = group%create(group_ids_a)
    group_b = group%create(group_ids_b)

    call eval_rdf(cells,group,group_a,group_b,rcut,nbin,rdf)
    do i = 1, nbin
       err = abs(ans(i)-rdf(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! check with other particles as center group

    call eval_rdf(cells,group,group_b,group_a,rcut,nbin,rdf)
    do i = 1, nbin
       err = abs(ans(i)-rdf(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! now try without cells

    cells%cells_exist = .false.
    call eval_rdf(cells,group,group_a,group_b,rcut,nbin,rdf)
    do i = 1, nbin
       err = abs(ans(i)-rdf(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do
    call eval_rdf(cells,group,group_b,group_a,rcut,nbin,rdf)
    do i = 1, nbin
       err = abs(ans(i)-rdf(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_analyt_rdf_rect

  ! ----------------------------------------------------------------------
  ! Is RDF of ideal gas 1 if q>0 with arbitrary concentration (box size)
  ! ----------------------------------------------------------------------

  subroutine test_random_rdf()

    real, parameter :: xprd = 100.0
    real, parameter :: yprd = 100.0
    real, parameter :: zprd = 100.0
    integer, parameter :: nparticles = 1000000
    integer :: i,agroup,bgroup
    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    integer, parameter :: nbin = 2
    real, dimension(nbin) :: rdf
    real, parameter :: rcut = 2.0
    real, dimension(3) :: boxlo,boxhi
    character(len=*), parameter :: test_name = 'random_rdf'
    real, parameter :: tol = 0.01 ! big, since dealing with random numbers
    logical, dimension(nparticles) :: group_ids_a,group_ids_b

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ xprd, yprd, zprd /)
    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,rcut,.false.)

    ! get rdf for ideal gas

    call random_number(particle%x(:,:))
    particle%x(1,:) = particle%x(1,:) * xprd
    particle%x(2,:) = particle%x(2,:) * yprd
    particle%x(3,:) = particle%x(3,:) * zprd
    call cells%assign_particles()
    call eval_rdf(cells,group,1,1,rcut,nbin,rdf)

    do i = 1, nbin
       if (abs(1.0 - rdf(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! now try with different groups

    do i = 1, nparticles
       group_ids_a(i) = (mod(i,2) == 0)
       group_ids_b(i) = (mod(i,2) /= 0)
    end do
    agroup = group%create(group_ids_a)
    bgroup = group%create(group_ids_b)
    call eval_rdf(cells,group,agroup,bgroup,rcut,nbin,rdf)
    do i = 1, nbin
       if (abs(1.0 - rdf(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! flip center particle

    call eval_rdf(cells,group,bgroup,agroup,rcut,nbin,rdf)
    do i = 1, nbin
       if (abs(1.0 - rdf(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_random_rdf

  ! ----------------------------------------------------------------------
  ! Does the rdf work well in periodic and nonperiodic triclinc boxes?
  ! Cell and non-cell versions should produce identical results.
  ! ----------------------------------------------------------------------

!!$  subroutine test_analyt_rdf_tri()
!!$    type(particle_t) :: particle
!!$    type(domain_t) :: domain
!!$    type(group_t) :: group
!!$    type(cells_t) :: cells
!!$    character(len=*), parameter :: test_name = 'analyt_rdf_tri'
!!$
!!$
!!$  end subroutine \

  ! ----------------------------------------------------------------------
  ! Lily: write tests for pair_dist_func
  ! See notes in email for what tests to make
  ! ----------------------------------------------------------------------


  subroutine test_analyt_pair_dist_func_rect()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    real, parameter :: rmax = 3
    integer, parameter :: nbin = 9
    real, dimension(nbin,nbin,nbin) :: pdf, ans
    character(len=*), parameter :: test_name = 'analyt_pair_dist_func_rect'
    integer :: i,j,k
    integer, parameter :: nx = 10
    integer, parameter :: ny = 10
    integer, parameter :: nz = 10
    integer, parameter :: nparticles = 10
    integer, dimension(2) :: p_groups
    integer, parameter :: g_max = 5
    logical, dimension(nparticles) :: group_a, group_b
    real :: delr,normfac,err
    real, parameter :: tol = 0.00001
    real, dimension(3) :: boxlo,boxhi

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(nx), real(ny), real(nz) /)

    call particle%init(nparticles)

    do i = 1, nparticles
       particle%x(:,i) = (/ 1.0, 1.0, 1.0 /)
    end do
    
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,rmax,.false.)
    call cells%assign_particles

    ! make sure to call eval_pair_dist_func(cells, group, agroup, bgroup, rmax, nbin, g)

    call eval_pair_dist_func(cells,group,1,1,rmax,nbin,pdf)

    delr = 2 * rmax / nbin
    normfac = product(cells%domain%prd(:)) &
         / (delr*delr*delr * nparticles * nparticles)

    ! Number of ordered non-identical particle pairs is N*(N-1)

    ans(:,:,:) = 0.0
    ans(5,5,5) = real(nparticles * (nparticles - 1))
    ans(:,:,:) = ans(:,:,:)*normfac

    do k = 1, nbin
       do j = 1, nbin
          do i = 1, nbin
             err = abs(ans(i,j,k)-pdf(i,j,k))
             if (err > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    ! Unit test two, will test groups of particles

    group_a(:) = .false.
    group_b(:) = .false.
    group_a(1:g_max) = .true.
    group_b(g_max+1:nparticles) = .true.

    p_groups(1) = group%create(group_a)
    p_groups(2) = group%create(group_b)
    call cells%assign_particles
    call eval_pair_dist_func(cells,group,p_groups(1),p_groups(2),rmax,nbin,pdf)

    ! BJL: I'd avoid using the symbol g_max, because it might get confused with
    !      the maximum value of the pair distribution function

    delr = 2 * rmax / nbin
    normfac = product(cells%domain%prd(:)) &
         / (delr*delr*delr * g_max * g_max)

    ! Number of particle pairs with alpha in the middle and beta on the outside is
    ! N_a * N_b

    ans(:,:,:) = 0.0
    ans(5,5,5) = real(g_max * g_max )
    ans(:,:,:) = ans(:,:,:)*normfac

    do k = 1, nbin
       do j = 1, nbin
          do i = 1, nbin
             err = abs(ans(i,j,k)-pdf(i,j,k))
             if (err > tol) then
                call print_test_result_pf(test_name, .false.)
                print *, i, j, k, ans(i,j,k), pdf(i,j,k)
                return
             end if
          end do
       end do
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_analyt_pair_dist_func_rect


  subroutine test_nc_dist_file()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    type(source_data_t) :: source_data
    character(len=*), parameter :: directory = 'tests/nc_dist/'
    integer :: i,j,this_nc,nsnaps,nparticles
    real :: target_size
    real, parameter :: delta = 0.1
    character(len=*), parameter :: test_name = 'nc_dist_file'
    real, parameter :: tol = 0.00001
    integer, dimension(:), allocatable :: nc
    integer, parameter :: maxnc = 20
    integer, dimension(maxnc+1) :: hist

    call source_data%init(directory, particle, domain)
    call group%init(domain)
    nparticles = particle%nparticles
    allocate(nc(nparticles))

    ! determine minimum dimensions for cells
    ! should be greater than 2*largest radius plus delta

    target_size = 2*maxval(particle%typeradius(:)) + delta

    if (abs(target_size - 1.2) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call cells%init(group, target_size, .false.)

    nsnaps = source_data%nsnaps

    if (nsnaps /= 4) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    do i = 1, nsnaps
       hist(:) = 0
       print *, 'snap ', i
       call source_data%read_snap(particle, domain, i)
       call cells%reset_grid()
       call cells%assign_particles()
       call eval_nc(cells, .true., delta, nc)
       do j = 1, nparticles
          this_nc = nc(j)
          if (this_nc <= maxnc) then
             hist(this_nc+1) = hist(this_nc+1) + 1
          end if
       end do
       print *, 'nc histo:'
       do j = 0, maxnc
          print *, j, hist(j+1)
       end do
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_nc_dist_file

  ! ----------------------------------------------------------------------
  ! Do we get the right contact numbers in a rectangular box?
  ! ----------------------------------------------------------------------

  subroutine test_analyt_nc_rect()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    integer :: i,j,k,id
    integer, parameter :: nx = 10, ny = 10, nz = 10
    integer, parameter :: nparticles = nx*ny*nz
    real :: r
    integer, dimension(:), allocatable :: nc
    real, dimension(3) :: boxlo,boxhi
    character(len=*), parameter :: test_name = 'analyt_nc_rect'

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(nx), real(ny), real(nz) /)

    call particle%init(nparticles)
    allocate(nc(nparticles))

    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)

    r = 1.001

    call cells%init(group,r,.false.)
    call cells%assign_particles()

    ! first try to get just the NN at distance = 1.0

    call eval_nc(cells,.false.,r,nc)

    do i = 1, nparticles
       if (nc(i) /= 6) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! next try to get just the NNN at distance of 1.42

    r = 1.43
    cells%target_size = r
    call cells%reset_grid()
    call cells%assign_particles()
    call eval_nc(cells,.false.,r,nc)

    do i = 1, nparticles
       if (nc(i) /= 18) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_analyt_nc_rect

  ! ----------------------------------------------------------------------
  ! Do we get the right contact numbers in a triclinic box?
  ! ----------------------------------------------------------------------

  subroutine test_analyt_nc_tri()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    integer :: i,j,k,n,id
    integer, parameter :: nx = 10, ny = 10, nz = 10
    integer, parameter :: nparticles = nx*ny*nz
    real :: r,xy
    integer, dimension(:), allocatable :: nc
    real, dimension(3) :: boxlo,boxhi
    character(len=*), parameter :: test_name = 'analyt_nc_tri'

    ! tilted box (45-degree angle)

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(nx), real(ny), real(nz) /)
    xy = 10.0

    call particle%init(nparticles)
    allocate(nc(nparticles))

    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             particle%x(:,id) = (/ real(i+j-2), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi,xy=xy)
    call group%init(domain)

    ! verify all particles in box

    if (.not. domain%particles_inside()) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    r = 1.001

    call cells%init(group,r,.false.)
    call cells%assign_particles()

    ! first try to get just the NN at distance = 1.0

    call eval_nc(cells,.false.,r,nc)

    do i = 1, nparticles
       if (nc(i) /= 6) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! next try to get just the NNN at distance of 1.42

    r = 1.43
    cells%target_size = r
    call cells%reset_grid()
    call cells%assign_particles()
    call eval_nc(cells,.false.,r,nc)

    do i = 1, nparticles
       if (nc(i) /= 18) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! shift back to just nearest neighbors, and make x nonperiodic

    domain%xperiodic = .false.
    r = 1.01
    cells%target_size = r
    call cells%reset_grid()
    call cells%assign_particles()
    call eval_nc(cells,.false.,r,nc)

    n = 0
    do i = 1, nparticles
       n = n + nc(i)
    end do
    if (n /= (6*nparticles - 4*ny*nz)) then
       print *, n
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! now make domain non-periodic in y

    domain%xperiodic = .true.
    domain%yperiodic = .false.
    call cells%reset_grid()
    call cells%assign_particles()
    call eval_nc(cells,.false.,r,nc)

    n = 0
    do i = 1, nparticles
       n = n + nc(i)
    end do
    if (n /= (6*nparticles - 2*nx*nz)) then
       print *, n
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! now make domain non-periodic in z

    domain%yperiodic = .true.
    domain%zperiodic = .false.
    call cells%reset_grid()
    call cells%assign_particles()
    call eval_nc(cells,.false.,r,nc)

    n = 0
    do i = 1, nparticles
       n = n + nc(i)
    end do
    if (n /= (6*nparticles - 2*nx*ny)) then
       print *, n
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_analyt_nc_tri

  ! ----------------------------------------------------------------------
  ! Test an analytical SSF in rectangular box.
  ! ----------------------------------------------------------------------

  subroutine test_analyt_ssf_rect()

    integer :: i,j,k,id
    integer, parameter :: nx = 8
    integer, parameter :: ny = 8
    integer, parameter :: nz = 8
    integer, parameter :: nparticles = nx*ny*nz
    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    real, dimension(:,:,:), allocatable :: ssf
    real, parameter :: rmin = 0.999
    real, dimension(3) :: boxlo,boxhi
    complex(c_double_complex), dimension(:,:,:), allocatable :: ansdens
    real, parameter :: tol = 0.00001
    character(len=*), parameter :: test_name = 'analyt_ssf_rect'

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(nx), real(ny), real(nz) /)
    allocate(ansdens(nx,ny,nz))
    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,rmin,.true.)

    ! get trivially correct SSF: all points at zero

    particle%x(:,:) = 0.0
    call cells%assign_particles()
    call eval_ssf_3d(cells, ssf)

    do i = 1, size(ssf,1)
       do j = 1, size(ssf,2)
          do k = 1, size(ssf,3)
             if (abs(ssf(i,j,k) - real(nparticles)) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    ! get ssf for uniform grid: points on integers
    ! should just get nparticles in DC spot and 0 elsewhere

    deallocate(ssf)
    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do
    call cells%assign_particles()
    call eval_ssf_3d(cells, ssf)

    if (abs(ssf(1,1,1)-real(nparticles)) >= tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    do i = 2, size(ssf,1)
       do j = 2, size(ssf,2)
          do k = 2, size(ssf,3)
             if (abs(ssf(i,j,k)) >= tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    ! now, put all particles at z = 0

    deallocate(ssf)
    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             particle%x(:,id) = (/ real(i-1), real(j-1), 0.0 /)
          end do
       end do
    end do
    call cells%assign_particles()
    call eval_ssf_3d(cells, ssf)
    do k = 1, size(ssf,3)
       if (abs(ssf(1,1,k)-real(nparticles)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_analyt_ssf_rect

  ! ----------------------------------------------------------------------
  ! Is SSF of ideal gas 1 if q>0 with arbitrary concentration (box size)
  ! ----------------------------------------------------------------------

  subroutine test_random_ssf()

    real :: xprd,yprd,zprd,xz
    integer :: nparticles
    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    real, dimension(:,:,:), allocatable :: ssf
    real, parameter :: rmin = 0.999
    real, dimension(3) :: boxlo,boxhi
    character(len=*), parameter :: test_name = 'random_ssf'
    real, parameter :: tol = 0.01 ! big, since dealing with random numbers

    xprd = 100
    yprd = 100
    zprd = 100
    nparticles = 1000000

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ xprd, yprd, zprd /)
    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,rmin,.true.)

    ! get ssf for ideal gas
    ! should be the same (1 for q>0), irrespective of density

    call random_number(particle%x(:,:))
    particle%x(1,:) = particle%x(1,:) * xprd
    particle%x(2,:) = particle%x(2,:) * yprd
    particle%x(3,:) = particle%x(3,:) * zprd
    call cells%assign_particles()
    call eval_ssf_3d(cells, ssf)

    ! get average for q>0 (remove q=0)

    ssf(1,1,1) = 0.0
    if (abs(1.0 - sum(ssf)/(size(ssf)-1)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! try again for scaled box, different size, same number of particles

    deallocate(ssf)
    xprd = 100
    yprd = 100
    zprd = 200
    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ xprd, yprd, zprd /)
    call domain%set_global_box(boxlo,boxhi)
    call random_number(particle%x(:,:))
    particle%x(1,:) = particle%x(1,:) * xprd
    particle%x(2,:) = particle%x(2,:) * yprd
    particle%x(3,:) = particle%x(3,:) * zprd
    call cells%reset_grid()
    call cells%assign_particles()
    call eval_ssf_3d(cells, ssf)
    ssf(1,1,1) = 0.0
    if (abs(1.0 - sum(ssf)/(size(ssf)-1)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! now try triclinic

    deallocate(ssf)
    xprd = 100
    yprd = 100
    zprd = 200
    xz = 8.0
    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ xprd, yprd, zprd /)
    call domain%set_global_box(boxlo,boxhi,xz=xz)
    call random_number(particle%x(:,:))
    particle%x(3,:) = particle%x(3,:) * zprd
    particle%x(2,:) = particle%x(2,:) * yprd
    particle%x(1,:) = particle%x(1,:) * xprd + particle%x(3,:)/zprd*xz
    if (.not. domain%particles_inside()) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    call cells%reset_grid()
    call cells%assign_particles()
    call eval_ssf_3d(cells, ssf)
    ssf(1,1,1) = 0.0
    if (abs(1.0 - sum(ssf)/(size(ssf)-1)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_random_ssf

  ! ----------------------------------------------------------------------
  ! Check to see if randomly placed point particles give a g(x,y,z) = 1
  ! so we know that the normalization works
  ! Lily look here
  ! ----------------------------------------------------------------------

  subroutine test_random_pair_dist_func_rect()
    
    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    real, parameter :: rmax = 3
    integer, parameter :: nbin = 9
    real, dimension(nbin,nbin,nbin) :: pdf
    character(len=*), parameter :: test_name = 'random_pair_dist_func_rect'
    integer :: i,j,k
    integer :: nparticles
    real :: err
    real, dimension(3) :: boxlo,boxhi
    real :: xprd,yprd,zprd
    real, parameter :: tol = 0.01 ! big, since dealing with random numbers

    xprd = 100
    yprd = 100
    zprd = 100
    nparticles = 1000000

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ xprd, yprd, zprd /)
    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,rmax,.false.)

    ! create randomly distributed particles, from test_random_ssf 

    call random_number(particle%x(:,:))
    particle%x(1,:) = particle%x(1,:) * xprd
    particle%x(2,:) = particle%x(2,:) * yprd
    particle%x(3,:) = particle%x(3,:) * zprd
    call cells%assign_particles()

    call eval_pair_dist_func(cells,group,1,1,rmax,nbin,pdf)

    ! everywhere, g(x,y,z) = 1.0 for randomly distributed particles

    do k = 1, nbin
       do j = 1, nbin
          do i = 1, nbin
             err = abs(1.0-pdf(i,j,k))
             if (err > tol) then
                print *, i, j, k, pdf(i,j,k), err
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_random_pair_dist_func_rect

  ! ----------------------------------------------------------------------
  ! Are cluster ids assigned properly?
  ! ----------------------------------------------------------------------

  subroutine test_cluster_id()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    integer :: i,j,k,id
    integer, parameter :: nx = 10, ny = 10, nz = 10
    integer, parameter :: nparticles = nx*ny*nz
    real :: r
    real, dimension(3) :: boxlo,boxhi
    character(len=*), parameter :: test_name = 'cluster_id'
    integer, dimension(:), allocatable :: cluster_id

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(nx), real(ny), real(nz) /)

    call particle%init(nparticles)
    allocate(cluster_id(nparticles))

    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)

    r = 1.001

    call cells%init(group,r,.false.)
    call cells%assign_particles()

    ! all particles should be in cluster if r > 1

    r = 1.001
    call eval_cluster_id(cells,.false.,r,cluster_id)
    do i = 1, nparticles
       if (cluster_id(i) /= 1) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! all particles should have their own id if r = 0.5

    r = 0.5
    call eval_cluster_id(cells,.false.,r,cluster_id)
    do i = 1, nparticles
       if (cluster_id(i) /= i) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! put even numbers on one point and odd on another
    ! testing if self loop works and makes two clusters: 1 and 2

    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             if (modulo(id,2) .eq. 1) then
                particle%x(:,id) = (/ 0.0, 0.0, 0.0 /)
             else
                particle%x(:,id) = (/ real(nx)/2.0, real(ny)/2.0, real(nz)/2.0 /)
             end if
          end do
       end do
    end do
    call cells%assign_particles()
    r = 0.01
    call eval_cluster_id(cells,.false.,r,cluster_id)
    do i = 1, nparticles
       if (cluster_id(i) .ne. 2-modulo(i,2)) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! put even numbers on one plane and odd on another
    ! testing distinct here
    ! should be two clusters, again

    do i = 1, nx
       do j = 1, ny
          do k = 1, nz
             id = (k-1)*nx*ny + (j-1)*nx + i
             particle%x(:,id) = (/ real((2-modulo(id,2))*nx)/2.0, real(j-1), real(k-1) /)
          end do
       end do
    end do
    call cells%assign_particles()
    r = 1.01
    call eval_cluster_id(cells,.false.,r,cluster_id)
    do i = 1, nparticles
       if (cluster_id(i) .ne. 2-modulo(i,2)) then
          call print_test_result_pf(test_name, .false.)
          print *, cluster_id
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_cluster_id

  ! ----------------------------------------------------------------------
  ! Are groups created properly?
  ! ----------------------------------------------------------------------

  subroutine test_group_create()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t), target :: group
    integer :: i
    integer, parameter :: nx = 10, ny = 10, nz = 10
    integer, parameter :: nparticles = nx*ny*nz
    real, dimension(3) :: boxlo,boxhi
    integer, dimension(2) :: igroups
    character(len=*), parameter :: test_name = 'group_create'
    logical, dimension(nparticles) :: flag
    integer, dimension(:), pointer :: mask => null(), bitmask => null()

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(nx), real(ny), real(nz) /)

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    mask => group%mask
    bitmask => group%bitmask

    ! there should be one group (1, all) to begin with

    if (group%ngroup /= 1) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! all particles should be in this group

    do i = 1, nparticles
       if (iand(group%bitmask(1),group%mask(i)) == 0) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! there should be nparticles in group 1

    if (group%count(1) /= nparticles) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! assign particle 7 to a group
    ! particles 4, 78, and 21, and 7 to another group

    flag(:) = .false.
    flag(7) = .true.
    igroups(1) = group%create(flag)
    flag(4) = .true.
    flag(78) = .true.
    flag(21) = .true.
    igroups(2) = group%create(flag)

    ! there should be 3 groups now

    if (group%ngroup /= 3) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! test counts of group 1 (all), group

    if (group%count(1) /= nparticles) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (group%count(igroups(1)) /= 1) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (group%count(igroups(2)) /= 4) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! make sure the particles are in the correct groups

    if (iand(mask(7),bitmask(1)) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(7),bitmask(igroups(1))) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(7),bitmask(igroups(2))) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(4),bitmask(1)) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(4),bitmask(igroups(2))) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(21),bitmask(1)) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(21),bitmask(igroups(2))) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(78),bitmask(1)) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (iand(mask(78),bitmask(igroups(2))) == 0) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    do i = 1, nparticles
       if (i == 4) cycle
       if (i == 7) cycle
       if (i == 21) cycle
       if (i == 78) cycle
       if (iand(mask(i),bitmask(1)) == 0) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       if (iand(mask(i),bitmask(igroups(1))) /= 0) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       if (iand(mask(i),bitmask(igroups(2))) /= 0) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_group_create

  ! ----------------------------------------------------------------------
  ! Are mean displacement variances calculated properly?
  ! ----------------------------------------------------------------------

  subroutine test_displ_var()

    type(particle_t) :: particle_0,particle
    type(domain_t) :: domain
    type(group_t), target :: group
    integer :: i
    integer, parameter :: nparticles = 5
    real, dimension(3) :: boxlo,boxhi
    integer :: igroup
    character(len=*), parameter :: test_name = 'displ_var'
    logical, dimension(nparticles) :: flag
    real, dimension(6) :: var,answer
    real, parameter :: tol = 0.00001

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ 2.0, 2.0, 2.0 /)

    call particle%init(nparticles)
    call particle%copy(particle_0)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)

    particle_0%x(:,1) = (/ 0.0, 41.7, -20.9 /)
    particle_0%x(:,2) = (/ -1.0, 21.7, 20.9 /)
    particle_0%x(:,3) = (/ 0.7, 1.7, 0.9 /)
    particle_0%x(:,4) = (/ 0.16, 31.7, -2.9 /)
    particle_0%x(:,5) = (/ -8.0, -41.7, -0.9 /)

    ! first, shift all particles by 3,-2,1

    do i = 1, 5
       particle%x(:,i) = particle_0%x(:,i) + (/ 3.0, -2.0, 1.0 /)
    end do

    ! call get_displ_var, and test

    answer = (/ 9.0, 4.0, 1.0, -2.0, 3.0, -6.0 /)
    call get_displ_var(particle_0, particle, group, var, .false.)
    do i = 1, 6
       if (abs(var(i)-answer(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! test subtract mean drift 
    ! should return 0 since all particles move the same amount

    answer = (/ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 /)
    call get_displ_var(particle_0, particle, group, var, .true.)
    do i = 1, 6
       if (abs(var(i)-answer(i)) > tol) then
          print *, i, var(i), answer(i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! now, assign particles 1 and 3 to a different group
    ! move just these by a displacement of (/ +2, -1, +1 /)

    flag(:) = .false.
    flag(1) = .true.
    flag(3) = .true.
    igroup = group%create(flag)
    particle%x(:,1) = particle%x(:,1) + (/ 2.0, -1.0, 1.0 /)
    particle%x(:,3) = particle%x(:,3) + (/ 2.0, -1.0, 1.0 /)
    call get_displ_var(particle_0, particle, group, var, .false., igroup)
    if (group%count(igroup) /= 2) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    answer = (/ 25.0, 9.0, 4.0, -6.0, 10.0, -15.0 /)
    do i = 1, 6
       if (abs(var(i)-answer(i)) > tol) then
          print *, i, var(i), answer(i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! check for subtracting mean motion on this group (particles 1,3) 

    answer = (/ 1.44, 0.36, 0.36, -0.36, 0.72, -0.72 /)
    call get_displ_var(particle_0, particle, group, var, .true., igroup)
    do i = 1, 6
       if (abs(var(i)-answer(i)) > tol) then
          print *, i, var(i), answer(i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! check that the var for particles 2,4,5 are the original

    flag(:) = .not. flag(:)
    igroup = group%create(flag)
    call get_displ_var(particle_0, particle, group, var, .false., igroup)
    if (group%count(igroup) /= 3) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    answer = (/ 9.0, 4.0, 1.0, -2.0, 3.0, -6.0 /)
    do i = 1, 6
       if (abs(var(i)-answer(i)) > tol) then
          print *, i, var(i), answer(i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! check for subtracting mean motion on this group (particles 2,4,5) 

    answer = (/ 0.64, 0.16, 0.16, -0.16, 0.32, -0.32 /)
    call get_displ_var(particle_0, particle, group, var, .true., igroup)
    do i = 1, 6
       if (abs(var(i)-answer(i)) > tol) then
          print *, i, var(i), answer(i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_displ_var

  ! ----------------------------------------------------------------------
  ! Check if particle pointer can be swapped
  ! ----------------------------------------------------------------------

  subroutine test_domain_swap_particle_pointer()

    type(particle_t) :: particle_init,particle_fini
    type(domain_t) :: domain
    integer, parameter :: nparticles = 1
    real, parameter :: tol = 0.00001
    integer :: i
    character(len=*), parameter :: test_name = 'domain_swap_particle_pointer'

    call particle_init%init(nparticles)
    particle_init%x(:,1) = (/ 1.0, 1.0, 1.0 /)
    call particle_init%copy(particle_fini)
    particle_fini%x(:,1) = (/ 5.0, 5.0, 5.0 /)

    ! first should point to init

    call domain%init(particle_init)
    do i = 1, 3
       if (abs(domain%particle%x(i,1) - particle_init%x(i,1)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! switch and verify that domain's coords are same as fini's

    call domain%swap_particle_pointer(particle_fini)
    do i = 1, 3
       if (abs(domain%particle%x(i,1) - particle_fini%x(i,1)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! verify particle_init and fini unchanged from orig values

    do i = 1, 3
       if (abs(particle_init%x(i,1) - 1.0) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do
    do i = 1, 3
       if (abs(particle_fini%x(i,1) - 5.0) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_domain_swap_particle_pointer

  ! ----------------------------------------------------------------------
  ! Should be no particle image updates when m,n,p = 0
  ! ----------------------------------------------------------------------

  subroutine test_image_flip()

    type(particle_t) :: particle,particle_0
    type(domain_t) :: domain
    real, dimension(3) :: boxlo,boxhi
    integer, parameter :: nparticles = 3
    character(len=*), parameter :: test_name = 'image_flip'
    integer :: i
    real :: err
    real, parameter :: tol = 0.0001

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ 2.0, 2.0, 2.0 /)

    call particle%init(nparticles)
    call particle_0%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)

    ! specify unwrapped coords

    particle%x(:,1) = (/ 27.1, -1.0, 3.8 /)
    particle%x(:,2) = (/ -0.01, 2.4, 2.01 /)
    particle%x(:,3) = (/ -13.1, 43.0, 0.001 /)
    particle_0%x(:,:) = particle%x(:,:)

    ! remap to set images
    ! incorrectly use image_flip before remap

    call domain%remap()
    call image_flip(particle, 0, 0, 0)
    call domain%unmap()

    ! verify that coords have not changed due to trivial image flip

    err = 0.0
    do i = 1, 3
       err = err + sum(abs(particle%x(:,i) - particle_0%x(:,i)))
    end do
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_image_flip

  ! ----------------------------------------------------------------------
  ! For 3 different shear planes, do we get the expected unmapped
  ! flowed coords?
  ! ----------------------------------------------------------------------

  subroutine test_get_flow_unmapped()

    type(particle_t) :: particle,particle_ans,particle_flow
    type(domain_t) :: domain,domain_tilted
    real, dimension(3) :: boxlo,boxhi
    real :: xy
    integer, parameter :: nx = 5
    integer, parameter :: ny = 10
    integer, parameter :: nz = 11
    integer, parameter :: nparticles = nx*ny*nz
    character(len=*), parameter :: test_name = 'get_flow_unwrapped'
    integer :: i,j,k,image
    integer, dimension(3) :: sum_flips
    real :: err
    real, parameter :: tol = 0.00001
    real, dimension(3) :: vec
    real, dimension(6) :: h_sto

    boxlo = (/ 0.0, -1.0, 0.1 /)
    boxhi = (/ real(nx), real(ny)-1.0, real(nz)+0.1 /)

    call particle%init(nparticles)
    call particle_ans%init(nparticles)
    call particle_flow%init(nparticles)

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)

    do k = 1, nz
       do j = 1, ny
          do i = 1, nx
             particle%x(:, i + nx*(j-1) + nx*ny*(k-1)) = &
                  (/ real(i-1), real(j-1)-1.0, real(k-1) + boxlo(3) /)
          end do
       end do
    end do

    ! verify all particles in box

    if (.not. domain%particles_inside()) then
       call print_test_result_pf(test_name, .false.)
       print *, 'not in box'
       return
    end if

    ! first try simple shear in xy ... no flips
    ! answer should be original with an xy strain of 0.1

    xy = 1.0
    particle_ans%x(:,:) = particle%x(:,:)
    do i = 1, nparticles
       particle_ans%x(1,i) = particle_ans%x(1,i) + &
            xy / (boxhi(2)-boxlo(2)) * (particle_ans%x(2,i) - boxlo(2))
    end do

    call domain_tilted%init(particle_flow)
    call domain_tilted%set_global_box(boxlo,boxhi,xy=xy)
    sum_flips = (/ 0, 0, 0/)

    h_sto(:) = domain%h(:)
    call domain_tilted%get_flow_unmapped(sum_flips, domain, particle_flow)

    ! verify no change in 'domain'
    do i = 1, 6
       if (abs(h_sto(i)-domain%h(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! verify that flowed particles are what the answers should be

    do i = 1, nparticles
       vec = particle_ans%x(:,i) - particle_flow%x(:,i)
       err = dot_product(vec,vec)
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! add 1 to the y image
    ! this should shift all unwrapped x coords by xy
    ! and all y coords by real(ny)
    ! sum_flips still zero

    particle_ans%x(1,:) = particle_ans%x(1,:) + xy
    particle_ans%x(2,:) = particle_ans%x(2,:) + real(ny)
    particle%x(2,:) = particle%x(2,:) + real(ny)
    call domain%remap()

    ! verify that all particles in box

    if (.not. domain%particles_inside()) then
       call print_test_result_pf(test_name, .false.)
       print *, 'not in box'
       return
    end if

    ! verify that all images same

    image = particle%image(1)
    do i = 2, nparticles
       if (particle%image(i) /= image) then
          print *, image, particle%image(i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    ! flow map again
    ! have to unmap first

    call domain%unmap()
    call domain_tilted%get_flow_unmapped(sum_flips, domain, particle_flow)

    ! verify that we get the right answer

    do i = 1, nparticles
       vec = particle_ans%x(:,i) - particle_flow%x(:,i)
       err = dot_product(vec,vec)
       if (err > tol) then
          print *, particle_ans%x(:,i), particle_flow%x(:,i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! FLIP IN XY (forward x direction)
    ! answer start from beginning

    call particle_ans%zero_image()
    call particle%zero_image()
    call particle%zero_image()
    do k = 1, nz
       do j = 1, ny
          do i = 1, nx
             particle_ans%x(:, i + nx*(j-1) + nx*ny*(k-1)) = &
                  (/ real(i-1) + boxlo(1), real(j-1) + boxlo(2), real(k-1) + boxlo(3) /)
          end do
       end do
    end do

    ! add ny to y coord

    particle_ans%x(2,:) = particle_ans%x(2,:) + real(ny)

    ! shift due to strain (with box flip)

    do i = 1, nparticles
       particle_ans%x(1,i) = particle_ans%x(1,i) + &
            (xy+(boxhi(1)-boxlo(1))) / (boxhi(2)-boxlo(2)) * (particle_ans%x(2,i) - boxlo(2))
    end do

    ! now for particle initial state
    ! +yimg but no tilt or flip

    do k = 1, nz
       do j = 1, ny
          do i = 1, nx
             particle%x(:, i + nx*(j-1) + nx*ny*(k-1)) = &
                  (/ real(i-1) + boxlo(1), real(j-1) + boxlo(2), real(k-1) + boxlo(3) /)
          end do
       end do
    end do
    particle%x(2,:) = particle%x(2,:) + real(ny)

    ! initial box checking

    do i = 1, 3
       if (abs(domain%boxlo(i) - boxlo(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       if (abs(domain%boxhi(i) - boxhi(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do
    if (abs(domain%xy) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (abs(domain%xz) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (abs(domain%yz) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    
    ! tilted box checking

    do i = 1, 3
       if (abs(domain_tilted%boxlo(i) - boxlo(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
       if (abs(domain_tilted%boxhi(i) - boxhi(i)) > tol) then
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do
    if (abs(domain_tilted%xy - xy) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (abs(domain_tilted%xz) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
    if (abs(domain_tilted%yz) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! associated domain_tilted with answer
    ! domain still associated with 'particle'

    call domain_tilted%swap_particle_pointer(particle_ans)

    sum_flips = (/ -1, 0, 0 /)
    call domain_tilted%get_flow_unmapped(sum_flips, domain, particle_flow)

    do i = 1, nparticles
       vec = particle_flow%x(:,i) - particle_ans%x(:,i)
       err = dot_product(vec,vec)
       if (err > tol) then
          print *, i
          print *, particle%x(:,i)
          print *, particle_flow%x(:,i)
          print *, particle_ans%x(:,i)
          call print_test_result_pf(test_name, .false.)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_get_flow_unmapped

  ! ----------------------------------------------------------------------
  ! Lily - add test for volume fraction calculation
  ! This is currently for the whole simulation box
  ! ----------------------------------------------------------------------

  subroutine test_eval_vfrac()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells 
    character(len=*), parameter :: test_name = 'eval_vfrac'
    integer, parameter :: n = 6
    integer :: i,j,k,id
    real, dimension(3) :: boxlo,boxhi
    real :: err,v_part,v_box,vfrac_ans,vfrac
    real, parameter :: tol = 0.000001
    integer, parameter :: nparticles = n*n*n
    
    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(n), real(n), real(n) /)

    ! From particle%init, there will be one type of particle
    ! thus only one size of radius for all particles
    call particle%init(nparticles, typeradius_flag = .true.)
    ! Set particle radius, allocated in mod_data which is not called 
    allocate(particle%typeradius(1))
    particle%typeradius(:) = 0.5
    
    do i = 1, n
       do j = 1, n
          do k = 1, n
             id = (k-1)*n*n + (j-1)*n + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,2.0,.true.)
    call cells%assign_particles

    call eval_vfrac(cells, vfrac)

    v_box = n*n*n
    v_part = 4*nparticles*pi*(particle%typeradius(1)**3)/3
    vfrac_ans = v_part/v_box
    
    err = abs(vfrac_ans - vfrac)
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! Now test bidisperse spheres
    deallocate(particle%typeradius)
    allocate(particle%typeradius(2))
    particle%typeradius(:) = (/ 0.5, 0.25 /)

    cells%particle%ntypes = 2
    cells%particle%type(:) = 1

    do i = int(nparticles/2 + 1), nparticles
       cells%particle%type(i) = 2
    end do
    call cells%assign_particles
    call cells%reset_grid

    call eval_vfrac(cells, vfrac)

    v_part = v_part/2 + 2*nparticles*pi*(particle%typeradius(2)**3)/3
    vfrac_ans = v_part/v_box

    err = abs(vfrac_ans - vfrac)
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       print *, vfrac
       print *, vfrac_ans
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_eval_vfrac

  ! ----------------------------------------------------------------------
  ! Lily - create unit test for the fabric tensor
  ! Not entirely sure if the calculation is really accurate or not
  ! Errors may start with the volume fraction calculation
  ! ----------------------------------------------------------------------

  subroutine test_eval_fabric()

    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells 
    character(len=*), parameter :: test_name = 'eval_fabric'
    integer, parameter :: n = 6
    integer :: i,j,k,id
    real, dimension(3) :: boxlo,boxhi
    real, dimension(6) :: fabric,fabric_ans
    real :: err,Rxx,vfrac,delta
!    real, parameter :: delta = 0.05
    real, parameter :: tol = 0.0001
    integer, parameter :: nparticles = n*n*n

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(n), real(n), real(n) /)

    ! From particle%init, there will be one type of particle 
    ! thus only one size of radius for all particles
    call particle%init(nparticles, typeradius_flag = .true.)
    ! Set particle radius, allocated in mod_data which is not called
    allocate(particle%typeradius(1))
    particle%typeradius(:) = 0.5

    do i = 1, n
       do j = 1, n
          do k = 1, n
             id = (k-1)*n*n + (j-1)*n + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,3.0,.false.)
    call cells%reset_grid
    call cells%assign_particles
    
    delta = 0.05
    call eval_fabric(cells, .true., delta, fabric)

    ! First test, small number of neighbors
    call eval_vfrac(cells, vfrac)
    ! Average number of neighbors = 6
    Rxx = vfrac*2
    fabric_ans(:) = (/ Rxx, Rxx, Rxx, 0.0, 0.0, 0.0 /)

    do i = 1, 6
       err = abs(fabric_ans(i) - fabric(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          print *, fabric(i)
          print *, fabric_ans(i)
          return
       end if
    end do

!    print *, yes_delta,typeradius_flag

    ! Second test, increase number of neighbors
    delta = 0.42
    ! Average number of neighbors should be 18
    call eval_fabric(cells, .true., delta, fabric)
    Rxx = vfrac*6
    fabric_ans(:) = (/ Rxx, Rxx, Rxx, 0.0, 0.0, 0.0 /)

    do i = 1, 6
       err = abs(fabric_ans(i) - fabric(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          print *, i
          print *, fabric(i)
          print *, fabric_ans(i)
          print *, fabric(:)
          return
       end if
    end do

    ! Third test, increase number of neighbors again to "whole box"
    delta = 0.74
    ! Average number of neighbors is now 26
    call eval_fabric(cells, .true., delta, fabric)
    Rxx = vfrac*26/3
    fabric_ans(:) = (/ Rxx, Rxx, Rxx, 0.0, 0.0, 0.0 /)

    do i = 1, 6
       err = abs(fabric_ans(i) - fabric(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          print *, i
          print *, fabric(i)
          print *, fabric_ans(i)
          print *, fabric(:)
          return
       end if
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_eval_fabric

  ! ----------------------------------------------------------------------
  ! Does symmetrizing a 3d array work?
  ! ----------------------------------------------------------------------

  subroutine test_symmetrize_3d()

    real, dimension(1,1,1) :: g_one
    real, dimension(2,2,2) :: g_two,g_two_ans
    real, dimension(3,3,3) :: g_three,g_three_ans
    real, dimension(2,5,4) :: g_mixed,g_mixed_ans
    character(len=*), parameter :: test_name = 'symmetrize_3d'
    integer :: i,j,k
    real :: err
    real, parameter :: tol = 0.00001

    g_one(:,:,:) = 0
    g_two(:,:,:) = 0
    g_two_ans(:,:,:) = 0
    g_three(:,:,:) = 0
    g_three_ans(:,:,:) = 0
    g_mixed(:,:,:) = 0
    g_mixed_ans(:,:,:) = 0

    ! Symmetrize 1x1x1 zero array

    call symmetrize_3d(g_one(:,:,:))
    if (abs(g_one(1,1,1) - 0.0) > tol) then
       call print_test_result_pf(test_name, .false.)
    end if

    ! Symmetrize 1x1x1 -1.0 array

    g_one(:,:,:) = -1.0
    call symmetrize_3d(g_one(:,:,:))
    if (abs(g_one(1,1,1) + 1.0) > tol) then
       call print_test_result_pf(test_name, .false.)
    end if

    ! Symmetrize 2x2x2 array

    g_two_ans(1,1,1) = 0.0
    g_two_ans(1,1,2) = 3.5
    g_two_ans(1,2,1) = -0.5
    g_two_ans(1,2,2) = 1.0
    g_two_ans(2,1,1) = 1.0
    g_two_ans(2,1,2) = -0.5
    g_two_ans(2,2,1) = 3.5
    g_two_ans(2,2,2) = 0.0
    g_two(1,1,1) = 1.0
    g_two(1,1,2) = 2.5
    g_two(1,2,1) = 1.0
    g_two(1,2,2) = 2.0
    g_two(2,1,1) = 0.0
    g_two(2,1,2) = -2.0
    g_two(2,2,1) = 4.5
    g_two(2,2,2) = -1.0
    call symmetrize_3d(g_two(:,:,:))
    do k = 1, 2
       do j = 1, 2
          do i = 1, 2
             err = g_two(i,j,k) - g_two_ans(i,j,k)
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    ! Symmetrize sparse 3x3x3 array

    g_three(2,2,2) = 1.0
    g_three(1,2,2) = 0.5
    g_three(1,1,2) = -0.5
    g_three_ans(2,2,2) = 1.0
    g_three_ans(1,2,2) = 0.25
    g_three_ans(3,2,2) = 0.25
    g_three_ans(1,1,2) = -0.25
    g_three_ans(3,3,2) = -0.25
    call symmetrize_3d(g_three(:,:,:))
    do k = 1, 3
       do j = 1, 3
          do i = 1, 3
             err = g_three(i,j,k) - g_three_ans(i,j,k)
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    ! Symmetrize it again.  Should be no change.

    call symmetrize_3d(g_three(:,:,:))
    do k = 1, 3
       do j = 1, 3
          do i = 1, 3
             err = g_three(i,j,k) - g_three_ans(i,j,k)
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    ! Symmetrize rotationally symmetric 3x3x3 array in 3 orientations
    ! Should be no change after symmetrization

    g_three(:,:,:) = 0
    g_three(2,1,1) = -1
    g_three(2,3,3) = -1
    g_three(2,3,1) = 1
    g_three(2,1,3) = 1
    g_three_ans(:,:,:) = g_three(:,:,:)
    call symmetrize_3d(g_three(:,:,:))
    do k = 1, 3
       do j = 1, 3
          do i = 1, 3
             err = g_three(i,j,k) - g_three_ans(i,j,k)
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do
    g_three(:,:,:) = 0
    g_three(1,2,1) = -1
    g_three(3,2,3) = -1
    g_three(3,2,1) = 1
    g_three(1,2,3) = 1
    g_three_ans(:,:,:) = g_three(:,:,:)
    call symmetrize_3d(g_three(:,:,:))
    do k = 1, 3
       do j = 1, 3
          do i = 1, 3
             err = g_three(i,j,k) - g_three_ans(i,j,k)
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do
    g_three(:,:,:) = 0
    g_three(1,1,2) = -1
    g_three(3,3,2) = -1
    g_three(3,1,2) = 1
    g_three(1,3,2) = 1
    g_three_ans(:,:,:) = g_three(:,:,:)
    call symmetrize_3d(g_three(:,:,:))
    do k = 1, 3
       do j = 1, 3
          do i = 1, 3
             err = g_three(i,j,k) - g_three_ans(i,j,k)
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    ! Symmetrize mixed even-odd dimension array, 2x5x4

    g_mixed(1,4,2) = -7.0
    g_mixed(2,4,1) = 8.2
    g_mixed(2,3,2) = -0.2
    g_mixed_ans(1,4,2) = -3.5
    g_mixed_ans(2,4,1) = 4.1
    g_mixed_ans(2,3,2) = -0.1
    g_mixed_ans(2,2,3) = -3.5
    g_mixed_ans(1,2,4) = 4.1
    g_mixed_ans(1,3,3) = -0.1
    call symmetrize_3d(g_mixed(:,:,:))
    do k = 1, 4
       do j = 1, 5
          do i = 1, 2
             err = g_mixed(i,j,k) - g_mixed_ans(i,j,k)
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
                return
             end if
          end do
       end do
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_symmetrize_3d

  ! ----------------------------------------------------------------------
  ! Does tidying SSF do anything funny?
  ! Should get all 1.0 for uniform density in rect. and tri. boxes
  ! ----------------------------------------------------------------------

  subroutine test_tidy_ssf()

    integer, parameter :: nx = 5
    integer, parameter :: ny = 7
    integer, parameter :: nz = 9
    integer, parameter :: nq = 31
    real, dimension(nx/2+1,ny,nz) :: ssf_raw
    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells
    real, dimension(3), parameter :: boxlo = (/ -62.6658, 0.0, 0.0 /)
    real, dimension(3), parameter :: boxhi = (/ 125.419, 125.419, 125.419 /)
    real, parameter :: xy = -62.6658
    real, parameter :: xz = 0
    real, parameter :: yz = 0
    integer, parameter :: nparticles = 753571
    integer :: i,j,k
    real :: err
    real, dimension(:,:,:), allocatable :: ssf_out
    real, parameter :: tol = 0.00001
    real, parameter :: target_size = 1
    real, parameter :: qmax = 4.1
    logical, parameter :: stay_under = .true.
    character(len=*), parameter :: test_name = 'tidy_ssf'

    call particle%init(nparticles)
    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi,xy,xz,yz)
    call group%init(domain)
    call cells%init(group, target_size, stay_under)

    ssf_raw(:,:,:) = 1.0
    call tidy_ssf(cells, ssf_raw, nq, qmax, ssf_out)
    do i = 1, nq
       do j = 1, nq
          do k = 1, nq
             err = ssf_out(i,j,k) - 1.0
             if (abs(err) > tol) then
                call print_test_result_pf(test_name, .false.)
             end if
          end do
       end do
    end do

    call print_test_result_pf(test_name, .true.)

  end subroutine test_tidy_ssf

  subroutine test_eval_mean_bond_dist()
    
    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells 
    character(len=*), parameter :: test_name = 'eval_mean_bond_dist'
    integer, parameter :: n = 6
    integer :: i,j,k,id
    real, dimension(3) :: boxlo,boxhi
    real :: mean_bond_dist,mean_bond_dist_ans
    real :: err,delta
    real, parameter :: tol = 0.000001
    integer, parameter :: nparticles = n*n*n

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(n), real(n), real(n) /)

    ! From particle%init, there will be one type of particle 
    ! thus only one size of radius for all particles
    call particle%init(nparticles, typeradius_flag = .true.)
    ! Set particle radius, allocated in mod_data which is not called
    allocate(particle%typeradius(1))
    particle%typeradius(:) = 0.5

    ! assign particle positions
    do i = 1, n
       do j = 1, n
          do k = 1, n
             id = (k-1)*n*n + (j-1)*n + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,3.0,.false.)
    call cells%reset_grid
    call cells%assign_particles
    
    delta = 0.05
    call eval_mean_bond_dist(cells, .true., delta, mean_bond_dist)

    ! First test, small number of neighbors
    ! Average number of neighbors = 6
    mean_bond_dist_ans = 0.0

    err = abs(mean_bond_dist_ans - mean_bond_dist)
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       print *, mean_bond_dist
       print *, mean_bond_dist_ans
       return
    end if

    ! Second test, increase number of neighbors
    delta = 0.42
    ! Average number of neighbors should be 18
    call eval_mean_bond_dist(cells, .true., delta, mean_bond_dist)

    mean_bond_dist_ans = 12*(sqrt(2.0) - 1)/18

    err = abs(mean_bond_dist_ans - mean_bond_dist)
    if (err > tol) then
       call print_test_result_pf(test_name, .false.)
       print *, mean_bond_dist
       print *, mean_bond_dist_ans
       return
    end if
    
    call print_test_result_pf(test_name, .true.)
    
  end subroutine test_eval_mean_bond_dist


  subroutine test_eval_bin_bond_dist()
    
    type(particle_t) :: particle
    type(domain_t) :: domain
    type(group_t) :: group
    type(cells_t) :: cells 
    character(len=*), parameter :: test_name = 'eval_bin_bond_dist'
    integer, parameter :: n = 6
    integer :: i,j,k,id,nbins
    real, dimension(3) :: boxlo,boxhi
    real :: bin_lo,bin_hi
    real :: err,delta
    real, parameter :: tol = 0.000001
    integer, parameter :: nparticles = n*n*n
    integer, dimension(:), allocatable :: bin_bond_dist,bin_bond_dist_ans

    boxlo(:) = (/ 0.0, 0.0, 0.0 /)
    boxhi(:) = (/ real(n), real(n), real(n) /)

    ! From particle%init, there will be one type of particle 
    ! thus only one size of radius for all particles
    call particle%init(nparticles, typeradius_flag = .true.)
    ! Set particle radius, allocated in mod_data which is not called
    allocate(particle%typeradius(1))
    particle%typeradius(:) = 0.5

    ! assign particle positions
    do i = 1, n
       do j = 1, n
          do k = 1, n
             id = (k-1)*n*n + (j-1)*n + i
             particle%x(:,id) = (/ real(i-1), real(j-1), real(k-1) /)
          end do
       end do
    end do

    call domain%init(particle)
    call domain%set_global_box(boxlo,boxhi)
    call group%init(domain)
    call cells%init(group,3.0,.false.)
    call cells%reset_grid
    call cells%assign_particles
    
    delta = 0.05
    nbins = 7
    bin_lo = -0.01
    bin_hi = 0.06
    allocate(bin_bond_dist(nbins))
    bin_bond_dist(:) = 0
    call eval_bin_bond_dist(cells, .true., delta, nbins, bin_lo, bin_hi, bin_bond_dist)

    ! First test, small number of neighbors
    allocate(bin_bond_dist_ans(nbins))
    bin_bond_dist_ans(:) = int(0)
    bin_bond_dist_ans(2) = int(648)

    do i = 1, nbins
       err = abs(bin_bond_dist_ans(i) - bin_bond_dist(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          print *, i
          print *, bin_bond_dist(:)
          print *, bin_bond_dist_ans(:)
          return
       end if
    end do

    ! Second test, increase number of neighbors
    delta = 0.42
    ! Average number of neighbors should be 18
    nbins = 44
    bin_lo = -0.01
    bin_hi = 0.43
    deallocate(bin_bond_dist)
    allocate(bin_bond_dist(nbins))
    bin_bond_dist(:) = 0
    call eval_bin_bond_dist(cells, .true., delta, nbins, bin_lo, bin_hi, bin_bond_dist)

    deallocate(bin_bond_dist_ans)
    allocate(bin_bond_dist_ans(nbins))
    bin_bond_dist_ans(:) = int(0)
    bin_bond_dist_ans(2) = int(648)
    bin_bond_dist_ans(44) = int(1296)

    do i = 1, nbins
       err = abs(bin_bond_dist_ans(i) - bin_bond_dist(i))
       if (err > tol) then
          call print_test_result_pf(test_name, .false.)
          print *, i
          print *, bin_bond_dist(:)
          print *, bin_bond_dist_ans(:)
          return
       end if
    end do
    
    call print_test_result_pf(test_name, .true.)

  end subroutine test_eval_bin_bond_dist

  subroutine test_affine_compose()

    real(8), dimension(4,4) :: a,b,m_intrin 
    real(8), dimension(12) :: a_append,b_append,m_func,m_final,err
    real, parameter :: tol = 0.00001
    integer :: i
    character(len=*), parameter :: test_name = 'affine_compose'
 
    call RANDOM_NUMBER(a)
    call RANDOM_NUMBER(b)
    a = a-1.0
    b = b-1.0
    a(4,:) = (/0.0,0.0,0.0,1.0/)
    b(4,:) = (/0.0,0.0,0.0,1.0/)

    m_intrin = matmul(a,b)
    do i = 1,3
       a_append(4*(i-1)+1:4*i) = a(i,:)
       b_append(4*(i-1)+1:4*i) = b(i,:)
       m_final(4*(i-1)+1:4*i) = m_intrin(i,:)
    end do
    m_func = affine_compose(a_append,b_append)
    
    err = m_func-m_final
    if (sum(abs(err)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if
 
    call print_test_result_pf(test_name, .true.)
   
  end subroutine test_affine_compose

  subroutine test_affine_invert()

    real(8), dimension(4,4) :: a,b
    real(8), dimension(12) :: a_append,b_append,b_inv,a_trans1,a_trans2, &
      err1,err2
    real, parameter :: tol = 0.00001
    integer :: i
    character(len=*), parameter :: test_name = 'affine_invert'

    call RANDOM_NUMBER(a)
    call RANDOM_NUMBER(b)
    a = a-0.5
    b = b-0.5
    a(4,:) = (/0.0,0.0,0.0,1.0/)
    b(4,:) = (/0.0,0.0,0.0,1.0/)
    do i = 1,3
       a_append(4*(i-1)+1:4*i) = a(i,:)
       b_append(4*(i-1)+1:4*i) = b(i,:)
    end do
    b_inv = affine_invert(b_append)
    a_trans1 = affine_compose(b_inv,affine_compose(b_append,a_append))
    a_trans2 = affine_compose(b_append,affine_compose(b_inv,a_append))
    
    err1 = a_append-a_trans1
    err2 = a_append-a_trans2
    if (sum(abs(err1)) > tol .or. sum(abs(err2)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_affine_invert

  subroutine test_affine_interpolate()

    real(8), dimension(2,2,2) :: a
    real(8), dimension(3) :: x
    real(8), dimension(4) :: plane
    real(8), dimension(2) :: line 
    real(8) :: point,point2,val_x,err
    integer :: i,j,k
    real, parameter :: tol = 0.00001
    character(len=*), parameter :: test_name = 'affine_interpolate'

    call RANDOM_NUMBER(a)
!    a = 10.0*(a-1.0)
!    do k = 1,2
!       do j = 1,2
!          do i = 1,2
!             print*, a(i,j,k)
!          end do
!       end do
!    end do
    call RANDOM_NUMBER(x)
!    x = 0.5
    ! add 1 b/c index starts from 1 instead of 0
    x = x+1.0
    val_x = affine_interpolate(a,x(1),x(2),x(3))
    ! need a fraction between 0 to 1 to do interp
    x = x-floor(x) 
    ! linear interp from cube to plane
    plane(1) = x(1)*a(2,1,1)+(1.0-x(1))*a(1,1,1)
    plane(2) = x(1)*a(2,1,2)+(1.0-x(1))*a(1,1,2)
    plane(3) = x(1)*a(2,2,1)+(1.0-x(1))*a(1,2,1)
    plane(4) = x(1)*a(2,2,2)+(1.0-x(1))*a(1,2,2)
    ! linear interp from plane to line
    line(1) = x(2)*plane(3)+(1.0-x(2))*plane(1)
    line(2) = x(2)*plane(4)+(1.0-x(2))*plane(2)
    ! linear interp from line to point
    point = x(3)*line(2)+(1.0-x(3))*line(1)

    err = point-val_x
    if (abs(err) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_affine_interpolate

  ! NOT testing affine_transform here.
  ! originally I did the transform matrix-wise, 
  ! but b/c of memory issue, I changed to point-wise transform.
  ! Now it's trivial.

  ! subroutine test_affine_transform()

  !   real(8), dimension(4,4,4) :: a
  !   real(8), dimension(4,4,4) :: b
  !   real(8), dimension(4,4) :: m
  !   real(8), dimension(12) :: m_append
  !   integer, dimension(6) :: bnds
  !   real(8), dimension(:,:,:), allocatable :: err
  !   real, parameter :: tol = 0.00001
  !   integer :: i
  !   character(len=*), parameter :: test_name = 'affine_transform'

  !   call RANDOM_NUMBER(a)
  !   a = a-0.5
  !   
  !   m = 0.0
  !   m(1,1) = 1.0
  !   m(2,2) = 1.0
  !   m(3,3) = 1.0
  !   m(4,4) = 1.0
  !   do i = 1,3
  !      m_append(4*(i-1)+1:4*i) = m(i,:)
  !   end do
  !   bnds = (/1,3,2,3,1,4/)
  !   allocate(err(bnds(2)-bnds(1)+1,bnds(4)-bnds(3)+1,bnds(6)-bnds(5)+1))
  !   call affine_transform(a,m_append,b,bnds)

  !   err = b(bnds(1):bnds(2),bnds(3):bnds(4),bnds(5):bnds(6)) &
  !       - a(bnds(1):bnds(2),bnds(3):bnds(4),bnds(5):bnds(6))
  !   if (sum(abs(err)) > tol) then
  !      call print_test_result_pf(test_name, .false.)
  !   else
  !      call print_test_result_pf(test_name, .true.)
  !   end if

  ! end subroutine test_affine_transform

  subroutine test_symmetrize_halfspace_rot_3d()

    real(8), dimension(3,7,12) :: a
    real(8), dimension(:,:,:), allocatable :: b,b_half
    real(8), dimension(:,:,:), allocatable :: err
    real(8), dimension(:,:), allocatable :: bxy_half,errxy,bxz_half,errxz
    real(8), dimension(:), allocatable :: bx_half,errx
    real, parameter :: tol = 0.00001
    integer :: i,nxa,nya,nza,nxb,nyb,nzb
    character(len=*), parameter :: test_name = 'symmetrize_halfspace_rot_3d'
    
    call RANDOM_NUMBER(a)
    a = a-0.5
    call symmetrize_halfspace_rot_3d(a,b)

    nxa = size(a,1)
    nya = size(a,2)
    nza = size(a,3)
    nxb = size(b,1)
    nyb = size(b,2) ! should equal to nya
    nzb = size(b,3) ! should equal to nza

    ! check dimensions

    if (nyb /= nya .or. nzb /= nza) then
       print *, "dimensions of the output array is wrong"
       call print_test_result_pf(test_name, .false.)
    end if

    ! check body part that does not include (1,:,:), (:,1,:) or (:,:,1)

    allocate(b_half(nxa-1,nyb-1,nzb-1))
    allocate(err(nxa-1,nyb-1,nzb-1))
    b_half = b(nxb:nxb-nxa+2:-1,nyb:2:-1,nzb:2:-1)
    err = b_half-a(2:nxa,2:nyb,2:nzb)
    if (sum(abs(err)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! check (:,1,:) but not include (1,1,:) or (:,1,1)

    allocate(bxz_half(nxa-1,nzb-1))
    allocate(errxz(nxa-1,nzb-1))
    bxz_half = b(nxb:nxb-nxa+2:-1,1,nzb:2:-1)
    errxz = bxz_half-a(2:nxa,1,2:nzb)
    if (sum(abs(errxz)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! check (:,:,1) but not include (1,:,1) or (:,1,1)

    allocate(bxy_half(nxa-1,nyb-1))
    allocate(errxy(nxa-1,nyb-1))
    bxy_half = b(nxb:nxb-nxa+2:-1,nyb:2:-1,1)
    errxy = bxy_half-a(2:nxa,2:nyb,1)
    if (sum(abs(errxy)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    ! finally, check (:,1,1) but not include (1,1,1)

    allocate(bx_half(nxa-1))
    allocate(errx(nxa-1))
    bx_half = b(nxb:nxb-nxa+2:-1,1,1)
    errx = bx_half-a(2:nxa,1,1)
    if (sum(abs(errx)) > tol) then
       call print_test_result_pf(test_name, .false.)
       return
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_symmetrize_halfspace_rot_3d

  subroutine test_centercorner_shift()

    real(8), dimension(3,4,5) :: a,a1,a2
    real(8) :: err1,err2
    real, parameter :: tol = 0.00001
    integer :: i,nxa,nya,nza
    character(len=*), parameter :: test_name = 'centercorner_shift'

    call RANDOM_NUMBER(a)
    a = a-0.5
    nxa = size(a,1)
    nya = size(a,2)
    nza = size(a,3)

    a1 = a
    call corner_to_center_3d(a)
    err1 = a(nxa/2+1,nya/2+1,nza/2+1)-a1(1,1,1)
    a2 = a
    call center_to_corner_3d(a)
    err2 = a(1,1,1)-a1(1,1,1)

    if (abs(err1) > tol .or. abs(err2) > tol) then
       call print_test_result_pf(test_name, .false.)
    end if

    call print_test_result_pf(test_name, .true.)

  end subroutine test_centercorner_shift

end program run_tests
