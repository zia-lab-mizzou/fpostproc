! mechanics.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    September 6, 2012
! PURPOSE:         Contains functions used for mechanical quantities, like the pressure.
! CONTAINS:        subroutines: get_part_virial, get_part_pressure
! NOTES:           get_part_virial just gets the virial stress-energy on a particle (negative trace of what LAMMPS outputs)
!                  get_part_pressure takes get_part_virial output from above and divides by 3 and the (somewhat arbitrary) particle volume

module mechanics

  use dump
  implicit none

!************************************************************
contains
!************************************************************

!************************************************************

  subroutine get_part_virial(part, part_virial)
  ! get per-atom virial from LAMMPS virial stress
  ! am I using the right terminology?
  ! no globals used

    type(particle), dimension(:), intent(in) :: part
    real, dimension(:), intent(out)          :: part_virial
    integer                                  :: npart, i

      ! check inputs
    if (size(part) .ne. size(part_virial)) then
       write(*, '(A)') 'Particle array and virial array not same size'
       stop
    end if

    npart = size(part)

    do i = 1, npart
       part_virial(i) = -sum(part(i)%stress(1:3))
    end do

  end subroutine get_part_virial

!************************************************************

  subroutine get_part_pressure(part, part_pressure)
  ! get array of particle pressures for a particle array
  ! uses box globals

    type(particle), dimension(:), intent(in) :: part
    real, dimension(:), intent(out) :: part_pressure
    integer :: i
    real, dimension(:), allocatable :: part_virial
    real, dimension(:), allocatable :: part_volume
    real :: diameter

    ! check inputs
    if (size(part_pressure) .ne. box%npart) then
       write(*, '(A)') 'Pressure array wrong size'
       stop
    end if

    allocate(part_virial(box%npart))
    allocate(part_volume(box%ntype))

    do i = 1, box%ntype
       diameter = box%sizes(i)
       part_volume(i) = pi / 6 * diameter * diameter * diameter
    end do

    call get_part_virial(part, part_virial)
    do i = 1, box%npart
       part_pressure(i) = part_virial(i) / 3 / part_volume(box%types(i))
    end do

  end subroutine get_part_pressure

!************************************************************

  subroutine get_part_pe(part, part_pe)

    type(particle), dimension(:), intent(in) :: part
    real, dimension(:), intent(out) :: part_pe
    integer :: i

    if (size(part_pe) .ne. box%npart) then
       write(*, '(A)') 'PE array wrong size'
       stop
    end if

    do i = 1, box%npart
       part_pe(i) = part(i)%pe(1)
    end do

  end subroutine get_part_pe

end module mechanics
