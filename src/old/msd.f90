! msd.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 28, 2012
! PURPOSE:         Get the mean-squared displacement, optionally subtracting mean motion.
! INPUT VARIABLES:
!    path     = path to directory containing paths.txt and box.txt
!    tstart   = time step to start measurements
!    tdur     = duration of measurement, in time steps
!    tinc     = time steps between measurements
!    subtr_yn = "yes" or "no" to subtract off the drift of entire system

program main

   use realspace
   use output
   implicit none

   character(len=300) :: path, truncpath, subtr_yn
   integer*8 :: tstart, tdur, tend, tinc
   integer :: nargs
   character(len=300), dimension(30) :: args
   character(len=300) buf
   integer*8, dimension(:), allocatable :: times
   type(dumpfile), dimension(:), allocatable :: dumps
   real, dimension(:), allocatable :: msd
   logical :: subtr_tf
   integer :: nsnap, i

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 5) then
      write(*, '(A)') 'msd [path] [tstart] [tdur] [tinc] [subtr_yn]'
      stop
   end if
   call getarg(1, path)
   call getarg(2, buf); read(buf, *) tstart
   call getarg(3, buf); read(buf, *) tdur
   call getarg(4, buf); read(buf, *) tinc
   call getarg(5, subtr_yn)
   if (trim(adjustl(subtr_yn)) .eq. 'yes') then
      subtr_tf = .true.
   else if (trim(adjustl(subtr_yn)) .eq. 'no') then
      subtr_tf = .false.
   else
      write(*, '(A)') '[subtr_yn] must be yes or no'
   end if

   ! Get run info.

   truncpath = path
   call parse(truncpath, '/', args, nargs)
   write(truncpath, *) args(nargs)
   call read_desc_files(trim(adjustl(path)), dumps)

   ! Create schedule of time steps, and allocate arrays.

   nsnap = tdur / tinc + 1
   allocate(times(nsnap))
   allocate(msd(nsnap))
   do i = 1, nsnap
      times(i) = tstart + (i-1) * tinc
   end do

   ! Get MSD, taking care of HDF5.

   call open_hdf5()
   msd = get_msd(dumps, times, subtr_tf)
   call close_hdf5()

   ! Get elapsed time steps for output.

   do i = 1, nsnap
      times(i) = times(i) - tstart
   end do

   ! Write the MSD.

   write(buf, *) tstart
   call write_real_function_int8(times, msd, trim(adjustl(buf))//'.msd')

end program main
