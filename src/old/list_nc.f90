! list_nc.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    January 15, 2014
! PURPOSE:         Write the contact numbers for all particles at given time step.
! INPUT VARIABLES:
!    path  = path to directory containing paths.txt and box.txt
!    ttarg = time step to determine contact numbers
!    rsep  = separation between particle surfaces to consider contacting

! NOT WORKING WITH TEXT OUTPUT!

module output_h5
  use hdf5
  implicit none

contains

  subroutine prepare_h5out(nsnap, npart, h5out, h5f, h5d, h5fs, h5ms, h5p)

    use hdf5
    implicit none

    integer, intent(in) :: nsnap, npart
    character(len=*), intent(in) :: h5out
    integer(hid_t), intent(out) :: h5f, h5d, h5fs, h5ms, h5p
    integer(hid_t) :: h5t
    integer :: hdferr
    integer, parameter :: chunkbytes = 256000
    integer(hsize_t), dimension(2) :: dims, count, chunk

    dims = (/ npart, nsnap /)
    chunk = (/ int(real(chunkbytes)/4), 1 /)
    count = (/ npart, 1 /)

    call h5fcreate_f(trim(h5out), H5F_ACC_TRUNC_F, h5f, hdferr)
    call h5screate_simple_f(2, count, h5ms, hdferr)
    call h5screate_simple_f(2, dims, h5fs, hdferr)
    call h5pcreate_f(H5P_DATASET_CREATE_F, h5p, hdferr)
    call h5pset_deflate_f(h5p, 9, hdferr)
    call h5pset_chunk_f(h5p, 2, chunk, hdferr)
    call h5tcopy_f(H5T_STD_U32LE, h5t, hdferr)
    call h5dcreate_f(h5f, "nc", h5t, h5fs, h5d, hdferr, h5p)

  end subroutine prepare_h5out

  subroutine write_nc_snap_h5(nneigh, snap, h5d, h5fs, h5ms)

    integer, dimension(:) :: nneigh
    integer, intent(in) :: snap
    integer(hid_t), intent(in) :: h5d, h5fs, h5ms
    integer(hsize_t), dimension(2) :: start, count
    integer :: hdferr

    start = (/ 0, snap-1 /)
    count = (/ size(nneigh), 1 /)

    print*, "start ", start
    print*, "count ", count

    call h5sselect_hyperslab_f(h5fs, H5S_SELECT_SET_F, start, count, hdferr)
    call h5dwrite_f(h5d, H5T_NATIVE_INTEGER, nneigh, count, hdferr, &
         file_space_id=h5fs, mem_space_id=h5ms)

  end subroutine write_nc_snap_h5

  subroutine close_h5out(h5f, h5d, h5fs, h5ms, h5p)

    integer(hid_t), intent(in) :: h5f, h5d, h5fs, h5ms, h5p
    integer :: hdferr

    call h5pclose_f(h5p, hdferr)
    call h5sclose_f(h5ms, hdferr)
    call h5sclose_f(h5fs, hdferr)
    call h5dclose_f(h5d, hdferr)
    call h5fclose_f(h5f, hdferr)

  end subroutine close_h5out

end module output_h5

program main

  use hdf5
  use output_h5
  use realspace
  implicit none

  character(len=300) :: path, buf, h5out
  integer*8 :: tstart, tdur, tinc
  real :: rsep
  integer :: nc, nsnap, nargs, i, j, filter_id, filter_info, filter_info_both
  integer :: hdferr

  integer*8, dimension(:), allocatable :: times
  type(dumpfile), dimension(:), allocatable :: dumps
  type(particle), dimension(:), allocatable :: part
  integer, dimension(:), allocatable :: nneigh
  character(len=50), dimension(20) :: args
  logical :: yes_h5out, avail

  integer(hid_t) :: h5f, h5d, h5fs, h5ms, h5p

   ! Get input arguments.

  nargs = iargc()
  if (nargs .lt. 5) then
     write(*, '(A)') 'list_nc [path] [tstart] [tdur] [tinc] [rsep] {h5out}'
     stop
  end if
  call getarg(1, buf); path = trim(adjustl(buf))
  call getarg(2, buf); read(buf, *) tstart
  call getarg(3, buf); read(buf, *) tdur
  call getarg(4, buf); read(buf, *) tinc
  call getarg(5, buf); read(buf, *) rsep
  if (nargs .eq. 6) then
     yes_h5out = .true.
     print*, "using hdf5"
     call getarg(6, buf); read(buf, *) h5out
  else
     yes_h5out = .false.
  end if

  call open_hdf5()

  if (yes_h5out) then

     ! check if gzip compression is available
     call h5zfilter_avail_f(H5Z_FILTER_DEFLATE_F, avail, hdferr)

     if (.not. avail) then
        write(*, '("gzip filter not available.",/)')
        stop
     end if
     call h5zget_filter_info_f(H5Z_FILTER_DEFLATE_F, filter_info, hdferr)
     filter_info_both = ior(H5Z_FILTER_ENCODE_ENABLED_F, &
          H5Z_FILTER_DECODE_ENABLED_F)
     if (filter_info .ne. filter_info_both) then
        write(*, '("gzip filter not available for encoding and decoding.",/)')
        stop
     end if
  end if

  ! Create schedule of time steps.

  nsnap = tdur / tinc + 1
  allocate(times(nsnap))
  do i = 1, nsnap
     times(i) = tstart + (i-1) * tinc
  end do

  ! Get the particle configuration at given time step.

  call read_desc_files(trim(adjustl(path)), dumps)
  allocate(nneigh(box%npart))

  if (yes_h5out) then
     call prepare_h5out(nsnap, box%npart, h5out, h5f, h5d, h5fs, h5ms, h5p)
  end if

  do i = 1, nsnap

     call read_from_series(dumps, times(i), part)

     ! Get the contact numbers for each particle.

     call get_nneigh_cell(part, rsep, nneigh)

     if (yes_h5out) then
        call write_nc_snap_h5(nneigh, i, h5d, h5fs, h5ms)
     else
        do j = 1, box%npart
           write(*, '(I11,I3)') j, nneigh(j)
        end do
     end if

  end do

  if (yes_h5out) then
     call close_h5out(h5f, h5d, h5fs, h5ms, h5p)
  end if

  call close_hdf5()

end program main
