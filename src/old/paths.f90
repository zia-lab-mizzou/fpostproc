! paths.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    May 20, 2013
! PURPOSE:         Provide absolute paths for data files (just one now)
!                  Need to modify BEFORE compilation

module paths

  ! triples.txt, used by fourier.f90
  character(len=*), parameter :: trippath = &
                     '/work/02398/blandrum/triples.txt'

end module paths
