! rdf.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 28, 2012
! PURPOSE:         Get the radial distribution function.
! INPUT VARIABLES:
!    path  = path to directory containing paths.txt and box.txt
!    ttarg = target time step
!    rcut  = max distance for the RDF
!    nbin  = number of histogram bins to use for the RDF

program main

   use output
   use realspace
   implicit none

   character(len=300)                        :: path, buf
   integer*8                                 :: ttarg
   real                                      :: rcut
   integer                                   :: nbin, nargs, i
   real, dimension(:), allocatable           :: rdf, rvec
   type(dumpfile), dimension(:), allocatable :: dumps
   type(particle), dimension(:), allocatable :: part

   ! Get input arguments.

   nargs = iargc()
   if (nargs /= 4) then
      write(*, '(A)') 'rdf [path] [ttarg] [rcut] [nbin]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) ttarg
   call getarg(3, buf); read(buf, *) rcut
   call getarg(4, buf); read(buf, *) nbin

   ! Get run info, open HDF5, allocate arrays,
   ! get particle configuration, calculate RDF,
   ! and close HDF5.

   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()
   allocate(rdf(nbin))
   allocate(rvec(nbin))
   call read_from_series(dumps, ttarg, part)
   rdf = get_rdf(part, rcut, nbin)
   call close_hdf5()

   ! Make vector for the RDF bin (as in histogram) locations.

   do i = 1, nbin
      rvec(i) = (i-0.5)*rcut/nbin
   end do

   ! Write the output, using the output module.

   write(buf, *) ttarg
   call write_real_function_real(rvec, rdf, trim(adjustl(buf))//'.rdf')

end program main
