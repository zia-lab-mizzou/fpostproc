! vfrac.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    December 2, 2013
! PURPOSE:         Get the volume fraction histogram
! INPUT VARIABLES:
!    path     = path to directory containing paths.txt and box.txt
!    ttarg    = time step to measure volume fraction
!    nx,ny,nz = number of cells in x,y,z direction
!    nbin     = number of bins for volume fraction histogram

!**********************************************************************

module octant
  implicit none

  integer :: oct
  integer, dimension(3) :: oct_trip
  real, dimension(3) :: offset

contains

  function octant_to_triple(i) result (v)
    implicit none
    integer :: i
    integer, dimension(3) :: v
    select case (i)
    case (1)
       v = (/ -1, -1, -1 /)
    case (2)
       v = (/ -1, -1,  1 /)
    case (3)
       v = (/ -1,  1, -1 /)
    case (4)
       v = (/ -1,  1,  1 /)
    case (5)
       v = (/  1, -1, -1 /)
    case (6)
       v = (/  1, -1,  1 /)
    case (7)
       v = (/  1,  1, -1 /)
    case (8)
       v = (/  1,  1,  1 /)
    end select
  end function octant_to_triple

  function triple_to_octant(v) result (i)
    implicit none
    integer, dimension(3) :: v
    integer :: i
    i = (v(1)+1)*2 + (v(2)+1) + (v(3)+1)/2 + 1
  end function triple_to_octant

end module octant


!**********************************************************************

! TODO encapsulate this somehow ... does not work in module yet

function func(x,y)
  use octant
  implicit none
  real, intent(in) :: x
  real, dimension(:), intent(in) :: y
  real, dimension(size(y)) :: func
  real, dimension(size(y)) :: z1t,z2t
  integer :: i
  real, dimension(size(y)) :: root

  ! NOTE: doing this to elminate change of sqrt(-)
  root = 1.0-(x-offset(1))**2-(y-offset(2))**2
  if (minval(root) .ge. 0.0) then

     z1t=offset(3)-sqrt(1.0-(x-offset(1))**2-(y-offset(2))**2)
     z2t=offset(3)+sqrt(1.0-(x-offset(1))**2-(y-offset(2))**2)
     if (oct_trip(3) .gt. 0) then
        do i = 1, size(y)
           func(i) = max(0.0,z2t(i)) - max(0.0,z1t(i))
        end do
     else
        do i = 1, size(y)
           func(i) = min(0.0,z2t(i)) - min(0.0,z1t(i))
        end do
     end if
  else
     func = 0.0
  end if
end function func

function y1(x) ! y limit function of x
  use octant
  implicit none
  real, intent(in) :: x
  real :: y1
  real :: y1t, root

  ! TEMPORARY FIX TO HANDLE WHEN OUTSIDE SPHERE
  root = 1.0-(x-offset(1))**2
  if (root .gt. 0.0) then
     y1t = offset(2) - sqrt(1.0-(x-offset(1))**2)
     if (oct_trip(2) .gt. 0) then
        y1 = max(0.0, y1t)
     else
        y1 = min(0.0, y1t) ! maybe unnec if guaranteed intersection
     end if
  else
     y1 = 0.0
  end if
end function y1

function y2(x) ! y limit function of x
  use octant
  implicit none
  real, intent(in) :: x
  real :: y2
  real :: y2t, root

  root = 1.0-(x-offset(1))**2
  if (root .gt. 0.0) then
     y2t = offset(2) + sqrt(1.0-(x-offset(1))**2)
     if (oct_trip(2) .gt. 0) then
        y2 = max(0.0,y2t) ! maybe unnec
     else
        y2 = min(0.0,y2t)
     end if
  else
     y2 = 0.0
  end if
end function y2

!**********************************************************************

function divy_up_volume_octant(del, diam) result (volumes)
  use dump
  use octant
  use quad2d_qgaus_mod
  implicit none

  real, dimension(3), intent(in) :: del
  real, intent(in) :: diam
  real, dimension(3) :: del_a
  real :: vol, x1, x2
  real, dimension(8) :: volumes
  integer :: i, j, k, nintersect
  integer, dimension(3) :: tmp_trip
  logical, dimension(3) :: intersections

  ! Scale distance from corner by the particle radius
  del_a = 2.0 * del / diam
  offset = del_a
  
  ! Find how many cells the particle is in
  intersections = .false.
  nintersect = 0
  do i = 1, 3
     if (abs(del_a(i)) .lt. 1.0) then
        intersections(i) = .true.
        nintersect = nintersect + 1
     end if
  end do

  select case (nintersect)
  case (0)
     do i = 1, 3
        if (del(i).gt.0.0) then
           tmp_trip(i) = 1
        else
           tmp_trip(i) = -1
        end if
     end do
     j = triple_to_octant(tmp_trip)
     volumes = 0.0
     volumes(j) = 4.0*pi/3.0 ! gets all volume for radius 1 at this point
!  case (1)
!     do i = 1, 3
!        if (intersections(i)) then
!           tmp_trip(i) = -1 ! get vol for one of the octants, subtract later
!           j = i ! keep track of the dimension that the sphere intersects
!        else
!           if (del(i).gt.0.0) then
!              tmp_trip(i) = 1
 !          else
!              tmp_trip(i) = -1
!           end if
!        end if
!     end do
!     print *, 'did not add hemispheres'
!     stop
     ! use the del_a that matters, using j !!!!!!!
!     k = triple_to_octant(tmp_trip)
!     volumes = 0.0
!     volumes(k) = vol
!     tmp_trip(j) = 1
!     k = triple_to_octant(tmp_trip)
!     volumes(k) = 4.0*pi/3.0 - vol
  case(1:3)
     ! Keeping this simple
     ! It'd be possible to save some calculations by using hemispheres
     !   and subtracting to get volumes (integrationg octants 1,2,3,5 only)
     volumes = 0.0
     do oct = 1, 8
        oct_trip = octant_to_triple(oct)
        if (oct_trip(1) .gt. 0) then
           x1 = max(0.0,offset(1)-1.0)
           x2 = max(0.0,offset(1)+1.0)
        else
           x1 = min(0.0,offset(1)-1.0)
           x2 = min(0.0,offset(1)+1.0)
        end if
        call quad2d_qgaus(x1,x2,volumes(oct))
     end do
  end select

  volumes = volumes * diam*diam*diam / 8.0

end function divy_up_volume_octant

!**********************************************************************

!function get_corner(part, ncell) result (corner)
!  use realspace
!  implicit none

  ! get the cell_id of the cell with the small-x,small-y,small-z
  ! corner that is closest to the center of the particle
  ! this is not necessarily the cell the particle was assigned to

!  type(particle)        :: part
!  integer               :: corner
!  integer, dimension(3) :: ncell, triplet, offset
!  real, dimension(3)    :: del, cell_pos

!  del = (box%hi-box%lo) / ncell




  ! cell containing the particle:
  !   triplet and small-x,small-y,small-z corner position
!  triplet = cell_to_triplet(cell, ncell)
!  cell_pos = (triplet-1) * del

  ! find the closest corner
!  offset = nint( (part%pos-cell_pos) / del )
!  triplet = triplet + offset
!  corner = triplet_to_cell(triplet, ncell)

!end function get_corner


subroutine test_stuff()
  use octant
  implicit none

  integer :: i
  real, dimension(3) :: del
  integer, dimension(3) :: trip
  real :: diam
  interface
     function divy_up_volume_octant(del, diam)
       real, dimension(3), intent(in) :: del
       real, intent(in) :: diam
       real, dimension(8) :: divy_up_volume_octant
     end function divy_up_volume_octant
  end interface

  diam = 1.0

  print *, 'Test octant: should go from 1 to 8'
  do i = 1, 8
     trip=octant_to_triple(i)
     print *, triple_to_octant(trip)
  end do

  print *, '3 intersections'
  del = (/ 0.0, 0.0, 0.0 /)
  print *, divy_up_volume_octant(del, diam)
  print *, ''

  print *, 'two intersections'
  del = (/ -0.5, 0.0, 0.0 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ 0.5, 0.0, 0.0 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ 0.0, -0.5, 0.0 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ 0.0, 0.5, 0.0 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ 0.0, 0.0, -0.5 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ 0.0, 0.0, 0.5 /)
  print *, divy_up_volume_octant(del, diam)
  print *, ''

  print *, 'one intersection'
  del = (/ -0.5, -0.5, 0 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ -0.5, +0.5, 0 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ +0.5, -0.5, 0 /)
  print *, divy_up_volume_octant(del, diam)
  del = (/ +0.5, +0.5, 0 /)
  print *, divy_up_volume_octant(del, diam)

end subroutine test_stuff

!**********************************************************************

program main

  use RCImageBasic
  use octant
  use stats
  use dump
  implicit none

  character(len=300) :: buf
  character(len=300) :: path, truncpath
  character(len=30), dimension(30) :: args
  integer*8 :: ttarg
  integer :: nx, ny, nz, nbin, nargs, i
  real, dimension(:), allocatable :: hist, vf_axis
  real, dimension(:), allocatable :: vol_tally
  real :: vfrac
  type(dumpfile), dimension(:), allocatable :: dumps
  type(particle), dimension(:), allocatable :: part
  integer, dimension(3) :: ncell

  ! Get input arguments.

  nargs = iargc()
  if (nargs .ne. 6) then
     write(*, '(A)') 'vfrac [path] [ttarg] [nx] [ny] [nz] [nbin]'
     stop
  end if
  call getarg(1, buf); path = trim(adjustl(buf))
  call getarg(2, buf); read(buf, *) ttarg
  call getarg(3, buf); read(buf, *) nx
  call getarg(4, buf); read(buf, *) ny
  call getarg(5, buf); read(buf, *) nz
  call getarg(6, buf); read(buf, *) nbin

  allocate(hist(nbin))
  allocate(vf_axis(nbin))
  allocate(vol_tally(nx*ny*nz))

 ! Get the run info.

  truncpath = path
  call parse(truncpath, '/', args, nargs)
  write(truncpath, *) args(nargs)
  allocate(dumps(1))
  call read_desc_files(trim(adjustl(path)), dumps)

  call open_hdf5()
  call read_from_series(dumps, ttarg, part)
  call close_hdf5()

  call get_vfrac_by_cell(part, nx, ny, nz, vol_tally)
  ncell = (/ nx, ny, nz /)
  call print_cartoon(vol_tally, ncell, nz)

  call render_img(ncell, vol_tally)

  call histo_bin(vol_tally, 0.0, 1.0, nbin, vf_axis, hist)

  do i = 1, nbin
     write(*, '(2F14.6)') vf_axis(i), hist(i)
  end do

contains

!**********************************************************************

subroutine get_vfrac_by_cell(part, nx, ny, nz, vfrac)
  use realspace
  implicit none

  type(particle), dimension(:), intent(in) :: part
  integer, intent(in) :: nx, ny, nz
  real, dimension(:) :: vfrac
  real, dimension(3) :: del_box, del
  integer, dimension(3) :: ncell, base_trip, tmp_trip
  real, dimension(8) :: oct_vols
  integer :: i, j, xi, yi, zi, cell
  real :: cell_vol
  interface
     function divy_up_volume_octant(del, diam)
       real, dimension(3), intent(in) :: del
       real, intent(in) :: diam
       real, dimension(8) :: divy_up_volume_octant
     end function divy_up_volume_octant
  end interface

  ncell(1) = nx; ncell(2) = ny; ncell(3) = nz
  del_box = (box%hi-box%lo) / ncell
  cell_vol = product(del_box)

  if ((ncell(1).le.2) .or. (ncell(2).le.2) .or. (ncell(3).le.2)) then
     write(*, '(A)') 'Cell size too big, error'
     stop
  end if

  if (minval(del_box) .le. maxval(box%sizes)) then
     write(*, '(A)') 'Cell size too small'
     stop
  end if

  vfrac = 0.0
  do i = 1, box%npart

     ! Cell with the +x,+y,+z corner closest to particle is the
     !   -x,-y,-z cell (cell 1) of the octant

     base_trip = floor(part(i)%pos/del_box + 0.5)

     ! Get displacement from that same +x,+y,+z corner

     del = part(i)%pos - base_trip*del_box

     ! Get volume in the eight cells around that corner (octant).

     oct_vols = divy_up_volume_octant(del, box%sizes(box%types(i)))
     !print *, i, minval(oct_vols), maxval(oct_vols)

     ! Add volumes to the appropriate cells.

     j = 0
     do xi = 0, 1
        do yi = 0, 1
           do zi = 0, 1
              j = j + 1
              tmp_trip = base_trip + (/ xi, yi, zi /)
              cell = triplet_to_cell(tmp_trip, ncell)
              vfrac(cell) = vfrac(cell) + oct_vols(j)
           end do
        end do
     end do

  end do

  vfrac = vfrac / cell_vol

  print *, maxval(vfrac)
  print *, minval(vfrac)

end subroutine get_vfrac_by_cell

subroutine print_cartoon(vfrac, ncell, zi)
  use realspace
  implicit none

  real, dimension(:), intent(in) :: vfrac
  integer, dimension(3), intent(in) :: ncell
  integer :: zi
  real :: val
  integer :: cell, i, j
  integer, dimension(3) :: trip

  do i = ncell(2), 1, -1
     do j = 1, ncell(1)
        trip = (/ j, i, zi /)
        cell = triplet_to_cell(trip, ncell)
        val = vfrac(cell)
        if (val .lt. 0.2) then
           write(*, '(a)', advance='no') '  '
        else if (val .lt. 0.4) then
           write(*, '(a)', advance='no') '++'
        else
           write(*, '(a)', advance='no') '00'
        end if
     end do
     write(*, '(a)') ''
  end do

end subroutine print_cartoon

subroutine render_img(ncell, vfrac)
  use realspace
  use RCImageIO

  integer, dimension(3), intent(in) :: ncell
  real, dimension(:), intent(in) :: vfrac
  type(rgbimage) :: img
  type(rgb) :: color
  integer :: level, nx, ny, i, j, cell
  integer, dimension(3) :: trip
  real, parameter :: inv_vfmax = 1.0/0.74048

  nx = ncell(1)
  ny = ncell(2)
  nz = ncell(3)

  call init_img(img)
  call alloc_img(img, nx, ny)

  ! Designed to match the images rendered by vmd

  do i = 1, nx
     do j = 1, ny
        trip = (/ i, j, nz /)
        cell = triplet_to_cell(trip, ncell)
        level = floor((inv_vfmax * vfrac(cell)) * 255)
        call set_color(color, level, level, level)
        call put_pixel(img, i-1, ny-j, color)
     end do
  end do

  open(8, file='vfrac.ppm', action='write')
  call output_ppm(8, img)
  call free_img(img)
  close(8)

end subroutine render_img

end program main
