! nc_flow.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    March 11, 2014
! PURPOSE:         Determine transfers (flow) between Nc populations.
! INPUT VARIABLES:
!    path   = path to directory containing paths.txt and box.txt
!    tstart = first time step for contact number determination
!    tsep   = distance in time steps since the first to check how particles changed Nc
!    rsep   = separation between particle surfaces to consider contacting
!    navg   = (optional) number of samples used to generate average (default 1)
!    tinc   = (optional) spacing between samples

program main

   use realspace
   use stats
   implicit none

   integer, parameter :: nbin = 17 ! number of bins
   integer :: navg, i, j, k, bin, bin_start, bin_end

   integer, dimension(:), allocatable :: nneigh_1, nneigh_2 ! particle neigh #
   integer*8, dimension(:), allocatable :: pop, pop_tmp
   integer*8, dimension(:,:), allocatable :: xfer, xfer_tmp
   type(running_stats), dimension(:), allocatable :: pop_rs
   type(running_stats), dimension(:,:), allocatable :: gross_xfer_rs
   type(running_stats), dimension(:,:), allocatable :: net_xfer_rs

   real, dimension(:,:), allocatable :: hist
   type(particle), dimension(:), allocatable :: part1, part2
   type(dumpfile), dimension(:), allocatable :: dumps

   integer :: nargs
   character(len=300) :: buf, path
   character(len=50), dimension(99) :: args
   integer*8 :: tstart, tsep, tend, tinc
   real :: rsep, distsq
   real, dimension(3) :: mean_origin, mean_disp

   ! Get input arguments.

   nargs = iargc()
   if ((nargs.ne.4) .and. (nargs.ne.6)) then
      write(*, '(A)') 'nc_flow [path] [tstart] [tsep] [rsep] {navg} {tinc}'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) tstart
   call getarg(3, buf); read(buf, *) tsep
   call getarg(4, buf); read(buf, *) rsep
   if (nargs .eq. 6) then
      call getarg(5, buf); read(buf, *) navg
      call getarg(6, buf); read(buf, *) tinc
   else
      navg = 1
      tinc = 0
   end if

   ! Get run info, and open HDF5.

   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()

   ! Allocate arrays.

   allocate(nneigh_1(box%npart)); allocate(nneigh_2(box%npart))
   allocate(pop(nbin))
   allocate(pop_tmp(nbin))
   allocate(hist(nbin,nbin))
   allocate(xfer(nbin,nbin))
   allocate(xfer_tmp(nbin,nbin))

   allocate(pop_rs(nbin))
   allocate(gross_xfer_rs(nbin,nbin))
   allocate(net_xfer_rs(nbin,nbin)) ! Using dense representation, no big deal

   ! Zero arrays.

   do i = 1, nbin
      call running_stats_clear(pop_rs(i))
      do j = 1, nbin
         call running_stats_clear(gross_xfer_rs(i,j))
         call running_stats_clear(net_xfer_rs(i,j))
      end do
   end do
   xfer = 0
   pop = 0

   ! Loop through the snaps.

   do i = 1, navg

      pop_tmp = 0
      xfer_tmp = 0

      if (i .gt. 1) then
         tstart = tstart + tinc
      end if
      tend = tstart + tsep

      ! Get the contact numbers, but don't recalculate them
      ! if we already did in the previous iteration and can
      ! use them now.

      if ((i.ne.1) .and. (tinc.eq.tsep)) then
         nneigh_1 = nneigh_2
      else
         call read_from_series(dumps, tstart, part1)
         call get_nneigh_cell(part1, rsep, nneigh_1)
      end if
      call read_from_series(dumps, tend, part2)
      call get_nneigh_cell(part2, rsep, nneigh_2)

      ! Place particles into histogram, depending upon
      ! what the Nc is at the beginning and end of the tsep.

      do j = 1, box%npart
         bin_start = nneigh_1(j) + 1
         bin_end   = nneigh_2(j) + 1
         xfer_tmp(bin_start, bin_end) = xfer_tmp(bin_start, bin_end) + 1
         xfer(bin_start, bin_end) = xfer(bin_start, bin_end) + 1
      end do

      ! Accumulate the initial Nc populations.

      do j = 1, box%npart
         bin = nneigh_1(j) + 1
         pop(bin) = pop(bin) + 1
         pop_tmp(bin) = pop_tmp(bin) + 1
      end do

      ! NEW push the rs variables with values from the tmp arrays.

      do j = 1, nbin
         call running_stats_push(pop_rs(j), dble(pop_tmp(j)))
         do k = 1, nbin
            call running_stats_push(gross_xfer_rs(j,k), dble(xfer_tmp(j,k)))
            call running_stats_push(net_xfer_rs(j,k), dble(xfer_tmp(j,k)-xfer_tmp(k,j)))
         end do
      end do

   end do

   ! Print Nc distribution statistics.  Normalizing.

   write(*, '(A)') 'pNc statistics'
   write(*, '(A)') 'Nc, pNc, sterr'
   do i = 1, nbin
      write(*, '(I4,2G14.6)') i, running_stats_mean(pop_rs(i))/box%npart, &
           running_stats_sterr(pop_rs(i))/box%npart
   end do
   write(*, *) ''

   ! Print the gross transfer values.  Normalize them.

   write(*, '(A)') 'Gross transfer statistics (normalized by total box npart'
   write(*, '(A)') 'Need to divide by time increment afterwards'
   write(*, '(A)') 'i, j, gross_xfer_per_particle_total, sterr'
   do i = 1, nbin
      do j = 1, nbin
         write(*, '(I4,I4,2G14.6)') i, j, running_stats_mean(gross_xfer_rs(i,j))/box%npart, &
              running_stats_sterr(gross_xfer_rs(i,j))/box%npart
      end do
   end do
   write(*, *) ''

   write(*, '(A)') 'Net transfer statistics (normalized by total box npart'
   write(*, '(A)') 'Need to divide by time increment afterwards'
   write(*, '(A)') 'i, j, net_xfer_per_particle_total, sterr'
   do i = 1, nbin
      do j = i+1, nbin
         write(*, '(I4,I4,2G14.6)') i, j, running_stats_mean(net_xfer_rs(i,j))/box%npart, &
              running_stats_sterr(net_xfer_rs(i,j))/box%npart
      end do
   end do


   ! We have accumulated gross transfers between the populations.
   ! Want to output averaged gross rates, net rates, and transition rates

   ! Check the population stats

   do i = 1, nbin
      print*, running_stats_count(pop_rs(i)), running_stats_mean(pop_rs(i)), &
           running_stats_sterr(pop_rs(i))
   end do

   ! Normalize by the initial population number to get a final distribution.

   hist = real(xfer)
   do i = 1, nbin
      if (pop(i) .gt. 0) hist(i, :) = hist(i, :) / pop(i)
   end do

   ! Write output.

   do i = 1, nbin
      do j = 1, nbin
         write(*, '(F14.6)', advance='no') hist(j, i)
      end do
      write(*, '(A)') ''
   end do

   call close_hdf5()

end program main
