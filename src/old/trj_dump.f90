! trj_dump.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    November 24, 2013
! PURPOSE:         Output a single-timestep LAMMPS dump file from HDF5 data.
! INPUT VARIABLES:
!    outfile  = output file path
!    path     = path to directory containing paths.txt and box.txt
!    tstart   = first timestep to output
!    tdur     = the duration of the output, in timesteps
!    tinc     = the number of timesteps between snaps
!    subtr_yn = subtract mean motion or not
!    extra_yn = also dump velocities and virial stresses (or not)
! NOTE:            currently a little rough, not sure if still working!

program main

   use realspace
   implicit none

   character(len=300) :: outfile, path, buf
   integer*8 :: tstart, tdur, tinc
   logical :: subtr_yn, extra_yn
   integer :: nargs, i, j, nsnap
   type(dumpfile), dimension(:), allocatable :: dumps
   type(particle), dimension(:), allocatable :: part
   type(particle), dimension(:), allocatable :: part_origin
   real, dimension(3) :: mean_origin, mean_disp
   integer*8, dimension(:), allocatable :: times

   nargs = iargc()
   if (nargs .ne. 7) then
      write(*, '(A)') 'trj_dump [outfile] [path] [tstart] [tdur] [tinc] [subtr_yn] [extra_yn]'
      stop
   end if
   call getarg(1, outfile)
   call getarg(2, path)
   call getarg(3, buf); read(buf, *) tstart
   call getarg(4, buf); read(buf, *) tdur
   call getarg(5, buf); read(buf, *) tinc
   call getarg(6, buf)
   if (trim(adjustl(buf)) .eq. 'yes') then
      subtr_yn = .true.
   elseif (trim(adjustl(buf)) .eq. 'no') then
      subtr_yn = .false.
   else
      write(*, '(A)') 'subtr_yn should be yes or no'
      stop
   end if
   call getarg(7, buf)
   if (trim(adjustl(buf)) .eq. 'yes') then
      extra_yn = .true.
   elseif (trim(adjustl(buf)) .eq. 'no') then
      extra_yn = .false.
   else
      write(*, '(A)') 'extra_yn should be yes or no'
      stop
   end if
   if (mod(tdur, tinc) .ne. 0) then
      write (*, '(a)') 'Bad tdur-tinc pair'
      stop
   end if

   ! Don't allow both mean motion subtraction and extra for now.
   if (subtr_yn .and. extra_yn) then
      write(*, '(A)') 'Not allowing both mean motion subtraction and extra dumping'
      stop
   end if

   ! Create schedule of time steps
   nsnap = tdur / tinc + 1
   allocate(times(nsnap))
   do i = 1, nsnap
      times(i) = tstart + (i-1) * tinc
   end do

   call read_desc_files(trim(adjustl(path)), dumps)

   call open_hdf5()

   ! Get origin to cut mean motion if desired.
   if (subtr_yn) then
      call read_from_series(dumps, dumps(1)%tstart, part_origin)
      call get_mean_pos(part_origin, mean_origin)
   end if

   open(8, file=trim(adjustl(outfile)), action='write')

   do i = 1, nsnap

      ! Get the snapshot for the image.
      call read_from_series(dumps, times(i), part)

      ! Subtract mean motion if desired.
      if (subtr_yn) then
         call get_mean_pos(part, mean_disp)
         mean_disp = mean_disp - mean_origin
         call subtract_displacement(part, mean_disp)
      end if

      ! Write the date from the particle array to a .lammpstrj dump file.
      write(8, '(A)') 'ITEM: TIMESTEP'
      write(8, '(I11)') times(i)
      write(8, '(A)') 'ITEM: NUMBER OF ATOMS'
      write(8, '(I8)') box%npart
      write(8, '(A)') 'ITEM: BOX BOUNDS pp pp pp'
      write(8, '(2F14.6)') box%lo(1), box%hi(1)
      write(8, '(2F14.6)') box%lo(2), box%hi(2)
      write(8, '(2F14.6)') box%lo(3), box%hi(3)
      if (extra_yn) then
         write(8, '(A)') 'ITEM: ATOMS id type xu yu zu vx vy vz c_get_pe c_get_stress[1] c_get_stress[2] c_get_stress[3] c_get_stress[4] c_get_stress[5] c_get_stress[6]'
         do j = 1, box%npart
            write(8, '(I8,I3,13F14.6)') j, box%types(j), part(j)%pos(1:3), &
                 part(j)%vel(1:3), part(j)%pe(1), part(j)%stress(1:6)
         end do
      else
         write(8, '(A)') 'ITEM: ATOMS id type xu yu zu'
         do j = 1, box%npart
            write(8, '(I8,I3,3F14.6)') j, box%types(j), part(j)%pos(1:3)
         end do
      endif
   end do

   close(8)

end program main
