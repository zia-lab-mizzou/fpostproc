! this module contains gaussian quadrature (simple but accuracy not controllable)

module quad2d_qgaus_mod

  private ! hide all names from outside
  public quad2d_qgaus ! except for this

  real :: xsav
  interface ! user-supplied functions
     function func(x,y) ! 2D function to integrate
       real, intent(in) :: x
       real, dimension(:), intent(in) :: y
       real, dimension(size(y)) :: func
     end function func

     function y1(x) ! y limit function of x
       real, intent(in) :: x
       real :: y1
     end function y1

     function y2(x) ! y limit function of x
       real, intent(in) :: x
       real :: y2
     end function y2

  end interface

contains

  function g(x) ! integral over y as function of x
    real, dimension(:), intent(in) :: x
    real, dimension(size(x)) :: g
    integer :: i
    do i=1,size(x)
       xsav=x(i)
       g(i)=qgaus(f,y1(xsav),y2(xsav))
    end do
  end function g

  function f(y)
    real, dimension(:), intent(in) :: y
    real, dimension(size(y)) :: f
    f=func(xsav,y)
  end function f

  recursive function qgaus(func,a,b) ! 1D integration
    real, intent(in) :: a,b
    real :: qgaus
    interface
       function func(x)
         real, dimension(:), intent(in) :: x
         real, dimension(size(x)) :: func
       end function func
    end interface
    real :: xm,xr
    real, dimension(5) :: dx, w = (/ 0.2955242247,0.2692667193,&
         0.2190863625,0.1494513491,0.0666713443 /),&
         x = (/ 0.1488743389,0.4333953941,0.6794095682,&
         0.8650633666,0.9739065285 /)
    xm=0.5*(b+a)
    xr=0.5*(b-a)
    dx(:)=xr*x(:)
    qgaus=xr*sum(w(:)*(func(xm+dx)+func(xm-dx)))
  end function qgaus

  subroutine quad2d_qgaus(x1,x2,ss)
    real, intent(in) :: x1,x2
    real, intent(out) :: ss
    ss=qgaus(g,x1,x2)
  end subroutine quad2d_qgaus

end module quad2d_qgaus_mod
