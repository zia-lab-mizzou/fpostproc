! nc_flow_combine_over.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    September 4, 2013
! PURPOSE:         Determine transfers (flow) between Nc populations.
!                  Combine populations greater than or equal to input.
! INPUT VARIABLES:
!    path   = path to directory containing paths.txt and box.txt
!    tstart = first time step for contact number determination
!    tsep   = distance in time steps since the first to check how particles changed Nc
!    rsep   = separation between particle surfaces to consider contacting
!    geq    = combine greater or equal to this Nc

program main

   use realspace
   implicit none

   integer, parameter :: nbin = 17 ! number of bins
   integer :: geq, i, j, bin, bin_start, bin_end, hist_bin, hist_altered_bin

   integer, dimension(:), allocatable :: nneigh_1, nneigh_2 ! particle neigh #
   integer, dimension(:), allocatable :: pop

   real, dimension(:,:), allocatable :: hist
   real, dimension(:,:), allocatable :: hist_altered
   type(particle), dimension(:), allocatable :: part1, part2
   type(dumpfile), dimension(:), allocatable :: dumps

   integer :: nargs, nsnap
   character(len=300) :: buf, path, shift_str
   character(len=50), dimension(99) :: args
   integer*8 :: tstart, tsep, tend
   real :: rsep, distsq
   real, dimension(3) :: mean_origin, mean_disp
   logical :: yes_shift

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 6) then
      write(*, '(A)') 'nc_flow_combine_over [path] [tstart] [tsep] [rsep] [geq] [yes/no]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) tstart
   call getarg(3, buf); read(buf, *) tsep
   call getarg(4, buf); read(buf, *) rsep
   call getarg(5, buf); read(buf, *) geq
   call getarg(6, buf); read(buf, *) shift_str
   if (trim(shift_str) .eq. 'yes') then
      yes_shift = .true.
   else if (trim(shift_str) .eq. 'no') then
      yes_shift = .false.
   else
      write(*, '(A)') 'nc_flow_combine_over [path] [tstart] [tsep] [rsep] [geq] [yes/no]'
      stop
   end if
   tend = tstart + tsep

   ! Get run info, and open HDF5.

   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()

   ! Allocate arrays.

   allocate(nneigh_1(box%npart)); allocate(nneigh_2(box%npart))
   allocate(pop(geq+1))
   allocate(hist(nbin,nbin))
   if (yes_shift) then
      allocate(hist_altered(geq+1,2*nbin-1))
   end if

   ! Read in the origin time, and wrap for neighbor finding (unwrap later).

   call read_from_series(dumps, tstart, part1)
   call get_nneigh_cell(part1, rsep, nneigh_1)
   call read_from_series(dumps, tend, part2)
   call get_nneigh_cell(part2, rsep, nneigh_2)
   call close_hdf5()

   ! Place particles into histogram, depending upon
   ! what the Nc is at the beginning and end of the tsep.

   hist = 0
   do i = 1, box%npart
      bin_start = nneigh_1(i) + 1
      bin_end   = nneigh_2(i) + 1
      hist(bin_start, bin_end) = hist(bin_start, bin_end) + 1
   end do

   ! Get the initial Nc populations, including the geq filter.

   pop = 0
   do i = 1, box%npart
      bin = nneigh_1(i) + 1
      if (bin .gt. geq) then
         bin = geq + 1
      end if
      pop(bin) = pop(bin) + 1
   end do

   ! Create altered histogram from original, using geq and shifting
   ! If not using altered, still have option for geq

   if (yes_shift) then
      do i = 1, geq+1
         do j = 1, 2*nbin-1
            hist_altered(i,j) = 0.0
         end do
      end do

      do i = 1, nbin
         if (i .le. geq) then
            hist_altered_bin = i
         else
            hist_altered_bin = geq+1
         end if
         do j = 1, 2*nbin-1
            hist_bin = j+i-nbin
            if ((hist_bin .ge. 1).and.(hist_bin .le. nbin)) then
               hist_altered(hist_altered_bin,j) = &
                    hist_altered(hist_altered_bin,j) + hist(i,hist_bin)
            end if
         end do
      end do
   else
      do i = geq+2, nbin
         do j = 1, nbin
            hist(geq+1, j) = hist(geq+1, j) + hist(i, j)
            hist(i, j) = 0.0
         end do
      end do
   end if

   ! Normalize by the initial population number to get a final distribution.

   if (yes_shift) then
      do i = 1, geq+1
         if (pop(i) .gt. 0) hist_altered(i, :) = hist_altered(i, :) / pop(i)
      end do
   else
      do i = 1, nbin
         if (pop(i) .gt. 0) hist(i, :) = hist(i, :) / pop(i)
      end do
   end if

   ! Write output.

   if (yes_shift) then
      do i = 1, 2*nbin-1
         do j = 1, geq+1
            write(*, '(F14.6)', advance='no') hist_altered(j, i)
         end do
         write(*, '(A)') ''
      end do
   else
      do i = 1, nbin
         do j = 1, geq+1
            write(*, '(F14.6)', advance='no') hist(j, i)
         end do
         write(*, '(A)') ''
      end do
   end if

end program main
