! realspace.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    October 3, 2013
! PURPOSE:         Contains functions and subroutines used to work in real space (wrapped coordinates, statistical functions, etc.)
! CONTAINS:        functions: cell_to_triplet, dist_pbc, distsq_pbc, get_msd, get_nneigh_cells, get_rdf, is_contact, realcorr2, triplet_to_cell
!                  subroutines: get_mean_pos, get_nneigh, get_nneigh_cell, get_vanhove, subtract_displacement, wrap_coords, get_vfrac_cell, assign_cells
! NOTES:           Using cells to reduce computation time is only partially implemented right now (in neighbor calculations, but not yet in rdf).
!                  The cell calculation assigns an id to each cell in the box, and I have functions that map an integer x,y,z triple to that id and that are able to find the neighboring cells.



module realspace

  use dump
  implicit none

!**********************************************************************
contains
!**********************************************************************
  
  subroutine assign_cells(part, nx, ny, nz, cells)
    ! note: assumes coords are wrapped

    type(particle), dimension(:), intent(inout) :: part
    integer, intent(in) :: nx, ny, nz
    integer, dimension(:), intent(out) :: cells
    real, dimension(3) :: del
    integer, dimension(3) :: ncell, triplet
    integer :: i
 
    ncell(1) = nx; ncell(2) = ny; ncell(3) = nz
    del = (box%hi-box%lo) / ncell

    if ((ncell(1).le.2) .or. (ncell(2).le.2) .or. (ncell(3).le.2)) then
       write(*, '(A)') 'Cell size too big, error'
       stop
    end if

    ! Find which cell particles belong to.
    do i = 1, box%npart
       triplet = (part(i)%pos - box%lo) / del + 1
       cells(i) = triplet_to_cell(triplet, ncell)
    end do
  end subroutine assign_cells

!**********************************************************************

  function dist_pbc(part1, part2) result (dist)
    ! get the distance between 2 particles in periodic box
    ! note: for unwrapped coords need to wrap first

    type(particle), intent(in) :: part1, part2
    real                       :: dist

    dist = sqrt(distsq_pbc(part1, part2))

  end function dist_pbc

!**********************************************************************

  function distsq_pbc(part1, part2) result (distsq)
    ! get the squared distance between 2 particles in periodic box
    ! note: for unwrapped coords need to wrap first

    type(particle), intent(in) :: part1, part2
    real                       :: distsq
    real, dimension(3)         :: del, prd
    integer                    :: i

    del = part1%pos - part2%pos
    prd = box%hi - box%lo

    do i = 1, 3
       if (abs(del(i)) > 0.5*prd(i)) then
          if (del(i) < 0.0) then
             del(i) = del(i) + prd(i)
          else
             del(i) = del(i) - prd(i)
          end if
       end if
    end do

    distsq = sum(del(1:3)*del(1:3))

  end function distsq_pbc

!**********************************************************************

  subroutine subtract_displacement(part, disp)

    type(particle), dimension(:), intent(inout) :: part
    real, dimension(3), intent(in) :: disp
    integer :: i

    do i = 1, box%npart
       part(i)%pos = part(i)%pos - disp
    end do

  end subroutine subtract_displacement

!**********************************************************************

  subroutine wrap_coords(part)
    ! convert unwrapped coordinates to wrapped coordinates
    ! used in RDF and van Hove functions, but not in MSD

    type(particle), dimension(:), intent(inout) :: part
    integer                                     :: i, j
    real, dimension(3)                          :: prd

    prd = box%hi - box%lo

    do i = 1, size(part)
       do j = 1, 3
          part(i)%pos(j) = box%lo(j) + modulo(part(i)%pos(j), prd(j))
       end do
    end do

  end subroutine wrap_coords

!**********************************************************************

  function get_rdf(part, rcut, nbin) result (rdf)
    ! binning by distance, not squared distance (see Rapaport)

    type(particle), dimension(:), intent(inout) :: part
    real, intent(in)                            :: rcut
    integer, intent(in)                         :: nbin
    real, dimension(nbin)                       :: rdf
    integer*8, dimension(nbin)                  :: hist
    real                                        :: distsq, rcutsq, normfac
    real                                        :: delr
    integer                                     :: i, j, n

    rcutsq = rcut * rcut
    delr = rcut / nbin

    ! wrap coordinates
    call wrap_coords(part)

    ! histogram the particle separations
    hist = 0
    do i = 1, size(part)-1
       do j = i+1, size(part)
          distsq = distsq_pbc(part(i), part(j))
          if (distsq < rcutsq) then
             n = int(sqrt(distsq) / delr) + 1 ! bins numbered from 1, not 0
             hist(n) = hist(n) + 1
          end if
       end do
    end do

    ! normalize the histogram
    normfac = product(box%hi - box%lo) &
         / (2.0 * pi * delr*delr*delr * size(part) * size(part))
    forall (i = 1:nbin) rdf(i) = hist(i) * normfac / ((i-0.5)*(i-0.5))

  end function get_rdf

!**********************************************************************

  function realcorr2(fullorself, part1, part2, rcut, nbin) result (corrfunc)
    ! correlate positions for full or distinct van Hove correlator

    type(particle), dimension(:)             :: part1, part2
    character(len=4), intent(in)             :: fullorself
    real, intent(in)                         :: rcut
    integer, intent(in)                      :: nbin
    real, dimension(nbin)                    :: corrfunc
    integer*8, dimension(nbin)               :: hist
    real                                     :: rcutsq, delr, distsq, normfac
    integer                                  :: i, j, n

    if (size(part1) /= size(part2)) then
       write(*, '(A)') 'Error: particle numbers different, cannot correlate'
       stop
    end if

    rcutsq = rcut * rcut
    delr = rcut / nbin
    hist = 0

    call wrap_coords(part1); call wrap_coords(part2)

    if (fullorself == 'full') then
       do i = 1, size(part1)
          do j = 1, size(part2)
             distsq = distsq_pbc(part1(i), part2(j))
             if (distsq < rcutsq) then
                n = int(sqrt(distsq) / delr) + 1 ! bins from 1, not 0
                hist(n) = hist(n) + 1
             end if
          end do
       end do
    else if (fullorself == 'self') then
       do i = 1, size(part1)
          distsq = distsq_pbc(part1(i), part2(i))
          if (distsq < rcutsq) then
             n = int(sqrt(distsq) / delr) + 1
             hist(n) = hist(n) + 1
          end if
       end do
    else
       write(*, '(A)') 'Error: should be full or self'
       stop
    end if

    ! normalize the histogram
    normfac = 1 / &
         (4.0 * pi * delr*delr*delr * size(part1))
    forall (i = 1:nbin) corrfunc(i) = hist(i) * normfac / ((i-0.5)*(i-0.5))

  end function realcorr2

!**********************************************************************

  subroutine get_vanhove(fullorself, dumps, times, rcut, nbin, vh)
    ! gets the self or full van Hove function
    ! this function operates on dump files, not particle arrays (unlike rdf)
    ! first time used as origin, and there is no origin-origin correlation

    type(dumpfile), dimension(:), intent(in)  :: dumps
    character(len=4), intent(inout)           :: fullorself
    integer*8, dimension(:), intent(in)       :: times
    real, intent(in)                          :: rcut
    integer, intent(in)                       :: nbin
    real, dimension(:, :), allocatable        :: vh
    integer*8                                 :: tzero
    integer                                   :: i, j
    type(particle), dimension(:), allocatable :: part1, part2
    real                                      :: t1, t2

    allocate(vh(nbin, size(times)-1))

    ! load the origin particle array
    call read_from_series(dumps, times(1), part1)

    ! load data from rest of times and correlate
    do i = 1, size(vh, 2)
       call read_from_series(dumps, times(i+1), part2)
       if (size(part1) /= size(part2)) then
          write(*, '(A)') 'Error: different numbers of particles'
          stop
       end if
       call cpu_time(t1)
       vh(:,i) = realcorr2(fullorself, part1, part2, rcut, nbin)
       call cpu_time(t2)
       write(*, '(A, I10, A, F5.2, A)') 'Correlation timestep ', &
                                          times(i+1), ' took ', t2-t1, ' secs'
    end do
    close(9)

  end subroutine get_vanhove

!**********************************************************************

  subroutine get_mean_pos(part, mean_pos)
    ! Get the mean position.  Assume unwrapped coordinates?
    ! This isn't getting the center of mass (unless all particles have same mass).

    type(particle), dimension(:), intent(in) :: part
    real, dimension(3), intent(out) :: mean_pos
    integer :: i

    mean_pos = 0
    do i = 1, box%npart
       mean_pos = mean_pos + part(i)%pos
    end do
    mean_pos = mean_pos / box%npart

  end subroutine get_mean_pos

!**********************************************************************

  function get_msd(dumps, times, subtr_tf) result (msd)

    type(dumpfile), dimension(:), intent(in) :: dumps
    integer*8, dimension(:), intent(in) :: times
    logical, intent(in) :: subtr_tf
    real, dimension(size(times)) :: msd
    type(particle), dimension(:), allocatable :: part1, part2
    real, dimension(3) :: mean_origin, mean_disp
    integer :: i, j

    ! Load the origin particle array.
    call read_from_series(dumps, times(1), part1)

    ! Get initial "average position" for average displacement calculation.
    if (subtr_tf) call get_mean_pos(part1, mean_origin)

    msd = 0
    do i = 1, size(times)

       call read_from_series(dumps, times(i), part2)

       ! Subtract mean motion if supposed to.
       if (subtr_tf) then
          call get_mean_pos(part2, mean_disp)
          mean_disp = mean_disp - mean_origin
          call subtract_displacement(part2, mean_disp)
       end if

       do j = 1, size(part1)
          msd(i) = msd(i) + sum((part2(j)%pos-part1(j)%pos)**2) / size(part1)
       end do

    end do

  end function get_msd

!**********************************************************************

  subroutine get_nneigh(part, rsep, nneigh)
    ! wrap coordinates, find neighbor pairs,
    !    and output the number of neighbors per particle, as well as populations
    ! note that the rcut is for the particle size with diamter 1.0
    ! rsep is the surface-to-surface separation
    ! assume size of pop array gives max number of nneigh for populations
    !   of interest (should be around 12 or a bit more)
    ! global size variable used

    type(particle), dimension(:), intent(in) :: part
    real, intent(in)                         :: rsep
    integer, dimension(:), intent(out)       :: nneigh
    real, dimension(:, :), allocatable :: cutsq
    type(particle), dimension(:), allocatable :: copy_part
    integer :: bin, i, j
    real :: distsq

    ! check inputs
    if (size(nneigh) .ne. size(part)) then
       write(*, '(A)') 'Nneigh should be same size as part'
       stop
    end if

    ! make temporary copy of particle matrix to avoid wrapping the input
    allocate(copy_part(box%npart))
    copy_part = part

    ! use global sizes to get a cutoff matrix
    allocate(cutsq(box%ntype, box%ntype))
    do i = 1, box%ntype
       do j = 1, box%ntype
          cutsq(i, j) = rsep + (box%sizes(i) + box%sizes(j)) / 2
          cutsq(i, j) = cutsq(i, j) * cutsq(i, j)
       end do
    end do

    call wrap_coords(copy_part)

    nneigh = 0
    do i = 1, box%npart-1
       do j = i+1, box%npart
          distsq = distsq_pbc(copy_part(i), copy_part(j))
          if (distsq < cutsq(box%types(i), box%types(j))) then
             nneigh(i) = nneigh(i) + 1
             nneigh(j) = nneigh(j) + 1
          end if
       end do
    end do

    !pop = 0
    !do i = 1, box%npart
    !   bin = nneigh(i) + 1
    !   if (bin .le. size(pop)) then
    !      pop(bin) = pop(bin) + 1
    !   end if
    !end do

  end subroutine get_nneigh

!**********************************************************************

  subroutine get_nneigh_cell(part, rsep, nneigh)

    type(particle), dimension(:), intent(inout) :: part
    real, intent(inout) :: rsep
    integer, dimension(:), intent(out) :: nneigh
    integer, dimension(3) :: ncell
 
    integer :: max_part_cell, i, j, k, m, index1, index2
    integer, dimension(3) :: triplet
    integer, dimension(13) :: neigh_cells
    integer :: neigh
    real :: cell_dist_min, box_delx, box_dely, box_delz
    integer, dimension(:), allocatable :: cell_pop, cell_num
    integer, dimension(:,:), allocatable :: cell_ids
    real, dimension(:,:), allocatable :: cutsq

    ! Get squared cutoffs for particle pairs, using global variables.
    ! This array will be used in the 'is_cut_sq' function.
    allocate(cutsq(box%ntype, box%ntype))
    do i = 1, box%ntype
       do j = 1, box%ntype
          cutsq(i, j) = rsep + (box%sizes(i) + box%sizes(j)) / 2
          cutsq(i, j) = cutsq(i, j) * cutsq(i, j)
       end do
    end do

    ! Set the number of cells in each direction so that the
    ! cell length in each direction is bigger than the largest separation
    ! we are looking for.
    cell_dist_min = sqrt(maxval(cutsq))
    ncell(:) = floor((box%hi(:) - box%lo(:)) / cell_dist_min)
    box_delx = (box%hi(1)-box%lo(1)) / ncell(1)
    box_dely = (box%hi(2)-box%lo(2)) / ncell(2)
    box_delz = (box%hi(3)-box%lo(3)) / ncell(3)
    
    if ((ncell(1).le.2) .or. (ncell(2).le.2) .or. (ncell(3).le.2)) then
       write(*, '(A)') 'Cell size too big, error'
       stop
    end if

    allocate(cell_pop(ncell(1)*ncell(2)*ncell(3)))
    allocate(cell_num(size(part)))

    call wrap_coords(part)

    ! Find which cell particles belong to.
    do i = 1, box%npart
       triplet(1) = (part(i)%pos(1) - box%lo(1)) / box_delx + 1
       triplet(2) = (part(i)%pos(2) - box%lo(2)) / box_dely + 1
       triplet(3) = (part(i)%pos(3) - box%lo(3)) / box_delz + 1
       cell_num(i) = triplet_to_cell(triplet, ncell)
    end do

    ! Find number of particles in each cell.
    cell_pop = 0
    do i = 1, box%npart
       cell_pop(cell_num(i)) = cell_pop(cell_num(i)) + 1
    end do

    ! The cell IDs are numbered from 1 to nx*ny*nz.
    ! Instead of 3 coordinates I am just using one.
    ! First the numbers count across the x axis, then the y axis,
    ! then the z axis.

    ! Allocate array of IDs in each cell.
    ! The dimensions are the maximum particle number
    ! times the number of cells.
    max_part_cell = maxval(cell_pop)
    allocate(cell_ids(max_part_cell, ncell(1)*ncell(2)*ncell(3)))

    ! Trick: Use last index of array as a position tracker.
    !        Initiate all to 1.
    cell_ids = 1

    ! Assign particle IDs to the above array.
    do i = 1, box%npart
       index2 = cell_num(i)
       index1 = cell_ids(max_part_cell, index2)
       cell_ids(max_part_cell, index2) = cell_ids(max_part_cell, index2) + 1
       cell_ids(index1, index2) = i
    end do

    ! Loop over primary cells, then neighboring cells.
    ! Get number of neighbors.
    nneigh = 0
    do i = 1, ncell(1)*ncell(2)*ncell(3)

       ! Loop over pairs within the current cell.
       do j = 1, cell_pop(i)-1
          do k = j+1, cell_pop(i)
             if (is_contact(part, cell_ids(j,i), cell_ids(k,i), cutsq)) then
                nneigh(cell_ids(j, i)) = nneigh(cell_ids(j, i)) + 1
                nneigh(cell_ids(k, i)) = nneigh(cell_ids(k, i)) + 1
             end if
          end do
       end do

       ! Loop over pairs between current cell and neighbor cells.
       neigh_cells = get_neigh_cells(i, ncell)
       do j = 1, cell_pop(i)
          do k = 1, 13 ! independent lattice directions
             neigh = neigh_cells(k)
             do m = 1, cell_pop(neigh)
                if (is_contact(part, cell_ids(j, i), cell_ids(m, neigh), &
                     cutsq)) then
                   nneigh(cell_ids(j, i)) = nneigh(cell_ids(j, i)) + 1
                   nneigh(cell_ids(m, neigh)) = &
                        nneigh(cell_ids(m, neigh)) + 1
                end if
             end do
          end do
       end do

    end do

  end subroutine get_nneigh_cell

!**********************************************************************

  subroutine get_vfrac_cell(part, nx, ny, nz, hist)

    type(particle), dimension(:), intent(inout) :: part
    integer, intent(in) :: nx, ny, nz
    real, dimension(:), intent(out) :: hist
    integer, dimension(:), allocatable :: cells
    real, dimension(:), allocatable :: cell_vfrac
    integer :: part_type, i, bin
    real :: diam, vpart, cell_vol

    cell_vol = (box%hi(1)-box%lo(1)) / nx * &
         (box%hi(2)-box%lo(2)) / ny * &
         (box%hi(3)-box%lo(3)) / nz
 
    allocate(cells(size(part)))
    allocate(cell_vfrac(nx*ny*nz))

    call wrap_coords(part)
    call assign_cells(part, nx, ny, nz, cells)

    ! Find volume fraction
    ! Particle volume in cell if their centers are (crude)
    cell_vfrac = 0.0
    do i = 1, box%npart
       part_type = box%types(i)
       diam = box%sizes(part_type)
       vpart = pi / 6.0 * diam*diam*diam
       cell_vfrac(cells(i)) = cell_vfrac(cells(i)) + vpart
    end do
    cell_vfrac = cell_vfrac / cell_vol

    ! Bin the cell volume fraction values
    hist = 0.0
    do i = 1, size(cell_vfrac)
       bin = floor(cell_vfrac(i)*size(hist)) + 1
       hist(bin) = hist(bin) + 1.0
    end do

    ! Normalize to approximate a pdf
    hist = hist * size(hist) / size(cell_vfrac)

  end subroutine get_vfrac_cell

!**********************************************************************

   ! INCOMPLETE!!

!   function get_stress_rdf(part, rcut, nbin) result (stress_rdf)

!      type(particle), dimension(:), intent(inout) :: part
!      real, intent(in)                            :: rcut
!      integer, intent(in)                         :: nbin
!      real, dimension(nbin)                       :: stress_rdf
!      real                                        :: distsq, rcutsq, normfac
!      real                                        :: delr
!      integer                                     :: i, j, n

!      rcutsq = rcut * rcut
!      delr = rcut / nbin
!      call wrap_coords(part)

      ! Histogram the stresses.
!      hist = 0
!      do i = 1, size(part)-1
!         do j = i+1, size(part)
!            distsq = distsq_pbc(part(i), part(j))
!            if (distsq < rcutsq) then
!               n = int(sqrt(distsq) / delr) + 1 ! bins numbered from 1, not 0
!               hist(n) = hist(n) + 1
!            end if
!         end do
!      end do

      ! normalize the histogram
      !normfac = product(box%hi - box%lo) &
      !          / (2.0 * pi * delr*delr*delr * size(part) * size(part))
      !forall (i = 1:nbin) rdf(i) = hist(i) * normfac / ((i-0.5)*(i-0.5))

!    end function get_stress_rdf


!**********************************************************************
! Helper functions
!**********************************************************************

  function is_contact(part, index1, index2, cutsq) result (answer)
    ! Takes a particle array, two particle IDs, and a matrix of squared
    ! cutoff distances to say if particles are contacting or not.
    ! This function assumes that coordinates are wrapped.

    type(particle), dimension(:) :: part
    integer                      :: index1, index2
    real, dimension(:,:)         :: cutsq
    logical                      :: answer
    real                         :: cutsq_pair, distsq

    cutsq_pair = cutsq(box%types(index1), box%types(index2))
    distsq = distsq_pbc(part(index1), part(index2))

    answer = (distsq < cutsq_pair)

  end function is_contact

!**********************************************************************

  function get_neigh_cells(cell, ncell) result(neigh_cells)

    integer, dimension(3)    :: ncell, triplet_in
    integer, dimension(3,13) :: triplet_out
    integer                  :: cell, i
    integer, dimension(13)   :: neigh_cells

    ! Sequence of neighbor cells:
    ! +x, +y, +z, +xy, +yz, +xz, +xyz

    triplet_in = cell_to_triplet(cell, ncell)

    triplet_out(:, 1) = triplet_in + (/  1,  0,  0 /) ! +x
    triplet_out(:, 2) = triplet_in + (/  0,  1,  0 /) ! +y
    triplet_out(:, 3) = triplet_in + (/  0,  0,  1 /) ! +z
    triplet_out(:, 4) = triplet_in + (/  1,  1,  0 /) ! +xy
    triplet_out(:, 5) = triplet_in + (/  0,  1,  1 /) ! +yz
    triplet_out(:, 6) = triplet_in + (/  1,  0,  1 /) ! +xz
    triplet_out(:, 7) = triplet_in + (/  1, -1,  0 /) ! (+x)(-y)
    triplet_out(:, 8) = triplet_in + (/  0,  1, -1 /) ! (+y)(-z)
    triplet_out(:, 9) = triplet_in + (/ -1,  0,  1 /) ! (-x)(+z)
    triplet_out(:,10) = triplet_in + (/  1,  1,  1 /) ! (+x)(+y)(+z)
    triplet_out(:,11) = triplet_in + (/  1,  1, -1 /) ! (+x)(+y)(-z)
    triplet_out(:,12) = triplet_in + (/  1, -1,  1 /) ! (+x)(-y)(+z)
    triplet_out(:,13) = triplet_in + (/ -1,  1,  1 /) ! (-x)(+y)(+z)


    do i = 1, 13
       neigh_cells(i) = triplet_to_cell(triplet_out(:,i), ncell)
    end do

  end function get_neigh_cells

!**********************************************************************

  function triplet_to_cell(triplet, ncell) result (cell)
    ! Take input cell x,y,z triplet vector and number of cells in box
    ! in each direction (vector), and output the cell number.

    integer, dimension(3) :: triplet, ncell, triplet_wrap
    integer               :: cell

    ! Wrap the input triplet.
    triplet_wrap = 1 + modulo(triplet-1, ncell)

    ! Change triplet to the cell number.
    cell = triplet_wrap(1) + (triplet_wrap(2)-1)*ncell(1) + &
         (triplet_wrap(3)-1)*ncell(1)*ncell(2)

  end function triplet_to_cell

!**********************************************************************

  function cell_to_triplet(cell, ncell) result (triplet)
    ! Take input cell and box cell dimensions, and output the triplet.

    integer, dimension(3) :: triplet, ncell
    integer               :: cell

    if ((cell.gt.ncell(1)*ncell(2)*ncell(3)) .or. (cell.lt.1)) then
       write(*, '(A)') 'Error: bad cell number'
       stop
    end if

    triplet(3) = (cell-1)/ncell(1)/ncell(2) + 1
    triplet(2) = mod(cell-1, ncell(1)*ncell(2)) / ncell(1) + 1
    triplet(1) = mod(mod(cell-1, ncell(1)*ncell(2)), ncell(1)) + 1

  end function cell_to_triplet

!**********************************************************************

  subroutine lamda2x(part)
    type(particle), dimension(:), intent(inout) :: part
    real :: xl, yl, zl
    real, dimension(6) :: h
    integer :: i

    h = box%h

    do i = 1, box%npart
       xl = part(i)%pos(1)
       yl = part(i)%pos(2)
       zl = part(i)%pos(3)

       part(i)%pos(1) = h(1)*xl + h(6)*yl + h(5)*zl + box%boxlo(1)
       part(i)%pos(2) = h(2)*yl + h(4)*zl + box%boxlo(2)
       part(i)%pos(3) = h(3)*zl + box%boxlo(3)
    end do

  end subroutine lamda2x

!**********************************************************************

  subroutine x2lamda(part)
    type(particle), dimension(:), intent(inout) :: part
    real, dimension(3) :: x, delta
    real, dimension(6) :: h_inv
    integer :: i

    hi = box%h_inv

    do i = 1, box%npart
       x = part(i)%pos
       delta = x - box%boxlo

       part(i)%pos(1) = hi(1)*delta(1) + hi(6)*delta(2) + hi(5)*delta(3)
       part(i)%pos(2) = hi(2)*delta(2) + hi(4)*delta(3)
       part(i)%pos(3) = hi(3)*delta(3)
    end do

  end subroutine x2lamda

!**********************************************************************

  subroutine xtri2xutri(part)
    type(particle), dimension(:), intent(inout) :: part
    real, dimension(6) :: h
    real, dimension(3) :: xtri
    integer :: i
    integer, dimension(3) :: img

    h = box%h

    do i = 1, box%npart
       xtri = part(i)%pos
       img = part(i)%image
       part(i)%pos(1) = xtri(1) + h(1)*img(1) + h(6)*img(2) + h(5)*img(3)
       part(i)%pos(2) = xtri(2) + h(2)*img(2) + h(4)*img(3)
       part(i)%pos(3) = xtri(3) + h(3)*img(3)
    end do

  end subroutine x2xutri

!**********************************************************************

  subroutine xutri2xtri(part)
    type(particle), dimension(:), intent(inout) :: part
    real, dimension(6) :: hi
    real, dimension(3) :: xutri
    integer :: i

    do i = box%npart

    end do
  end subroutine xutri2xtri

!**********************************************************************

  subroutine minimum_image_tri(dx, dy, dz)
    ! minimum image convention
    ! use 1/2 of box size as test
    ! for triclinic, also add/subtract tilt factors in other dims as needed

    real, intent(inout) :: dx,dy,dz

    if (.not. box%triclinic) then
       if (box%xperiodic) then
          if (abs(dx) > box%xprd_half) then
             if (dx < 0.0) then
                dx = dx + box%xprd
             else
                dx = dx - box%xprd
             end if
          end if
       end if
       if (box%yperiodic) then
          if (abs(dy) > box%yprd_half) then
             if (dy < 0.0) then
                dy = dy + box%yprd
             else
                dy = dy - box%yprd
             end if
          end if
       end if
       if (box%zperiodic) then
          if (abs(dz) > box%zprd_half) then
             if (dz < 0.0) then
                dz = dz + box%zprd
             else
                dz = dz - box%zprd
             end if
          end if
       end if

    else ! box is triclinic
       if (box%zperiodic) then
          if (abs(dz) > box%zprd_half) then
             if (dz < 0.0) then
                dz = dz + box%zprd
                dy = dy + box%yz
                dx = dx + box%xz
             else
                dz = dz - box%zprd
                dy = dy - box%yz
                dx = dx - box%xz
             end if
          end if
       end if
       if (box%yperiodic) then
          if (abs(dy) > box%yprd_half) then
             if (dy < 0.0) then
                dy = dy + box%yprd
                dx = dx + box%xy
             else
                dy = dy - box%yprd
                dx = dx - box%xy
             end if
          end if
       end if
       if (box%xperiodic) then
          if (abs(dx) > box%xprd_half) then
             if (dx < 0.0) then
                dx = dx + box%xprd
             else
                dx = dx - box%xprd
             end if
          end if
       end if
    end if
  end subroutine minimum_image_tri

!**********************************************************************

  subroutine minimum_image_vec(delta)
    ! minimum image convention
    ! use 1/2 of box size as test
    ! for triclinic, also add/subtract tilt factors in other dims as needed
    real, dimension(3), intent(in) :: delta

    if (.not. box%triclinic) then
       if (box%xperiodic) then
          if (abs(delta(1)) > box%xprd_half) then
             if (delta(1) < 0.0) then
                delta(1) = delta(1) + box%xprd
             else
                delta(1) = delta(1) - box%xprd
             end if
          end if
       end if
       if (box%yperiodic) then
          if (abs(delta(2)) > box%yprd_half) then
             if (delta(2) < 0.0) then
                delta(2) = delta(2) + box%yprd
             else
                delta(2) = delta(2) - box%yprd
             end if
          end if
       end if
       if (box%zperiodic) then
          if (abs(delta(3)) > box%zprd_half) then
             if (delta(3) < 0.0) then
                delta(3) = delta(3) + box%zprd
             else
                delta(3) = delta(3) - box%zprd
             end if
          end if
       end if

    else ! box is triclinic
       if (box%zperiodic) then
          if (abs(delta(3)) > box%zprd_half) then
             if (delta(3) < 0.0) then
                delta(3) = delta(3) + box%zprd
                delta(2) = delta(2) + box%yz
                delta(1) = delta(1) + box%xz
             else
                delta(3) = delta(3) - box%zprd
                delta(2) = delta(2) - box%yz
                delta(1) = delta(1) - box%xz
             end if
          end if
       end if
       if (box%yperiodic) then
          if (abs(delta(2)) > box%yprd_half) then
             if (delta(2) < 0.0) then
                delta(2) = delta(2) + box%yprd
                delta(1) = delta(1) + box%xy
             else
                delta(2) = delta(2) - box%yprd
                delta(1) = delta(1) - box%xy
             end if
          end if
       end if
       if (box%xperiodic) then
          if (abs(delta(1)) > box%xprd_half) then
             if (delta(1) < 0.0) then
                delta(1) = delta(1) + box%xprd
             else
                delta(1) = delta(1) - box%xprd
             end if
          end if
       end if
    end if

  end subroutine minimum_image_vec

!**********************************************************************

  subroutine closest_image(xi, xj, xjimage)
    ! find and return Xj image = periodic image of Xj that is closest to Xi
    ! for triclinic, add/subtract tilt factors in other dims as needed
    real, dimension(3), intent(in)    :: xi, xj
    real, dimension(3), intent(inout) :: xjimage
    real, dimension(3)                :: dx, dy, dz

    dx = xj(1) - xi(1)
    dy = xj(2) - xi(2)
    dz = xj(3) - xi(3)

    if (.not. triclinic) then
       if (box%xperiodic) then
          if (dx < 0.0) then
             do while (dx < 0.0)
                dx = dx + box%xprd
             end do
             if (dx > box%xprd_half) then
                dx = dx - box%xprd
             end if
          else
             do while (dx > 0.0)
                dx = dx - box%xprd
             end do
             if (dx < -box%xprd_half) then
                dx = dx + box%xprd
             end if
          end if
       end if
       if (box%yperiodic) then
          if (dy < 0.0) then
             do while (dy < 0.0)
                dy = dy + box%yprd
             end do
             if (dy > box%yprd_half) then
                dy = dy - box%yprd
             end if
          else
             do while (dy > 0.0)
                dy = dy - box%yprd
             end do
             if (dy < -box%yprd_half) then
                dy = dy + box%yprd
             end if
          end if
       end if
       if (box%zperiodic) then
          if (dz < 0.0) then
             do while (dz < 0.0)
                dz = dz + box%zprd
             end do
             if (dz > box%zprd_half) then
                dz = dz - box%zprd
             end if
          else
             do while (dz > 0.0)
                dz = dz - box%zprd
             end do
             if (dz < -box%zprd_half) then
                dz = dz + box%zprd
             end if
          end if
       end if

    else ! triclinic
       if (box%zperiodic) then
          if (dz < 0.0) then
             do while (dz < 0.0)
                dz = dz + box%zprd
                dy = dy + box%yz
                dx = dx + box%xz
             end do
             if (dz > box%zprd_half) then
                dz = dz - box%zprd
                dy = dy - box%yz
                dx = dx - box%xz
             end if
          else
             do while (dz > 0.0)
                dz = dz - box%zprd
                dy = dy - box%yz
                dx = dx - box%xz
             end do
             if (dz < -box%zprd_half) then
                dz = dz + box%zprd
                dy = dy + box%yz
                dx = dx + box%xz
             end if
          end if
       end if
       if (box%yperiodic) then
          if (dy < 0.0) then
             do while (dy < 0.0)
                dy = dy + box%yprd
                dx = dx + box%xy
             end do
             if (dy > box%yprd_half) then
                dy = dy - box%yprd
                dx = dx - box%xy
             end if
          else
             do while (dy > 0.0)
                dy = dy - box%yprd
                dx = dx - box%xy
             end do
             if (dy < -box%yprd_half) then
                dy = dy + box%yprd
                dx = dx + box%xy
             end if
          end if
       end if
       if (box%xperiodic) then
          if (dx < 0.0) then
             do while (dx < 0.0)
                dx = dx + box%xprd
             end do
             if (dx > box%xprd_half) then
                dx = dx - box%xprd
             end if
          else
             do while (dx > 0.0)
                dx = dx - box%xprd
             end do
             if (dx < -box%xprd_half) then
                dx = dx + box%xprd
             end if
          end if
       end if
    end if

    xjimage(1) = xi(1) + dx
    xjimage(2) = xi(2) + dy
    xjimage(3) = xi(3) + dz
  end subroutine closest_image

!**********************************************************************

  ! remap the point into the periodic box no matter how far away
  ! adjust 3 image flags encoded in image accordingly
  ! resulting coord must satisfy lo <= coord < hi
  ! MAX is important since coord - prd < lo can happen when coord = hi
  ! for triclinic, point is converted to lamda coords (0-1) before doing remap
  ! image = 10 bits for each dimension
  ! increment/decrement in wrap-around fashion     

  subroutine remap(part)
    type(particle), intent(inout) :: part

    ! pointers here?

    if (.not. box%triclinic) then
       


  end subroutine remap

!**********************************************************************

  subroutine remap_all(part)
    type(particle), dimension(:), intent(inout) :: part
  end subroutine remap_all

!**********************************************************************

  subroutine remap_near()
  end subroutine remap_near

!**********************************************************************

  subroutine unmap()
  end subroutine unmap

!**********************************************************************

  ! convert triclinic 0-1 lamda coords to box coords for all N atoms
  ! x = H lamda + x0

  subroutine lamda2x(part)
    type(particle), dimension(:), intent(inout) :: part
    integer                                     :: i

    do i = 1, box%npart
       part(i)%pos(1) = box%h(1)*part(i)%pos(1) + box%h(6)*part(i)%pos(2) &
            + box%h(5)*part(i)%pos(3) + box%boxlo(1)
       part(i)%pos(2) = box%h(2)*part(i)%pos(2) + box%h(4)*part(i)%pos(3) &
            + box%boxlo(2)
       part(i)%pos(3) = box%h(3)*part(i)%pos(3) + box%boxlo(3)
    end do

  end subroutine lamda2x

!**********************************************************************

  ! convert box coords to triclinic 0-1 lamda coords for all N atoms
  ! lamda = H^-1 (x - x0)

  subroutine x2lamda(part)
    type(particle), dimension(:), intent(inout) :: part
    integer                                     :: i
    real, dimension(3)                          :: delta

    do i = 1, box%npart
       delta = part(i)%pos - box%boxlo
       part(i)%pos(1) = box%h_inv(1)*delta(1) + box%h_inv(6)*delta(2) &
            + box%h_inv(5)*delta(3)
       part(i)%pos(2) = box%h_inv(2)*delta(2) + box%h_inv(4)*delta(3)
       part(i)%pos(3) = box%h_inv(3)*delta(3)
    end do

  end subroutine x2lamda

!**********************************************************************

!**********************************************************************
end module realspace
!**********************************************************************
