! bin_msd.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 28, 2012
! PURPOSE:         Get the MSD for subpopulations with different contact numbers.
! INPUT VARIABLES:
!    path     = path to directory containing paths.txt and box.txt
!    tstart   = time step to determine contact numbers and to start measurements
!    tdur     = duration of measurement, in time steps
!    tinc     = time steps between measurements
!    rsep     = separation between particle surfaces to consider contacting
!    subtr_yn = "yes" or "no" to subtract off the drift of entire system

program main

   use realspace
   implicit none

   integer, parameter :: nbin = 17  ! number of bins
   integer :: i, j, bin, bin1, bin2 ! loop variables

   integer, dimension(:), allocatable :: nneigh_1, nneigh_2 ! particle contact numbers
   integer, dimension(:), allocatable :: pop_1, pop_2       ! bin populations
   integer, dimension(:,:), allocatable :: pop_xfer         ! bin transfers

   real, dimension(:,:), allocatable :: msd
   type(particle), dimension(:), allocatable :: part1, part2
   type(dumpfile), dimension(:), allocatable :: dumps

   integer :: nargs, nsnap
   character(len=300) :: buf, path, truncpath
   character(len=50), dimension(99) :: args
   integer*8 :: tstart, tdur, tinc, tcurr
   integer*8, dimension(:), allocatable :: times
   real :: rsep, distsq
   logical :: subtr_yn
   real, dimension(3) :: mean_origin, mean_disp

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 6) then
      write(*, '(A)') 'bin_msd [path] [tstart] [tdur] [tinc] [rsep] [subtr_yn]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) tstart
   call getarg(3, buf); read(buf, *) tdur
   call getarg(4, buf); read(buf, *) tinc
   call getarg(5, buf); read(buf, *) rsep
   call getarg(6, buf)
   if (trim(adjustl(buf)) .eq. 'yes') then
      subtr_yn = .true.
   else if (trim(adjustl(buf)) .eq. 'no') then
      subtr_yn = .false.
   else
      write(*, '(A)') '[subtr_yn] must be yes or no'
   end if

   ! Check if the time duration and increment make sense.

   if (mod(tdur, tinc) .ne. 0) then
      write(*, '(A)') 'Bad tdur-tinc pair'
      stop
   end if

   ! Create schedule of time steps to visit for measurement.

   nsnap = tdur / tinc + 1
   allocate(times(nsnap))
   do i = 1, nsnap
      times(i) = tstart + (i-1) * tinc
   end do

   ! Get ready to read the HDF5 data.

   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()

   ! Allocate arrays.

   allocate(nneigh_1(box%npart)); allocate(nneigh_2(box%npart))
   allocate(pop_1(nbin)); allocate(pop_2(nbin))
   allocate(pop_xfer(nbin,nbin))
   allocate(msd(nbin, nsnap))

   ! Read in the origin time, and wrap coordinates for neighbor finding.

   call read_from_series(dumps, times(1), part1)
   call get_nneigh_cell(part1, rsep, nneigh_1)
   call read_from_series(dumps, times(nsnap), part2)
   call get_nneigh_cell(part2, rsep, nneigh_2)

   ! Reload the origin particle array to get unwrapped coordinates.

   call read_from_series(dumps, times(1), part1) ! to remove PBC wrapping

   ! Get initial "average position" for average displacement calculation.

   if (subtr_yn) call get_mean_pos(part1, mean_origin)

   ! Loop over designated time steps to get the binned MSD.
   ! Subtract mean motion if desired.

   msd = 0
   do i = 2, size(times)
      call read_from_series(dumps, times(i), part2)
      if (subtr_yn) then
         call get_mean_pos(part2, mean_disp)
         mean_disp = mean_disp - mean_origin
         call subtract_displacement(part2, mean_disp)
      end if
      do j = 1, box%npart
         distsq = sum((part2(j)%pos-part1(j)%pos)**2)
         bin = nneigh_1(j) + 1
         msd(bin, i) = msd(bin, i) + distsq
      end do
   end do
   call close_hdf5()

   ! Determine number of particles in each population.

   pop_1 = 0; pop_2 = 0
   do i = 1, box%npart
      bin1 = nneigh_1(i)+1; bin2 = nneigh_2(i)+1 ! 0-neigh bin in position 1
      pop_1(bin1) = pop_1(bin1) + 1
      pop_2(bin2) = pop_2(bin2) + 1
   end do

   ! Normalize summed squared displacements by dividing initial population sizes.

   do i = 1, nbin
      if (pop_1(i) .ne. 0) then
         do j = 1, nsnap
            msd(i, j) = msd(i, j) / pop_1(i)
         end do
      end if
   end do

   ! Determine transfer between populations.

   pop_xfer = 0
   do i = 1, box%npart
      pop_xfer(nneigh_1(i)+1, nneigh_2(i)+1) = &
           pop_xfer(nneigh_1(i)+1, nneigh_2(i)+1) - 1
      pop_xfer(nneigh_2(i)+1, nneigh_1(i)+1) = &
           pop_xfer(nneigh_2(i)+1, nneigh_1(i)+1) + 1
   end do

   ! Write output.
   ! This is custom output (not using the output module).

   write(buf, *) tstart
   !open(8, file=trim(adjustl(buf))//'.msd_bin.ndist', action='write')
   !open(9, file=trim(adjustl(buf))//'.msd_bin.msd', action='write')
   !write(8, '(A)') 'Neigh distribution first, second'
   !do i = 1, nbin
   !   write(8, '(I4,2I11)') i-1, pop_1(i), pop_2(i)
   !end do
   !write(8, '(A)') ''
   !write(8, '(A)') 'Population transfers'
   !write(8, '(A)') 'Rows show net gain for a nnpop from exchange with nnpop associated with column'
   !do i = 1, nbin
   !   do j = 1, nbin
   !      write(8, '(I11)', advance='no') pop_xfer(i, j)
   !   end do
   !   write(8, '(A)') ''
   !end do
   !write(9, '(A)') 'Mean squared displacement for each population'
   do i = 1, nbin
      write(*, '(I4)', advance='no') i-1
   end do
   write(*, '(A)') ''
   do i = 1, nsnap
      do j = 1, nbin
         write(*, '(F14.6)', advance='no') msd(j, i)
      end do
      write(*, '(A)') ''
   end do

   !close(8); close(9)

end program main
