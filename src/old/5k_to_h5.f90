! 5k_to_h5.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    September 6, 2012
! PURPOSE:         Read through a LAMMPS trajectory and write an HDF5 file.
!                  You have to play with the parameters to fit your LAMMPS trajectory (number of time steps written to file, columns, etc.)
! INPUT VARIABLES:
! NOTE:            This isn't a flexible executable yet and is very specific in its current form.

program main

   use hdf5
   implicit none

   character(len=300) :: dumppath, filename, buf
   integer            :: nargs

   character(len=3) , parameter :: name_pos = 'pos'
   character(len=3) , parameter :: name_vel = 'vel'
   character(len=2) , parameter :: name_pe  = 'pe'
   character(len=12), parameter :: name_sts = 'virialstress'

   integer, parameter :: npart = 753571
   integer, parameter :: nsnap = 500
   integer, parameter :: chunktime = 1
   integer, parameter :: complevel = 9
   integer, parameter :: chunkbytes = 256000
   integer, parameter :: part_chunk_pos = int(real(chunkbytes) / 4 / 3)
   integer, parameter :: part_chunk_vel = int(real(chunkbytes) / 4 / 3)
   integer, parameter :: part_chunk_pe  = int(real(chunkbytes) / 4 / 1)
   integer, parameter :: part_chunk_sts = int(real(chunkbytes) / 4 / 6)

   integer :: hdferr, filter_id, filter_info, filter_info_both, flags
   integer(SIZE_T) :: nelmts
   integer, dimension(4) :: cd_values
   logical :: avail
   integer, parameter :: MaxChrLen = 90
   character(len=MaxChrLen) :: name
   integer(HID_T) :: file, filetype
   integer(HID_T) :: s_f_pos, s_f_vel, s_f_pe, s_f_sts ! file dataspace
   integer(HID_T) :: s_m_pos, s_m_vel, s_m_pe, s_m_sts ! memory dataspace
   integer(HID_T) ::   d_pos,   d_vel,   d_pe,   d_sts ! datasets
   integer(HID_T) :: loc_pos, loc_vel, loc_pe, loc_sts ! locations w/in .h5
   integer(HID_T) ::   p_pos,   p_vel,   p_pe,   p_sts ! property lists

   integer(HSIZE_T), dimension(3) :: dim_pos = (/ 3, npart, nsnap /)
   integer(HSIZE_T), dimension(3) :: dim_vel = (/ 3, npart, nsnap /)
   integer(HSIZE_T), dimension(3) :: dim_pe  = (/ 1, npart, nsnap /)
   integer(HSIZE_T), dimension(3) :: dim_sts = (/ 6, npart, nsnap /)

   integer(HSIZE_T), dimension(3) :: chunk_pos = (/ 3, part_chunk_pos, 1 /)
   integer(HSIZE_T), dimension(3) :: chunk_vel = (/ 3, part_chunk_vel, 1 /)
   integer(HSIZE_T), dimension(3) :: chunk_pe  = (/ 1, part_chunk_pe , 1 /)
   integer(HSIZE_T), dimension(3) :: chunk_sts = (/ 6, part_chunk_sts, 1 /)

   ! starting hyperslab selection start points
   integer(HSIZE_T), dimension(3) :: start_pos = (/ 0, 0, 0 /)
   integer(HSIZE_T), dimension(3) :: start_vel = (/ 0, 0, 0 /)
   integer(HSIZE_T), dimension(3) :: start_pe  = (/ 0, 0, 0 /)
   integer(HSIZE_T), dimension(3) :: start_sts = (/ 0, 0, 0 /)

   integer(HSIZE_T), dimension(3) :: count_pos = (/ 3, npart, 1 /)
   integer(HSIZE_T), dimension(3) :: count_vel = (/ 3, npart, 1 /)
   integer(HSIZE_T), dimension(3) :: count_pe  = (/ 1, npart, 1 /)
   integer(HSIZE_T), dimension(3) :: count_sts = (/ 6, npart, 1 /)

   integer(HSIZE_T), dimension(3) :: start, end

   integer :: id, type
   integer :: i, j, classtype, nfilters

   real :: tstart, tend

   integer(SIZE_T) :: size
   integer, parameter :: ndims = 3
   integer(HSIZE_T), dimension(ndims) :: dims

   type particle ! compound data type
      integer :: type
      real, dimension(3) :: pos
      real, dimension(3) :: vel
      real :: pe
      real, dimension(6) :: stress
   end type particle

   ! to hold particle data, the main structured type and arrays
   ! to pass to H5
   type(particle), dimension(753571) :: part ! type for rest of fpp
   real, dimension(3, npart, 1), target :: buf_pos
   real, dimension(3, npart, 1), target :: buf_vel
   real, dimension(1, npart, 1), target :: buf_pe
   real, dimension(6, npart, 1), target :: buf_sts


   !================================================================
   ! Main program

   ! get input arguments
   nargs = iargc()
   if (nargs /= 2) then
      write(*, '(A)') '5k_to_h5 [path] [filename]'
      stop
   end if
   call getarg(1, buf); dumppath = trim(adjustl(buf))
   call getarg(2, buf); filename = trim(adjustl(buf))

   open(7, file=trim(dumppath))

   ! init Fortran interface to HDF5
   call h5open_f(hdferr)

  ! check if gzip compression is available                                     
   call h5zfilter_avail_f(H5Z_FILTER_DEFLATE_F, avail, hdferr)

   if (.not. avail) then
      write(*, '("gzip filter not available.",/)')
      stop
   end if
   call h5zget_filter_info_f(H5Z_FILTER_DEFLATE_F, filter_info, hdferr)
   filter_info_both = ior(H5Z_FILTER_ENCODE_ENABLED_F, &
                          H5Z_FILTER_DECODE_ENABLED_F)
   if (filter_info .ne. filter_info_both) then
     write(*, '("gzip filter not available for encoding and decoding.",/)')
     stop
   end if

   ! create the .h5 file
   call h5fcreate_f(trim(filename), H5F_ACC_TRUNC_F, file, hdferr)

   ! create the memory dataspaces, dimensions same as hyperslab selection count
   call h5screate_simple_f(3, count_pos, s_m_pos, hdferr)
   call h5screate_simple_f(3, count_vel, s_m_vel, hdferr)
   call h5screate_simple_f(3, count_pe , s_m_pe , hdferr)
   call h5screate_simple_f(3, count_sts, s_m_sts, hdferr)

   ! create file dataspaces
   call h5screate_simple_f(3, dim_pos, s_f_pos, hdferr)
   call h5screate_simple_f(3, dim_vel, s_f_vel, hdferr)
   call h5screate_simple_f(3, dim_pe , s_f_pe , hdferr)
   call h5screate_simple_f(3, dim_sts, s_f_sts, hdferr)

   ! create the dataset creation property lists
   call h5pcreate_f(H5P_DATASET_CREATE_F, p_pos, hdferr)
   call h5pcreate_f(H5P_DATASET_CREATE_F, p_vel, hdferr)
   call h5pcreate_f(H5P_DATASET_CREATE_F, p_pe , hdferr)
   call h5pcreate_f(H5P_DATASET_CREATE_F, p_sts, hdferr)

   ! use gzip compression for all                                            
   call h5pset_deflate_f(p_pos, complevel, hdferr)
   call h5pset_deflate_f(p_vel, complevel, hdferr)
   call h5pset_deflate_f(p_pe , complevel, hdferr)
   call h5pset_deflate_f(p_sts, complevel, hdferr)

   ! set the chunking                                                        
   call h5pset_chunk_f(p_pos, 3, chunk_pos, hdferr)
   call h5pset_chunk_f(p_vel, 3, chunk_vel, hdferr)
   call h5pset_chunk_f(p_pe , 3, chunk_pe , hdferr)
   call h5pset_chunk_f(p_sts, 3, chunk_sts, hdferr)

   ! create datasets
   loc_pos = file; loc_vel = file; loc_pe = file; loc_sts = file
   call h5tcopy_f(H5T_IEEE_F32LE, filetype, hdferr)
   call h5dcreate_f(loc_pos, name_pos, filetype, s_f_pos, d_pos, hdferr, p_pos)
   call h5dcreate_f(loc_vel, name_vel, filetype, s_f_vel, d_vel, hdferr, p_vel)
   call h5dcreate_f(loc_pe , name_pe , filetype, s_f_pe , d_pe , hdferr, p_pe )
   call h5dcreate_f(loc_sts, name_sts, filetype, s_f_sts, d_sts, hdferr, p_sts)

   ! loop over snaps in file
   do i = 1, nsnap
      write(*, '(A,I3)') 'Reading snapshot ', i
      do j = 1, 9
         read(7, *)
      end do
      do j = 1, npart
         read(7, *) id, type, part(id)%pos(:), part(id)%vel(:), &
                    part(id)%pe, part(id)%stress(:)
      end do

      ! copy contents of structure into a real array used to make H5 file
      do j = 1, npart
         buf_pos(:, j, 1) = part(j)%pos
         buf_vel(:, j, 1) = part(j)%vel
         buf_pe (:, j, 1) = part(j)%pe
         buf_sts(:, j, 1) = part(j)%stress
      end do

      ! use hyperslabs to select relevant portion of data to write to
      ! each of these sets selection within the appropriate space
      write(*, '(A)') 'Selecting regions with hyperslabs'
      call h5sselect_hyperslab_f(s_f_pos, H5S_SELECT_SET_F, start_pos, &
                                count_pos, hdferr)
      call h5sselect_hyperslab_f(s_f_vel, H5S_SELECT_SET_F, start_vel, &
                                count_vel, hdferr)
      call h5sselect_hyperslab_f(s_f_pe , H5S_SELECT_SET_F, start_pe , &
                                count_pe , hdferr)
      call h5sselect_hyperslab_f(s_f_sts, H5S_SELECT_SET_F, start_sts, &
                                count_sts, hdferr)

      ! write data to datasets
      write(*, '(A)') 'Writing to datasets'
      print*, 'pos'
      call h5dwrite_f(d_pos, H5T_NATIVE_REAL, buf_pos, count_pos, hdferr, &
                      file_space_id=s_f_pos, mem_space_id=s_m_pos)
      print*, 'vel'
      call h5dwrite_f(d_vel, H5T_NATIVE_REAL, buf_vel, count_vel, hdferr, &
                      file_space_id=s_f_vel, mem_space_id=s_m_vel)
      print*, 'pe'
      call h5dwrite_f(d_pe , H5T_NATIVE_REAL, buf_pe , count_pe , hdferr, &
                      file_space_id=s_f_pe , mem_space_id=s_m_pe )
      print*, 'sts'
      call h5dwrite_f(d_sts, H5T_NATIVE_REAL, buf_sts, count_sts, hdferr, &
                      file_space_id=s_f_sts, mem_space_id=s_m_sts)

      ! update starting position for hyperslab selection (advance 1 in time)
      start_pos = start_pos + (/ 0, 0, 1 /)
      start_vel = start_vel + (/ 0, 0, 1 /)
      start_pe  = start_pe  + (/ 0, 0, 1 /)
      start_sts = start_sts + (/ 0, 0, 1 /)

   end do

   ! close and release resources
   call h5pclose_f(p_pos, hdferr)
   call h5pclose_f(p_vel, hdferr)
   call h5pclose_f(p_pe , hdferr)
   call h5pclose_f(p_sts, hdferr)

   call h5dclose_f(d_pos, hdferr)
   call h5dclose_f(d_vel, hdferr)
   call h5dclose_f(d_pe , hdferr)
   call h5dclose_f(d_sts, hdferr)

   call h5sclose_f(s_m_pos, hdferr)
   call h5sclose_f(s_m_vel, hdferr)
   call h5sclose_f(s_m_pe , hdferr)
   call h5sclose_f(s_m_sts, hdferr)

   call h5sclose_f(s_f_pos, hdferr)
   call h5sclose_f(s_f_vel, hdferr)
   call h5sclose_f(s_f_pe , hdferr)
   call h5sclose_f(s_f_sts, hdferr)

   call h5fclose_f(file, hdferr)

   close(7)

 end program main
