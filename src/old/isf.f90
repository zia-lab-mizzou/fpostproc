! isf.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 28, 2012
! PURPOSE:         Get the self-intermediate scattering function.
! INPUT VARIABLES:
!    path     = path to directory containing paths.txt and box.txt
!    tstart   = time step to start measurements
!    tdur     = duration of measurement, in time steps
!    tinc     = time steps between measurements
!    qtarg    = target wavenumber, may be modified to satisfy PBC box requirements
!    mlimit   = maximum # of modes to use (300 is good enough)
!    subtr_yn = "yes" or "no" to subtract off the drift of entire system

program main

   use fourier
   implicit none

   character(len=300) :: buf, buf2, subtr_yn
   character(len=300) :: path, truncpath
   character(len=30), dimension(30) :: args
   integer*8 :: tstart, tdur, tinc
   real :: qtarg, qact
   integer :: nargs, msqtarg, msqact, mlimit, nsnap, i
   logical :: subtr_tf
   complex, dimension(:), allocatable :: isf
   type(dumpfile), dimension(:), allocatable :: dumps
   type(particle), dimension(:), allocatable :: part
   integer*8, dimension(:), allocatable :: times

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 7) then
      write(*, '(a)') 'isf [path] [tstart] [tdur] [tinc] [qtarg] [mlimit] [subtr_yn]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) tstart
   call getarg(3, buf); read(buf, *) tdur
   call getarg(4, buf); read(buf, *) tinc
   call getarg(5, buf); read(buf, *) qtarg
   call getarg(6, buf); read(buf, *) mlimit
   call getarg(7, buf); read(buf, *) subtr_yn
   if (trim(adjustl(subtr_yn)) .eq. 'yes') then
      subtr_tf = .true.
   else if (trim(adjustl(subtr_yn)) .eq. 'no') then
      subtr_tf = .false.
   else
      write(*, '(a)') '[subtr_yn] must be yes or no'
   end if
   if (mod(tdur, tinc) .ne. 0) then
      write(*, '(a)') 'Bad tdur-tinc pair'
      stop
   end if

   ! Get the run info.

   truncpath = path
   call parse(truncpath, '/', args, nargs)
   write(truncpath, *) args(nargs)
   call read_desc_files(trim(adjustl(path)), dumps)

   ! Get an appropriate q^2 based on the inputs (may be changed slightly later).

   msqtarg = nint((qtarg * (box%hi(1)-box%lo(1)) / (2*pi))**2)

   ! Create schedule of time steps for the ISF.

   nsnap = tdur / tinc + 1
   allocate(times(nsnap))
   allocate(isf(nsnap))
   do i = 1, nsnap
      times(i) = tstart + (i-1) * tinc
   end do

   ! Open HDF5, read the modes from triples.txt,
   ! return the actual q used, calculate the ISF,
   ! and close HDF5.

   call open_hdf5()
   call read_modes('one', msqtarg, msqact, mlimit)
   qact = sqrt(real(msqact)) * 2 * pi / (box%hi(1)-box%lo(1))
   call get_isf('self', dumps, times, subtr_tf, isf)
   call close_hdf5()

   ! Write the custom output.

   write(*, '(a,f14.6)') 'q*sigma =', qact
   write(*, '(a,i10)') 'Wait time = ', tstart
   write(*, '(a)') 'Offset timesteps, isf_real, isf_imag'
   do i = 1, nsnap
      write(*, '(i10,2f10.6)') times(i)-times(1), real(isf(i)), aimag(isf(i))
   end do

end program main
