! has_nc.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 28, 2012
! PURPOSE:         List all particles with given contact number.
! INPUT VARIABLES:
!    path  = path to directory containing paths.txt and box.txt
!    ttarg = time step to determine contact numbers
!    rsep  = separation between particle surfaces to consider contacting
!    nc    = desired contact number

program main

   use realspace
   implicit none

   character(len=300) :: path, buf, nc_buf, time_buf
   integer*8 :: ttarg
   real :: rsep
   integer :: nc

   type(dumpfile), dimension(:), allocatable :: dumps
   type(particle), dimension(:), allocatable :: part
   integer, dimension(:), allocatable :: nneigh

   character(len=50), dimension(20) :: args
   integer :: nargs, i

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 4) then
      write(*, '(A)') 'has_nc [path] [ttarg] [rsep] [nc]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) ttarg
   call getarg(3, buf); read(buf, *) rsep
   call getarg(4, buf); read(buf, *) nc

   ! Get the configuration.

   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()
   call read_from_series(dumps, ttarg, part)

   ! Get the contact numbers for each particle.

   allocate(nneigh(box%npart))
   call get_nneigh_cell(part, rsep, nneigh)
   call close_hdf5()

   ! Go through Nc list, getting all with given Nc, and write to file.

   write(time_buf, *) ttarg
   write(nc_buf, *) nc
   write(buf, *) trim(time_buf)//'.nc'//trim(adjustl(nc_buf))
   open(8, file=trim(buf), action='write')
   do i = 1, box%npart
      if (nneigh(i) .eq. nc) then
         write(buf, *) i
         write(8, '(A)') trim(adjustl(buf))
      end if
   end do
   close(8)

end program main
