! not_really_free_volume.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    October 9, 2013
! PURPOSE:         Not-really-free-volume based pressure, binned by Nc.
!                  Get the volume that is not occupied by particles with other Nc
! INPUT VARIABLES:
!    path     = path to directory containing paths.txt and box.txt
!    ttarg    = time step to measure volume fraction
!    tdur     = duration of calculation
!    tinc     = timesteps between measurements
!    rsep     = cutoff
!    incl_kin = 'yes' to include ideal gas contribution, 'no' to leave out

! output average, 

program main

  use mechanics
  use realspace
  implicit none

  character(len=300) :: buf, incl_kin
  character(len=300) :: path
  character(len=30), dimension(30) :: args
  integer*8 :: tstart, tdur, tinc
  integer*8, dimension(:), allocatable :: times
  real :: rsep
  integer :: nargs, i, j, nsnap
  integer, parameter :: nbin = 17
  integer, dimension(:), allocatable :: nneigh
  real, dimension(:), allocatable :: occupied_vol_nc, single_part_vol, free_vol_nc, virial, press_nc, press_vol_nc
  real, dimension(:), allocatable :: kin_press_vol_nc, virial_nc, frac_nc
  type(dumpfile), dimension(:), allocatable :: dumps
  type(particle), dimension(:), allocatable :: part
  real :: box_vol, diam, vol, part_vol_sum, tot_press
  logical :: yes_kin

  ! Get input arguments.

  nargs = iargc()
  if (nargs .ne. 6) then
     write(*, '(a)') 'not_really_free_volume [path] [tstart] [tdur] [tinc] [rsep] [incl_kin]'
     stop
  end if
  call getarg(1, buf); path = trim(adjustl(buf))
  call getarg(2, buf); read(buf, *) tstart
  call getarg(3, buf); read(buf, *) tdur
  call getarg(4, buf); read(buf, *) tinc
  call getarg(5, buf); read(buf, *) rsep
  call getarg(6, buf); read(buf, *) incl_kin
  if (trim(adjustl(incl_kin)) .eq. 'yes') then
     yes_kin = .true.
  else if (trim(adjustl(incl_kin)) .eq. 'no') then
     yes_kin = .false.
  else
     write (*, '(a)') '[incl_kin] must be yes or no'
  end if

  ! Check if the time duration and increment make sense.

  if (mod(tdur, tinc) .ne. 0) then
     write(*, '(a)') 'Bad tdur-tinc pair'
     stop
  end if

  ! Create schedule of time steps to visit for measurement.

  nsnap = tdur / tinc + 1
  allocate(times(nsnap))
  do i = 1, nsnap
     times(i) = tstart + (i-1) * tinc
  end do

  ! Get the run info.

  call read_desc_files(trim(adjustl(path)), dumps)
  box_vol = (box%hi(1)-box%lo(1)) * &
            (box%hi(2)-box%lo(2)) * &
            (box%hi(3)-box%lo(3))

  ! Allocate arrays.

  allocate(virial(box%npart))
  allocate(single_part_vol(box%ntype))
  allocate(occupied_vol_nc(nbin))
  allocate(free_vol_nc(nbin))
  allocate(press_vol_nc(nbin))
  allocate(press_nc(nbin))
  allocate(kin_press_vol_nc(nbin))
  allocate(nneigh(box%npart))
  allocate(virial_nc(nbin))
  allocate(frac_nc(nbin))

  ! Volume of one particle.

  do i = 1, box%ntype
     diam = box%sizes(i)
     single_part_vol(i) = pi / 6.0 * diam*diam*diam
  end do

  call open_hdf5()
  do i = 1, size(times)
     call read_from_series(dumps, times(i), part)
     call get_nneigh_cell(part, rsep, nneigh)

     ! Get the fraction of each contact number in the total particle system.

     frac_nc = 0.0
     do j = 1, box%npart
        frac_nc(nneigh(j)+1) = frac_nc(nneigh(j)+1) + 1
     end do
     frac_nc = frac_nc / size(part)

     ! Accumulate particle volumes, depending on particle size.

     occupied_vol_nc = 0.0
     do j = 1, box%npart
        occupied_vol_nc(nneigh(j)+1) = occupied_vol_nc(nneigh(j)+1) + &
             single_part_vol(box%types(j))
     end do

     ! Get the sum of the particle volumes.

     part_vol_sum = 0.0
     do j = 1, nbin
        part_vol_sum = part_vol_sum + occupied_vol_nc(j)
     end do

     ! Get the "free volume" for each contact number.

     free_vol_nc = box_vol - part_vol_sum + occupied_vol_nc

     ! Accumulate the total (kinetic+virial) pressure-energy for each particle.
     ! Note: assuming mass of 1!

     kin_press_vol_nc = 0.0
     virial_nc = 0.0
     call get_part_virial(part, virial)
     do j = 1, box%npart
        kin_press_vol_nc(nneigh(j)+1) = kin_press_vol_nc(nneigh(j)+1) + &
             (part(j)%vel(1)**2 + part(j)%vel(2)**2 + part(j)%vel(3)**2)
        virial_nc(nneigh(j)+1) = virial_nc(nneigh(j)+1) + virial(j)
     end do
     kin_press_vol_nc = kin_press_vol_nc / 3.0
     virial_nc = virial_nc / 3.0
     if (yes_kin) then
        press_vol_nc = kin_press_vol_nc + virial_nc
     else
        press_vol_nc = virial_nc
     end if

     ! Divide by the "free volume" to get the total pressure.

     press_nc = press_vol_nc / free_vol_nc
     
     ! Do the same for the total pressure, but using the box volume.

     tot_press = 0.0
     do j = 1, nbin
        tot_press = tot_press + kin_press_vol_nc(j) + virial_nc(j)
     end do
     tot_press = tot_press / box_vol

     ! Output to screen.

     do j = 1, nbin
        write(*, '(f14.6)', advance='no') press_nc(j)
     end do
     write(*, '(f14.6)') tot_press

  end do

  call close_hdf5()

end program main
