! press_nc.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    December 11, 2013
! PURPOSE:         Output press*volume by contact number

! INPUT VARIABLES:
!    path     = path to directory containing paths.txt and box.txt
!    ttarg    = time step to start measurement
!    tdur     = duration of calculation
!    tinc     = timesteps between measurements
!    rsep     = cutoff
!    incl_kin = 'yes' to include ideal gas contribution, 'no' to leave out
!    div_vol  = 'yes' to output terms you would add to get the total pressure

program main

  use mechanics
  use realspace
  implicit none

  character(len=300) :: buf
  character(len=300) :: path
  character(len=30), dimension(30) :: args
  integer*8 :: tstart, tdur, tinc
  integer*8, dimension(:), allocatable :: times
  real :: rsep
  integer :: nargs, i, j, nsnap, bin
  integer, parameter :: nbin = 17
  integer, dimension(:), allocatable :: nneigh
  integer*8, dimension(:), allocatable :: count_nc
  real, dimension(:), allocatable :: virial, press_nc, press_nc_step
  type(dumpfile), dimension(:), allocatable :: dumps
  type(particle), dimension(:), allocatable :: part
  real :: box_vol
  logical :: incl_kin, div_vol

  ! Get input arguments.

  nargs = iargc()
  if (nargs .ne. 7) then
     write(*, '(a)') &
          'press_nc [path] [tstart] [tdur] [tinc] [rsep] [incl_kin] [div_vol]'
     stop
  end if
  call getarg(1, buf); path = trim(adjustl(buf))
  call getarg(2, buf); read(buf, *) tstart
  call getarg(3, buf); read(buf, *) tdur
  call getarg(4, buf); read(buf, *) tinc
  call getarg(5, buf); read(buf, *) rsep
  call getarg(6, buf)
  if (trim(adjustl(buf)) .eq. 'yes') then
     incl_kin = .true.
  else if (trim(adjustl(buf)) .eq. 'no') then
     incl_kin = .false.
  else
     write (*, '(a)') '[incl_kin] must be yes or no'
     stop
  end if
  call getarg(7, buf)
  if (trim(adjustl(buf)) .eq. 'yes') then
     div_vol = .true.
  else if (trim(adjustl(buf)) .eq. 'no') then
     div_vol = .false.
  else
     write (*, '(a)') '[div_vol] must be yes or no'
     stop
  end if

  ! Check if the time duration and increment make sense.

  if (mod(tdur, tinc) .ne. 0) then
     write(*, '(a)') 'Bad tdur-tinc pair'
     stop
  end if

  ! Create schedule of time steps to visit for measurement.

  nsnap = tdur / tinc + 1
  allocate(times(nsnap))
  do i = 1, nsnap
     times(i) = tstart + (i-1) * tinc
  end do

  ! Get the run info.

  call read_desc_files(trim(adjustl(path)), dumps)
  box_vol = (box%hi(1)-box%lo(1)) * &
            (box%hi(2)-box%lo(2)) * &
            (box%hi(3)-box%lo(3))
  call open_hdf5()

  ! Allocate arrays.

  allocate(virial(box%npart))
  allocate(press_nc(nbin))
  allocate(press_nc_step(nbin))
  allocate(nneigh(box%npart))
  allocate(count_nc(nbin))

  count_nc = 0
  press_nc = 0.0
  do i = 1, size(times)
     call read_from_series(dumps, times(i), part)
     call get_nneigh_cell(part, rsep, nneigh)

     ! Accumulate the particles counts for each contact number

     do j = 1, box%npart
        bin = nneigh(j) + 1
        if (bin .le. nbin) then
           count_nc(bin) = count_nc(bin) + 1
        end if
     end do

     ! Get pair (and kinetic) pressure terms for each particle
     ! Accumulate in order to get an average for each Nc
     ! When done loop, divide by 3
     ! IMPORTANT: assuming particle mass of 1!

     call get_part_virial(part, virial)
     press_nc_step = 0.0
     do j = 1, box%npart
        bin = nneigh(j) + 1
        press_nc_step(bin) = press_nc_step(bin) &
             + virial(j)
     end do
     if (incl_kin) then
        do j = 1, box%npart
           bin = nneigh(j) + 1
           press_nc_step(bin) = press_nc_step(bin) &
                + part(j)%vel(1)**2 &
                + part(j)%vel(2)**2 &
                + part(j)%vel(3)**2
        end do
     end if

     ! Add timestep's values to total.

     press_nc = press_nc + press_nc_step

  end do

  call close_hdf5()

  ! Divide by 3, and then divide by either
  !  the total number of particles counted as that Nc (!div_vol) or
  !  the number of snapshots (to get the terms you sum to get total pressure)

  press_nc = press_nc / 3.0

  if (div_vol) then
     press_nc = press_nc / box_vol / size(times)
  else
     do i = 1, nbin
        if (count_nc(i) .ne. 0) then
           press_nc(i) = press_nc(i) / count_nc(i)
        else
           press_nc(i) = 0.0
        end if
     end do
  end if

  ! Output to screen.

  do i = 1, nbin
     write(*, '(g14.6)', advance='no') press_nc(i)
  end do
  write(*, '(a)') ''

end program main
