! nc_dist.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    February 26, 2014
! PURPOSE:         Get (time-averaged) contact number distributions
! INPUT VARIABLES:
!    path   = path to directory containing paths.txt and box.txt
!    tstart = first time step to determine contact numbers
!    tdur   = duration to average over
!    tinc   = spacing between snapshots
!    rsep   = separation between particle surfaces to consider contacting

program main

  use hdf5
  use realspace
  implicit none

  integer, parameter :: nbin = 17

  character(len=300) :: path, buf, h5out
  integer*8 :: tstart, tdur, tinc
  real :: rsep
  integer :: nc, nsnap, nargs, i, j, bin

  integer*8, dimension(:), allocatable :: times
  type(dumpfile), dimension(:), allocatable :: dumps
  type(particle), dimension(:), allocatable :: part
  integer, dimension(:), allocatable :: hist, nneigh
  real, dimension(:), allocatable :: distribution, stderr
  character(len=50), dimension(20) :: args

   ! Get input arguments.

  nargs = iargc()
  if (nargs .ne. 5) then
     write(*, '(A)') 'nc_dist [path] [tstart] [tdur] [tinc] [rsep]'
     stop
  end if
  call getarg(1, buf); path = trim(adjustl(buf))
  call getarg(2, buf); read(buf, *) tstart
  call getarg(3, buf); read(buf, *) tdur
  call getarg(4, buf); read(buf, *) tinc
  call getarg(5, buf); read(buf, *) rsep

  call open_hdf5()

  ! Create schedule of time steps.

  nsnap = tdur / tinc + 1
  allocate(times(nsnap))
  do i = 1, nsnap
     times(i) = tstart + (i-1) * tinc
  end do

  ! Get the particle configuration at given time step.

  call read_desc_files(trim(adjustl(path)), dumps)
  allocate(nneigh(box%npart))
  allocate(hist(nbin))
  allocate(distribution(nbin))
  allocate(stderr(nbin))

  ! Accumulate contact number counts

  distribution = 0.0
  do i = 1, nsnap
     call read_from_series(dumps, times(i), part)
     call get_nneigh_cell(part, rsep, nneigh)
     hist = 0
     do j = 1, box%npart
        bin = nneigh(j) + 1
        hist(bin) = hist(bin) + 1
     end do
     distribution = distribution + hist
  end do

  ! Divide by number of snaps now (divide by number of particles later)
  ! This is avg. number of particles for each contact number

  distribution = distribution / nsnap

  ! Get standard error if number of snaps greater than 1

  if (nsnap .gt. 1) then
     stderr = 0.0
     do i = 1, nsnap
        call read_from_series(dumps, times(i), part)
        call get_nneigh_cell(part, rsep, nneigh)
        hist = 0
        do j = 1, box%npart
           bin = nneigh(j) + 1
           hist(bin) = hist(bin) + 1
        end do
        stderr = stderr + (real(hist)-distribution)*(real(hist)-distribution)
     end do
     stderr = stderr / (nsnap-1) / nsnap
     stderr = sqrt(stderr)
  end if

  ! Divide by number of particles

  distribution = distribution / box%npart
  stderr = stderr / box%npart

  ! Output to screen

  do i = 1, nbin
     write(*, '(I4,G14.6)', advance='no') i-1, distribution(i)
     if (nsnap .gt. 1) then
        write(*, '(G14.6)', advance='no') stderr(i)
     end if
     write(*, '')
  end do

  call close_hdf5()

end program main
