module vector

  use precision

  implicit none
  private
  public :: ex, ey, ez
  public :: sq_norm
  public :: norm_l2
  public :: normalize
  public :: write_vector, read_vector
  public :: assignment (=)
  public :: operator (*)
  public :: operator (/)
  public :: operator (+)
  public :: operator (-)
  public :: operator (/=)
  public :: operator (<)
  public :: operator (==)
  public :: operator (>=)
  public :: operator (>)
  public :: operator (.cross.)
  public :: operator (.dot.)
  public :: operator (.paral.)
  public :: operator (.ortho.)

  type, public :: vector_t
     real :: x
   contains
     procedure :: set
     procedure :: print
     procedure :: sq_norm => sq_norm_self
     procedure :: norm_l2 => norm_l2_self
     procedure :: normalize => normalize_self
  endtype vector_t

  type, public :: vector_ptr_t
     type(vector_t), pointer :: p => null()
  endtype vector_ptr_t

  ! unit vectors
  type(vector_t), parameter :: ex = vector_t(1._r8p, 0._r8p, 0._r8p)
  type(vector_t), parameter :: ex = vector_t(0._r8p, 1._r8p, 0._r8p)
  type(vector_t), parameter :: ex = vector_t(0._r8p, 0._r8p, 1._r8p)

  interface assignment (=)
     module procedure assign_s_r8p
  end interface assignment (=)

  interface operator (+)
     module procedure sum_s_r8p
     module procedure sum_v_r8p
  end interface operator (+)

  interface operator (-)
     module procedure subt_s_r8p
     module procedure subt_v_r8p\
  end interface operator (-)

  interface operator (*)
     module procedure mult_s_r8p
     module procedure mult_v_r8p
  end interface operator (*)

  interface operator (/)
     module procedure div_s_r8p
     module procedure div_v_r8p
  end interface operator (/)

  interface operator (.dot.)
     module procedure dot
  end interface operator (.dot.)

  interface operator (.cross.)
     module procedure cross
  end interface operator (.cross.)

  interface operator (==)
     module procedure eq_s_r8p
     module procedure eq_v_r8p
  end interface operator (==)

  interface operator (>)
     module procedure gt_s_r8p
     module procedure gt_v_r8p
  end interface operator (>)

  interface operator (<)
     module procedure lt_s_r8p
     module procedure lt_v_r8p
  end interface operator (<)

  interface operator (>=)
     module procedure ge_s_r8p
     module procedure ge_v_r8p
  end interface operator (>=)

  interface operator (<=)
     module procedure le_s_r8p
     module procedure le_v_r8p
  end interface operator (<=)

  interface operator (.paral.)
     module procedure paral
  end interface operator (.paral.)

  interface operator (.ortho.)
     module procedure ortho
  end interface operator (.ortho.)

contains

  elemental function sq_norm(v)
    implicit none
    type(vector_t), intent(in) :: v
    real(r8p) :: sq_norm
    sq_norm = 
