! smooth_dist_bin_press.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 28, 2012
! PURPOSE:         Time-average the distribution of the single-particle pressure, and resolve by contact number.
! INPUT VARIABLES:
!    path      = path to directory containing paths.txt and box.txt
!    ttarg     = time step to start measurements
!    tavg      = time interval, in time steps, to average over
!    tinc      = time steps between measurements
!    rcut      = separation between particle surfaces to consider contacting
!    plo       = low limit of pressures for the histogram
!    phi       = high limit of pressures for the histogram
!    npressbin = number of bins for the histogram

program main

   use stats
   use mechanics
   use realspace
   implicit none

   character(len=300) :: path
   integer*8 :: ttarg, tavg, tinc
   real :: rsep, plo, phi
   integer :: npressbin

   integer :: nsnap
   integer*8, dimension(:), allocatable :: times
   type(dumpfile), dimension(:), allocatable :: dumps
   type(particle), dimension(:), allocatable :: part
   integer, dimension(:), allocatable :: nneigh, pop
   integer, dimension(:, :), allocatable :: pop_xfer
   real, dimension(:), allocatable :: press, press_bins, press_avg_all
   real, dimension(:), allocatable :: freq
   real, dimension(:,:), allocatable :: freq_nc
   real, dimension(:), allocatable :: freq_navg, freq_yavg
   real, dimension(:, :), allocatable :: freq_nc_bin_navg, freq_nc_bin_yavg
   integer, parameter :: nc_max = 16
   integer, parameter :: ncontbin = nc_max + 1

   character(len=300) :: buf
   character(len=50), dimension(20) :: args
   integer :: nargs, i, j, bin

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 8) then
      write(*, '(A)') 'bin_press [path] [ttarg] [tavg] [tinc] [rsep] [plo] [phi] [npressbin]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) ttarg
   call getarg(3, buf); read(buf, *) tavg
   call getarg(4, buf); read(buf, *) tinc
   call getarg(5, buf); read(buf, *) rsep
   call getarg(6, buf); read(buf, *) plo
   call getarg(7, buf); read(buf, *) phi
   call getarg(8, buf); read(buf, *) npressbin

   ! Input error checking.

   if (mod(tavg, tinc) .ne. 0) then
      write(*, '(A)') 'Bad tavg-tinc pair'
      stop
   end if

   ! Create schedule.

   nsnap = tavg / tinc + 1
   allocate(times(nsnap))
   do i = 1, nsnap
      times(i) = ttarg + (i-1) * tinc
   end do

   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()

   allocate(nneigh(box%npart))                     ! # neighbors for each atom
   allocate(pop(ncontbin))                         ! contact number pop size
   allocate(press(box%npart))                      ! pressure for each atom
   allocate(press_avg_all(box%npart))              ! time-avg press each atom
   allocate(press_bins(npressbin))                 ! pressure bin locations
   allocate(freq(npressbin))
   allocate(freq_nc(npressbin, ncontbin))
   allocate(freq_navg(npressbin))                  ! frequency for pressure
   allocate(freq_yavg(npressbin))                  ! time-avg freq for pressure
   allocate(freq_nc_bin_navg(npressbin, ncontbin)) ! nc-resolved freq for press
   allocate(freq_nc_bin_yavg(npressbin, ncontbin)) ! nc-res, time-avg freq

   ! Get pressure distribution for first timestep.

   call read_from_series(dumps, times(1), part)
   call get_part_pressure(part, press)
   call histo_bin(press, plo, phi, npressbin, press_bins, freq_navg)

   ! Bin the particles by contact number for the first timestep.
   ! Then get the contact number-resolved pressure distributions.

   call get_nneigh_cell(part, rsep, nneigh)
   call pop_histo_bin(press, nneigh, plo, phi, npressbin, ncontbin, 0, &
        press_bins, freq_nc_bin_navg)

   ! Get time-smoothed pressure distribution (time-avg of distribution).

   freq_yavg = 0; freq_nc_bin_yavg = 0
   do i = 1, nsnap
      print*, 'On snap ', i, ' of ', nsnap
      call read_from_series(dumps, times(i), part)
      call get_part_pressure(part, press)
      call histo_bin(press, plo, phi, npressbin, press_bins, freq)
      call get_nneigh_cell(part, rsep, nneigh)
      call pop_histo_bin(press, nneigh, plo, phi, npressbin, ncontbin, &
           0, press_bins, freq_nc)

      ! Extract "instantaneous" data only from first timestep in series.

      if (i.eq.1) then
         freq_navg = freq
         freq_nc_bin_navg = freq_nc

         ! Get instantaneous contact number distribution to output later.

         pop = 0
         do j = 1, box%npart
            bin = nneigh(j) + 1
            if ((bin .ge. 1) .and. (bin .le. ncontbin)) then
               pop(bin) = pop(bin) + 1
            end if
         end do

      end if

      ! Add distribution to running total.  Will normalize after loop.

      freq_yavg = freq_yavg + freq
      freq_nc_bin_yavg = freq_nc_bin_yavg + freq_nc

   end do

   ! Normalize the distributions.

   freq_yavg = freq_yavg / nsnap
   freq_nc_bin_yavg = freq_nc_bin_yavg / nsnap
   call close_hdf5()

   ! Write output: smoothed and inst pressure distribution for entire system.

   write(buf, *) ttarg
   open(8, file=trim(adjustl(buf))//'.press_all', action='write')
   write(8, '(A)') 'ITEM: TIMESTEP'
   write(8, *) trim(adjustl(buf))
   write(8, '(A)') 'ITEM: smoothed_over'
   write(buf, *) tavg
   write(8, *) trim(adjustl(buf))
   write(8, '(A)') 'ITEM: pressure p(press_inst) p(press_smooth)'
   do i = 1, npressbin
      write(8, '(3G14.6)') press_bins(i), freq_navg(i), freq_yavg(i)
   end do
   close(8)

   ! Write output: binned pressure distribution for a single timestep.

   write(buf, *) ttarg
   open(8, file=trim(adjustl(buf))//'.press_bins_inst', action='write')
   write(8, '(A)') 'ITEM: TIMESTEP'
   write(8, '(A)') trim(adjustl(buf))
   write(8, '(A)') 'ITEM: contact_numbers'
   write(8, '(A)') '0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16'
   do i = 1, npressbin
      write(8, '(17G14.6)') freq_nc_bin_navg(i, 1:17)
   end do
   close(8)

   ! Write output, time-averaged and binned pressure distribution.

   write(buf, *) ttarg
   open(8, file=trim(adjustl(buf))//'.press_bins_smooth', action='write')
   write(8, '(A)') 'ITEM: TIMESTEP'
   write(8, '(A)') trim(adjustl(buf))
   write(8, '(A)') 'ITEM: averaged over'
   write(buf, *) tavg
   write(8, *) trim(adjustl(buf))
   write(8, '(A)') 'ITEM: contact_numbers'
   write(8, '(A)') '0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16'
   do i = 1, npressbin
      write(8, '(17G14.6)') freq_nc_bin_yavg(i, 1:17)
   end do
   close(8)

   ! Write output: instantaneous contact number distribution.

   write(buf, *) ttarg
   open(8, file=trim(adjustl(buf))//'.nc_dist', action='write')
   write(8, '(A)') 'ITEM: TIMESTEP'
   write(8, '(A)') trim(adjustl(buf))
   write(8, '(A)') 'ITEM: Nc prob'
   do i = 1, ncontbin
      write(8, '(I4,G14.6)') i-1, real(pop(i))/box%npart
   end do
   close(8)

end program main
