! output.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    September 6, 2012
! PURPOSE:         Contains output methods for different "types" (functions, etc).
! CONTAINS:        subroutines: write_real_function_real, write_real_function_int8
! NOTES:           Not fully utilized.  Some of my executables do not use this to write output.

module output

   implicit none

contains

   subroutine write_real_function_real(x, y, fname)

      real, dimension(:), intent(in) :: x, y
      character(len=*), intent(in) :: fname
      integer :: i

      if (size(x) .ne. size(y)) then
         write(*, '(A)') 'Vectors to be written not same size'
         stop
      end if

      open(8, file=trim(adjustl(fname)), action='write')
      do i = 1, size(x)
         write(8, '(2G14.6)') x(i), y(i)
      end do
      close(8)

   end subroutine write_real_function_real

   subroutine write_real_function_int8(x, y, fname)

      integer*8, dimension(:), intent(in) :: x
      real, dimension(:), intent(in) :: y
      character(len=*), intent(in) :: fname
      integer :: i

      if (size(x) .ne. size(y)) then
         write(*, '(A)') 'Vectors to be written not same size'
         stop
      end if

      open(8, file=trim(adjustl(fname)), action='write')
      do i = 1, size(x)
         write(8, '(I14,G14.6)') x(i), y(i)
      end do
      close(8)

   end subroutine write_real_function_int8


end module output
