! dump.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 14, 2013
! PURPOSE:         Contains everything needed to read HDF5 data files and defines data structures for the particles used in all of the post-processing.
! CONTAINS:        subroutines: close_hdf5, open_hdf5, read_desc_files, read_from_series
!                  types: particle, mdbox, dumpfile
!                  globals: pi, box

module dump

   use strings
   use hdf5
   implicit none

   type particle ! store particle data for post-processing
      real, dimension(3)    :: pos, vel
      real, dimension(1)    :: pe
      real, dimension(6)    :: stress
      integer*8             :: image
   end type particle

   type mdbox ! molecular dynamics box, type for a global box variable

      logical, dimension(3) :: per    ! .true. if periodic in x, y, z
      integer               :: npart
      integer               :: nsnap
      integer               :: ntype
      integer, dimension(:), allocatable :: types ! list of types
      real, dimension(:), allocatable :: sizes ! list of diameters by type

      ! Below has same terminology as in LAMMPS domain.h

      real, dimension(3) :: prd         ! array form of dimensions
      real, dimension(3) :: prd_lamda   ! lamda box = (1,1,1)
      real, dimension(3) :: boxlo,boxhi ! orthogonal box global bounds
      real, dimension(3) :: boxlo_lamda,boxhi_lamda ! lamda box = (0,1)
      real, dimension(3) :: boxlo_bound,boxhi_bound ! bounding box of
                                                    ! tilted domain
      real, dimension(6) :: h,h_inv     ! shape matrix in Voigt notation

      logical :: triclinic       ! false if orthog box, true if triclinic
      logical :: change_size     ! true if box shape changes, false if not
      logical :: change_shape    ! true if box shape changes, false if not
      real    :: xy,xz,yz        ! 3 tilt factors
      real    :: xprd,yprd,xprd  ! global box dimensions

   end type mdbox

   type dumpfile ! used to catalog dump files
      character(len=235)             :: path
      integer*8                      :: tstart, tend, tinc
   end type dumpfile

   ! global variables
   real, parameter :: pi = 3.1415927
   type(mdbox)     :: box

!**********************************************************************
contains
!**********************************************************************

!   subroutine get_schedule(tstart, tdur, tinc, times)

!      integer*8, intent(in) :: tstart, tdur, tinc
!      integer*8, dimension(:), allocatable :: times
!      integer :: nsnap, i

!      nsnap = tdur / tinc + 1
!      allocate(times(nsnap))
!      do i = 1, nsnap
!         times(i) = tstart + (i-1) * tinc
!      end do

!   end subroutine get_schedule

!**********************************************************************

   subroutine read_desc_files(directory, dumps)
   ! reads in run parameters from files in a folder
   ! the files needed are ...
   !   paths.txt - list of HDF5 file locations, timesteps
   !     box.txt - list of the mdbox parameters and particle data

      character(len=*), intent(in)                           :: directory
      type(dumpfile), dimension(:), allocatable, intent(out) :: dumps
      integer :: i, nargs
      character(len=2000000) :: buf
      character(len=300), dimension(5) :: args

      box%nsnap = 0

      ! read in the paths.txt data
      open(7, file=trim(adjustl(directory))//'paths.txt')
      read(7, *) ! ITEM: path tstart tend tinc
      i = 0
      do
         read(7, '(A)', end=100) buf
         i = i + 1
      end do
100   close(7)
      allocate(dumps(i))
      open(7, file=trim(adjustl(directory))//'paths.txt')
      read(7, *) ! ITEM: path tstart tend tinc
      do i = 1, size(dumps)
         read(7, '(A)') buf
         call parse(buf, ' ', args, nargs)
         write(dumps(i)%path, *) trim(adjustl(directory))//trim(adjustl(args(1)))
         read(args(2), *) dumps(i)%tstart
         read(args(3), *) dumps(i)%tend
         read(args(4), *) dumps(i)%tinc
         box%nsnap = box%nsnap + &
              (dumps(i)%tend - dumps(i)%tstart) / dumps(i)%tinc + 1
      end do
      close(7)

      print *, 'box nsnap is ', box%nsnap

      ! read in the particle.txt data

      print *, 'reading particle.txt'
      open(7, file=trim(adjustl(directory))//'particle.txt')
      read(7, *) ! ITEM: NUMBER OF PARTICLES
      read(7, *) box%npart
      read(7, *) ! ITEM: NUMBER OF TYPES
      read(7, *) box%ntype
      allocate(box%sizes(box%ntype))
      read(7, *) ! ITEM: SIZES IN TYPE ORDER
      do i = 1, box%ntype
         read(7, *) box%sizes(i)
      end do
      allocate(box%types(box%npart))
      read(7, *) ! ITEM: LIST OF TYPES IN LAMMPS ATOM ORDER
      do i = 1, box%npart
         read(7, '(I10)', end=200) box%types(i)
      end do
200   close(7)
      
      ! read in the box.txt data

      print *, 'reading box.txt'
      open(7, file=trim(adjustl(directory))//'box.txt')
      read(7, *) ! ITEM: NUMBER OF PARTICLES
      read(7, *) box%npart
      read(7, *) ! ITEM: BOX BOUNDS FOR EACH SNAPSHOT
      do i = 1, box%nsnap
         



      read(7, *) box%lo(1), box%hi(1)
      read(7, *) box%lo(2), box%hi(2)
      read(7, *) box%lo(3), box%hi(3)

    end subroutine read_desc_files

!**********************************************************************

   subroutine open_hdf5()

      integer :: hdferr

      call h5open_f(hdferr)

   end subroutine open_hdf5

!**********************************************************************

   subroutine close_hdf5()

      integer :: hdferr

      call h5close_f(hdferr)

   end subroutine close_hdf5

!**********************************************************************

   subroutine read_from_series(dumps, ttarg, part)
   ! read data from target timestep that exists in a dump file series
   ! in this version, files assumed to be .h5 files
   ! contents assumed to have "pos", "vel", "pe", and "virialstress" arrays
   ! these arrays are (in Fortran indices) ele*npart*nsnap,
   ! where ele = 3 for position and velocity, 1 for potential energy, and
   !   6 for virial stress

      type(dumpfile), dimension(:), intent(in)               :: dumps
      integer*8, intent(in)                                  :: ttarg
      type(particle), dimension(:), allocatable, intent(out) :: part
      real, dimension(:,:,:), allocatable :: buf_pos, buf_vel, buf_pe, buf_sts
      integer :: i, dump, tint, npart
      integer(HSIZE_T), dimension(3) :: start
      integer*8 :: tdiff
      integer :: hdferr
      integer(HID_T) :: d_pos, d_vel, d_pe, d_sts
      integer(HID_T) :: s_pos, s_vel, s_pe, s_sts
      integer(HID_T) :: s_m_pos, s_m_vel, s_m_pe, s_m_sts
      integer(HID_T) :: file_id
      integer(HSIZE_T), dimension(3) :: ct_pos, ct_vel, ct_pe, ct_sts

      integer(HSIZE_T) :: diag_fsize, diag_dsize
      logical :: diag_flag

      do i = 1, size(dumps)
         if ((dumps(i)%tstart.le.ttarg) .and. (dumps(i)%tend.ge.ttarg)) then
            dump = i
            exit
         end if
      end do

      allocate(buf_pos(3, box%npart, 1))
      allocate(buf_vel(3, box%npart, 1))
      allocate(buf_pe (1, box%npart, 1))
      allocate(buf_sts(6, box%npart, 1))
      allocate(part(box%npart))

      ! raise error if we can't find an appropriate file
      if (dump == -1) then
         write(*, '(A)') 'Error: could not find file in series for timestep'
         stop
      end if

      call h5fopen_f(trim(adjustl(dumps(dump)%path)), H5F_ACC_RDONLY_F, &
           file_id, hdferr)

      ! get dataset handles
      call h5dopen_f(file_id, 'pos'         , d_pos, hdferr)
      call h5dopen_f(file_id, 'vel'         , d_vel, hdferr)
      call h5dopen_f(file_id, 'pe'          , d_pe , hdferr)
      call h5dopen_f(file_id, 'virialstress', d_sts, hdferr)

      ! get file dataspace handles
      call h5dget_space_f(d_pos, s_pos, hdferr)
      call h5dget_space_f(d_vel, s_vel, hdferr)
      call h5dget_space_f(d_pe , s_pe , hdferr)
      call h5dget_space_f(d_sts, s_sts, hdferr)

      ! get starting position for selection
      tdiff = ttarg - dumps(dump)%tstart
      if (mod(tdiff, dumps(dump)%tinc) .ne. 0) then
         write(*, '(A)') 'Bad timestep target'
         stop
      end if
      tint = tdiff / dumps(dump)%tinc ! don't add 1: index for H5 starts at 0
      start = (/ 0, 0, tint /)

      ! set count for hyperslab selection
      ct_pos = (/ 3, box%npart, 1 /)
      ct_vel = (/ 3, box%npart, 1 /)
      ct_pe  = (/ 1, box%npart, 1 /)
      ct_sts = (/ 6, box%npart, 1 /)

      ! get dataspace handle for memory
      call h5screate_simple_f(3, ct_pos, s_m_pos, hdferr)
      call h5screate_simple_f(3, ct_vel, s_m_vel, hdferr)
      call h5screate_simple_f(3, ct_pe , s_m_pe , hdferr)
      call h5screate_simple_f(3, ct_sts, s_m_sts, hdferr)

      ! select by hyperslabs
      call h5sselect_hyperslab_f(s_pos, H5S_SELECT_SET_F, &
           start, ct_pos, hdferr)
      call h5sselect_hyperslab_f(s_vel, H5S_SELECT_SET_F, &
           start, ct_vel, hdferr)
      call h5sselect_hyperslab_f(s_pe , H5S_SELECT_SET_F, &
           start, ct_pe , hdferr)
      call h5sselect_hyperslab_f(s_sts, H5S_SELECT_SET_F, &
           start, ct_sts, hdferr)

      ! read file data to buffers (most expensive part of subroutine by far)
      call h5dread_f(d_pos, H5T_NATIVE_REAL, buf_pos, ct_pos, hdferr, &
           mem_space_id=s_m_pos, file_space_id=s_pos)
      call h5dread_f(d_vel, H5T_NATIVE_REAL, buf_vel, ct_vel, hdferr, &
           mem_space_id=s_m_vel, file_space_id=s_vel)
      call h5dread_f(d_pe , H5T_NATIVE_REAL, buf_pe , ct_pe , hdferr, &
           mem_space_id=s_m_pe , file_space_id=s_pe )
      call h5dread_f(d_sts, H5T_NATIVE_REAL, buf_sts, ct_sts, hdferr, &
           mem_space_id=s_m_sts, file_space_id=s_sts)

      ! read buffers to particle structure
      do i = 1, box%npart
         part(i)%pos(:) = buf_pos(:, i, 1)
      end do
      do i = 1, box%npart
         part(i)%vel(:) = buf_vel(:, i, 1)
      end do
      do i = 1, box%npart
         part(i)%pe (:) = buf_pe (:, i, 1)
      end do
      do i = 1, box%npart
         part(i)%stress(:) = buf_sts(:, i, 1)
      end do

      ! free resources
      call h5sclose_f(s_pos, hdferr)
      call h5sclose_f(s_vel, hdferr)
      call h5sclose_f(s_pe , hdferr)
      call h5sclose_f(s_sts, hdferr)
      call h5sclose_f(s_m_pos, hdferr)
      call h5sclose_f(s_m_vel, hdferr)
      call h5sclose_f(s_m_pe , hdferr)
      call h5sclose_f(s_m_sts, hdferr)
      call h5dclose_f(d_pos, hdferr)
      call h5dclose_f(d_vel, hdferr)
      call h5dclose_f(d_pe , hdferr)
      call h5dclose_f(d_sts, hdferr)
      call h5fclose_f(file_id, hdferr)

    end subroutine read_from_series

!**********************************************************************

end module dump
