! ssf.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    August 28, 2012
! PURPOSE:         Get the static structure factor.
! INPUT VARIABLES:
!    path   = path to directory containing paths.txt and box.txt
!    ttarg  = time step to get SSF
!    qmax   = maximum wavenumber for SSF
!    mlimit = maximum number of independent wavenumbers per magnitude
!    nbin   = number of bins to use for SSF

program main
! Get the SSF for each wavevector used.  Also output when the SSF is a binned function.
! (Average over small groups of wavenumbers.)

   use fourier
   use output
   implicit none

   character(len=300) :: buf
   character(len=300) :: path, truncpath
   character(len=30), dimension(30) :: args
   integer*8 :: ttarg
   real :: qmax
   integer :: mlimit, nbin, nargs, msqmax, msqact
   real, dimension(:), allocatable :: ssf, qvec, ssf_each, qvec_each
   type(dumpfile), dimension(:), allocatable :: dumps
   type(particle), dimension(:), allocatable :: part

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 5) then
      write(*, '(A)') 'ssf [path] [ttarg] [qmax] [mlimit] [nbin]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) ttarg
   call getarg(3, buf); read(buf, *) qmax
   call getarg(4, buf); read(buf, *) mlimit
   call getarg(5, buf); read(buf, *) nbin

   ! Get run info and read configuration.

   truncpath = path
   call parse(truncpath, '/', args, nargs)
   write(truncpath, *) args(nargs)
   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()
   call read_from_series(dumps, ttarg, part)
   call close_hdf5()

   ! Get an appropriate msqmax: larger than needed for SSF but not too large.

   msqmax = int((qmax * (box%hi(1)-box%lo(1)) / (2*pi))**2 + 1)

   ! Read modes up to calculated msqmax.

   call read_modes('max', msqmax, msqact, mlimit)

   ! Allocate arrays.

   allocate(ssf_each(size(modes)))
   allocate(qvec_each(size(modes)))
   allocate(ssf(nbin))
   allocate(qvec(nbin))

   ! Get raw SSF, and radially average.

   call get_ssf_each(part, qmax, qvec_each, ssf_each)
   call bin_avg_real(qvec_each, ssf_each, 0.0, qmax, nbin, qvec, ssf)

   ! Write the radially averaged SSF.

   write(buf, *) ttarg
   call write_real_function_real(qvec, ssf, trim(adjustl(buf))//'.ssf')

end program main
