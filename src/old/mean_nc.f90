! mean_nc.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    March 4, 2014
! PURPOSE:         Determine transfers (flow) between Nc populations.
! INPUT VARIABLES:
!    path   = path to directory containing paths.txt and box.txt
!    tstart = first time step for contact number determination
!    tdur   = timesteps to cover in total
!    tinc   = separation between measurments
!    rsep   = separation between particle surfaces to consider contacting

program main

   use realspace
   implicit none

   integer, parameter :: nbin = 17 ! number of bins
   integer :: i, j, k, bin

   integer, dimension(:), allocatable :: nneigh_1, nneigh_2 ! particle neigh #
   integer, dimension(:), allocatable :: pop, bin_start

   real, dimension(:,:), allocatable :: hist
   type(particle), dimension(:), allocatable :: part1, part2
   type(dumpfile), dimension(:), allocatable :: dumps

   integer :: nargs, nsnap
   character(len=300) :: buf, path
   character(len=50), dimension(99) :: args
   integer*8 :: tstart, tdur, tinc
   integer*8, dimension(:), allocatable :: times
   real :: rsep, distsq, val
   real, dimension(3) :: mean_origin, mean_disp

   ! Get input arguments.

   nargs = iargc()
   if (nargs .ne. 5) then
      write(*, '(A)') 'mean_nc [path] [tstart] [tdur] [tinc] [rsep]'
      stop
   end if
   call getarg(1, buf); path = trim(adjustl(buf))
   call getarg(2, buf); read(buf, *) tstart
   call getarg(3, buf); read(buf, *) tdur
   call getarg(4, buf); read(buf, *) tinc
   call getarg(5, buf); read(buf, *) rsep

   ! Check if the time duration and increment make sense.

   if (mod(tdur, tinc) .ne. 0) then
      write(*, '(A)') 'Bad tdur-tinc pair'
      stop
   end if

   ! Create schedule of time steps to visit for measurement.

   nsnap = tdur / tinc + 1
   allocate(times(nsnap))
   do i = 1, nsnap
      times(i) = tstart + (i-1) * tinc
   end do

   ! Get run info, and open HDF5.

   call read_desc_files(trim(adjustl(path)), dumps)
   call open_hdf5()

   ! Allocate arrays.

   allocate(nneigh_1(box%npart)); allocate(nneigh_2(box%npart))
   allocate(pop(nbin))
   allocate(hist(nbin,nbin))
   allocate(bin_start(box%npart))

   ! Read in the origin time, and get initial contact numbers.

   call read_from_series(dumps, tstart, part1)
   call get_nneigh_cell(part1, rsep, nneigh_1)

   ! Fill vectors for starting contact numbers, and tally these up.

   pop = 0
   do i = 1, box%npart
      bin_start(i) = nneigh_1(i) + 1
      pop(bin_start(i)) = pop(bin_start(i)) + 1
   end do

   ! Loop over timesteps.

   do i = 1, size(times)
      call read_from_series(dumps, times(i), part2)
      call get_nneigh_cell(part2, rsep, nneigh_2)

      ! Place particles into histogram, depending upon
      ! what the Nc is at the beginning and end of the tsep.

      hist = 0
      do j = 1, box%npart
         bin = nneigh_2(j) + 1
         hist(bin_start(j), bin) = hist(bin_start(j), bin) + 1
      end do

      ! Normalize by the initial population number to get a final distribution.

      do j = 1, nbin
         if (pop(j) .gt. 0) hist(j, :) = hist(j, :) / pop(j)
      end do

      ! Write output.

      do j = 1, nbin
         val = 0
         do k = 1, nbin
            val = val + (k-1)*hist(j, k)
         end do
         write(*, '(F14.6)', advance='no') val
      end do
      write (*, '(A)') ''

   end do

   call close_hdf5()

end program main
