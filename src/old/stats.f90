! stats.f90

! AUTHOR:          Ben Landrum
! LAST UPDATED:    March 5, 2014
! PURPOSE:         Contains statistical (histogramming, etc.) subroutines.
! CONTAINS:        subroutines: bin_avg_real, cont_histo_bin, get_pop_xfer, histo_bin, pop_histo_bin

! http://www.johndcook.com/standard_deviation.html
! http://www.johndcook.com/skewness_kurtosis.html
! The Art of Computer Programming, Vol 2, 3rd edition, page 232

module stats

   implicit none

   type running_stats ! keep track of statistics in real time
      integer*8 :: n = 0
      double precision :: m1=0, m2=0, m3=0, m4=0
   end type running_stats

!************************************************************
contains
!************************************************************

   subroutine bin_avg_real(x, y, xmin, xmax, nbin, xbinned, ybinned)
   ! Take vector of independent variables x and statistical variable y.
   ! Bin according to the value of x, adding to the bin the y variable.
   ! Then, normalize by dividing by the total number of pairs from x and y.

      real, dimension(:), intent(in) :: x, y
      real, intent(in) :: xmin, xmax
      integer, intent(in) :: nbin
      real, dimension(nbin), intent(out) :: xbinned, ybinned
      integer :: i, bin
      integer, dimension(nbin) :: hist
      real :: delx

      if (size(x) .ne. size(y)) then
         write(*, '(A)') 'Bin average requires vectors of equal size'
         stop
      end if

      write(*, '(A)') 'Binning'

      delx = (xmax - xmin) / nbin

      ! create the xbinned array (positions at bin centers)
      do i = 1, nbin
         xbinned(i) = xmin + (i-0.5) * delx
      end do

      ! place into bins
      ybinned = 0
      hist = 0
      do i = 1, size(x)
         bin = int((x(i) - xmin) / delx) + 1
         if ((bin .ge. 1) .and. (bin .le. nbin)) then
            ybinned(bin) = ybinned(bin) + y(i)
            hist(bin) = hist(bin) + 1
         end if
      end do

      ! normalize
      do i = 1, nbin
         if (hist(i) .ne. 0) then
            ybinned(i) = ybinned(i) / hist(i)
         end if
      end do

    end subroutine bin_avg_real

!************************************************************

   subroutine cont_histo_bin(x, y, xlo, xhi, nxbin, ylo, yhi, nybin, &
        xbins, ybins, freq)
   ! create bins from y vector with ylo, yhi, and nybin
   ! for each x associated with a y, find all x that have a y in a ybin
   ! then bin those x's (for constant ybin) in bins for x (from xlo, xhi, ...)
   ! then return the frequency for each ybin series of xbins, the locations
   !    of the xbins, and the locations of the ybins

      real, dimension(:), intent(in) :: x, y
      real, intent(in) :: xlo, xhi, ylo, yhi
      integer, intent(in) :: nxbin, nybin
      real, dimension(:), intent(out) :: xbins, ybins
      real, dimension(:, :), intent(out) :: freq
      integer, dimension(size(y)) :: ybinned
      integer :: i
      real :: dely

      ! check inputs (add later)
      if (ylo .gt. yhi) then
         write(*, '(A)') 'ylo bigger than yhi'
         stop
      end if
      if (nybin .le. 0) then
         write(*, '(A)') 'Bad nybin'
         stop
      end if
      if (size(x) .ne. size(y)) then
         write(*, '(A)') 'Vectors not same size'
         stop
      end if

      dely = (yhi - ylo) / nybin
      do i = 1, size(y)
         ybinned(i) = (y(i) - ylo) / dely + 1
      end do

      call pop_histo_bin(x, ybinned, xlo, xhi, nxbin, nybin, 1, xbins, freq)

      do i = 1, nybin
         ybins(i) = ylo + (i - 0.5) * dely
      end do

    end subroutine cont_histo_bin

!**********************************************************************

   subroutine get_pop_xfer(x1, x2, min_element, mat)
   ! for vectors with integer elements
   ! find "flows" in the elements from one state to another
   ! e.g. changes from state x1(i) to x2(i) for particle i
   ! 'min_element' corrects for elements of x1, x2 starting at a number
   !    other than one (e.g. use 0 for number of nearest neighbors)
   ! no global variables used

      integer, dimension(:), intent(in)     :: x1, x2
      integer, intent(in)                   :: min_element
      integer, dimension(:, :), intent(out) :: mat
      integer :: size_vec, size_mat, i, bin1, bin2

      ! check inputs
      if (size(x1) .ne. size(x2)) then
         write(*, '(A)') 'Population identity vectors should be same length'
         stop
      end if
      if (size(mat, 1) .ne. size(mat, 2)) then
         write(*, '(A)') 'Population transfer matrix should be square'
         stop
      end if

      size_vec = size(x1)
      size_mat = size(mat, 1)

      mat = 0
      do i = 1, size_vec
         if ((x1(i) .ge. min_element) .and. (x2(i) .ge. min_element) .and. &
             (x1(i) .le. (size_mat - 1 + min_element)) .and. &
             (x2(i) .le. (size_mat - 1 + min_element))) then
            bin1 = x1(i) + 1 - min_element
            bin2 = x2(i) + 1 - min_element
            mat(bin1, bin2) = mat(bin1, bin2) - 1
            mat(bin2, bin1) = mat(bin2, bin1) + 1
         end if
      end do

   end subroutine get_pop_xfer

!**********************************************************************

   subroutine histo_bin(x, xlo, xhi, nbin, xbins, freq)
   ! take raw x data, bin across the x axis over nbin bins, from
   ! xlo to xhi, and return xbins bin locations and normalized freq

      real, dimension(:), intent(in) :: x
      real, intent(in) :: xlo, xhi
      integer, intent(in) :: nbin
      real, dimension(nbin), intent(out) :: xbins, freq
      integer :: i, bin
      real :: delx

      ! check inputs
      if (xlo .gt. xhi) then
         write(*, '(A)') 'Low bound to bin higher than high bound'
      end if

      ! get xbinned coordinates, with the coordinate in the middle
      ! of the bin
      delx = (xhi - xlo) / nbin
      do i = 1, nbin
         xbins(i) = xlo + (i-0.5) * delx
      end do

      ! get frequencies
      freq = 0
      do i = 1, size(x)
         bin = int((x(i) - xlo) / delx) + 1
         if ((bin .ge. 1) .and. (bin .le. nbin)) then
            freq(bin) = freq(bin) + 1.0
         end if
      end do

      ! normalize them
      freq = freq / size(x) / delx

   end subroutine histo_bin

!************************************************************

   subroutine pop_histo_bin(x, pop, xlo, xhi, nbin, npop, min_element, &
                            xbins, freq)
   ! get a population-based set of histograms
   ! useful for getting parametrized distributions from known 
   !    sub-populations

      real, dimension(:), intent(in) :: x
      integer, dimension(:), intent(in) :: pop
      real, intent(in) :: xlo, xhi
      integer, intent(in) :: nbin, npop, min_element
      real, dimension(:), intent(out) :: xbins
      real, dimension(:, :), intent(out) :: freq
      real, dimension(:, :), allocatable :: x_spread
      integer, dimension(:), allocatable :: pop_sizes
      integer :: i, pop_num, fill_num

      ! check inputs
      if (size(x) .ne. size(pop)) then
         write(*, '(A)') 'Input vector and population list not same size'
         stop
      end if
      if (xlo .gt. xhi) then
         write(*, '(A)') 'Input xlo bigger than xhi'
         stop
      end if
      if (size(xbins) .ne. nbin) then
         write(*, '(A)') 'Bin output vector wrong size'
         print*, size(xbins), nbin
         stop
      end if
      if ((size(freq, 1) .ne. nbin) .or. (size(freq, 2) .ne. npop)) then
         write(*, '(A)') 'Freq output array wrong size'
         stop
      end if

      allocate(x_spread(size(x), npop))
      allocate(pop_sizes(npop))

      ! spread the x values into a 2D array with 2nd index based on pop #
      ! index1 is the (advancing) position in the array to insert a value
      ! index2 is the location in the array for the relevant population
      pop_sizes = 0
      do i = 1, size(x)
         pop_num = pop(i) + 1 - min_element
         if ((pop_num .ge. 1) .and. (pop_num .le. npop)) then
            fill_num = pop_sizes(pop_num) + 1
            x_spread(fill_num, pop_num) = x(i)
            pop_sizes(pop_num) = pop_sizes(pop_num) + 1
         end if
      end do

      ! for each population, get the histo_bin
      freq = 0
      do i = 1, npop
         if (pop_sizes(i) .ne. 0) then
            call histo_bin(x_spread(1:pop_sizes(i), i), xlo, xhi, nbin, &
                 xbins, freq(:, i))
         end if
      end do

   end subroutine pop_histo_bin

!**********************************************************************

   subroutine running_stats_push(rs, x)
     type(running_stats) :: rs
     double precision, intent(in) :: x
     double precision :: delta, delta_n, delta_n2, term1
     integer*8 :: n, n1

     n1 = rs%n
     rs%n = rs%n + 1
     delta = x - rs%m1
     delta_n = delta / rs%n
     delta_n2 = delta_n * delta_n
     term1 = delta * delta_n * n1
     rs%m1 = rs%m1 + delta_n
     rs%m4 = rs%m4 &
          + term1 * delta_n2 * ( rs%n*rs%n - 3*rs%n + 3 ) &
          + 6 * delta_n2 * rs%m2 &
          - 4 * delta_n * rs%m3
     rs%m3 = rs%m3 &
          + term1 * delta_n * (n-2) &
          - 3 * delta_n * rs%m2
     rs%m2 = rs%m2 + term1

   end subroutine running_stats_push

!**********************************************************************

   subroutine running_stats_clear(rs)
     type(running_stats), intent(out) :: rs
     rs%n = 0
     rs%m1 = 0.0
     rs%m2 = 0.0
     rs%m3 = 0.0
     rs%m4 = 0.0
   end subroutine running_stats_clear

!**********************************************************************

   function running_stats_count(rs) result (n)
     type(running_stats), intent(in) :: rs
     integer*8 :: n
     n = rs%n
   end function running_stats_count

!**********************************************************************

   function running_stats_mean(rs) result (x)
     type(running_stats), intent(in) :: rs
     double precision :: x
     x = rs%m1
   end function running_stats_mean

!**********************************************************************

   function running_stats_variance(rs) result (x)
     type(running_stats), intent(in) :: rs
     double precision :: x
     x = rs%m2 / (rs%n-1)
   end function running_stats_variance

!**********************************************************************

   function running_stats_stdev(rs) result (x)
     type(running_stats), intent(in) :: rs
     double precision :: x
     x = dsqrt(running_stats_variance(rs))
   end function running_stats_stdev

!**********************************************************************

   function running_stats_sterr(rs) result (x)
     type(running_stats), intent(in) :: rs
     double precision :: x
     x = running_stats_stdev(rs) / dsqrt(dble(rs%n))
   end function running_stats_sterr

!**********************************************************************

   function running_stats_skewness(rs) result (x)
     type(running_stats), intent(in) :: rs
     double precision :: x
     x = dsqrt(dble(rs%n)) * rs%m3 / rs%m2**1.5d00
   end function running_stats_skewness

!**********************************************************************

   function running_stats_kurtosis(rs) result (x)
     type(running_stats), intent(in) :: rs
     double precision :: x
     x = dble(rs%n) * rs%m4 / (rs%m2*rs%m2) &
          - 3.D00
   end function running_stats_kurtosis

!**********************************************************************

!**********************************************************************


!**********************************************************************

end module stats
