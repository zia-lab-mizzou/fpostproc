module coarsegrain

   use realspace
   implicit none

contains

   subroutine coords_to_bin_img_of_spheres(part, npix, delta)
   ! Output a binary image stack (xy images stacked in z).
   ! White pixels for vacant, black for occupied.
   ! The diameter is given by the run info.
   ! You can add a delta to all diameters if desired.
   ! Parameters are pixel numbers per unit diameter and delta (set 0 if unwanted).

      type(particle), dimension(:), intent(in) :: part
      integer :: npix
      real :: delta
      logical, dimension(:,:,:), allocatable :: binarray
      type(particle) :: cellpart

      ! Check inputs.

      if (npix .lt. 0) then
         write(*, '(A)') 'npix (suggested # pix per unit length) must be > 0'
         stop
      end if
      if (delta .lt. 0.) then
         write(*, '(A)') 'delta must be > 0.'
         stop
      end if

      ! Get grid variables.

      nx = ceiling((box%hi(1)-box%lo(1)) * npix)
      ny = ceiling((box%hi(2)-box%lo(2)) * npix)
      nz = ceiling((box%hi(3)-box%lo(3)) * npix)
      delx = (box%hi(1) - box%lo(1)) / nx
      dely = (box%hi(2) - box%lo(2)) / ny
      delz = (box%hi(3) - box%lo(3)) / nz

      ! Get box dimensions that would fit the biggest sphere in the system.
      ! Should be large enough to accomodate sphere center (of biggest sphere)
      !   at border of a voxel.  IS THIS RIGHT?
      bx = 1 + 2*ceiling((max_diam + delta)/2/delx)
      by = 1 + 2*ceiling((max_diam + delta)/2/dely)
      bz = 1 + 2*ceiling((max_diam + delta)/2/delz)

      allocate(binarray(nx, ny, nz))
      binarray = .false.
      do n = 1, box%npart
         GET THE DISTANCE SQ FOR THE PARTICLE
         call get_cell(part(n), cx, cy, cz)
         do i = 1, bx
            ncellx = i + (bx+1)/2
            cellpart%pos(1) = (ncellx-0.5) * delx
            do j = 1, by
               ncelly = j + (by+1)/2
               cellpart%pos(2) = (ncelly-0.5) * dely
               do k = 1, bz
                  ncellz = k + (bz+1)/2
                  cellpart%pos(3) = (ncellz-0.5) * delz
                  distsq = distsq_pbc(part(n), cellpart)
                  if (distsq .le. cutsq) then
                     arrayx = 
                     arrayy = 
                     arrayz = 
                     binarray(
         LOOP WITHIN UNWRAPPED BOX, GETTING DISTSQ, COMPARING TO PARTICLE DISTSQ, AND ORing the binarray
         
      end do
