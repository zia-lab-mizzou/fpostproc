! this module contains gaussian quadrature (simple but accuracy not controllable)

module quad3d_qgaus_mod

  use nrtype

  private ! hide all names from outside
  public quad3d_qgaus ! except for this

  real(SP) :: xsav,ysav
  interface ! user-supplied functions
     function func(x,y,z) ! 3D function to integrate, needs this name
       use nrtype
       real(sp), intent(in) :: x,y
       real(sp), dimension(:), intent(in) :: z
       real(sp), dimension(size(z)) :: func
     end function func

     function y1(x) ! y limit function of x
       use nrtype
       real(sp), intent(in) :: x
       real(sp) :: y1
     end function y1

     function y2(x) ! y limit function of x
       use nrtype
       real(sp), intent(in) :: x
       real(sp) :: y2
     end function y2

     function z1(x,y) ! z limit function of x,y
       use nrtype
       real(sp), intent(in) :: x,y
       real(sp) :: z1
     end function z1

     function z2(x,y) ! z limit function of x,y
       use nrtype
       real(sp), intent(in) :: x,y
       real(sp) :: z2
     end function z2
  end interface

contains

  function h(x) ! integral over y as function of x
    real(sp), dimension(:), intent(in) :: x
    real(sp), dimension(size(x)) :: h
    integer(i4b) :: i
    do i=1,size(x)
       xsav=x(i)
       h(i)=qgaus(g,y1(xsav),y2(xsav))
    end do
  end function h

  function g(y) ! integral over z as function of x,y
    real(sp), dimension(:), intent(in) :: y
    real(sp), dimension(size(y)) :: g
    integer(i4b) :: j
    do j=1,size(y)
       ysav=y(j)
       g(j)=qgaus(f,z1(xsav,ysav),z2(xsav,ysav))
    end do
  end function g

  function f(z) ! function evaluated at x,y,z
    real(sp), dimension(:), intent(in) :: z
    real(sp), dimension(size(z)) :: f
    f=func(xsav,ysav,z)
  end function f

  recursive function qgaus(func,a,b) ! 1D integration
    real(sp), intent(in) :: a,b
    real(sp) :: qgaus
    interface
       function func(x)
         use nrtype
         real(sp), dimension(:), intent(in) :: x
         real(sp), dimension(size(x)) :: func
       end function func
    end interface
    real(sp) :: xm,xr
    real(sp), dimension(5) :: dx, w = (/ 0.2955242247_sp,0.2692667193_sp,&
         0.2190863625_sp,0.1494513491_sp,0.0666713443_sp /),&
         x = (/ 0.1488743389_sp,0.4333953941_sp,0.6794095682_sp,&
         0.8650633666_sp,0.9739065285_sp /)
    xm=0.5_sp*(b+a)
    xr=0.5_sp*(b-a)
    dx(:)=xr*x(:)
    qgaus=xr*sum(w(:)*(func(xm+dx)+func(xm-dx)))
  end function qgaus

  subroutine quad3d_qgaus(x1,x2,ss)
    real(sp), intent(in) :: x1,x2
    real(sp), intent(out) :: ss
    ss=qgaus(h,x1,x2)
  end subroutine quad3d_qgaus
end module quad3d_qgaus_mod
