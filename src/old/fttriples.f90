! fttriples.f90

! AUTHOR:         Ben Landrum
! LAST UPDATED:   September 6, 2012
! PURPOSE:        Generate integer vectors (i,j,k) of squared magnitude q^2
!                 for use in functions that require fourier transforms in PBC.
!                 Throw out redundant triples that are just -1 times another triple.
!                 Creates a (big) text file for this purpose.
! INPUT VARIABLE: qsqmax = maximum integer q^2 that squared magnitude of found triples may have

program main

   implicit none

   integer :: qsqmax, qmax, qsq
   integer,dimension(:,:),allocatable :: triparray, temparray
   integer,dimension(:),allocatable :: magcount, sortarray, sortpoint
   logical,dimension(:),allocatable :: flag
   integer,dimension(:),allocatable :: multiplicity
   integer :: i, j, k, ic, jc, kc
   integer*8 :: m, counter, newcounter
   integer :: numargs
   integer,parameter :: maxmagcount=300
   character(len=300) :: buf
   integer :: left
   integer :: pad, val, bookmark, binnum
   integer :: nzero, nuniq
   integer,dimension(3) :: testvec
   integer, dimension(:), allocatable :: msq_by_bin

   ! Check input.

   numargs = iargc()
   if (numargs .ne. 1) then
      print*, 'triples [qsqmax]'
      stop
   end if
   call getarg(1, buf)
   read(buf, *) qsqmax

   ! Allocate memory for the array of found triples.
   ! This has a maximum size that depends on qsqmax:
   !    i,j,k = (-qmax, ..., 0, ..., +qmax).
   ! Need two copies in order to sort the triples.

   qmax = floor(sqrt(real(qsqmax)))
   allocate(triparray((2*qmax+1)*(2*qmax+1)*(2*qmax+1),5))
   allocate(temparray((2*qmax+1)*(2*qmax+1)*(2*qmax+1),5))
   do i = 1, size(triparray,1)
      triparray(i,1) = 0
      triparray(i,2) = 0
      triparray(i,3) = 0
      triparray(i,4) = 0
      triparray(i,5) = 0
      temparray(i,1) = 0
      temparray(i,2) = 0
      temparray(i,3) = 0
      temparray(i,4) = 0
      temparray(i,5) = 0
   end do
   print*, 'Size triparray = ', size(triparray,1)

   ! Loop through indices to fill in triparray.
   ! Loop creates triples sorted by 3rd, then 2nd, the 1st index:
   ! Example: (-2, -2, -2) -> (-1, -2, -2) -> ... -> (0, 0, -2) -> ...

   print*, 'Generating triples'
   counter = 0
   do i = -qmax, qmax
      do j = -floor(sqrt(real(qsqmax-i*i))), floor(sqrt(real(qsqmax-i*i)))
         do k = -floor(sqrt(real(qsqmax-i*i-j*j))), &
              floor(sqrt(real(qsqmax-i*i-j*j)))
            qsq = i*i + j*j + k*k
            counter = counter + 1
            triparray(counter,1) = k
            triparray(counter,2) = j
            triparray(counter,3) = i
            triparray(counter,4) = qsq
         end do
      end do
   end do

   ! Have to sort primarily by the 4th index = squared magnitude, rather than the 3rd.
   ! But want to preserve the order otherwise.
   ! To do this, use merge sort, a "stable" sort.
   ! We copy the fourth column and sort it, returning the new locations of the items in the list.
   ! We then reorder the triparray by these new locations.

   print*, 'Allocating arrays'
   allocate(sortarray(counter))
   allocate(sortpoint(counter))
   sortarray(:) = triparray(1:counter,4)
   print*, 'Merge sorting'
   call merge_sort(sortarray,sortpoint)
   do i = 1, counter
      temparray(i,1:4) = triparray(sortpoint(i),1:4)
   end do
   triparray = temparray

   ! Go through groups of squared magnitude from beginning of list to end.
   ! Flag triples that is are repeats of preceding triples.

   print*, 'Flagging ...'
   allocate(flag(counter))
   forall(i=1:counter) flag(i) = .false.
   val = -1
   binnum = 0
   do i = 1, counter
      if (triparray(i,4) == val) then
         do j = bookmark, i-1
            if (((triparray(i,1) == triparray(j,1)).and. & ! is there a more
                 (triparray(i,2) == triparray(j,2)).and. & ! compact way to write?
                 (triparray(i,3) == triparray(j,3))     ).or. &
                 ((-triparray(i,1) == triparray(j,1)).and. &
                 (-triparray(i,2) == triparray(j,2)).and. &
                 (-triparray(i,3) == triparray(j,3))     )) then
               flag(i) = .true.
               exit
            end if
         end do
      else
         val = triparray(i,4)
         binnum = binnum + 1
         bookmark = i
      end if
   end do
   newcounter = 0
   forall(i=1:counter) temparray(i,:) = (/ 0, 0, 0, 0, 0 /)
   do i = 1, counter
      if (flag(i) .eqv. .false.) then
         newcounter = newcounter + 1
         temparray(newcounter,:) = triparray(i,:)
      end if
   end do
   triparray = temparray

   ! Add bin numbers to column 5.
   ! These "bin numbers" are the ordering of the squared magnitudes.

   print*, 'About to add bin nums ...'
   allocate(multiplicity(newcounter))
   allocate(msq_by_bin(newcounter))
   forall(i=1:newcounter) multiplicity(i) = 0
   binnum = 0
   val = -1
   do i = 1, newcounter
      if (triparray(i,4) == val) then
         triparray(i,5) = binnum
         multiplicity(binnum) = multiplicity(binnum) + 1
         msq_by_bin(binnum) = triparray(i,4) ! wasteful but correct
      else
         binnum = binnum + 1
         triparray(i,5) = binnum
         multiplicity(binnum) = 1
         val = triparray(i,4)
      end if
   end do

   ! Write the triples.txt file.

   open(7, file='triples.txt', action='write')
   write(7, '(A)') 'TRIPLES FOR BOX FOURIER TRANSFORM'
   write(7, '(A)') 'ITEM: maximum k^2'
   write(buf, *) qsqmax
   write(7, '(A)') trim(adjustl(buf))
   write(7, '(A)') 'ITEM: multiplicity by bin_number'
   do i = 1, binnum
      write(7, '(3i14)') i, msq_by_bin(i), multiplicity(i)
   end do
   write(7, '(A)') 'ITEMS: kx ky kz k^2 bin_number'
   do i = 1, newcounter
      write(7, '(3i5,2i14)') triparray(i,:)
   end do
   close(7)

 contains

   subroutine merge_sort(m,ind)

      integer,dimension(:),intent(inout) :: m
      integer,dimension(:),intent(out) :: ind
      integer :: i

      if (size(ind) /= size(m)) then
         print *, 'Array to be sorted and index array not same size'
         stop
      end if

      ! Create index vector to keep track of swaps.
      ! Then call the recursive merge sort.

      forall(i = 1:size(m)) ind(i) = i
      call rsort(m,ind)

   end subroutine merge_sort

   recursive subroutine rsort(m,ind)

      integer,dimension(:),intent(inout) :: m, ind
      integer :: middle

      ! If array is size 1, consider it sorted and return it.

      if (size(m) <= 1) then
         return
      else ! array size > 1, so split into 2 sub-arrays
         middle = size(m) / 2
         call rsort(m(1:middle),ind(1:middle))
         call rsort(m(middle+1:size(m)),ind(middle+1:size(ind)))

         ! Merge the sublists that were returned

         call merge(m,middle,ind)

      end if

   end subroutine rsort

   subroutine merge(m,middle,ind)
      integer,dimension(:),intent(inout) :: m, ind
      integer,intent(in) :: middle
      integer,dimension(middle) :: left, leftind
      integer,dimension(size(m)-middle) :: right, rightind
      integer :: countl, countr, countm, firstl, firstr

      left = m(1:middle); leftind = ind(1:middle)
      right = m(middle+1:size(m)); rightind = ind(middle+1:size(ind))

      ! initialize counters.

      countl = middle
      countr = size(m) - middle
      countm = 0
      do while ((countl > 0).or.(countr > 0))
         countm = countm + 1
         if ((countl > 0).and.(countr > 0)) then
            firstl = left(size(left)-countl+1)
            firstr = right(size(right)-countr+1)
            if (firstl <= firstr) then
               m(countm) = firstl
               ind(countm) = leftind(size(left)-countl+1)
               countl = countl - 1
            else
               m(countm) = firstr
               ind(countm) = rightind(size(right)-countr+1)
               countr = countr - 1
            end if
         else if (countl > 0) then
            firstl = left(size(left)-countl+1)
            m(countm) = firstl
            ind(countm) = leftind(size(left)-countl+1)
            countl = countl - 1
         else if (countr > 0) then
            firstr = right(size(right)-countr+1)
            m(countm) = firstr
            ind(countm) = rightind(size(right)-countr+1)
            countr = countr - 1
         end if
      end do

   end subroutine merge

end program main
