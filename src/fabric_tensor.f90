program fabric_tensor

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_vfrac,eval_fabric
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,nparticles,nargs,snap,nsnaps
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory
  real, dimension(6) :: fabric
  real :: rsep,minsize
  integer(int64) :: tstart,tdur,tinc,tstep
  integer, parameter :: ncmax = 20

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 5) then
     write(*, '(A)') 'fabric_tensor [directory] [tstart] [tdur] [tinc] [rsep]'
     stop
  end if
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) tstart
  call get_command_argument(3, buf); read(buf, *) tdur
  call get_command_argument(4, buf); read(buf, *) tinc
  call get_command_argument(5, buf); read(buf, *) rsep

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles

  ! init group and cells

  call group%init(domain)
  minsize = 2*maxval(particle%typeradius(:)) + rsep
  call cells%init(group, minsize, .false.)

  ! loop over snaps

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     ! get fabric tensor

     fabric(:) = 0.0
     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()
     call eval_fabric(cells, .true., rsep, fabric)

     ! print output

     write(*, '(i14)', advance='no') tstep
     do j = 1, 5
        write(*, '(2X,f10.6)', advance='no') fabric(j)
     end do
     write(*, '(2X,f10.6)') fabric(6)

  end do

end program fabric_tensor
