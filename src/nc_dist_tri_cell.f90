program nc_dist_tri_cell

  use, intrinsic :: iso_fortran_env
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,k,nparticles,nargs,snap,nsnaps,c,nparticle_cell,l,ncellsx
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  character(len=300) :: buf,directory,mode
  integer, dimension(:), allocatable :: nc
  real, dimension(:), allocatable :: nc_cell,ncdist_cell
  real :: rsep,minsize,meanval
  integer(int64) :: tstart,tdur,tinc,tstep
  integer, parameter :: ncmax = 20
  integer :: bin_scale
  real, dimension(ncmax+1) :: ncdist
  logical :: mean_only,per_cell

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 6) then
     write(*, '(A)') 'nc_dist_tri [mode] [directory] [tstart] [tdur] [tinc] [rsep] {ncells} {bin_scale}'
     stop
  end if
  call get_command_argument(1, buf); mode = trim(adjustl(buf))
  call get_command_argument(2, buf); directory = trim(adjustl(buf))
  call get_command_argument(3, buf); read(buf, *) tstart
  call get_command_argument(4, buf); read(buf, *) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); read(buf, *) rsep

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (trim(adjustl(mode)) == 'mean') then
     mean_only = .true.
     per_cell = .false.
     minsize = 2*maxval(particle%typeradius(:)) + rsep
  else if (trim(adjustl(mode)) == 'full') then
     mean_only = .false.
     per_cell = .false.
     minsize = 2*maxval(particle%typeradius(:)) + rsep
  else if (trim(adjustl(mode)) == 'band') then
     mean_only = .false.
     per_cell = .false.
     minsize = 2*maxval(particle%typeradius(:)) + rsep
  else if (trim(adjustl(mode)) == 'cell') then
     if (nargs .lt. 8) then
        print *, 'must specify number of cells and bin scaling for seventh and eighth arguments when mode is cell'
        stop
     else
        call get_command_argument(7, buf); read(buf, *) ncellsx
        call get_command_argument(8, buf); read(buf, *) bin_scale
        minsize = domain%xprd/ncellsx
     end if
     per_cell = .true.
  else
     print *, 'mode must be mean or full or band (full and band) or cell'
     stop
  end if

  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles
  allocate(nc(nparticles))

  ! init group and cells

  call group%init(domain)
  call cells%init(group, minsize, .false.)

  ! loop over snaps

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     ! get contact numbers

     nc(:) = 0
     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()
     call eval_nc(cells, .true., rsep*particle%typeradius(3)*2.0, nc)

     ! get the distribution

     ncdist(:) = 0.0
     do j = 1, nparticles
        k = nc(j)
        if (k <= ncmax) then
           ncdist(k+1) = ncdist(k+1) + 1
        end if
     end do
     ncdist = ncdist / nparticles

     allocate(nc_cell(cells%ncells))
     allocate(ncdist_cell(ncmax*bin_scale+1))
     nc_cell(:) = 0.0
     ncdist_cell(:) = 0.0
     do c = 1, cells%ncells
        l = cells%cellhead(c)
        if (l == 0) cycle
        nparticle_cell = 0
        do while (l > 0)
          nc_cell(c) = nc(l)+nc_cell(c)
          l = cells%next(l)
          nparticle_cell = nparticle_cell + 1
        end do
        !write(*,*) nparticle_cell
        nc_cell(c) = nc_cell(c) / nparticle_cell
        k = nint(nc_cell(c)*bin_scale)
        if (k <= ncmax*bin_scale) then
          ncdist_cell(k+1) = ncdist_cell(k+1) + 1
        end if
     end do
     ncdist_cell = ncdist_cell/cells%ncells

     ! print output

     write(*, '(i10)', advance='no') tstep
     if (per_cell) then
        do j = 0, ncmax*bin_scale
           write(*, '(f8.5)' , advance='no') ncdist_cell(j+1)
        end do
        write (*, '(a)') ''

     else if (.not. mean_only) then
        do j = 0, ncmax
           write(*, '(f8.5)', advance='no') ncdist(j+1)
        end do
        write (*, '(a)') ''
     else 
        meanval = 0.0
        do j = 0, ncmax
           meanval = meanval + j*ncdist(j+1)
        end do
        write (*, '(f8.5)') meanval
     end if

  end do

end program nc_dist_tri_cell
