module mod_geom

  use, intrinsic :: iso_fortran_env
  implicit none
  private

  public :: symmetric_eccentricity
