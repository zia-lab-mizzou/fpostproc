program plot_ssf

  use, intrinsic :: iso_fortran_env
  use, intrinsic :: iso_c_binding
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_statistics, only : running_stat_t
  use mod_stat_func, only : eval_ssf_3d,init_ffts,close_ffts
  use mod_data, only : source_data_t
  use mod_constants, only : pi
  use mod_image, only : render_array
  implicit none

  integer :: i,j,k,n,m,nparticles,nargs,snap,nsnaps,nq,qbin
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  type(running_stat_t), dimension(:,:), allocatable :: rs_array_slice_yz
  type(running_stat_t), dimension(:,:), allocatable :: rs_array_slice_xz
  type(running_stat_t), dimension(:,:), allocatable :: rs_array_slice_xy
  type(running_stat_t), dimension(:), allocatable :: rs_array_rad
  character(len=300) :: fname,buf,directory
  real(real64) :: sqmin,sqmax,qx
  real :: target_size,qmax,qval,delq
  integer(int64) :: tstart,tdur,tinc,tstep,tout
  integer, parameter :: nyquist_multiplier = 1
  real(real64), dimension(:,:,:), allocatable :: fin
  real, dimension(:,:,:), allocatable :: ssf
  real, dimension(:,:), allocatable :: sq_xyz
  real, dimension(:), allocatable :: sq_rad
  real(real64), dimension(:,:,:), allocatable :: qxx,qxy,qxz
  real(real64), dimension(:), allocatable :: ssf_rad,qx_bin
  real(real64), dimension(6) :: h_inv

  ! read inputs

  nargs = command_argument_count()
  if (nargs .ne. 8) then
     write(*, '(a)') 'plot_ssf [directory] [tstart] [tdur] [tinc] &
                              &[qmax] [nq] [sqmin] [sqmax]'
     stop
  end if
  call get_command_argument(1, buf); directory = trim(adjustl(buf))
  call get_command_argument(2, buf); read(buf, *) tstart
  call get_command_argument(3, buf); read(buf, *) tdur
  call get_command_argument(4, buf); read(buf, *) tinc
  call get_command_argument(5, buf); read(buf, *) qmax
  call get_command_argument(6, buf); read(buf, *) nq
  call get_command_argument(7, buf); read(buf, *) sqmin
  call get_command_argument(8, buf); read(buf, *) sqmax

  if (modulo(tdur,tinc) /= 0) then
     write(*,'(a)') 'tdur is not a multiple of tinc'
     stop
  end if
  if (qmax <= 0.0) then
     write(*,'(a)') 'qmax must be > 0.0'
     stop
  end if
  if (nq < 1) then
     write(*,'(a)') 'nq must be > 0'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     write(*,'(a)') 'tdur is not a multiple of tinc'
     stop
  end if
  if (sqmax <= 0.0) then
     write(*,'(a)') 'sqmax must be > 0.0'
     stop
  end if
  if (sqmin < 0.0) then
     write(*,'(a)') 'sqmin must be >= 0.0'
     stop
  end if
  if (sqmin >= sqmax) then
     write(*,'(a)') 'sqmax must be > sqmin'
     stop
  end if

  ! Init the key classes.

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     write(*,'(a)') 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     write(*,'(a)') 'hdf5 timesteps appear irregular'
     write(*,'(a)') 'not supporting nonzero tdur for irregular'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles
  call group%init(domain)

  ! Get wavenumber spacing, and allocate output arrays.
  ! Number of samples in S(q) vs. q is nq.
  ! Number of pixels in output image is 2*nq-1.

  delq = qmax / (nq-1)
  allocate(rs_array_rad(nq))
  forall (i=1:nq) qx_bin(i) = (i-1) * delq
  allocate(qx_bin(nq))
  allocate(ssf_rad(nq))
  allocate(sq_xyz(2*nq-1,2*nq-1)
  allocate(rs_array_slice_yz(2*nq-1,2*nq-1))
  allocate(rs_array_slice_xz(2*nq-1,2*nq-1))
  allocate(rs_array_slice_xy(2*nq-1,2*nq-1))
  forall (i=1:2*nq-1, j=1:2*nq-1) call rs_array_slice_yz(i,j)%init
  forall (i=1:2*nq-1, j=1:2*nq-1) call rs_array_slice_xz(i,j)%init
  forall (i=1:2*nq-1, j=1:2*nq-1) call rs_array_slice_xy(i,j)%init

  ! Determine real-space sampling required to resolve maximum wavenumber.
  ! In 1D, need sample increment to be less than (1/2)*(2*pi/qmax).
  ! In 3D, seems okay to find the maximum triclinic sub-cell that fits
  ! inside of a sphere of diameter of (1/2)*(2*pi/qmax).
  ! The .true. option specifies to use the maximizing procedure noted above.

  target_size = pi / qmax / nyquist_multiplier
  call cells%init(group, target_size, .true.)

  ! Loop over snaps.

  do n = 1, nsnaps

     tstep = tstart + (n-1)*tinc
     snap = source_data%step_index(tstep)

     call source_data%read_snap(particle,domain,snap)
     call cells%reset_grid()
     write(*,'(a,3i6)') 'fft nodes:', cells%ncellx, cells%ncelly, cells%ncellz
     call cells%assign_particles()
     call eval_ssf_3d(cells, ssf)

     h_inv(:) = real(domain%h_inv(:),real64)

     ! BJL: turn this function into something that updates the running
     !      averages

     call render_ssf(ssf, fin, h_inv, plane, tstep, &
          sqmin, sqmax, real(qmax,real64), nq)

     

     ssf_rad = 0.0
     forall (i=1:nq, j=1:nq, k=1:nq)
        m =     (i-nq/2-1) * (i-nq/2-1)
        m = m + (j-nq/2-1) * (j-nq/2-1)
        m = m + (k-nq/2-1) * (k-nq/2-1)
        qx = sqrt(real(m),real64)
        if (qx <= qmax) then
           call decide_qx_bin(qx,qx_bin,qbin)
           ssf_rad(qbin) = ssf_rad(qbin) &
                +
     end forall
                 
                 ssf_rad(qbin) = ssf_rad(qbin)+fin(i,j,k)/&
                      (4.0*pi*(0.5*(qx_bin(qbin)+qx_bin(qbin+1)))**2*(qx_bin(qbin+1)-qx_bin(qbin)))


     ! update running average

     do concurrent (i=1:nq, j=1:nq)
        call rs_array_slice_xyz(j,k)%push(real(fin(nq/2+1,j,k), real64))
        call rs_array_slice_xyz(i,k)%push(real(fin(i,nq/2+1,k), real64))
        call rs_array_slice_xyz(i,j)%push(real(fin(i,j,nq/2+1), real64))
     end do

     ! Have to deallocate fin, since render_ssf allocates it

     deallocate(fin)
     deallocate(ssf)

  end do

  ! output running average

  call image_ssf(fin, plane, tstep, sqmin, sqmax, real(qmax,real64), nq)

  if (mode == 'xyz') then
     do i = 1, nq
        do j = 1, nq
           sq_xyz(i,j) = real(rs_array_slice_xyz(i,j)%mean())
        end do
     end do
     write(fname,'(5a)') 'ssf-', plane, '-runningave.ppm'
     call render_array(real(sq_xyz,real64),sqmin,sqmax,.false.,'gs',fname)
  else if (mode == 'rad') then
     do i = 1,nq-1
        sq_rad(i) = real(rs_array_rad(i)%mean())
        qval = 0.5*(qx_bin(i)+qx_bin(i+1))
        print *, qval, sq_rad(i)
     end do
  end if

        do i = 1,nq-1
           call rs_array_rad(i)%push(real(ssf_rad(i), real64))
        end do
        deallocate(ssf_rad)


  ! transform from q_pixel grid to q_x grid

!  call pixel2x_ssf(sq_xyz, qxx, qxy, qxz, nq, real(qmax,real64))

contains

  ! ----------------------------------------------------------------------
  ! Render an image of the ssf
  ! 'flava' can be 'raw' for raw FFTW output, 'sym' for symmetrized
  ! version, or 'fin' for finalized version.  Only the finalized version
  ! has the correct final wavevectors.  'raw' and 'sym' are just used
  ! for debugging purposes.
  ! ----------------------------------------------------------------------

  subroutine render_ssf(ssf, fin, h_inv, plane, tstep, sqmin, sqmax, qmax, nq)

    use mod_image, only : render_array, symmetrize_halfspace_rot_3d, &
         corner_to_center_3d, displacement_to_affine, affine_compose, &
         lower_voigt_to_affine, affine_transform, scale_to_affine

    real, dimension(:,:,:), intent(in) :: ssf
    real(real64), dimension(6), intent(in) :: h_inv
    character(len=2), intent(in) :: plane
    integer(int64), intent(in) :: tstep
    real(real64), intent(in) :: sqmin,sqmax
    real(real64), intent(in) :: qmax
    integer, intent(in) :: nq
    real(real64), dimension(:,:,:), allocatable :: sym
    real(real64), dimension(:,:,:), allocatable, intent(out) :: fin
    character(len=300) :: fname,buf
    real(real64), dimension(12) :: m,mcomp
    real(real64) :: dx,dy,dz,scale
    integer, dimension(6) :: bnds

    if ((plane /= 'yz') .and. (plane .ne. 'xz') .and. (plane /= 'xy')) then
       write(*,'(a)') 'need yz, xz, or xy for plane in render_ssf'
       stop
    end if

    allocate(fin(nq,nq,nq))

    ! Symmetrize the ssf, allocating an array in the process.
    ! All this does is fills in the redundant (x) halfspace
    ! of S(q).

    call symmetrize_halfspace_rot_3d(real(ssf,real64), sym)

    ! Temporarily bring corner, where DC is, to the center of the
    ! symmetric array.  This way we will get the S(q) rings in
    ! the center of the image.  Below performs cyclic shifting.

    call corner_to_center_3d(sym)

    ! Compose linear transformations, to be performed in one shot
    ! to avoid losing pixels off of the edge of the image, to do
    ! the following:
    !
    !   1. Shift the DC point back to the edge.
    !   2. Map each lambda-wavevector to original wavevectors by
    !          q_x = H^{-T} q_\lambda
    !   3. Shift the DC point back to the center.
    !
    ! I'm doing this by composing these 'affines', finding their
    ! inverse, and finding which pixels to interpolate from in
    ! the raw data to generate the 2D image.

    dx = real(size(sym,1)/2+1,real64)
    dy = real(size(sym,2)/2+1,real64)
    dz = real(size(sym,3)/2+1,real64)
    mcomp = displacement_to_affine(-dx,-dy,-dz)

    ! h_inv is a voigt_upper matrix
    ! saying that it is a voigt_lower matrix is taking its transpose:
    ! H^{-T}

    m(:) = lower_voigt_to_affine(h_inv(:))
    mcomp(:) = affine_compose(m(:), mcomp(:))

    ! Above would be used to go from FT of Lambda coords
    ! to FT of real coords, but we also have to scale to
    ! get things in terms of pixels.

    scale = pi * nq / qmax
    m(:) = scale_to_affine(scale, scale, scale)
    mcomp(:) = affine_compose(m(:), mcomp(:))

    ! Shift the corner back to the center.
    ! The shift is in terms of pixels in the output image.

    dx = real(nq/2+1,real64)
    m(:) = displacement_to_affine(dx, dx, dx)
    mcomp(:) = affine_compose(m(:), mcomp(:))

    ! Peform the affine transformation from the input to the image.

    bnds = (/1, nq, 1, nq, 1, nq/) ! transform the whole array
    call affine_transform(sym, mcomp, fin, bnds)

  end subroutine render_ssf

  subroutine pixel2x_ssf(sq_xyz, qxx, qxy, qxz, nq, qmax)

    use mod_image, only : displacement_to_affine, affine_compose, &
         scale_to_affine, affine_invert

    real(real64), dimension(:,:,:), intent(in) :: sq_xyz 
    real(real64), dimension(:,:,:), allocatable, intent(out) :: qxx,qxy,qxz
    real(real64), intent(in) :: qmax
    integer, intent(in) :: nq
    integer :: i,j,k
    real(real64), dimension(12) :: m,mcomp,minv
    real(real64) :: scale,dx

    ! Compose linear transformations, to be performed in one shot
    ! to avoid losing pixels off of the edge of the image, to do
    ! the following:
    !
    !   1. Shift the DC point back to the edge.
    !   2. Map each lambda-wavevector to original wavevectors by
    !          q_x = H^{-T} q_\lambda
    !   3. Shift the DC point back to the center.
    !
    ! I'm doing this by composing these 'affines', finding their
    ! inverse, and finding which pixels to interpolate from in
    ! the raw data to generate the 2D image.

    ! Above would be used to go from FT of Lambda coords
    ! to FT of real coords, but we also have to scale to
    ! get things in terms of pixels.

    scale = pi * nq / qmax
    mcomp(:) = scale_to_affine(scale, scale, scale)

    ! Shift the corner back to the center.
    ! The shift is in terms of pixels in the output image.

    m(:) = displacement_to_affine(dx, dx, dx)
    mcomp(:) = affine_compose(m(:), mcomp(:))

    minv = affine_invert(m)

    ! do we have to add 1?
    print *, 'do we have to add one in affine_transform'

    allocate(qxx(size(sq_xyz,1),size(sq_xyz,2),size(sq_xyz,3)))
    allocate(qxy(size(sq_xyz,1),size(sq_xyz,2),size(sq_xyz,3)))
    allocate(qxz(size(sq_xyz,1),size(sq_xyz,2),size(sq_xyz,3)))

    do concurrent (i=1:size(sq_xyz,1), j=1:size(sq_xyz,2), k=1:size(sq_xyz,3))
       qxx(i,j,k) = minv(1)*(i-1) + minv( 2)*(j-1) + minv( 3)*(k-1) + minv( 4)!)*2.0*pi/125.419
       qxy(i,j,k) = minv(5)*(i-1) + minv( 6)*(j-1) + minv( 7)*(k-1) + minv( 8)!)*2.0*pi/125.419
       qxz(i,j,k) = minv(9)*(i-1) + minv(10)*(j-1) + minv(11)*(k-1) + minv(12)!)*2.0*pi/125.419
    end do

  end subroutine pixel2x_ssf

  subroutine image_ssf(fin, plane, tstep, sqmin, sqmax, qmax, nq)

    use mod_image, only : render_array

    real(real64), dimension(:,:,:), intent(in) :: fin 
    character(len=2), intent(in) :: plane
    integer(int64), intent(in) :: tstep
    real(real64), intent(in) :: sqmin,sqmax
    real(real64), intent(in) :: qmax
    integer, intent(in) :: nq
    character(len=300) :: fname,buf
    integer, dimension(6) :: bnds
   
    if (plane == 'yz') then
       bnds = (/ nq/2+1, nq/2+1, 1, nq, 1, nq /)
    else if (plane == 'xz') then
       bnds = (/ 1, nq, nq/2+1, nq/2+1, 1, nq /)
    else
       bnds = (/ 1, nq, 1, nq, nq/2+1, nq/2+1 /)
    end if
    write(buf,'(i10)') tstep
    write(fname,'(5a)') 'ssf-', plane, '-', trim(adjustl(buf)), '2pi.ppm'
    if (plane == 'yz') then
       call render_array(fin(nq/2+1,:,:),sqmin,sqmax,.false.,'gs',fname)
    else if (plane == 'xz') then
       call render_array(fin(:,nq/2+1,:),sqmin,sqmax,.false.,'gs',fname)
    else
       call render_array(fin(:,:,nq/2+1),sqmin,sqmax,.false.,'gs',fname)
    end if

  end subroutine image_ssf

  subroutine decide_qx_bin(qx, qx_bin, qbin)

    real(real64), intent(in) :: qx
    real(real64), dimension(:), intent(in) :: qx_bin
    real(real64) :: qdecide
    integer, intent(out) :: qbin
    integer :: i,nbin

    nbin = size(qx_bin)-1
    do i = 1,nbin
       if (qx < qx_bin(i+1) .and. qx > qx_bin(i) ) then
          qbin = i
          EXIT
       end if
    end do

  end subroutine decide_qx_bin

end program plot_ssf
