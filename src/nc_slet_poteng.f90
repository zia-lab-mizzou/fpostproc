program nc_slet_poteng

  use, intrinsic :: iso_fortran_env
  use mod_statistics, only : group_stat_t
  use mod_particle, only : particle_t
  use mod_domain, only : domain_t
  use mod_group, only : group_t
  use mod_cells, only : cells_t
  use mod_stat_func, only : eval_nc
  use mod_data, only : source_data_t
  implicit none

  integer :: i,j,k,nparticles,nargs,snap,nsnaps,igroup
  type(particle_t) :: particle
  type(domain_t) :: domain
  type(group_t) :: group
  type(cells_t) :: cells
  type(source_data_t) :: source_data
  type(group_stat_t) :: group_stat
  character(len=300) :: buf,directory,mode
  integer, dimension(:), allocatable :: nc
  real :: rsep,minsize
  integer(int64) :: tstart,tdur,tinc,tstep
  integer, parameter :: ncmax = 20
  integer, dimension(ncmax+1) :: nc_igroup
  logical :: yes_slet
  logical, dimension(:), allocatable :: group_membership
  real(real64), dimension(:), allocatable :: vals

  ! read inputs

  nargs = command_argument_count()
  if (nargs .lt. 6) then
     write(*, '(a)') 'nc_slet_poteng [mode] [directory] [tstart] [tdur] [tinc] [rsep]'
     stop
  end if
  call get_command_argument(1, buf); mode=trim(adjustl(buf))
  call get_command_argument(2, buf); directory = trim(adjustl(buf))
  call get_command_argument(3, buf); read(buf, *) tstart
  call get_command_argument(4, buf); read(buf, *) tdur
  call get_command_argument(5, buf); read(buf, *) tinc
  call get_command_argument(6, buf); read(buf, *) rsep

  if (trim(adjustl(mode)) == 'slet') then
     yes_slet = .true.
  else if (trim(adjustl(mode)) == 'poteng') then
     yes_slet = .false.
  else
     print *, 'mode must be slet or poteng'
     stop
  end if

  call source_data%init(trim(adjustl(directory)), particle, domain)
  if (.not. source_data%step_exists(tstart)) then
     print *, 'bad tstart value'
     stop
  end if
  if (.not. source_data%regular) then
     print *, 'hdf5 timesteps appear irregular'
     print *, 'not supporting nonzero tdur for irregular'
     stop
  end if
  if (modulo(tdur,tinc) /= 0) then
     print *, 'tdur is not a multiple of tinc'
     stop
  end if
  nsnaps = int(tdur/tinc) + 1
  nparticles = particle%nparticles
  allocate(nc(nparticles))
  allocate(vals(nparticles))

  ! make sure that we can get potential energies or presslets
  ! TODO

  ! init group and cells

  call group%init(domain)
  minsize = 2*maxval(particle%typeradius(:)) + rsep
  call cells%init(group, minsize, .false.)

  ! add an empty group for each nc
  ! will be updating these in each loop iteration

  allocate(group_membership(nparticles))
  do i = 0, ncmax
     group_membership(:) = .false.
     nc_igroup(i+1) = group%create(group_membership)
  end do

  call group_stat%init()

  ! loop over snaps

  do i = 1, nsnaps

     tstep = tstart + (i-1)*tinc
     snap = source_data%step_index(tstep)

     ! get contact numbers

     nc(:) = 0
     call source_data%read_snap(particle, domain, snap)
     call cells%reset_grid()
     call cells%assign_particles()
     call eval_nc(cells, .true., rsep, nc)

     ! update groups from the nc

     do j = 0, ncmax
        group_membership(:) = .false.
        do k = 1, nparticles
           if (nc(k) == j) then
              group_membership(k) = .true.
           end if
        end do
        call group%update(nc_igroup(j+1), group_membership)
     end do

     ! use running stats to get averages and standard errors of
     ! potential energy or -1/3 trace of particle xF tensor

     if (yes_slet) then
        vals(:) = real(particle%stress(1,:) &
                     + particle%stress(2,:) &
                     + particle%stress(3,:),real64)
        vals(:) = -vals(:) / 3.0_real64
     else
        vals(:) = real(particle%pe(:),real64)
     end if

     call group_stat%push(group, vals)

  end do

  ! print averages

  do i = 0, ncmax
     igroup = nc_igroup(i+1)
     write(*, '(i4,2g14.5)') i, group_stat%mean(igroup), group_stat%sterr(igroup)
  end do

end program nc_slet_poteng
